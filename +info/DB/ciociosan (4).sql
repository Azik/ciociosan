-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2021 at 08:35 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ciociosan`
--

-- --------------------------------------------------------

--
-- Table structure for table `s_blocks`
--

CREATE TABLE `s_blocks` (
  `blocksid` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `position` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blocks`
--

INSERT INTO `s_blocks` (`blocksid`, `catid`, `active`, `position`) VALUES
(51, 17, 1, 0),
(60, 19, 1, 0),
(50, 17, 1, 0),
(49, 17, 1, 0),
(48, 17, 1, 0),
(59, 19, 1, 0),
(58, 18, 1, 0),
(57, 18, 1, 0),
(56, 18, 1, 0),
(55, 14, 1, 3),
(47, 17, 1, 0),
(46, 17, 1, 0),
(54, 14, 1, 2),
(53, 14, 1, 1),
(52, 17, 1, 0),
(62, 20, 1, 0),
(61, 19, 1, 0),
(63, 20, 1, 0),
(64, 16, 0, 0),
(65, 16, 0, 0),
(66, 16, 0, 0),
(67, 21, 1, 0),
(68, 21, 1, 0),
(69, 21, 0, 0),
(70, 21, 0, 0),
(71, 25, 1, 0),
(72, 25, 1, 0),
(73, 25, 1, 0),
(74, 26, 1, 0),
(75, 26, 1, 0),
(76, 26, 1, 0),
(77, 26, 1, 0),
(78, 26, 1, 0),
(79, 26, 1, 0),
(80, 16, 1, 0),
(81, 16, 1, 0),
(82, 29, 1, 1),
(83, 29, 1, 2),
(84, 29, 1, 5),
(85, 29, 1, 3),
(86, 29, 1, 4),
(87, 30, 1, 0),
(88, 30, 1, 0),
(89, 30, 1, 0),
(90, 30, 1, 0),
(91, 30, 1, 0),
(92, 30, 1, 0),
(93, 30, 1, 0),
(94, 30, 1, 0),
(95, 31, 1, 0),
(96, 31, 1, 0),
(97, 31, 1, 0),
(98, 33, 1, 0),
(99, 33, 1, 0),
(100, 33, 1, 0),
(101, 34, 1, 0),
(102, 34, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_blockscategories`
--

CREATE TABLE `s_blockscategories` (
  `catid` int(11) NOT NULL,
  `position` int(5) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockscategories`
--

INSERT INTO `s_blockscategories` (`catid`, `position`, `active`) VALUES
(16, 3, 1),
(15, 2, 1),
(14, 1, 1),
(17, 4, 1),
(18, 5, 0),
(19, 6, 0),
(20, 7, 0),
(21, 8, 1),
(22, 9, 0),
(23, 10, 0),
(24, 11, 1),
(25, 12, 1),
(26, 13, 1),
(27, 14, 1),
(28, 15, 0),
(29, 16, 0),
(30, 17, 0),
(31, 18, 0),
(32, 19, 0),
(33, 20, 0),
(34, 21, 0),
(35, 22, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_blockscategorylocalizations`
--

CREATE TABLE `s_blockscategorylocalizations` (
  `catid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `more_txt` text DEFAULT NULL,
  `imgid` int(11) NOT NULL DEFAULT 0,
  `info` text NOT NULL,
  `embed_code` text DEFAULT NULL,
  `url` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockscategorylocalizations`
--

INSERT INTO `s_blockscategorylocalizations` (`catid`, `lang`, `name`, `more_txt`, `imgid`, `info`, `embed_code`, `url`) VALUES
(16, 'en', 'Testimonials', '', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(17, 'es', 'Fogonadura', '', 0, '', '', ''),
(15, 'en', 'Top Holiday Destinations', 'All', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(21, 'es', 'Síguenos', '', 0, '', '', ''),
(14, 'en', 'About Us', '', 0, '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.&nbsp;<br />\r\nAenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', '', ''),
(22, 'en', 'Recent Posts', '', 0, '', '', ''),
(22, 'es', 'Mensajes recientes', '', 0, '', '', ''),
(23, 'en', 'News letter', '', 0, '<p>If you want to receive our latest news send directly to your email, please leave your email address bellow. Subscription is free and you can cancel anytime.</p>', '', ''),
(17, 'en', 'Partners', '', 0, '', '', ''),
(14, 'es', 'Sobre nosotros', '', 0, '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.&nbsp;<br />\r\nAenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', '', ''),
(15, 'es', 'Los mejores destinos de vacaciones', 'Todas', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(16, 'es', 'Testimonios', '', 0, '<p>Lorem ipsum dolor sit amet</p>', '', ''),
(18, 'en', 'Lake Wanaka', '', 0, '', '', 'LakeWanaka'),
(19, 'en', 'Ohama Beach', '', 0, '', '', 'OhamaBeach'),
(20, 'en', 'Taupo Central', '', 0, '', '', 'TaupoCentral'),
(20, 'es', 'Taupo Central', '', 0, '', '', 'TaupoCentral'),
(18, 'es', 'Lake Wanaka', '', 0, '', '', 'LakeWanaka'),
(19, 'es', 'Ohama Beach', '', 0, '', '', 'OhamaBeach'),
(21, 'en', 'Follow us', '', 0, '', '', ''),
(23, 'es', 'Hoja informativa', '', 0, '<p>Si desea recibir las &uacute;ltimas noticias env&iacute;a directamente a su correo electr&oacute;nico, por favor deje su direcci&oacute;n de correo electr&oacute;nico abajo. La suscripci&oacute;n es gratuita y se puede cancelar en cualquier momento.</p>', '', ''),
(24, 'en', 'Information Service:', '', 0, '<p>800-2345-6789</p>', '', ''),
(24, 'es', 'Servicio de información:', '', 0, '<p>800-2345-6789</p>', '', ''),
(25, 'en', 'Offering a wide range of laboratory services', 'Read More', 0, '<p>Our fully computerised laboratory group offers a high quality, cost-effective service and clinical expertise to local general practitioners and other trusts, hospitals and healthcare providers. Our innovative laboratory services enhance patient health.</p>', '', 'services'),
(25, 'es', 'Ofreciendo una amplia gama de servicios de laboratorio', 'Lee mas', 0, '<p>Nuestro grupo de laboratorio totalmente informatizado ofrece una alta calidad, un servicio rentable y experiencia cl&iacute;nica para los profesionales locales generales y otros fideicomisos, hospitales y proveedores de atenci&oacute;n m&eacute;dica. Nuestros servicios innovadores de laboratorio mejoran la salud del paciente.</p>', '', 'servicios'),
(26, 'en', 'Most common tests', 'The List Links', 292, '<p>Our laboratory provides comprehensive, multidisciplinary pathology services. As part of a regular checkup, to get a diagnosis, or perhaps to provide a benchmark, your doctor may request one or more laboratory tests. Here is a list of the most common tests ordered and their purpose. Most tests are done using a blood sample. Some standard tests are usually performed on admission to a hospital or as part of an annual physical.</p>', '', ''),
(26, 'es', 'Pruebas más comunes', 'La Lista de Enlaces', 292, '<p>Nuestro laboratorio ofrece servicios integrales de patolog&iacute;a multidisciplinares. Como parte de un examen de rutina, para obtener un diagn&oacute;stico, o tal vez para proporcionar un punto de referencia, el m&eacute;dico puede solicitar una o m&aacute;s pruebas de laboratorio. Aqu&iacute; est&aacute; una lista de las pruebas m&aacute;s comunes ordenados y su prop&oacute;sito. La mayor&iacute;a de las pruebas se realizaron utilizando una muestra de sangre. Algunas pruebas est&aacute;ndar se realizan generalmente al ingreso en un hospital o como parte de un examen f&iacute;sico anual.</p>', '', ''),
(27, 'en', 'Offering a wide range of laboratory services', 'Read More', 0, '<p>Our fully computerised laboratory group offers a high quality, cost-effective service and clinical expertise to local general practitioners and other trusts, hospitals and healthcare providers. Our innovative laboratory services enhance patient health.</p>', '', ''),
(27, 'es', 'Ofreciendo una amplia gama de servicios de laboratorio', 'Lee mas', 0, '<p>Nuestro grupo de laboratorio totalmente informatizado ofrece una alta calidad, un servicio rentable y experiencia cl&iacute;nica para los profesionales locales generales y otros fideicomisos, hospitales y proveedores de atenci&oacute;n m&eacute;dica. Nuestros servicios innovadores de laboratorio mejoran la salud del paciente.</p>', '', ''),
(28, 'en', 'Home', 'LEARN MORE', 0, '<h1>ZENTRO RESTAURANT</h1>\r\n\r\n<h2>CLEAN &amp; SIMPLE DESIGN</h2>', '', '#gallery'),
(28, 'es', 'Inicio', 'APRENDE MÁS', 0, '<h1>ZENTRO RESTAURANTE</h1>\r\n\r\n<h2>DISE&Ntilde;O LIMPIO Y SENCILLO</h2>', '', '#gallery'),
(29, 'en', 'Food Gallery', '', 0, '', '', ''),
(29, 'es', 'Galería de comida', '', 0, '', '', ''),
(30, 'en', 'Special Menu', '', 0, '', '', ''),
(30, 'es', 'Menú especial', '', 0, '', '', ''),
(31, 'en', 'Chefs', '', 0, '<p>Meet Zentro chefs</p>', '', ''),
(31, 'es', 'Cocinero', '', 0, '<p>Conoce a los cocineros Zentro</p>', '', ''),
(32, 'en', 'Contact', '', 0, '<p>Contact Us</p>', '', ''),
(32, 'es', 'Contacto', '', 0, '<p>Cont&aacute;ctenos</p>', '', ''),
(33, 'en', 'Open Hours', '', 0, '', '', ''),
(33, 'es', 'Horarios de apertura', '', 0, '', '', ''),
(34, 'en', 'Contact Info.', '', 0, '', '', ''),
(34, 'es', 'Datos de contacto.', '', 0, '', '', ''),
(35, 'en', 'Bootstrap theme', '', 0, '', '', ''),
(35, 'es', 'El tema de Bootstrap', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_blockslocalizations`
--

CREATE TABLE `s_blockslocalizations` (
  `blocksid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `img` int(11) NOT NULL DEFAULT 0,
  `img02` int(11) NOT NULL DEFAULT 0,
  `img_url` text NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `link_name` text DEFAULT NULL,
  `text` longtext NOT NULL,
  `url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_blockslocalizations`
--

INSERT INTO `s_blockslocalizations` (`blocksid`, `lang`, `img`, `img02`, `img_url`, `name`, `link_name`, `text`, `url`) VALUES
(47, 'en', 261, 0, '', 'Creative market', '', '', ''),
(48, 'en', 262, 0, '', 'designmodo', '', '', ''),
(49, 'en', 263, 0, '', 'evanto', '', '', ''),
(50, 'en', 264, 0, '', 'WordPress', '', '', ''),
(51, 'en', 265, 0, '', 'themeforest', '', '', ''),
(52, 'en', 266, 0, '', 'microlancer', '', '', ''),
(46, 'es', 260, 0, '', 'ae tust', '', '', ''),
(46, 'en', 260, 0, '', 'ae tust', '', '', ''),
(53, 'en', 275, 0, '', 'Text Heading A', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(54, 'en', 276, 0, '', 'Text Heading B', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(55, 'en', 277, 0, '', 'Text Heading C', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(56, 'en', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(57, 'en', 282, 283, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(58, 'en', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(59, 'en', 286, 287, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(60, 'en', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(61, 'en', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(62, 'en', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(63, 'en', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(64, 'en', 257, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(65, 'en', 258, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(66, 'en', 259, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(67, 'en', 0, 0, '', 'Twitter', 'fa fa-twitter', '', 'https://twitter.com/microphp'),
(68, 'en', 0, 0, '', 'Facebook', 'fa fa-facebook', '', 'https://www.facebook.com/webdevelopmentgroup/'),
(69, 'en', 0, 0, '', 'Linkedin', 'fa fa-linkedin', '', '#'),
(70, 'en', 0, 0, '', 'Pinterest', 'fa fa-pinterest', '', '#'),
(51, 'es', 265, 0, '', 'themeforest', '', '', ''),
(50, 'es', 264, 0, '', 'WordPress', '', '', ''),
(52, 'es', 266, 0, '', 'microlancer', '', '', ''),
(49, 'es', 263, 0, '', 'evanto', '', '', ''),
(48, 'es', 262, 0, '', 'designmodo', '', '', ''),
(47, 'es', 261, 0, '', 'Creative market', '', '', ''),
(53, 'es', 275, 0, '', 'Text Heading A', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(54, 'es', 276, 0, '', 'Text Heading B', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(55, 'es', 277, 0, '', 'Text Heading C', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(64, 'es', 257, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(65, 'es', 258, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(66, 'es', 259, 0, '', 'WILL SMITH', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non. Pellentesque rutrum fringilla elementum. Curabitur tincidunt porta lorem vitae accumsan.</p>', ''),
(58, 'es', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(57, 'es', 282, 283, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(56, 'es', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(61, 'es', 280, 281, '', '1', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(60, 'es', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(59, 'es', 286, 287, '', '2', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(63, 'es', 282, 283, '', '3', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(62, 'es', 285, 284, '', '4', '', '<p>Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', ''),
(67, 'es', 0, 0, '', 'Twitter', 'fa fa-twitter', '', 'https://twitter.com/microphp'),
(68, 'es', 0, 0, '', 'Facebook', 'fa fa-facebook', '', 'https://www.facebook.com/webdevelopmentgroup/'),
(69, 'es', 0, 0, '', 'Linkedin', 'fa fa-linkedin', '', '#'),
(70, 'es', 0, 0, '', 'Pinterest', 'fa fa-pinterest', '', '#'),
(71, 'es', 291, 0, '', 'La mayor red de centros de laboratorio', 'Lee mas', '<p>Tenemos contratos con laboratorios de todo el mundo para proporcionar servicios de laboratorio a los mejores precios. Tambi&eacute;n tenemos contratos con los laboratorios de independiente independientes para ...</p>', ''),
(71, 'en', 291, 0, '', 'The largest network of laboratory centers', 'Read More', '<p>We have contracts with laboratories all over the world to provide lab services at the best prices. We also contract with the free-standing independent laboratories for...</p>', '#'),
(72, 'en', 289, 0, '', 'The modern laboratory facilities', 'Read More', '<p>We&rsquo;ve done our research to ensure that the medical laboratory equipment we use has been industry approved and has surpassed statistical benchmarks for risk assessment. Our team members are highly trained...</p>', '#'),
(72, 'es', 289, 0, '', 'Las modernas instalaciones de laboratorio', 'Lee mas', '<p>Hemos hecho nuestra investigaci&oacute;n para asegurarse de que el equipo de laboratorio m&eacute;dica que usamos ha sido aprobada la industria y ha superado los puntos de referencia estad&iacute;sticos para la evaluaci&oacute;n de riesgos. Los miembros de nuestro equipo est&aacute;n altamente capacitados ...</p>', ''),
(73, 'en', 290, 0, '', 'Helpful test tips', 'Read More', '<p>If you are nervous or have a tendency to feel woozy or faint, tell the phlebotomist before you begin. Your blood can be drawn while you are lying down, which will help you avoid fainting and injuring yourself. If, at any time, you feel faint or lightheaded...</p>', '#'),
(73, 'es', 290, 0, '', 'Puntas de prueba votos', 'Lee mas', '<p>Si est&aacute; nervioso o tiene una tendencia a sentirse mareado o d&eacute;bil, decirle al flebotomista antes de empezar. Su sangre se puede dibujar mientras se est&aacute; acostado, lo que ayudar&aacute; a evitar desmayos y lesiones personales. Si, en cualquier momento, usted siente mareos o v&eacute;rtigo ...</p>', '#'),
(74, 'en', 0, 0, '', 'Flu tests', '', '', ''),
(74, 'es', 0, 0, '', 'Pruebas para la gripe', '', '', ''),
(75, 'en', 0, 0, '', 'Glucose', '', '', ''),
(75, 'es', 0, 0, '', 'Glucosa', '', '', ''),
(76, 'en', 0, 0, '', 'Semen Analysis', '', '', ''),
(76, 'es', 0, 0, '', 'El análisis del semen', '', '', ''),
(77, 'en', 0, 0, '', 'Uric Acid', '', '', ''),
(77, 'es', 0, 0, '', 'Ácido úrico', '', '', ''),
(78, 'en', 0, 0, '', 'Complete Blood Count', '', '', ''),
(78, 'es', 0, 0, '', 'Hemograma completo', '', '', ''),
(79, 'en', 0, 0, '', 'Hemoglobin or Glycohemoglobin', '', '', ''),
(79, 'es', 0, 0, '', 'Hemoglobina o Glicohemoglobina', '', '', ''),
(80, 'en', 293, 0, '', 'Timmy', '', '<p>Your company understands today&rsquo;s requirements, but that&rsquo;s not what makes you stand apart. You also understand today&rsquo;s business world and how to keep customers happy.</p>', '#'),
(80, 'es', 293, 0, '', 'Timmy', '', '<p>Su empresa es consciente de las necesidades actuales, pero eso no es lo que te hace estar al margen. Tambi&eacute;n entiende el mundo empresarial actual y la forma de mantener a los clientes contentos.</p>', ''),
(81, 'en', 294, 0, '', 'Timmy', '', '<p>Your company understands today&rsquo;s requirements, but that&rsquo;s not what makes you stand apart. You also understand today&rsquo;s business world and how to keep customers happy.</p>', ''),
(81, 'es', 294, 0, '', 'Timmy', '', '<p>Su empresa es consciente de las necesidades actuales, pero eso no es lo que te hace estar al margen. Tambi&eacute;n entiende el mundo empresarial actual y la forma de mantener a los clientes contentos.</p>', ''),
(82, 'en', 307, 0, '', 'Lemon-Rosemary Prawn', '', '<p>Seafood / Shrimp / Lemon</p>', ''),
(83, 'en', 306, 0, '', 'Lemon-Rosemary Vegetables', '', '<p>Tomato / Rosemary / Lemon</p>', ''),
(84, 'en', 308, 0, '', 'Lemon-Rosemary Bakery', '', '<p>Bread / Rosemary / Orange</p>', ''),
(85, 'en', 309, 0, '', 'Lemon-Rosemary Salad', '', '<p>Chicken / Rosemary / Green</p>', ''),
(86, 'en', 310, 0, '', 'Lemon-Rosemary Pizza', '', '<p>Pasta / Rosemary / Green</p>', ''),
(82, 'es', 307, 0, '', 'Limón-Romero gamba', '', '<p>Mariscos / camar&oacute;n / Lim&oacute;n</p>', ''),
(83, 'es', 306, 0, '', 'Verduras de limón y romero', '', '<p>Tomate / Romero / Lim&oacute;n</p>', ''),
(85, 'es', 309, 0, '', 'Ensalada de limón y romero', '', '<p>Pollo / Romero / verde</p>', ''),
(86, 'es', 310, 0, '', 'Limón Romero-pizza', '', '<p>Pasta / Romero / Verde</p>', ''),
(84, 'es', 308, 0, '', 'Limón-Romero Panadería', '', '<p>Pan / Romero / Naranja</p>', ''),
(87, 'es', 0, 0, '', 'Vegetal Limón-Romero ........................', '$20.50', '<p>Pollo / Romero / Lim&oacute;n</p>', ''),
(87, 'en', 0, 0, '', 'Lemon-Rosemary Vegetable ................', '$20.50', '<p>Chicken / Rosemary / Lemon</p>', ''),
(88, 'en', 0, 0, '', 'Lemon-Rosemary Meat ...........................', '$30.50', '<p>Meat / Rosemary / Lemon</p>', ''),
(89, 'en', 0, 0, '', 'Lemon-Rosemary Pork ........................', '$40.75', '<p>Pork / Tooplate / Lemon</p>', ''),
(90, 'es', 0, 0, '', 'Ensalada de naranja-Romero ...................', '$55.00', '<p>Ensalada / Romero / Naranja</p>', ''),
(90, 'en', 0, 0, '', 'Orange-Rosemary Salad ..........................', '$55.00', '<p>Salad / Rosemary / Orange</p>', ''),
(91, 'es', 0, 0, '', 'Limón-Romero Calamar .......................', '$65.00', '<p>Calamar / Romero / Lim&oacute;n</p>', ''),
(91, 'en', 0, 0, '', 'Lemon-Rosemary Squid ......................', '$65.00', '<p>Squid / Rosemary / Lemon</p>', ''),
(92, 'es', 0, 0, '', 'Camarones Naranja-Romero ....................', '$70.50', '<p>Camarones / Romero / Naranja</p>', ''),
(92, 'en', 0, 0, '', 'Orange-Rosemary Shrimp ........................', '$70.50', '<p>Shrimp / Rosemary / Orange</p>', ''),
(93, 'en', 0, 0, '', 'Lemon-Rosemary Prawn ...................', '$110.75', '<p>Chicken / Rosemary / Lemon</p>', ''),
(94, 'en', 0, 0, '', 'Lemon-Rosemary Seafood .....................', '$220.50', '<p>Seafood / Rosemary / Lemon</p>', ''),
(94, 'es', 0, 0, '', 'Limón-Romero Mariscos .........................', '$220.50', '<p>Mariscos / Romero / Lim&oacute;n</p>', ''),
(93, 'es', 0, 0, '', 'Limón-Romero gamba .........................', '$110.75', '<p>Pollo / Romero / Lim&oacute;n</p>', ''),
(89, 'es', 0, 0, '', 'Cerdo Limón-Romero ...........................', '$40.75', '<p>Cerdo / Tooplate / Lim&oacute;n</p>', ''),
(88, 'es', 0, 0, '', 'Limón-Romero Carne ................................', '$30.50', '<p>Carne / Romero / Lim&oacute;n</p>', ''),
(95, 'en', 312, 0, '', 'Thanya', '', '<p>Main Chef</p>', ''),
(95, 'es', 312, 0, '', 'Thanya', '', '<p>Chef principal</p>', ''),
(96, 'en', 311, 0, '', 'Lynda', '', '<p>Pizza Specialist</p>', ''),
(96, 'es', 311, 0, '', 'Lynda', '', '<p>Especialista de pizza</p>', ''),
(97, 'en', 313, 0, '', 'Jenny Ko', '', '<p>New Baker</p>', ''),
(97, 'es', 313, 0, '', 'Jenny Ko', '', '<p>Nueva Panadero</p>', ''),
(98, 'en', 0, 0, '', 'Sunday', '', '<p>10:30 AM - 10:00 PM</p>', ''),
(98, 'es', 0, 0, '', 'Domingo', '', '<p>10:30 AM - 10:00 PM</p>', ''),
(99, 'en', 0, 0, '', 'Mon-Fri', '', '<p>9:00 AM - 8:00 PM</p>', ''),
(99, 'es', 0, 0, '', 'De lunes a viernes', '', '<p>9:00 AM - 8:00 PM</p>', ''),
(100, 'en', 0, 0, '', 'Saturday', '', '<p>11:30 AM - 10:00 PM</p>', ''),
(100, 'es', 0, 0, '', 'Sábado', '', '<p>11:30 AM - 10:00 PM</p>', ''),
(101, 'en', 0, 0, '', 'Phone', 'fa fa-phone', '<p>090-080-0760</p>', ''),
(101, 'es', 0, 0, '', 'Teléfono', 'fa fa-phone', '<p>090-080-0760</p>', ''),
(102, 'en', 0, 0, '', 'Our Location', 'fa fa-map-marker', '<p>120 Duis aute irure, California, USA</p>', ''),
(102, 'es', 0, 0, '', 'Nuestra ubicación', 'fa fa-map-marker', '<p>120 Duis aute irure, California, USA</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_dashboard`
--

CREATE TABLE `s_dashboard` (
  `x_blocks` bigint(21) DEFAULT NULL,
  `x_pages` bigint(21) DEFAULT NULL,
  `x_formentries` bigint(21) DEFAULT NULL,
  `x_files` bigint(21) DEFAULT NULL,
  `x_modules` bigint(21) DEFAULT NULL,
  `x_news` bigint(21) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_feedback`
--

CREATE TABLE `s_feedback` (
  `fid` int(11) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT 0,
  `phone` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `create_date` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_feedback`
--

INSERT INTO `s_feedback` (`fid`, `rate`, `phone`, `name`, `create_date`) VALUES
(1, 5, 'asdas', 'dasdasdasd', 1637263369);

-- --------------------------------------------------------

--
-- Table structure for table `s_files`
--

CREATE TABLE `s_files` (
  `fileid` int(11) NOT NULL,
  `originalname` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(20) NOT NULL DEFAULT '',
  `category` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `add_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_files`
--

INSERT INTO `s_files` (`fileid`, `originalname`, `filename`, `extension`, `category`, `title`, `add_time`) VALUES
(281, '1.jpg', '1.jpg', 'jpg', 'image', '', 1475498320),
(282, '3-thumb.jpg', '3-thumb.jpg', 'jpg', 'image', '', 1475499062),
(283, '3.jpg', '3.jpg', 'jpg', 'image', '', 1475499062),
(275, 'key.png', 'key.png', 'png', 'image', '', 1475496127),
(276, 'money.png', 'money.png', 'png', 'image', '', 1475496128),
(280, '1-thumb.jpg', '1-thumb.jpg', 'jpg', 'image', '', 1475498320),
(277, 'days.png', 'days.png', 'png', 'image', '', 1475496131),
(284, '4.jpg', '4.jpg', 'jpg', 'image', '', 1475499062),
(285, '4-thumb.jpg', '4-thumb.jpg', 'jpg', 'image', '', 1475499062),
(286, '2-thumb.jpg', '2-thumb.jpg', 'jpg', 'image', '', 1475499687),
(287, '2.jpg', '2.jpg', 'jpg', 'image', '', 1475499690),
(288, 'about.jpg', 'about.jpg', 'jpg', 'image', '', 1475526308),
(257, 'people1.jpg', 'people1.jpg', 'jpg', 'image', '', 1475482444),
(258, 'people2.jpg', 'people2.jpg', 'jpg', 'image', '', 1475482448),
(259, 'people3.jpg', 'people3.jpg', 'jpg', 'image', '', 1475482450),
(260, '15.jpg', '15.jpg', 'jpg', 'image', '', 1475482469),
(261, '16.jpg', '16.jpg', 'jpg', 'image', '', 1475482471),
(262, '17.jpg', '17.jpg', 'jpg', 'image', '', 1475482473),
(263, '18.jpg', '18.jpg', 'jpg', 'image', '', 1475482477),
(264, '19.jpg', '19.jpg', 'jpg', 'image', '', 1475482479),
(265, '20.jpg', '20.jpg', 'jpg', 'image', '', 1475482481),
(266, '21.jpg', '21.jpg', 'jpg', 'image', '', 1475482482),
(256, 'banner2.jpg', 'banner2.jpg', 'jpg', 'image', '', 1475481623),
(255, 'banner1.jpg', 'banner1.jpg', 'jpg', 'image', '', 1475481623),
(289, 'icon3.png', 'icon3.png', 'png', 'image', '', 1475758048),
(290, 'icon2.png', 'icon2.png', 'png', 'image', '', 1475758048),
(291, 'icon1.png', 'icon1.png', 'png', 'image', '', 1475758048),
(292, '6.jpg', '6.jpg', 'jpg', 'image', '', 1475758506),
(293, 'tester1.jpg', 'tester1.jpg', 'jpg', 'image', '', 1475759109),
(294, 'tester2.jpg', 'tester2.jpg', 'jpg', 'image', '', 1475759109),
(295, '7.jpg', '7.jpg', 'jpg', 'image', '', 1475760849),
(296, '9.jpg', '9.jpg', 'jpg', 'image', '', 1475760849),
(297, '8.jpg', '8.jpg', 'jpg', 'image', '', 1475760850),
(298, '11.jpg', '11.jpg', 'jpg', 'image', '', 1475761619),
(299, '10.jpg', '10.jpg', 'jpg', 'image', '', 1475761619),
(300, '12.jpg', '12.jpg', 'jpg', 'image', '', 1475761619),
(301, '13.jpg', '13.jpg', 'jpg', 'image', '', 1475761619),
(302, '14.jpg', '14.jpg', 'jpg', 'image', '', 1475761619),
(303, '15.jpg', '15.jpg', 'jpg', 'image', '', 1475761619),
(304, '16.jpg', '16.jpg', 'jpg', 'image', '', 1475761619),
(305, '17.jpg', '17.jpg', 'jpg', 'image', '', 1475761619),
(306, 'gallery-img2.jpg', 'gallery-img2.jpg', 'jpg', 'image', '', 1475820765),
(307, 'gallery-img1.jpg', 'gallery-img1.jpg', 'jpg', 'image', '', 1475820765),
(308, 'gallery-img3.jpg', 'gallery-img3.jpg', 'jpg', 'image', '', 1475820765),
(309, 'gallery-img4.jpg', 'gallery-img4.jpg', 'jpg', 'image', '', 1475820766),
(310, 'gallery-img5.jpg', 'gallery-img5.jpg', 'jpg', 'image', '', 1475820766),
(311, 'team2.jpg', 'team2.jpg', 'jpg', 'image', '', 1475823576),
(312, 'team1.jpg', 'team1.jpg', 'jpg', 'image', '', 1475823576),
(313, 'team3.jpg', 'team3.jpg', 'jpg', 'image', '', 1475823577),
(315, 'mcms_profile.jpg', 'mcms_profile.jpg', 'jpg', 'image', '', 1497967530),
(317, 'zagruzheno.jpg', 'zagruzheno.jpg', 'jpg', 'image', '', 1516895823),
(343, 'dino-banner-4.jpg', 'dino-banner-4.jpg', 'jpg', 'image', 'dino-banner-4', 1593200563),
(344, 'dino-banner-3.jpg', 'dino-banner-3.jpg', 'jpg', 'image', 'dino-banner-3', 1593200591),
(345, 'store.png', 'store.png', 'png', 'image', 'store', 1598040915);

-- --------------------------------------------------------

--
-- Table structure for table `s_files_image_sizes`
--

CREATE TABLE `s_files_image_sizes` (
  `imgid` int(11) NOT NULL DEFAULT 0,
  `size` varchar(255) NOT NULL DEFAULT '',
  `x1` int(5) NOT NULL DEFAULT 0,
  `y1` int(5) NOT NULL DEFAULT 0,
  `x2` int(5) NOT NULL DEFAULT 0,
  `y2` int(5) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_files_image_sizes`
--

INSERT INTO `s_files_image_sizes` (`imgid`, `size`, `x1`, `y1`, `x2`, `y2`) VALUES
(140, '400x400', 174, 90, 574, 490),
(140, '430x330', 162, 105, 592, 435),
(140, '227x330', 241, 128, 468, 458),
(140, '325x160', 215, 187, 540, 347),
(140, '325x340', 189, 73, 514, 413),
(140, '325x155', 210, 155, 535, 310),
(140, '328x247', 259, 162, 587, 409),
(251, '328x247', 21, 121, 349, 368),
(251, '325x155', 357, 176, 682, 331),
(251, '325x340', 33, 53, 358, 393),
(251, '325x160', 39, 179, 340, 327),
(251, '227x330', 102, 44, 329, 374),
(251, '430x330', 151, 23, 581, 353),
(251, '400x400', 7, 0, 400, 393),
(316, '325x340', 0, 0, 325, 340),
(316, '325x160', 260, 184, 585, 344),
(316, '227x330', 66, 83, 293, 413),
(316, '430x330', 0, 90, 430, 420),
(316, '400x400', 49, 0, 513, 464),
(316, '325x155', 0, 0, 325, 155),
(316, '328x247', 0, 0, 328, 247);

-- --------------------------------------------------------

--
-- Table structure for table `s_gallery`
--

CREATE TABLE `s_gallery` (
  `gid` int(11) NOT NULL,
  `gdate` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `position` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallery`
--

INSERT INTO `s_gallery` (`gid`, `gdate`, `active`, `position`) VALUES
(63, 1475526525, 1, 1),
(65, 1516894753, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `s_gallerylocalizations`
--

CREATE TABLE `s_gallerylocalizations` (
  `gid` int(11) NOT NULL DEFAULT 0,
  `galleryname` varchar(255) NOT NULL DEFAULT '',
  `gallery_desc` text DEFAULT NULL,
  `lang` char(2) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallerylocalizations`
--

INSERT INTO `s_gallerylocalizations` (`gid`, `galleryname`, `gallery_desc`, `lang`) VALUES
(63, 'Nuestra Galería', '<p>Lorem ipsum dolor sit amet</p>', 'es'),
(63, 'Our Gallery', '<p>Lorem ipsum dolor sit amet</p>', 'en'),
(65, 'Auto', '', 'en'),
(65, '', '', 'es');

-- --------------------------------------------------------

--
-- Table structure for table `s_gallery_images`
--

CREATE TABLE `s_gallery_images` (
  `imgid` int(11) NOT NULL,
  `gid` int(11) NOT NULL DEFAULT 0,
  `img_name` varchar(255) NOT NULL DEFAULT '',
  `img_date` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_gallery_images`
--

INSERT INTO `s_gallery_images` (`imgid`, `gid`, `img_name`, `img_date`) VALUES
(545, 63, '1.jpg', 1475571289),
(546, 63, '2.jpg', 1475571289),
(547, 63, '3.jpg', 1475571289),
(548, 63, '4.jpg', 1475571289),
(554, 65, '12-2013-ford-mustang-gt-review.jpg', 1516894776),
(555, 65, '13FordMustang_22.jpg', 1516894777),
(556, 65, '2013-ford-mustang-califor_600x0w.jpg', 1516894777),
(557, 65, '2013-Ford-Mustang-Rear-Sequential-LEDs.jpg', 1516894777),
(558, 65, '2013-ford-mustang_100381239_l.jpg', 1516894777),
(559, 65, '2013-Ford-Mustang-GT-Ringbrothers-front-three-quarter-2.jpg', 1516894777),
(560, 65, '2013-Ford-Mustang-Shelby-GT500-Interior.jpg', 1516894777),
(561, 65, '2013-ford-mustang-shelby-gt500-super-snake-widebody.jpg', 1516894777),
(562, 65, '056307-2013-ford-mustang-gt-review-by-john-heilig.1-lg.jpg', 1516894777),
(563, 65, 'ford_mustang_gt_2013_interior.jpg', 1516894777),
(564, 65, 'lamborghini-sesto-elemento-concept-in-detail-49.jpg', 1516894778),
(565, 65, 'New-Ford-Mustang-2013-HD-Wallpaper.jpg', 1516894778);

-- --------------------------------------------------------

--
-- Table structure for table `s_logs`
--

CREATE TABLE `s_logs` (
  `logsid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(100) DEFAULT NULL,
  `comp` text DEFAULT NULL,
  `date_int` int(11) NOT NULL DEFAULT 0,
  `module_name` varchar(255) NOT NULL,
  `moduleid` int(11) NOT NULL DEFAULT 0,
  `operation_mode` varchar(150) NOT NULL,
  `page` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_logs`
--

INSERT INTO `s_logs` (`logsid`, `user_id`, `ip`, `comp`, `date_int`, `module_name`, `moduleid`, `operation_mode`, `page`) VALUES
(1336, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636396297, 'login', 0, 'login_page', 'module=login'),
(1337, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636396301, 'login', 0, 'login_ok', 'module=login'),
(1338, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636399630, 'modules', 1000072, 'add_module', 'module=modules&mode=add'),
(1339, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636399679, 'modules', 1000072, 'delete_module', 'module=modules'),
(1340, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636399749, 'modules', 1000073, 'add_module', 'module=modules&mode=add'),
(1341, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636402611, 'section', 535, 'section_delete', 'module=section'),
(1342, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636402615, 'section', 536, 'section_delete', 'module=section&exp=261'),
(1343, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636402936, 'section', 536, 'section_delete', 'module=products_category'),
(1344, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636402943, 'section', 535, 'section_delete', 'module=products_category'),
(1345, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636402956, 'section', 297, 'section_delete', 'module=products_category&exp=261'),
(1346, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403033, 'section', 571, 'section_delete', 'module=products_category'),
(1347, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403035, 'section', 574, 'section_delete', 'module=products_category&exp=119'),
(1348, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403037, 'section', 572, 'section_delete', 'module=products_category&exp=119'),
(1349, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403040, 'section', 573, 'section_delete', 'module=products_category&exp=119'),
(1350, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403072, 'section', 570, 'section_delete', 'module=products_category&exp=119'),
(1351, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 544, 'section_delete', 'module=products_category&exp=260'),
(1352, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 545, 'section_delete', 'module=products_category&exp=260'),
(1353, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 546, 'section_delete', 'module=products_category&exp=260'),
(1354, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 547, 'section_delete', 'module=products_category&exp=260'),
(1355, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 548, 'section_delete', 'module=products_category&exp=260'),
(1356, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 549, 'section_delete', 'module=products_category&exp=260'),
(1357, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 550, 'section_delete', 'module=products_category&exp=260'),
(1358, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 551, 'section_delete', 'module=products_category&exp=260'),
(1359, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 552, 'section_delete', 'module=products_category&exp=260'),
(1360, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403075, 'section', 533, 'section_delete', 'module=products_category&exp=260'),
(1361, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403078, 'section', 532, 'section_delete', 'module=products_category&exp=260'),
(1362, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636403082, 'section', 537, 'section_delete', 'module=products_category&exp=260'),
(1363, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636564792, 'login', 0, 'login_page', 'module=login'),
(1364, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636564795, 'login', 0, 'login_ok', 'module=login'),
(1365, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566145, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1366, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566167, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1367, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566191, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1368, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566198, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1369, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566231, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1370, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566296, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1371, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566307, 'section', 0, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1372, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566520, 'section', 295, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1373, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566527, 'section', 295, 'section_delete', 'module=products_category&exp=295'),
(1374, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566535, 'section', 296, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1375, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566553, 'section', 297, 'section_add', 'module=products_category&c_id=-1&to=296'),
(1376, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566561, 'section', 297, 'section_delete', 'module=products_category&exp=297'),
(1377, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566561, 'section', 296, 'section_delete', 'module=products_category&exp=297'),
(1378, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566570, 'section', 298, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1379, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566581, 'section', 299, 'section_add', 'module=products_category&c_id=-1&to=294'),
(1380, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636566778, 'modules', 1000074, 'add_module', 'module=modules&mode=add'),
(1381, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636571602, 'section', 298, 'section_update', 'module=products_category&c_id=298'),
(1382, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636571617, 'section', 300, 'section_add', 'module=products_category&c_id=-1&to=298'),
(1383, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636571629, 'section', 301, 'section_add', 'module=products_category&c_id=-1&to=298'),
(1384, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636571856, 'section', 302, 'section_add', 'module=products_category&c_id=-1&to=299'),
(1385, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572144, 'products', 0, 'add', 'module=products&productseditid=-1'),
(1386, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572209, 'products', 1, 'add', 'module=products&productseditid=-1'),
(1387, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572218, 'products', 1, 'update', 'module=products&productseditid=1'),
(1388, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572518, 'products', 1, 'update', 'module=products&productseditid=1'),
(1389, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572534, 'products', 1, 'update', 'module=products&productseditid=1'),
(1390, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572711, 'products', 1, 'update', 'module=products&productseditid=1'),
(1391, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572716, 'products', 1, 'update', 'module=products&productseditid=1'),
(1392, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636572805, 'products', 1, 'update', 'module=products&productseditid=1'),
(1393, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636573101, 'products', 1, 'update', 'module=products&productseditid=1'),
(1394, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636573341, 'products', 1, 'update', 'module=products&productseditid=1'),
(1395, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636573348, 'products', 1, 'update', 'module=products&productseditid=1'),
(1396, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636573503, 'products', 1, 'delete', 'module=products'),
(1397, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636574761, 'modules', 1000069, 'delete_module', 'module=modules'),
(1398, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636574989, 'modules', 1000071, 'delete_module', 'module=modules'),
(1399, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636651898, 'login', 0, 'login_page', 'module=login'),
(1400, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636651901, 'login', 0, 'login_ok', 'module=login'),
(1401, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636651985, 'modules', 1000060, 'delete_module', 'module=modules'),
(1402, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36', 1636652600, 'modules', 1000066, 'delete_module', 'module=modules'),
(1403, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637006840, 'login', 0, 'login_page', 'module=login'),
(1404, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637006844, 'login', 0, 'login_ok', 'module=login'),
(1405, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637006866, 'section', 299, 'section_update', 'module=products_category&c_id=299'),
(1406, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637006883, 'section', 298, 'section_update', 'module=products_category&c_id=298'),
(1407, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637008913, 'section', 300, 'section_update', 'module=products_category&c_id=300'),
(1408, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637008931, 'section', 303, 'section_add', 'module=products_category&c_id=-1&to=300'),
(1409, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637008945, 'section', 304, 'section_add', 'module=products_category&c_id=-1&to=300'),
(1410, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637009051, 'section', 301, 'section_update', 'module=products_category&c_id=301'),
(1411, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637009065, 'section', 305, 'section_add', 'module=products_category&c_id=-1&to=298'),
(1412, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637097792, 'login', 0, 'login_page', 'module=login'),
(1413, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637097797, 'login', 0, 'login_ok', 'module=login'),
(1414, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637098791, 'products', 2, 'add', 'module=products&productseditid=-1'),
(1415, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637099177, 'products', 2, 'update', 'module=products&productseditid=2'),
(1416, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637099841, 'products', 2, 'update', 'module=products&productseditid=2'),
(1417, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637099915, 'products', 2, 'update', 'module=products&productseditid=2'),
(1418, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637100045, 'products', 2, 'update', 'module=products&productseditid=2'),
(1419, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637177579, 'login', 0, 'login_page', 'module=login'),
(1420, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637177582, 'login', 0, 'login_ok', 'module=login'),
(1421, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637177653, 'products', 3, 'add', 'module=products&productseditid=-1'),
(1422, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637178046, 'section', 302, 'section_update', 'module=products_category&c_id=302'),
(1423, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637178872, 'products', 4, 'add', 'module=products&productseditid=-1'),
(1424, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637178892, 'products', 5, 'add', 'module=products&productseditid=-1'),
(1425, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637178903, 'products', 5, 'update', 'module=products&productseditid=5'),
(1426, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637179562, 'modules', 1000075, 'add_module', 'module=modules&mode=add'),
(1427, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637181573, 'modules', 1000076, 'add_module', 'module=modules&mode=add'),
(1428, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637262084, 'login', 0, 'login_page', 'module=login'),
(1429, 0, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637262084, 'login', 0, 'login_page', 'module=login'),
(1430, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637262086, 'login', 0, 'login_ok', 'module=login'),
(1431, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1637262123, 'section', 297, 'section_update', 'module=section&c_id=297');

-- --------------------------------------------------------

--
-- Table structure for table `s_media`
--

CREATE TABLE `s_media` (
  `media_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `add_date` int(11) DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_media_likes`
--

CREATE TABLE `s_media_likes` (
  `media_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `add_date` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_members`
--

CREATE TABLE `s_members` (
  `member_id` int(11) NOT NULL,
  `m_user` varchar(255) NOT NULL,
  `m_mail` varchar(255) NOT NULL,
  `oauth_provider` enum('facebook','google','site','') NOT NULL,
  `facebook_id` varchar(64) NOT NULL,
  `google_id` varchar(64) NOT NULL,
  `referral_code` varchar(16) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `m_bal` double NOT NULL DEFAULT 1000,
  `m_pass` varchar(255) NOT NULL,
  `m_phone` varchar(256) NOT NULL,
  `m_birthdate` varchar(16) NOT NULL,
  `comment` text CHARACTER SET utf32 NOT NULL,
  `activate_info` varchar(255) NOT NULL,
  `reg_date` int(11) DEFAULT 0,
  `last_visit_date` varchar(256) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_members`
--

INSERT INTO `s_members` (`member_id`, `m_user`, `m_mail`, `oauth_provider`, `facebook_id`, `google_id`, `referral_code`, `m_name`, `m_bal`, `m_pass`, `m_phone`, `m_birthdate`, `comment`, `activate_info`, `reg_date`, `last_visit_date`, `active`) VALUES
(1, 'abbasov.azik@gmail.com', 'abbasov.azik@gmail.com', 'facebook', '4823718014320865', '', 'VF4FMU', 'Azer Abbasov', 994, '', '', '', '', '', 1598989636, '1599079684', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_menu`
--

CREATE TABLE `s_menu` (
  `mid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL DEFAULT 0,
  `htaccessname` varchar(255) NOT NULL DEFAULT '',
  `indexed` tinyint(1) NOT NULL DEFAULT 1,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `template` smallint(5) NOT NULL DEFAULT 0,
  `menutype` enum('content','link','module','nolink','welcome','static','contacts','top_menu','middle_menu','foot_menu','bottom_menu') NOT NULL DEFAULT 'content',
  `link` varchar(255) NOT NULL DEFAULT '',
  `newwindow` tinyint(1) NOT NULL DEFAULT 0,
  `position` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_menu`
--

INSERT INTO `s_menu` (`mid`, `parentid`, `htaccessname`, `indexed`, `visible`, `template`, `menutype`, `link`, `newwindow`, `position`) VALUES
(294, 0, 'top_menu', 1, 1, 0, 'top_menu', '', 0, 1),
(119, 0, 'foot_menu', 1, 1, 0, 'foot_menu', '', 0, 3),
(260, 0, 'middle_menu', 1, 1, 0, 'middle_menu', '', 0, 2),
(261, 0, 'bottom_menu', 1, 1, 0, 'bottom_menu', '', 0, 4),
(297, 295, 'contacts', 0, 0, 0, 'content', '', 0, 2),
(295, 0, 'static', 1, 1, 0, 'static', '', 0, 5),
(537, 260, '', 1, 1, 0, 'module', 'news', 0, 2),
(533, 260, '', 1, 1, 5, 'content', '', 0, 3),
(532, 260, '', 1, 1, 3, 'content', '', 0, 1),
(571, 119, '', 1, 1, 0, 'content', '', 0, 1),
(544, 533, '', 1, 1, 0, 'content', '', 0, 1),
(545, 533, '', 1, 1, 0, 'content', '', 0, 2),
(546, 533, '', 1, 1, 0, 'content', '', 0, 3),
(547, 533, '', 1, 1, 0, 'content', '', 0, 4),
(548, 533, '', 1, 1, 0, 'content', '', 0, 5),
(549, 533, '', 1, 1, 0, 'content', '', 0, 6),
(550, 533, '', 1, 1, 0, 'content', '', 0, 7),
(551, 533, '', 1, 1, 0, 'content', '', 0, 8),
(552, 533, '', 1, 1, 0, 'content', '', 0, 9),
(572, 119, '', 1, 1, 0, 'content', '', 0, 2),
(573, 119, '', 1, 1, 0, 'content', '', 0, 3),
(574, 119, '', 1, 1, 0, 'content', '', 1, 4),
(570, 260, '', 1, 1, 0, 'link', '535', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `s_menulocalizations`
--

CREATE TABLE `s_menulocalizations` (
  `mid` int(11) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `text` longtext NOT NULL,
  `seo_header` varchar(250) NOT NULL,
  `seo_description` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_menulocalizations`
--

INSERT INTO `s_menulocalizations` (`mid`, `lang`, `name`, `header`, `comment`, `text`, `seo_header`, `seo_description`, `seo_keywords`, `link`, `slug`) VALUES
(294, 'en', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(119, 'az', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(260, 'az', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(261, 'az', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(295, 'en', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(295, 'az', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(294, 'az', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(261, 'ru', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(294, 'ru', 'top_menu', '', '', '', 'top_menu', '', 'top_menu', 'http://', 'top-menu'),
(260, 'en', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(260, 'ru', 'middle_menu', '', '', '', 'middle_menu', '', 'middle_menu', 'http://', 'middle-menu'),
(119, 'en', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(119, 'ru', 'foot_menu', '', '', '', 'foot_menu', '', 'foot_menu', 'http://', 'foot-menu'),
(295, 'ru', 'static', '', '', '', 'static', '', 'static', 'http://', 'static'),
(261, 'en', 'bottom_menu', '', '', '', 'bottom_menu', '', 'bottom_menu', 'http://', 'bottom-menu'),
(532, 'en', 'Gallery', '', '', '<p>{GALLERY_63}</p>', '', '', '', 'http://', 'galery'),
(532, 'es', 'Galería', '', '', '<p>{GALLERY_63}</p>', '', '', '', 'http://', 'galera'),
(533, 'es', 'Servicios', '', '', '', '', '', '', 'http://', 'servicios'),
(297, 'en', 'Feedback', '', '', '', 'Feedback', 'Feedback', 'Feedback', 'http://', 'feedback'),
(297, 'az', 'Feedback', '', '', '', '', '', '', 'http://', 'feedback'),
(297, 'ru', 'Feedback', '', '', '', '', '', '', 'http://', 'feedback'),
(537, 'en', 'News', '', '', '', '', '', '', 'http://', 'name1'),
(537, 'es', 'Noticias', '', '', '', '', '', '', 'http://', 'noticias'),
(571, 'en', 'Haqqımızda', '', '', '', '', '', '', 'http://', 'haqqimizda1'),
(574, 'en', 'Qaydalar', '', '', '', '', '', '', 'http://', 'qaydalar'),
(572, 'en', 'Seçilmişlər', '', '', '', '', '', '', 'http://', 'secilmisler'),
(533, 'en', 'Services', '', '', '', '', '', '', 'http://', 'services'),
(544, 'en', 'Image Format', '298', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format'),
(544, 'es', 'Formato de imagen', '298', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen'),
(545, 'en', 'Image Format', '299', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format1'),
(545, 'es', 'Formato de imagen', '299', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen1'),
(546, 'en', 'Image Format', '300', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format2'),
(546, 'es', 'Formato de imagen', '300', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen2'),
(547, 'en', 'Image Format', '301', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format3'),
(547, 'es', 'Formato de imagen', '301', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen3'),
(548, 'en', 'Image Format', '302', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format4'),
(548, 'es', 'Formato de imagen', '302', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen4'),
(549, 'en', 'Image Format', '303', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format5'),
(549, 'es', 'Formato de imagen', '303', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen5'),
(550, 'en', 'Image Format', '304', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format6'),
(550, 'es', 'Formato de imagen', '304', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen6'),
(551, 'en', 'Image Format', '305', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format7'),
(551, 'es', 'Formato de imagen', '305', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen7'),
(552, 'en', 'Image Format', '302', '', '<p>This format perfectly fits in case you need only a single image for your post display. Use Featured image option to add image to the...</p>', '', '', '', 'http://', 'image-format8'),
(552, 'es', 'Formato de imagen', '302', '', '<p>Este formato se adapta perfectamente en caso de que necesite una &uacute;nica imagen para su visualizaci&oacute;n posterior. Utilice la opci&oacute;n Imagen destacada para agregar una imagen a la ...</p>', '', '', '', 'http://', 'formato-de-imagen8'),
(570, 'es', 'Inside link', '', '', '', '', '', '', 'http://', 'inside-link'),
(570, 'en', 'Inside link', '', '', '', '', '', '', 'http://', 'inside-link'),
(573, 'en', 'Mağaza', '', '', '', '', '', '', 'http://', 'magaza');

-- --------------------------------------------------------

--
-- Table structure for table `s_message_sends`
--

CREATE TABLE `s_message_sends` (
  `msgid` int(11) NOT NULL,
  `create_date` int(11) NOT NULL DEFAULT 0,
  `send_datetime` int(11) NOT NULL DEFAULT 0,
  `mail_to` varchar(255) NOT NULL DEFAULT '',
  `mail_subject` text NOT NULL,
  `mail_txt` longtext NOT NULL,
  `sended` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_message_sends`
--

INSERT INTO `s_message_sends` (`msgid`, `create_date`, `send_datetime`, `mail_to`, `mail_subject`, `mail_txt`, `sended`) VALUES
(1, 1598989636, 1598989655, 'abbasov.azik@gmail.com', 'Test Subject', 'activate link: http://localhost/iddiaweb/public_html/regactivate/0ca95c49b1b27304d5c05bafd990ab38513314b5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_modulelocalizations`
--

CREATE TABLE `s_modulelocalizations` (
  `moduleid` int(11) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `seo_title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_modulelocalizations`
--

INSERT INTO `s_modulelocalizations` (`moduleid`, `lang`, `title`, `seo_title`) VALUES
(1000056, 'en', 'Events log', ''),
(9, 'ru', 'Files', ''),
(999799, 'ru', 'Manage Users', ''),
(999899, 'en', 'Modules', ''),
(999999, 'es', 'Profile Options', ''),
(9, 'en', 'Files', ''),
(999799, 'en', 'Manage Users', ''),
(10, 'es', 'Gallery', ''),
(10, 'ru', 'Gallery', ''),
(50, 'ru', 'Slider', ''),
(50, 'en', 'Slider', ''),
(100, 'es', 'Contactos', 'Contact us'),
(101, 'es', 'Suscripción', 'Suscripción'),
(1, 'en', 'Main Page', ''),
(550, 'es', 'Blocks', ''),
(550, 'ru', 'Blocks', ''),
(7, 'es', 'News', ''),
(9, 'az', 'Fayllar', ''),
(10, 'en', 'Gallery', ''),
(50, 'az', 'Slider', ''),
(550, 'en', 'Blocks', ''),
(1, 'az', 'İlk Səhifə', ''),
(999799, 'az', 'Manage Users', ''),
(999899, 'az', 'Modullar', ''),
(1000056, 'es', 'Events log', ''),
(10, 'az', 'Qalereya', ''),
(5, 'en', 'Section Manager', ''),
(7, 'en', 'News', ''),
(100, 'en', 'Contacts', 'Contact us'),
(50, 'es', '', ''),
(9, 'es', '', ''),
(1, 'ru', 'Main Page', ''),
(1, 'es', '', ''),
(999999, 'en', 'Profile Options', ''),
(999799, 'es', '', ''),
(999899, 'ru', 'Modules', ''),
(999899, 'es', '', ''),
(101, 'en', 'Subscribe', 'Subscription'),
(1000067, 'az', 'Dashboard', 'Dashboard'),
(1000017, 'en', 'Inside pages', ''),
(1000017, 'az', 'Daxili dəhifələr', ''),
(1000018, 'az', 'Xəbərlər ətraflı', ''),
(1000018, 'en', 'News more', ''),
(1000031, 'az', 'Abune - imtina', ''),
(1000031, 'en', 'Unsubscribe', ''),
(1000031, 'ru', 'Unsubscribe', ''),
(1000031, 'es', 'Unsubscribe', ''),
(1000067, 'en', 'Dashboard', 'Dashboard'),
(1000017, 'es', '', ''),
(550, 'az', 'Blocks', ''),
(1000054, 'es', 'Correos enviados', 'Correos enviados'),
(999998, 'ru', 'Site options', ''),
(999998, 'en', 'Site options', ''),
(1000054, 'en', 'Sent mails', 'Sent mails'),
(1000058, 'az', 'Members', 'Members'),
(1000058, 'en', 'Members', 'Members'),
(1000058, 'ru', 'Members', 'Members'),
(1000059, 'az', 'Media', 'Media'),
(1000059, 'en', 'Media', 'Media'),
(1000059, 'ru', 'Media', 'Media'),
(1000075, 'en', 'Məhsullar ətraflı', 'Məhsullar ətraflı'),
(1000067, 'ru', 'Dashboard', 'Dashboard'),
(1000075, 'az', 'Məhsullar ətraflı', 'Məhsullar ətraflı'),
(1000068, 'az', 'Təhlükəsizlik', 'Təhlükəsizlik'),
(1000068, 'en', 'Təhlükəsizlik', 'Təhlükəsizlik'),
(1000076, 'en', 'Favorites', 'Favorites'),
(1000076, 'az', 'Favorites', 'Favorites'),
(1000070, 'az', 'Qeydiyyat', 'Qeydiyyat'),
(1000070, 'en', 'Qeydiyyat', 'Qeydiyyat'),
(1000075, 'ru', 'Məhsullar ətraflı', 'Məhsullar ətraflı'),
(1000073, 'ru', 'Məhsul kateqoriyaları', 'Məhsul kateqoriyaları'),
(1000073, 'en', 'Məhsul kateqoriyaları', 'Məhsul kateqoriyaları'),
(1000073, 'az', 'Məhsul kateqoriyaları', 'Məhsul kateqoriyaları'),
(1000074, 'az', 'Məhsullar', 'Məhsullar'),
(1000074, 'en', 'Məhsullar', 'Məhsullar'),
(1000074, 'ru', 'Məhsullar', 'Məhsullar'),
(1000076, 'ru', 'Favorites', 'Favorites');

-- --------------------------------------------------------

--
-- Table structure for table `s_modules`
--

CREATE TABLE `s_modules` (
  `moduleid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `deleteable` tinyint(1) NOT NULL DEFAULT 1,
  `have_view` tinyint(1) NOT NULL DEFAULT 1,
  `have_admin` tinyint(1) NOT NULL DEFAULT 1,
  `have_cnfg` tinyint(1) NOT NULL DEFAULT 0,
  `admin_files` text NOT NULL,
  `view_files` text NOT NULL,
  `admin_template_files` text NOT NULL,
  `view_template_files` text NOT NULL,
  `tables` varchar(255) NOT NULL DEFAULT '',
  `seo_table` varchar(50) NOT NULL DEFAULT '',
  `table_id` varchar(15) NOT NULL DEFAULT 'mid',
  `title_header` varchar(20) NOT NULL DEFAULT 'name',
  `title_description` varchar(20) NOT NULL DEFAULT 'text',
  `title_keywords` varchar(20) NOT NULL DEFAULT 'name',
  `type` tinyint(1) NOT NULL DEFAULT 3,
  `icon` varchar(100) NOT NULL DEFAULT 'fa fa-check',
  `view_type` enum('stat','stat1x','tile','tile2x','gallery') NOT NULL DEFAULT 'stat',
  `position` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_modules`
--

INSERT INTO `s_modules` (`moduleid`, `name`, `description`, `deleteable`, `have_view`, `have_admin`, `have_cnfg`, `admin_files`, `view_files`, `admin_template_files`, `view_template_files`, `tables`, `seo_table`, `table_id`, `title_header`, `title_description`, `title_keywords`, `type`, `icon`, `view_type`, `position`, `active`) VALUES
(5, 'section', 'For manage site menus', 0, 0, 1, 0, 'tree_json.php,tree_move.php', '', '', '', 'menu,menulocalizations', '', 'mid', 'seo_header', 'seo_description', 'seo_keywords', 3, 'fa fa-bars', 'stat', 1, 1),
(9, 'file_manager', 'Photo, document - Files manager', 0, 0, 1, 0, 'ajax_file_list.php', '', '', '', 'files', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-upload', 'stat1x', 3, 1),
(999799, 'manage_users', 'Manage admin users and permissions', 0, 0, 1, 0, '', '', '', '', 'users', '', 'mid', 'name', 'text', 'name', 2, 'fa fa-user', 'stat1x', 11, 1),
(999899, 'modules', 'Manage site modules', 0, 0, 1, 0, '', '', '', '', 'modules', '', 'mid', 'name', 'text', 'name', 1, 'fa fa-code', 'stat1x', 12, 1),
(999999, 'options_profile', 'Profile Options', 0, 1, 1, 0, '', '', '', '', 'options', '', '', '', '', '', 3, 'fa fa-user', 'stat', 16, 1),
(10, 'gallery_manager', 'Photo Gallery manager', 0, 0, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-camera', 'gallery', 4, 1),
(50, 'slider', 'Slider manager', 0, 0, 1, 0, '', '', '', '', 'slider', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-tasks', 'stat', 6, 1),
(7, 'news', 'Site news and events', 0, 1, 1, 0, 'ajax_news_list.php,news_drag.php', '', '', '', '', 'newscategorylocalizations', 'catid', 'name', 'header', 'name', 3, 'fa fa-newspaper', 'tile2x', 2, 1),
(100, 'contacts', 'Contacts manager', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-user', 'tile', 7, 1),
(101, 'subscribe', 'Manage subscribers, templates, send broadcast', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-envelope', 'tile', 8, 1),
(1, 'homepage', 'module for click and back to index page', 0, 1, 0, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-check', 'stat', 18, 1),
(550, 'blocks', 'Manage site banners,&nbsp;blocks, widgets', 0, 1, 1, 0, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-eye', 'tile2x', 5, 1),
(1000017, 'inside', 'Site menu link more page (inside page)', 0, 1, 0, 0, '', '', '', '', 'menu,menulocalizations', 'menulocalizations', 'mid', 'name', 'text', 'name', 3, 'fa fa-file', 'stat', 13, 1),
(1000018, 'news_more', 'Site news more page', 0, 1, 0, 0, '', '', '', '', 'news,newslocalizations', 'newslocalizations', 'newsid', 'seo_header', 'seo_description', 'seo_keywords', 3, 'fa fa-check', 'stat', 14, 1),
(1000031, 'unsubscribe', 'Unsubscribtion module', 0, 1, 0, 1, '', '', '', '', '', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-envelope', 'stat', 17, 1),
(1000054, 'mail_log', 'Sent mails', 0, 0, 1, 0, '', '', 'mail_log.tpl', '', '', '', '', '', '', '', 3, 'fa fa-envelope-open', 'tile', 9, 1),
(999998, 'options_site', 'Site options, meta tags', 0, 0, 1, 0, '', '', '', '', 'options', '', 'mid', 'name', 'text', 'name', 3, 'fa fa-wrench', 'stat', 15, 1),
(1000056, 'mcms_log', 'Events log', 0, 0, 1, 0, '', '', 'mcms_log.tpl', '', '', '', '', '', '', '', 3, 'fa fa-calendar-alt', 'tile', 10, 1),
(1000058, 'members', 'Members', 0, 0, 1, 0, 'members.admin.php', '', 'members.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000059, 'media', 'Media', 0, 0, 1, 0, 'media.admin.php', '', 'media.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 4, 1),
(1000067, 'member_dashboard', 'Dashboard', 0, 1, 0, 0, '', 'member_dashboard.view.php', '', 'member_dashboard.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000075, 'products_more', 'Məhsullar ətraflı', 1, 0, 1, 0, '', 'products_more.view.php', '', 'products_more.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000068, 'member_security', 'Təhlükəsizlik', 1, 1, 0, 0, '', 'member_security.view.php', '', 'member_security.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000070, 'register', 'Qeydiyyat', 1, 1, 0, 0, '', 'register.view.php', '', 'register.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000076, 'favorites', 'Favorites', 1, 0, 0, 0, '', 'favorites.view.php', '', 'favorites.tpl', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000073, 'products_category', 'Məhsul kateqoriyaları', 1, 0, 1, 0, 'products_category.admin.php', '', 'products_category.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1),
(1000074, 'products', 'Məhsullar', 1, 0, 1, 0, 'products.admin.php', '', 'products.tpl', '', '', '', '', '', '', '', 3, 'fa fa-list-alt', 'tile', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_news`
--

CREATE TABLE `s_news` (
  `newsid` int(11) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT 0,
  `newsdate` varchar(255) NOT NULL DEFAULT '',
  `expiredate` varchar(256) NOT NULL,
  `no_coefficient` varchar(16) NOT NULL DEFAULT '0',
  `yes_coefficient` varchar(16) NOT NULL DEFAULT '0',
  `answer` varchar(6) NOT NULL COMMENT 'yes, no cavablari ala biler.',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `index` tinyint(1) NOT NULL DEFAULT 1,
  `template` smallint(5) NOT NULL DEFAULT 0,
  `img_show` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_news`
--

INSERT INTO `s_news` (`newsid`, `catid`, `newsdate`, `expiredate`, `no_coefficient`, `yes_coefficient`, `answer`, `active`, `index`, `template`, `img_show`) VALUES
(1, 43, '1597175700', '1597215000', '3.20', '1.15', 'yes', 1, 1, 0, 1),
(2, 43, '1597522200', '1600325400', '1.25', '4.5', '', 1, 1, 0, 1),
(3, 43, '1597524000', '1604483100', '3.12', '2', '', 1, 1, 0, 1),
(4, 43, '1597524060', '1600753200', '3.12', '3', '', 1, 1, 0, 1),
(5, 43, '1597524060', '1600321500', '3.12', '3.6', '', 1, 1, 0, 1),
(6, 43, '1597524060', '1597758300', '4', '5.6', '', 1, 1, 0, 1),
(7, 43, '1597529700', '1597529700', '7', '2', '', 1, 1, 0, 1),
(8, 43, '1597530060', '1597530060', '100', '200', '', 1, 1, 0, 1),
(9, 43, '1597530060', '1597530060', '142', '160', '', 1, 1, 0, 1),
(10, 43, '1597530060', '1597530060', '175', '1.42', '', 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_newscategories`
--

CREATE TABLE `s_newscategories` (
  `catid` int(11) NOT NULL,
  `position` int(5) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `index` tinyint(1) NOT NULL DEFAULT 1,
  `event` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newscategories`
--

INSERT INTO `s_newscategories` (`catid`, `position`, `active`, `index`, `event`) VALUES
(44, 2, 1, 0, 0),
(43, 1, 1, 0, 0),
(45, 3, 1, 0, 0),
(48, 6, 1, 0, 0),
(47, 5, 1, 0, 0),
(49, 7, 1, 0, 0),
(50, 8, 1, 0, 0),
(51, 9, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_newscategorylocalizations`
--

CREATE TABLE `s_newscategorylocalizations` (
  `catid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(512) CHARACTER SET utf32 NOT NULL,
  `header` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newscategorylocalizations`
--

INSERT INTO `s_newscategorylocalizations` (`catid`, `lang`, `name`, `slug`, `header`) VALUES
(45, 'en', 'Şou biznes', 'soubiznes', ''),
(43, 'en', 'İqtisadiyyat', 'iqtisadiyyat', '<p>İqtisadiyyat</p>'),
(44, 'en', 'İdman', 'idman', ''),
(47, 'en', 'Texnologiya', 'texnologiya', ''),
(48, 'en', 'Gündəm', 'gundem', ''),
(49, 'en', 'Avto', 'avto', ''),
(50, 'en', 'Cəmiyyət', 'cemiyyet', ''),
(51, 'en', 'Turizm', 'turizm', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_newslocalizations`
--

CREATE TABLE `s_newslocalizations` (
  `newsid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `text` longtext NOT NULL,
  `helper` text CHARACTER SET utf32 NOT NULL,
  `seo_header` varchar(250) NOT NULL,
  `seo_description` text NOT NULL,
  `seo_keywords` text NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_newslocalizations`
--

INSERT INTO `s_newslocalizations` (`newsid`, `lang`, `name`, `header`, `comment`, `text`, `helper`, `seo_header`, `seo_description`, `seo_keywords`, `slug`) VALUES
(2, 'en', 'test news2', '343', 'test', '<p>test</p>', '', '', '', '', 'test-news2'),
(3, 'en', 'test news 3', '344', 'test news 3', '<p>test news 3</p>', '', '', '', '', 'test-news-3'),
(4, 'en', 'test news 4', '309', '', '<p>test news 4</p>', '', '', '', '', 'test-news-4'),
(5, 'en', 'test news 5', '312', 'test news 5', '<p>test news 5</p>', '', '', '', '', 'test-news-5'),
(6, 'en', 'test news 6', '317', '', '<p>test news 6</p>', '', '', '', '', 'test-news-6'),
(7, 'en', 'test 7', '301', '', '<p>test 7</p>', '', '', '', '', 'test-7'),
(8, 'en', 'test news 8', '292', '', '<p>test news 8</p>', '', '', '', '', 'test-news-8'),
(9, 'en', 'test news 9', '258', '', '<p>test news 9</p>', '', '', '', '', 'test-news-9'),
(10, 'en', 'test news 10', '259', '', '<p>test news 10</p>', '', '', '', '', 'test-news-10'),
(1, 'en', 'test xeber 1', '306', 'test xeber 1', '<p>test xeber 1&nbsp;test xeber 1&nbsp;test xeber 1&nbsp;test xeber 1</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>test xeber 1&nbsp;test xeber 1&nbsp;test xeber 1&nbsp;test xeber 1</p>', '', '', '', '', 'test-xeber-1');

-- --------------------------------------------------------

--
-- Table structure for table `s_news_read`
--

CREATE TABLE `s_news_read` (
  `newsid` int(11) NOT NULL DEFAULT 0,
  `read_date` int(11) NOT NULL DEFAULT 0,
  `total_views` int(11) NOT NULL DEFAULT 1,
  `month_views` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_news_read`
--

INSERT INTO `s_news_read` (`newsid`, `read_date`, `total_views`, `month_views`) VALUES
(1, 1598555821, 223, 223),
(2, 1599079698, 208, 68),
(5, 1598555972, 29, 29),
(10, 1598217629, 2, 2),
(3, 1598652779, 99, 99),
(4, 1599079721, 15, 2),
(9, 1598385136, 4, 4),
(0, 1637180339, 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `s_notifications`
--

CREATE TABLE `s_notifications` (
  `nid` int(11) NOT NULL,
  `to_member_id` int(11) NOT NULL DEFAULT 0,
  `from_member_id` int(11) NOT NULL DEFAULT 0,
  `n_subject` text NOT NULL,
  `n_text` longtext NOT NULL,
  `n_date` int(11) NOT NULL DEFAULT 0,
  `read_date` int(11) NOT NULL DEFAULT 0,
  `send_mail` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_notifications`
--

INSERT INTO `s_notifications` (`nid`, `to_member_id`, `from_member_id`, `n_subject`, `n_text`, `n_date`, `read_date`, `send_mail`, `active`) VALUES
(1, 1, 0, 'Azer Abbasov, qeydiyyatınız uğurla tamamlandı, iddia.az sizə 1000 bal hədiyyə edir', '', 1598989636, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_options`
--

CREATE TABLE `s_options` (
  `id` int(11) NOT NULL,
  `opt_key` varchar(255) NOT NULL DEFAULT '',
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_options`
--

INSERT INTO `s_options` (`id`, `opt_key`, `value`) VALUES
(1, 'charset', 'utf-8'),
(22, 'contacts_mail_user', 'YWJiYXNvdi5hemlrQGdtYWlsLmNvbQ=='),
(23, 'contacts_mail_pass', 'QWJiYXNvdjE5OTI='),
(6, 'lang_langs', 'en,az,ru'),
(7, 'lang_showlangs', 'en,az,ru'),
(8, 'lang_mainlang', 'az'),
(24, 'contacts_mail_pop3', 'c210cC5nbWFpbC5jb20='),
(11, 'lang_adminmainlang', 'en'),
(21, 'contacts_mail_from', 'bm9yZXBseUBpZGRpYS5heg=='),
(20, 'contacts_mail_to', 'YWJiYXNvdi5hemlrQGdtYWlsLmNvbQ=='),
(19, 'slider', '1'),
(25, 'contacts_mail_smtp', 'c210cC5nbWFpbC5jb20='),
(26, 'contacts_mail_host', 'aWRkaWEuYXo='),
(27, 'watermark', '1'),
(28, 'watermark_name', 'MicroPHP'),
(29, 'watermark_color', '#bf3030'),
(30, 'news_foot_limit', '3'),
(31, 'news_index_limit', '4'),
(32, 'news_page_limit', '10'),
(33, 'news_block_seperator', '3'),
(34, 'news_date_format', 'd.m.Y'),
(35, 'parallax', '0');

-- --------------------------------------------------------

--
-- Table structure for table `s_optionslocalizations`
--

CREATE TABLE `s_optionslocalizations` (
  `lang` char(2) NOT NULL,
  `opt_key` varchar(255) NOT NULL DEFAULT '',
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_optionslocalizations`
--

INSERT INTO `s_optionslocalizations` (`lang`, `opt_key`, `value`) VALUES
('en', 'site_name', 'iddia.az'),
('es', 'site_name', 'Sharebm'),
('en', 'meta_description', 'Gəlmərc edək'),
('es', 'meta_description', ''),
('en', 'site_title', 'Gəl mərcə!'),
('es', 'site_title', 'Sharebm'),
('es', 'meta_keywords', ''),
('en', 'meta_keywords', 'iddia'),
('en', 'meta_email', ''),
('es', 'meta_email', ''),
('en', 'meta_author', ''),
('es', 'meta_author', ''),
('en', 'site_copyright', '[YEAR] iddia'),
('es', 'site_copyright', '[YEAR] Sharebm'),
('ru', 'site_name', 'Dinosaur of Music'),
('ru', 'site_title', 'Dinosaur of Music'),
('ru', 'meta_keywords', 'Dinosaur of Music'),
('ru', 'meta_description', 'Dinosaur of Music'),
('ru', 'meta_email', ''),
('ru', 'meta_author', ''),
('ru', 'site_copyright', ''),
('uz', 'site_name', 'Portal'),
('uz', 'site_title', 'Portal'),
('uz', 'meta_keywords', 'Portal'),
('uz', 'meta_description', 'Portal'),
('uz', 'meta_email', ''),
('uz', 'meta_author', ''),
('uz', 'site_copyright', ''),
('az', 'site_name', ''),
('az', 'site_title', ''),
('az', 'meta_keywords', ''),
('az', 'meta_description', ''),
('az', 'meta_email', ''),
('az', 'meta_author', ''),
('az', 'site_copyright', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_products`
--

CREATE TABLE `s_products` (
  `sid` int(11) UNSIGNED NOT NULL,
  `category` int(11) NOT NULL DEFAULT 0,
  `amount` float NOT NULL DEFAULT 0,
  `discount` float NOT NULL DEFAULT 0,
  `img` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `position` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_products`
--

INSERT INTO `s_products` (`sid`, `category`, `amount`, `discount`, `img`, `position`, `active`) VALUES
(2, 301, 100, 0, 344, 1, 1),
(3, 302, 200, 0, 1, 2, 1),
(4, 301, 100, 0, 311, 3, 1),
(5, 305, 50, 0, 307, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_productslocalizations`
--

CREATE TABLE `s_productslocalizations` (
  `sid` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `address` varchar(1028) CHARACTER SET utf32 NOT NULL,
  `slug` varchar(512) CHARACTER SET utf32 NOT NULL,
  `more_txt` text DEFAULT NULL,
  `text` text NOT NULL,
  `url` varchar(512) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_productslocalizations`
--

INSERT INTO `s_productslocalizations` (`sid`, `lang`, `name`, `address`, `slug`, `more_txt`, `text`, `url`) VALUES
(2, 'ru', 'test salat 1', '', 'test-salat-1', 'test salat 1', '<p>desc</p>', ''),
(2, 'az', 'test salat 1', '', 'test-salat-1', 'test salat 1', '<p>test salat 1 desc</p>', ''),
(2, 'en', 'test salat 1', '', 'test-salat-1', 'test salat 1', '<p>test salat 1 desc</p>', ''),
(3, 'en', 'test', '', 'test', '', '', ''),
(3, 'az', 'test', '', 'test', '', '', ''),
(3, 'ru', 'test', '', 'test', '', '', ''),
(4, 'en', 'test salat2', '', 'test-salat2', '', '', ''),
(4, 'az', 'test salat2', '', 'test-salat2', '', '', ''),
(4, 'ru', 'test salat2', '', 'test-salat2', '', '', ''),
(5, 'ru', 'desert 1', '', 'desert-1', '', '', ''),
(5, 'az', 'desert 1', '', 'desert-1', '', '', ''),
(5, 'en', 'desert 1', '', 'desert-1', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_products_category`
--

CREATE TABLE `s_products_category` (
  `mid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL DEFAULT 0,
  `htaccessname` varchar(255) NOT NULL DEFAULT '',
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `position` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_products_category`
--

INSERT INTO `s_products_category` (`mid`, `parentid`, `htaccessname`, `visible`, `position`) VALUES
(294, 0, 'cat', 1, 1),
(298, 294, '', 1, 1),
(299, 294, '', 1, 2),
(300, 298, '', 1, 1),
(301, 298, '', 1, 2),
(302, 299, '', 1, 1),
(303, 300, '', 1, 1),
(304, 300, '', 1, 2),
(305, 298, '', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `s_products_categorylocalizations`
--

CREATE TABLE `s_products_categorylocalizations` (
  `mid` int(11) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `header` varchar(255) NOT NULL DEFAULT '',
  `comment` longtext NOT NULL,
  `text` longtext NOT NULL,
  `link` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_products_categorylocalizations`
--

INSERT INTO `s_products_categorylocalizations` (`mid`, `lang`, `name`, `header`, `comment`, `text`, `link`, `slug`) VALUES
(294, 'en', 'cat', '', '', '', 'http://', 'cat'),
(294, 'az', 'cat', '', '', '', 'http://', 'cat'),
(294, 'ru', 'cat', '', '', '', 'http://', 'cat'),
(299, 'ru', 'İçkilər', '', '', '', '', 'test-cat2'),
(299, 'az', 'İçkilər', '', '', '', '', 'test-cat2'),
(298, 'ru', 'Yeməklər', '', '', '', '', 'test-cat1'),
(298, 'az', 'Yeməklər', '', '', '', '', 'test-cat1'),
(298, 'en', 'Yeməklər', '', '', '', '', 'test-cat1'),
(299, 'en', 'İçkilər', '', '', '', '', 'test-cat2'),
(300, 'ru', 'Sushi rols', '', '', '', '', 'sushi-new-york'),
(300, 'az', 'Sushi rols', '', '', '', '', 'sushi-new-york'),
(300, 'en', 'Sushi rols', '', '', '', '', 'sushi-new-york'),
(301, 'ru', 'Salatlar', '', '', '', '', 'sushi-new-york2'),
(301, 'az', 'Salatlar', '', '', '', '', 'sushi-new-york2'),
(301, 'en', 'Salatlar', '', '', '', '', 'sushi-new-york2'),
(302, 'ru', 'aaaaaaaaa', '', '', '', '', 'aaaaaaaaa'),
(302, 'az', 'aaaaaaaaa', '', '', '', '', 'aaaaaaaaa'),
(302, 'en', 'aaaaaaaaa', '', '', '', '', 'aaaaaaaaa'),
(303, 'en', 'Sushi rols 1', '', '', '', '', 'sushi-rols-1'),
(303, 'az', 'Sushi rols 1', '', '', '', '', 'sushi-rols-1'),
(303, 'ru', 'Sushi rols 1', '', '', '', '', 'sushi-rols-1'),
(304, 'en', 'Sushi Hot', '', '', '', '', 'sushi-hot'),
(304, 'az', 'Sushi Hot', '', '', '', '', 'sushi-hot'),
(304, 'ru', 'Sushi Hot', '', '', '', '', 'sushi-hot'),
(305, 'en', 'Desertlər', '', '', '', '', 'desertler'),
(305, 'az', 'Desertlər', '', '', '', '', 'desertler'),
(305, 'ru', 'Desertlər', '', '', '', '', 'desertler');

-- --------------------------------------------------------

--
-- Table structure for table `s_search`
--

CREATE TABLE `s_search` (
  `name` varchar(255) DEFAULT NULL,
  `text` longtext DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `lang` varchar(5) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `catid` bigint(20) DEFAULT NULL,
  `pg` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_slider`
--

CREATE TABLE `s_slider` (
  `sid` int(11) UNSIGNED NOT NULL,
  `slide_img` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `position` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_slider`
--

INSERT INTO `s_slider` (`sid`, `slide_img`, `position`, `active`) VALUES
(24, 256, 2, 1),
(23, 255, 1, 1),
(25, 315, 3, 1),
(26, 343, 4, 1),
(27, 344, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_sliderlocalizations`
--

CREATE TABLE `s_sliderlocalizations` (
  `sid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `more_txt` text DEFAULT NULL,
  `text` text NOT NULL,
  `embed_code` text NOT NULL,
  `url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_sliderlocalizations`
--

INSERT INTO `s_sliderlocalizations` (`sid`, `lang`, `name`, `more_txt`, `text`, `embed_code`, `url`) VALUES
(24, 'es', 'PASAR SUS VACACIONES SUEÑO!', 'Aprende más', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://mcms.ws'),
(24, 'en', 'SPEND YOUR DREAM HOLIDAY!', 'Learn More', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://mcms.ws'),
(23, 'es', '¡LA BIENVENIDA A NOSOTROS!', 'Aprende más', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://www.microphp.com/es/'),
(23, 'en', 'WELCOME TO US!', 'Learn More', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima quo. Aenean feugiat in ante et blandit. Vestibulum posuere molestie risus, ac interdum magna porta non.</p>', '', 'http://www.microphp.com'),
(25, 'en', 'mCMS', 'more', '<p>Multilingual Content Management System</p>', '', 'https://codecanyon.net/item/mcms-multilingual-content-management-system/18293619'),
(25, 'es', 'mCMS', 'more', '<p>Multilingual Content Management System</p>', '', 'https://codecanyon.net/item/mcms-multilingual-content-management-system/18293619'),
(26, 'ru', 'banner1', 'banner1', '<p>banner1</p>', '', ''),
(26, 'uz', 'banner1', 'banner1', '', '', ''),
(27, 'ru', 'banner2', 'banner2', '', '', ''),
(27, 'uz', 'banner2', 'banner2', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe`
--

CREATE TABLE `s_subscribe` (
  `sid` int(11) NOT NULL,
  `add_date` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL DEFAULT '',
  `lang` varchar(5) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe`
--

INSERT INTO `s_subscribe` (`sid`, `add_date`, `name`, `email`, `phone`, `lang`, `active`) VALUES
(9, 1475587175, 'Rashad', 'rashad@aliev.info', '6482737', 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcast`
--

CREATE TABLE `s_subscribe_broadcast` (
  `bid` int(11) NOT NULL,
  `tid` int(11) NOT NULL DEFAULT 0,
  `startdate` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `finished` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcast`
--

INSERT INTO `s_subscribe_broadcast` (`bid`, `tid`, `startdate`, `active`, `finished`) VALUES
(11, 5, 1477162800, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcastlocalizations`
--

CREATE TABLE `s_subscribe_broadcastlocalizations` (
  `bid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `text` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcastlocalizations`
--

INSERT INTO `s_subscribe_broadcastlocalizations` (`bid`, `lang`, `text`) VALUES
(11, 'en', 'blabla'),
(11, 'es', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_broadcast_mails`
--

CREATE TABLE `s_subscribe_broadcast_mails` (
  `bid` int(11) NOT NULL DEFAULT 0,
  `sid` int(11) NOT NULL DEFAULT 0,
  `sendtime` int(11) NOT NULL DEFAULT 0,
  `sended` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_broadcast_mails`
--

INSERT INTO `s_subscribe_broadcast_mails` (`bid`, `sid`, `sendtime`, `sended`) VALUES
(11, 9, 1499495914, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_templates`
--

CREATE TABLE `s_subscribe_templates` (
  `tid` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_templates`
--

INSERT INTO `s_subscribe_templates` (`tid`, `active`) VALUES
(5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_subscribe_templateslocalizations`
--

CREATE TABLE `s_subscribe_templateslocalizations` (
  `tid` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(5) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `template` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_subscribe_templateslocalizations`
--

INSERT INTO `s_subscribe_templateslocalizations` (`tid`, `lang`, `name`, `template`) VALUES
(5, 'ru', 'test mail - ru', 'Test mail template in Russian\r\n[TEXT]'),
(5, 'en', 'test mail - en', 'Test mail template in English\r\n[TEXT]'),
(5, 'az', 'test mail - az', 'Test mail template in Azeri\r\n[TEXT]'),
(5, 'es', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_users`
--

CREATE TABLE `s_users` (
  `userid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `usertype` int(2) NOT NULL DEFAULT 3,
  `password` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `updated` int(42) NOT NULL DEFAULT 0,
  `permissions` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_users`
--

INSERT INTO `s_users` (`userid`, `username`, `usertype`, `password`, `name`, `surname`, `email`, `phone`, `mobile`, `updated`, `permissions`, `active`) VALUES
(1, 'admin', 1, '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'MicroPHP', '', 'info@microphp.com', '', '', 1529310435, '0', 1),
(9, 'test', 3, '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'test', '', 'test@test.az', '', '', 1501233815, ';5::01001;9::00101;999999::11111;10::00000;50::00011;7::00001;100::00001;101::01011;550::00111;', 1),
(10, 'user1', 2, '0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90', 'user1', '', 'user1@user1.az', '', '', 1530071121, ';5::00000;9::00111;999999::00111;10::00111;50::00111;7::00111;100::00111;101::00111;550::00111;', 1),
(11, 'user2', 3, '6025d18fe48abd45168528f18a82e265dd98d421a7084aa09f61b341703901a3', 'user2', '', 'user2@user2.az', '', '', 1463604738, ';5::00101;9::00101;999999::00101;10::00101;50::00101;7::00101;100::00101;101::00101;550::00101;1000030::00101;', 1),
(12, 'user3', 3, '5860faf02b6bc6222ba5aca523560f0e364ccd8b67bee486fe8bf7c01d492ccb', 'user3', '', 'user3@user3.az', '', '', 1463604756, ';5::00011;9::00011;999999::11111;10::00011;50::00011;7::00011;100::00011;101::00011;550::00011;1000054::00000;999998::00000;', 1),
(13, 'user4', 3, '5269ef980de47819ba3d14340f4665262c41e933dc92c1a27dd5d01b047ac80e', 'user4', '', 'user4@user4.com', '', '', 1530071174, ';5::00001;9::00001;999999::00001;10::00001;50::00001;7::00001;100::00001;101::00001;550::00001;1000030::00001;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `s_user_follows`
--

CREATE TABLE `s_user_follows` (
  `from_member_id` int(11) NOT NULL,
  `to_member_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `add_date` varchar(128) NOT NULL,
  `modify_date` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_blocks`
--
ALTER TABLE `s_blocks`
  ADD PRIMARY KEY (`blocksid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_blockscategories`
--
ALTER TABLE `s_blockscategories`
  ADD PRIMARY KEY (`catid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_blockscategorylocalizations`
--
ALTER TABLE `s_blockscategorylocalizations`
  ADD UNIQUE KEY `catid_lang` (`catid`,`lang`) USING BTREE,
  ADD KEY `catid` (`catid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_blockslocalizations`
--
ALTER TABLE `s_blockslocalizations`
  ADD UNIQUE KEY `blocksid_lang` (`blocksid`,`lang`) USING BTREE,
  ADD KEY `blocksid` (`blocksid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_feedback`
--
ALTER TABLE `s_feedback`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `s_files`
--
ALTER TABLE `s_files`
  ADD PRIMARY KEY (`fileid`),
  ADD KEY `category` (`category`),
  ADD KEY `extension` (`extension`);

--
-- Indexes for table `s_files_image_sizes`
--
ALTER TABLE `s_files_image_sizes`
  ADD KEY `imgid` (`imgid`),
  ADD KEY `size` (`size`);

--
-- Indexes for table `s_gallery`
--
ALTER TABLE `s_gallery`
  ADD PRIMARY KEY (`gid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_gallerylocalizations`
--
ALTER TABLE `s_gallerylocalizations`
  ADD UNIQUE KEY `gid_lang` (`gid`,`lang`) USING BTREE,
  ADD KEY `gid` (`gid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_gallery_images`
--
ALTER TABLE `s_gallery_images`
  ADD PRIMARY KEY (`imgid`),
  ADD KEY `gid` (`gid`);

--
-- Indexes for table `s_logs`
--
ALTER TABLE `s_logs`
  ADD PRIMARY KEY (`logsid`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `s_media_likes`
--
ALTER TABLE `s_media_likes`
  ADD PRIMARY KEY (`media_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `s_members`
--
ALTER TABLE `s_members`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `m_mail` (`m_mail`);

--
-- Indexes for table `s_menu`
--
ALTER TABLE `s_menu`
  ADD PRIMARY KEY (`mid`),
  ADD KEY `parentid` (`parentid`),
  ADD KEY `indexed` (`indexed`),
  ADD KEY `visible` (`visible`),
  ADD KEY `newwindow` (`newwindow`),
  ADD KEY `template` (`template`),
  ADD KEY `menutype` (`menutype`);

--
-- Indexes for table `s_menulocalizations`
--
ALTER TABLE `s_menulocalizations`
  ADD UNIQUE KEY `mid_lang` (`mid`,`lang`) USING BTREE,
  ADD KEY `lang` (`lang`),
  ADD KEY `mid` (`mid`);

--
-- Indexes for table `s_message_sends`
--
ALTER TABLE `s_message_sends`
  ADD PRIMARY KEY (`msgid`),
  ADD KEY `sended` (`sended`);

--
-- Indexes for table `s_modulelocalizations`
--
ALTER TABLE `s_modulelocalizations`
  ADD UNIQUE KEY `moduleid_lang` (`moduleid`,`lang`) USING BTREE,
  ADD KEY `moduleid` (`moduleid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_modules`
--
ALTER TABLE `s_modules`
  ADD PRIMARY KEY (`moduleid`),
  ADD KEY `have_view` (`have_view`),
  ADD KEY `have_cnfg` (`have_cnfg`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_news`
--
ALTER TABLE `s_news`
  ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `s_newscategories`
--
ALTER TABLE `s_newscategories`
  ADD PRIMARY KEY (`catid`),
  ADD KEY `active` (`active`),
  ADD KEY `index` (`index`),
  ADD KEY `event` (`event`);

--
-- Indexes for table `s_newscategorylocalizations`
--
ALTER TABLE `s_newscategorylocalizations`
  ADD UNIQUE KEY `catid_lang` (`catid`,`lang`) USING BTREE,
  ADD KEY `catid` (`catid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_news_read`
--
ALTER TABLE `s_news_read`
  ADD KEY `newsid` (`newsid`);

--
-- Indexes for table `s_notifications`
--
ALTER TABLE `s_notifications`
  ADD PRIMARY KEY (`nid`);

--
-- Indexes for table `s_options`
--
ALTER TABLE `s_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `opt_key` (`opt_key`);

--
-- Indexes for table `s_optionslocalizations`
--
ALTER TABLE `s_optionslocalizations`
  ADD KEY `lang` (`lang`),
  ADD KEY `opt_key` (`opt_key`);

--
-- Indexes for table `s_products`
--
ALTER TABLE `s_products`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `s_products_category`
--
ALTER TABLE `s_products_category`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `s_slider`
--
ALTER TABLE `s_slider`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_sliderlocalizations`
--
ALTER TABLE `s_sliderlocalizations`
  ADD UNIQUE KEY `sid_lang` (`sid`,`lang`) USING BTREE,
  ADD KEY `sid` (`sid`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `s_subscribe`
--
ALTER TABLE `s_subscribe`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `s_subscribe_broadcast`
--
ALTER TABLE `s_subscribe_broadcast`
  ADD PRIMARY KEY (`bid`);

--
-- Indexes for table `s_subscribe_broadcastlocalizations`
--
ALTER TABLE `s_subscribe_broadcastlocalizations`
  ADD UNIQUE KEY `bid_lang` (`bid`,`lang`) USING BTREE,
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `s_subscribe_templates`
--
ALTER TABLE `s_subscribe_templates`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `s_subscribe_templateslocalizations`
--
ALTER TABLE `s_subscribe_templateslocalizations`
  ADD UNIQUE KEY `tid_lang` (`tid`,`lang`) USING BTREE,
  ADD KEY `tid` (`tid`) USING BTREE;

--
-- Indexes for table `s_users`
--
ALTER TABLE `s_users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `usertype` (`usertype`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `s_user_follows`
--
ALTER TABLE `s_user_follows`
  ADD UNIQUE KEY `from_member_id` (`from_member_id`,`to_member_id`),
  ADD UNIQUE KEY `from_member_id_2` (`from_member_id`,`to_member_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_blocks`
--
ALTER TABLE `s_blocks`
  MODIFY `blocksid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `s_blockscategories`
--
ALTER TABLE `s_blockscategories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `s_feedback`
--
ALTER TABLE `s_feedback`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_files`
--
ALTER TABLE `s_files`
  MODIFY `fileid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=346;

--
-- AUTO_INCREMENT for table `s_gallery`
--
ALTER TABLE `s_gallery`
  MODIFY `gid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `s_gallery_images`
--
ALTER TABLE `s_gallery_images`
  MODIFY `imgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=566;

--
-- AUTO_INCREMENT for table `s_logs`
--
ALTER TABLE `s_logs`
  MODIFY `logsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1432;

--
-- AUTO_INCREMENT for table `s_members`
--
ALTER TABLE `s_members`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_menu`
--
ALTER TABLE `s_menu`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=581;

--
-- AUTO_INCREMENT for table `s_message_sends`
--
ALTER TABLE `s_message_sends`
  MODIFY `msgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_modules`
--
ALTER TABLE `s_modules`
  MODIFY `moduleid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000077;

--
-- AUTO_INCREMENT for table `s_news`
--
ALTER TABLE `s_news`
  MODIFY `newsid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `s_newscategories`
--
ALTER TABLE `s_newscategories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `s_notifications`
--
ALTER TABLE `s_notifications`
  MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `s_options`
--
ALTER TABLE `s_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `s_products`
--
ALTER TABLE `s_products`
  MODIFY `sid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_products_category`
--
ALTER TABLE `s_products_category`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=306;

--
-- AUTO_INCREMENT for table `s_slider`
--
ALTER TABLE `s_slider`
  MODIFY `sid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `s_subscribe`
--
ALTER TABLE `s_subscribe`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `s_subscribe_broadcast`
--
ALTER TABLE `s_subscribe_broadcast`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `s_subscribe_templates`
--
ALTER TABLE `s_subscribe_templates`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_users`
--
ALTER TABLE `s_users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
