<?php
/******************* index.php *******************
 *
 * Index file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** index.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class indexPage extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    /**
     * Build page
     */
    private function buildPage()
    {
        /**
         * Build main settins for all pages
         */
        $this->buildMain();

        /**
         * Build titles for Meta title, SEO, languages
         */
        $this->buildTitle();

        /**
         * Build Menu from section manager for all pages
         */
        $this->buildMenu();
    }

    /**
     * Build main settins for all pages
     */
    private function buildMain()
    {
        $toYear = '&mdash;' . date('Y');
        $this->template->assign_vars(array(
            'TITLE' => $this->getKeyLang('site_name', $this->lang, ''),
            'TITLE_UP' => $this->nameup($this->getKeyLang('site_name', $this->lang, '')),
            'SLOGAN' => $this->getKeyLang('site_title', $this->lang, ''),
            'YEAR' => $toYear,
            'CURR_YEAR' => date('Y'),
            'COPYRIGHT' => '&copy; ' . $this->getKeyLang('meta_author', $this->lang, ''),
            'COPYRIGHT_INFO' => $this->fromLangIndex('copyright_info'),
            'SITE_COPYRIGHT' => str_replace('[YEAR]', date('Y'), $this->getKeyLang('site_copyright', $this->lang, '[YEAR]')),
            'CHARSET' => $this->getKey('charset', 'utf-8'),
            'AUTHOR' => $this->getKeyLang('meta_author', $this->lang, ''),
            'KEYWORDS' => $this->getKeyLang('meta_keywords', $this->lang, ''),
            'DESCRIPTION' => $this->getKeyLang('meta_description', $this->lang, ''),
            'META_EMAIL' => $this->getKeyLang('meta_email', $this->lang, ''),
            'LANGUAGE' => $this->fromLangIndex('language'),
            'LANG_QS' => $this->lang_qs,
            'MODULE_QS' => $this->module_qs,
            'CURR_LANG' => $this->lang,
            'CURR_LANG_UP' => $this->nameup($this->lang),
            'BODY_CLASS' => ($this->module != 'main') ? ' class="grey"' : ' class="home"',
        ));
    }
}

/**
 * Assign page class
 */
$index = new indexPage();
include $index->lg_folder . '/index.lang.php';

/**
 * if from from admin Site Options checked "Parallax(One page) site" show page as parallax
 */
if ($index->parallax == 1) {
// If parallax

    switch ($index->module) {
        default: {
            require_once $index->lg_folder . '/' . $index->module . '.lang.php';
            $index->onLoad();
            $index->insertModule();
            break;
        }
    }

// E: If parallax
} else {
// B: If normal site
    switch ($index->module) {
        case '404': {
            // 404 page
            $index->onLoad();
            $index->insertModule();
            break;
        }
        case 'search': {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $steiken = $index->utils->UserPost('steiken');
                $searchtoken = $index->utils->dataFullFilter(@$_SESSION['stoken']);
                $stoken = (array_key_exists('stoken', $_SESSION)) ? $index->utils->dataFullFilter($_SESSION['stoken']) : '';
                if ((strlen($steiken) > 5) && ($steiken == $searchtoken)) {
                    require_once $index->lg_folder . '/' . $index->module . '.lang.php';
                    $index->onLoad();
                    $index->template->pparse('index.header');
                    $index->insertModule();
                    $index->template->pparse('index.footer');
                } else {
                    //echo "222";
                    @header('location: ' . $index->curr_folder . str_replace('[lang]', $index->lang, $index->permalinks[$index->perma_type]['index'][$index->curr_lang]));
                    $index->utils->Redirect($index->curr_folder . str_replace('[lang]', $index->lang, $index->permalinks[$index->perma_type]['index'][$index->curr_lang]));
                    exit();
                }
            } else {
                @header('location: ' . $index->curr_folder . str_replace('[lang]', $index->lang, $index->permalinks[$index->perma_type]['index'][$index->curr_lang]));
                $index->utils->Redirect($index->curr_folder . str_replace('[lang]', $index->lang, $index->permalinks[$index->perma_type]['index'][$index->curr_lang]));
                exit();
            }
            break;
        }

        default: {
            require_once $index->lg_folder . '/' . $index->module . '.lang.php';
            $index->onLoad();
            $index->template->pparse('index.header');
            $index->insertModule();
            $index->template->pparse('index.footer');
            break;
        }
    }
// E: If normal site
}

/******************* index.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** index.php ******************/;
