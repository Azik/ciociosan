<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{_TITLE_404_}</title>

    <!-- Bootstrap core CSS -->
    <link href="{SITE_FOLDER}bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="{SITE_FOLDER}bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{SITE_FOLDER}bootstrap/doc/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{SITE_FOLDER}theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="./bootstrap/doc/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <!-- BEGIN blocks_35 -->
          <a class="navbar-brand" href="{INDEX_URL}">{blocks_35.NAME}</a>
		  <!-- END blocks_35 -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<!-- BEGIN top_menu -->
            <li 
			<!-- BEGIN curr_class -->
			class="active"
			<!-- END curr_class -->
			 <!-- BEGIN sub -->
			 class="dropdown"
			 <!-- END sub -->
			 >
				<a href="{top_menu.URL}"
				<!-- BEGIN sub -->
				class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
				<!-- END sub -->
				>{top_menu.NAME}
				<!-- BEGIN sub -->
				<span class="caret"></span>
				<!-- END sub -->
				</a>
				<!-- BEGIN sub -->
				<ul class="dropdown-menu">
					<!-- BEGIN menu -->
					<li><a href="{top_menu.sub.menu.URL}">{top_menu.sub.menu.NAME}</a></li>
					<!-- END menu -->
				</ul>
				<!-- END sub -->
			</li>
			<!-- END top_menu -->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container theme-showcase" role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>{_TITLE_404_}</h1>
        <p>{_TITLE_404_INFO_}</p>
      </div>

    </div> <!-- /container -->

	<div id="page-content">
		<br/><br/>
		<div class="container">
			<div class="row">
				<!-- BEGIN blocks_14 -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{blocks_14.NAME}</h3>
					</div>
					<div class="panel-body">
						<p>{blocks_14.INFO_NOTAG}</p>
					</div>
				</div>
				</div>
				<!-- END blocks_14 -->
				<!-- BEGIN blocks_21 -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{blocks_21.NAME}</h3>
					</div>
					<div class="panel-body">
						<ul>
							<!-- BEGIN items -->
							<li><a {blocks_21.items.HREF}><i class="{blocks_21.items.LINK_NAME}"></i> {blocks_21.items.NAME}</a></li>
							<!-- END items -->
						</ul>
					</div>
				</div>
				</div>
				<!-- END blocks_21 -->
				<!-- BEGIN foot_menu -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{foot_menu.NAME}</h3>
					</div>
					<!-- BEGIN sub -->
					<div class="panel-body">
						<ul>
							<!-- BEGIN menu -->
							<li><a href="{foot_menu.sub.menu.URL}"><i class="{foot_menu.sub.menu.COMMENT}"></i> {foot_menu.sub.menu.NAME}</a></li>
							<!-- END menu -->
						</ul>
					</div>
					<!-- END sub -->
				</div>
				</div>
				<!-- END foot_menu -->
			</div>
		</div>
		<div class="container">
			<div class="row">
			<div class="col-sm-6">
				<p>{SITE_COPYRIGHT}</p>
			</div>
			<div class="col-sm-6 text-right">
			 <!-- BEGIN bottom_menu -->
			 <br/><a href="{bottom_menu.URL}">{bottom_menu.NAME}</a>
			 <!-- END bottom_menu -->
			</div>
			</div>
		</div>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{SITE_FOLDER}bootstrap/doc/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{SITE_FOLDER}bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>