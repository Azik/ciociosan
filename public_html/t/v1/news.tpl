<!-- Wrapper Begin -->
<section id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- SectionTitle Begin -->
				<div class="section_title">

					<!-- BEGIN where -->
					  <!-- BEGIN end -->
						<h3>{where.end.NAME}</h3>
					  <!-- END end -->
					<!-- END where -->
					
					<!-- BEGIN sort -->
					<div class="dropdown">
						<a href="#" role="button" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Sırala
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="filter">
						<!-- BEGIN list -->
						  <a class="dropdown-item" href="{sort.list.URL}" title="{sort.list.VALUE}">{sort.list.VALUE}</a>
						<!-- END list -->
						</div>
					  </div>
					  <!-- END sort -->
				</div>
				<!-- SectionTitle Begin -->
				<!-- News Begin -->

				<!-- BEGIN news -->
				
				<div class="news mt50">
					<div class="row">
					<!-- BEGIN items -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="news_box">
								<a href="{news.items.URL}" title="{news.items.NAME}">
									<div class="head">
										<figure>
											<img src="{news.items.IMG_380X280}" alt="{news.items.NAME}">
										</figure>
										<span class="btn_favorite"></span>
										<span class="time">{news.items.DURATION}</span>
										<div class="title">
											<h2>{news.items.NAME}</h2>
										</div>
									</div>
									<!-- BEGIN is_expired -->
									<div class="bottom">
										<span class="val">Gəl mərcə</span>
										<div class="count">
											<span class="txt">{news.items.is_expired.YES} Hə</span>
											<span class="icon"></span>
											<span class="txt">{news.items.is_expired.NO} Yox</span>
										</div>
									</div>
									<!-- END is_expired -->

									<!-- BEGIN is_not_expired -->
									<div class="bottom">
										<span class="val">Gəl mərcə</span>
										<div class="count">
											<span class="icon"></span>
											<span class="txt">{news.items.is_not_expired.TOTAL} nəfər iddia edir</span>
										</div>
									</div>
									<!-- END is_not_expired -->
								</a>
							</div>
						</div>
						<!-- END items -->

					</div>
				</div>
				
				


				<!-- News End -->
				<!-- Pagination Begin -->


				<!-- BEGIN pages -->
				<nav class="pag clearfix">
					<ul class="list-unstyled">
						<!-- <li class="prev"><a href="#" title="">&laquo;</a></li> -->
						<!-- BEGIN is_yes_prev_page -->
						<li><a href="{news.pages.is_yes_prev_page.PREV_PAGE}" title="">‹</a></li>
						<!-- END is_yes_prev_page -->

						<!-- BEGIN pg -->
						<!-- BEGIN is_curr -->
						<li class="active">{news.pages.pg.NUM}</li>
						<!-- END is_curr -->

						<!-- BEGIN is_not_curr -->
						<li><a href="{news.pages.pg.URL}" title="">{news.pages.pg.NUM}</a></li>
						<!-- END is_not_curr -->
						
						<!-- END pg -->
	
						<!-- BEGIN is_yes_next_page -->
						<li><a href="{news.pages.is_yes_next_page.NEXT_PAGE}" title="">›</a></li>
						<!-- END is_yes_next_page -->
						<!-- <li><a href="#" title="">&raquo;</a></li> -->
						
					</ul>
				</nav>
				<!-- Pagination End -->
				<!-- END pages -->
				<!-- END news -->

			</div>
		</div>
	</div>
</section>
<!-- Wrapper End -->
