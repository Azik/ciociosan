<!DOCTYPE html>
<html lang="az">
<head>
	<title>{TITLE_HEADER}{TITLE_SEPERATOR}{TITLE}{TITLE_INSIDE2}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS -->
	<link href="{SITE_FOLDER}fonts/fonts.css" rel="stylesheet">
	<link href="{SITE_FOLDER}css/bootstrap.min.css" rel="stylesheet">
	<link href="{SITE_FOLDER}css/libs.css" rel="stylesheet">
	<link href="{SITE_FOLDER}css/style.css" rel="stylesheet">
	<link href="{SITE_FOLDER}css/responsive.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="{SITE_FOLDER}js/html5shiv.min.js"></script>
		<script src="{SITE_FOLDER}js/respond.min.js"></script>
	<![endif]-->
</head>
<body>

