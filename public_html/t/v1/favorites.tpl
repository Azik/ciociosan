<!-- BEGIN where -->
<!-- Header Begin -->
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<div class="header_inner">
				<!-- BtnBack -->
				<button type="button" class="btn_back" onCLick="window.location.href='{INDEX_URL}'"></button>
				<!-- BEGIN end -->
				<span class="current_page">{where.end.NAME}</span>
				<!-- END end -->
			</div>
		</div>
	</div>
</header>
<!-- Header End -->
<!-- END where -->

<!-- Products Begin -->
<section class="products">
	<div class="container-fluid">
		<!-- BEGIN products -->
		<div class="row">
			<!-- BEGIN items -->
			<div class="box">
				<!-- BEGIN img -->
				<figure>
				<img src="{products.items.img.IMG_FILE}"/>
				</figure>
				<!-- END img -->
				<div class="body">
				<a href="{products.items.URL}">
					<h2 class="title">{products.items.NAME}</h2>
					<div class="text">
						<p>{products.items.TITLE}</p>
					</div>
					<div class="bottom">
						<span class="price">{products.items.AMOUNT}</span>
						 <button type="button" data-id="{products.items.ID}" class="btn-favorite {products.items.FAVORITES_CLASS}"></button>
					</div>
					</a>
				</div>
			</div>
			<!-- END items -->
			
			
		</div>
		<!-- END products -->
	</div>
</section>
<!-- Products End -->