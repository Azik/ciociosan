<!-- BEGIN where -->
<!-- Header Begin -->
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<div class="header_inner">
				<!-- BtnBack -->
				<button type="button" class="btn_back" onCLick="window.location.href='{INDEX_URL}'"></button>
				<!-- BEGIN end -->
				<span class="current_page">{where.end.NAME}</span>
				<!-- END end -->
			</div>
		</div>
	</div>
</header>
<!-- Header End -->
<!-- END where -->


				
			


<!-- ProductType Begin -->
<section class="product_type">
	<div class="container-fluid">
		<div class="row">
			<div class="inner">
				<!-- BEGIN product_cats -->
				<a href="{product_cats.URL}" title="{product_cats.NAME}"
				<!-- BEGIN curr_class -->
				class="active"
				<!-- END curr_class -->
				>{product_cats.NAME}</a>
				<!-- END product_cats -->
			</div>
		</div>
	</div>
</section>
<!-- ProductType End -->
<!-- Category Begin -->
<section class="category">
	<div class="container-fluid">
		<div class="row">
			<div class="head">
				<h4>Katigoriyalar</h4>
				<a href="{FAVORITES_URL}" title="" class="get_favorites">Favourites</a>
			</div>
		
			<div class="scr">
				<ul class="list-unstyled category_list">
					<!-- BEGIN sub_cats -->
							<!-- BEGIN not_exist_sub_items -->
							<li><a href="{sub_cats.URL}" title="{sub_cats.TITLE}">{sub_cats.NAME}</a></li>
							<!-- END not_exist_sub_items -->

							<!-- BEGIN exist_sub_items -->
							<li class="sub">
								<a href="#" title="{sub_cats.TITLE}">{sub_cats.NAME}</a>
								<ul class="list-unstyled">
									<!-- BEGIN items -->
									<li><a href="{sub_cats.exist_sub_items.items.URL}" title="{sub_cats.exist_sub_items.items.TITLE}">{sub_cats.exist_sub_items.items.NAME}</a></li>
									<!-- END items -->
								</ul>
							</li>
							<!-- END exist_sub_items -->

					<!-- END sub_cats -->
				</ul>
			</div>	
			
			
		</div>
	</div>
</section>
<!-- Category End -->
<!-- Detail Begin -->
<section class="detail">
	<div class="container-fluid">
		<div class="row">
			<div class="box">
				<figure>

				</figure>
				<h2 class="title">{TITLE}</h2>
				<div class="body">
					<div class="text">
						<p>{TEXT}</p>
					</div>
					<span class="price">{AMOUNT}</span>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Detail End -->
<!-- Products Begin -->
<!--
<section class="products">
	<div class="container-fluid">
		<div class="row">
			<h2 class="other_title">Oxşar Yeməklər</h2>
		</div>
		<div class="row">
			<div class="box">
				<figure>

				</figure>
				<div class="body">
					<h2 class="title">Tekka Maki</h2>
					<div class="text">
						<p>Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum </p>
					</div>
					<div class="bottom">
						<span class="price">12.00</span>
						 <button type="button" class="btn-favorite off"></button>
					</div>
				</div>
			</div>
			<div class="box">
				<figure>

				</figure>
				<div class="body">
					<h2 class="title">Tekka Maki</h2>
					<div class="text">
						<p>Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum </p>
					</div>
					<div class="bottom">
						<span class="price">12.00</span>
						 <button type="button" class="btn-favorite on"></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>  -->
<!-- Products End -->