$(function(){
    // Menu
    $(document).on('click', '.sub', function (e) {
        var t = $(this);
        if (t.hasClass("active")) {
            t.removeClass("active");
            t.stop(true, true).children("ul").slideUp();
        } else {
            t.addClass("active");
            t.stop(true, true).children("ul").slideDown();
        }
    })

    //MobileMenu
	$(document).on('click', '.btn_menu', function (e) {
		$("#menu").stop(true, true).addClass("show");
		$("body").stop(true, true).addClass("noscroll");
        $("#menubg").stop(true, true).show()
	})
    
	$("#menubg").click(function(){
		$("#menu").stop(true, true).removeClass("show");
		$("body").stop(true, true).removeClass("noscroll");
        $("#menubg").stop(true, true).hide()
	});

    $('#rate').barrating('show', {
		theme: 'fontawesome-stars-o'
	});

});