
    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->

      <div class="page-header">
        <h1>{NAME}</h1>
      </div>
      <div class="well">
        {TEXT}
		{MESSAGE}
		
		<!-- BEGIN subscribe_form -->
		<form action="{SUBSCRIBE_MODULE_FORM_URL}" method="post">
		  <div class="form-group">
			<label for="subscribe_name">{_MODULE_SUBSCRIBE_NAME_}:</label>
			<input id="subscribe_name" name="subscribe_name" type="text" class="form-control" placeholder="{_MODULE_SUBSCRIBE_NAME_}">
		  </div>
		  <div class="form-group">
			<label for="subscribe_email">{_MODULE_SUBSCRIBE_EMAIL_}:</label>
			<input id="subscribe_email" name="subscribe_email" type="email" class="form-control" placeholder="{_MODULE_SUBSCRIBE_EMAIL_}">
		  </div>
		  <div class="form-group">
			<label for="subscribe_phone">{_MODULE_SUBSCRIBE_PHONE_}:</label>
			<input id="subscribe_phone" name="subscribe_phone" type="text" class="form-control" placeholder="{_MODULE_SUBSCRIBE_PHONE_}">
		  </div>
		  <button type="submit" class="btn btn-default">{_MODULE_SUBSCRIBE_SEND_}</button>
		</form>
		<!-- END subscribe_form -->
		
		<!-- BEGIN sub -->
		<div><a href="{sub.URL}" {sub.BLANK}>{sub.NAME}</a></div>
		<!-- END sub -->
      </div>

    </div> <!-- /container -->

