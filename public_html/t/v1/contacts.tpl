<!-- BEGIN where -->
<!-- Header Begin -->
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<div class="header_inner">
				<!-- BtnBack -->
				<button type="button" class="btn_back" onCLick="window.location.href='{INDEX_URL}'"></button>
				<!-- BEGIN end -->
				<span class="current_page">{where.end.NAME}</span>
				<!-- END end -->
			</div>
		</div>
	</div>
</header>
<!-- Header End -->
<!-- END where -->

<!-- BEGIN contact_info -->

<!-- Feedback Begin -->
<section class="feedback">
	<div class="container-fluid">
		
		<form action="{CONTACTS_URL}" method="post">
		  <input type="hidden" name="teiken" value="{TOKEN}">

		<div class="row">
			<div class="inner">
				<span class="lbl">Please rate the dishes</span>
				<div class="rate_box">
					<select id="rate" name="contact_rate" data-current-rating="4.0" autocomplete="on" name="vote">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4" selected="selected">4</option>
						<option value="5">5</option>
					</select>
				</div>
				<input type="text" id="contact_phone" name="contact_phone" placeholder="Phone" class="ipt_style">
				<input type="text" id="contact_name" name="contact_name" placeholder="Name" class="ipt_style mb0">
				<input type="submit" name="" value="Send feedback">
			</div>
		</div>
		
		</FORM>
	</div>
</section>
<!-- Feedback End -->
<!-- END contact_info -->


<!-- BEGIN ok -->
<!-- Feedback Begin -->
<section class="feedback">
	<div class="container-fluid">
		<div class="row">
			<div class="inner">
				<div class="feedback_success">
					<i></i>
					<span class="first_title">Feedback sended</span>
					<span class="second_title">Thanks for helping us</span>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Feedback End -->
<!-- END ok -->


