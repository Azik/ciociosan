<!-- Wrapper Begin -->
<section id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- BEGIN where -->
				<!-- Breadcrumbs Begin -->
				<div class="breadcrumbs">
					<!-- BEGIN for -->
					<a href="{where.for.URL}" title="{where.for.NAME}">{where.for.NAME}</a>
					<span class="line">/</span>
					<!-- END for -->
					<!-- BEGIN end -->
					<span class="current">{where.end.NAME}</span>
					<!-- END end -->
				</div>
				<!-- Breadcrumbs End -->
				<!-- END end -->
				
				<!-- Profile Begin -->
				<div class="profile">
					<div class="row">
						<div class="col-md-4">
							<!-- ProfileMenu Begin -->
							<div class="profile_menu">
								<h3 class="user_name">{USERNAME}</h3>
								<!-- BEGIN menus -->
								<ul class="list-unstyled">
									<!-- BEGIN items -->
									<li class="{menus.items.ACTIVE}"><a href="{menus.items.URL}" title="{menus.items.TITLE}"><i class="ico {menus.items.ICON}"></i>{menus.items.TITLE}</a></li>
									<!-- END items -->
								</ul>
								<!-- END menus -->
							</div>
							<!-- ProfileMenu End -->
						</div>
						<div class="col-md-8">
							<!-- ProfileContent Begin -->
							<div class="profile_content">
								<!-- SectionTitle Begin -->
								<div class="section_title">
									<h3>Xoş gəldiniz, {USERNAME}. </h3>
								</div>
								<!-- SectionTitle Begin -->
								<!-- UserTotalBonus -->
								<span class="user_total_bonus">Sizin <span>{BAL}</span> Bonusunuz var.</span>
								<!-- CouponStatistics -->
								<div class="coupon_statistics">
									<div class="row">
										<div class="col-sm-4 col-xs-12">
											<div class="box">
												<span>250</span>
												Tutan kuponlar
											</div>
										</div>
										<div class="col-sm-4 col-xs-12">
											<div class="box">
												<span>250</span>
												Tutmayan kuponlar
											</div>
										</div>
										<div class="col-sm-4 col-xs-12">
											<div class="box">
												<span>250</span>
												Gözləyən kuponlar
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- ProfileContent End -->
						</div>
					</div>
				</div>
				<!-- Profile End -->
			</div>
		</div>
	</div>
</section>
<!-- Wrapper End -->