<!-- Menu Begin -->
<div id="menubg"></div>
<div id="menu">

	<!-- BEGIN langs -->
	<!-- Lang -->
	<div class="lang">
		<span class="title">Dil seçimi</span>

		<nav>
			<!-- BEGIN list -->
			<a href="{langs.list.URL}" title="{langs.list.NAME}" {langs.list.ACT_CLASS}>{langs.list.NAME}</a>
			<!-- END list -->
		</nav>
	</div>
	  <!-- END langs -->
	<!-- Social -->
	<nav class="social">
		<a href="#" title="" class="site">Website</a>
		<a href="#" title="" class="fb">Facebook</a>
		<a href="#" title="" class="instagram">İnstagram</a>
		<a href="#" title="" class="tripad">Tripadvisor</a>
	</nav>
	<!-- SendFeedback -->
	<a href="{SEND_FEEDBACK_URL}" title="Send Feedback" class="send_feedback">Send Feedback</a>
</div>
<!-- Menu End -->
<!-- Header Begin -->
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<div class="header_inner">
				<!-- Logo -->
				<div class="logo">
					<a href="{INDEX_URL}" title="">
						<img src="{SITE_FOLDER}images/logo.svg" alt="">
					</a>
				</div>
				<!-- BtnMenu -->
				<button type="button" class="btn_menu"></button>
			</div>
		</div>
	</div>
</header>
<!-- Header End -->



<!-- ProductType Begin -->
<section class="product_type">
	<div class="container-fluid">
		<div class="row">
			<div class="inner">
				<!-- BEGIN product_cats -->
				<a href="{product_cats.URL}" title="{product_cats.NAME}"
				<!-- BEGIN curr_class -->
				class="active"
				<!-- END curr_class -->
				>{product_cats.NAME}</a>
				<!-- END product_cats -->
			</div>
		</div>
	</div>
</section>
<!-- ProductType End -->
<!-- Category Begin -->
<section class="category">
	<div class="container-fluid">
		<div class="row">
			<div class="head">
				<h4>Katigoriyalar</h4>
				<a href="{FAVORITES_URL}" title="" class="get_favorites">Favourites</a>
			</div>
		
			<div class="scr">
				<ul class="list-unstyled category_list">
					<!-- BEGIN sub_cats -->
							<!-- BEGIN not_exist_sub_items -->
							<li><a href="{sub_cats.URL}" title="{sub_cats.TITLE}">{sub_cats.NAME}</a></li>
							<!-- END not_exist_sub_items -->

							<!-- BEGIN exist_sub_items -->
							<li class="sub">
								<a href="#" title="{sub_cats.TITLE}">{sub_cats.NAME}</a>
								<ul class="list-unstyled">
									<!-- BEGIN items -->
									<li><a href="{sub_cats.exist_sub_items.items.URL}" title="{sub_cats.exist_sub_items.items.TITLE}">{sub_cats.exist_sub_items.items.NAME}</a></li>
									<!-- END items -->
								</ul>
							</li>
							<!-- END exist_sub_items -->

					<!-- END sub_cats -->
				</ul>
			</div>	
			
			
		</div>
	</div>
</section>
<!-- Category End -->
<!-- Products Begin -->
<section class="products">
	<div class="container-fluid">
		<!-- BEGIN products -->
		<div class="row">
			<!-- BEGIN items -->
			<div class="box">
				<!-- BEGIN img -->
				<figure>
				<img src="{products.items.img.IMG_FILE}"/>
				</figure>
				<!-- END img -->
				<div class="body">
				<a href="{products.items.URL}">
					<h2 class="title">{products.items.NAME}</h2>
					<div class="text">
						<p>{products.items.TITLE}</p>
					</div>
					<div class="bottom">
						<span class="price">{products.items.AMOUNT}</span>
						 <button type="button" data-id="{products.items.ID}" class="btn-favorite {products.items.FAVORITES_CLASS}"></button>
					</div>
					</a>
				</div>
			</div>
			<!-- END items -->
			
			<!--<div class="box">
				<figure>

				</figure>
				<div class="body">
					<h2 class="title">Tekka Maki</h2>
					<div class="text">
						<p>Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum has been the industry's standard Lorem Ipsum </p>
					</div>
					<div class="bottom">
						<span class="price">12.00</span>
						 <button type="button" class="btn-favorite on"></button>
					</div>
				</div>
			</div> -->
		</div>
		<!-- END products -->
	</div>
</section>
<!-- Products End -->