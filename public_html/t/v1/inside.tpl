<!-- Wrapper Begin -->
<div id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- StaticPage Begin -->
				<div class="static_page">
					<h1 class="title">{NAME}</h1>
					<div class="text">
					{TEXT}
					</div>
				</div>
				<!-- StaticPage End -->
				
			</div>
		</div>
	</div>
</div>
<!-- Wrapper End -->

