<!-- Wrapper Begin -->
<section id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			
				<!-- BEGIN where -->
				<!-- Breadcrumbs Begin -->
				<div class="breadcrumbs">
					<!-- BEGIN for -->
					<a href="{where.for.URL}" title="{where.for.NAME}">{where.for.NAME}</a>
					<span class="line">/</span>
					<!-- END for -->
					<!-- BEGIN end -->
					<span class="current">{where.end.NAME}</span>
					<!-- END end -->
				</div>
				<!-- Breadcrumbs End -->
				<!-- END end -->
				<div class="row">
					<div class="col-md-9">
						<!-- NewsDetail Begin -->
						<div class="news_detail">
							<h1 class="title">{TITLE}</h1>
							<div class="news_info">
								<span><i class="date"></i>{DATE_DD} {DATE_MONTH_LANG} {DATE_YYYY}</span>
								<span><i class="view"></i>{TOTAL_VIEWS_COUNT}</span>
							</div>
							
							<!-- BEGIN news_img -->
							<!-- NewsPhoto -->
							<div class="news_photo">
								<figure>
									<img src="{news_img.IMG_400x300}" alt="{TITLE}">
								</figure>
							</div>
							<!-- END news_img -->
							
							<!-- OtherNews -->
							<!--
							<div class="other_news">
								<span class="txt">Əlaqəli xəbər: </span>
								<h2 class="title"><a href="#">{TITLE}</a></h2>
							</div>
							-->
							<!-- NewsText -->
							<div class="news_text">
								{TEXT}
							</div>
						</div>
						<!-- NewsDetail End -->
					</div>
					<!-- BEGIN is_not_expired -->
					<div class="col-md-3" id="iddia_list">

					</div>
					<!-- END is_not_expired -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Wrapper End -->

<!-- BEGIN is_not_expired -->
<script>
	$( document ).ready(function() {
		loadAllIddia();
	});


		function loadAllIddia(){
     
            let _self = $(this);
			debugger;
				
			$.ajax({
				'url' : '/ajax.coupon.php?lang={LANG}',
				'type' : 'POST',
				'data' : {
				  'action' : 'loadAllIddia',
				  'id' : {ID},
				},
				'dataType' : 'json',
				'success' : function(data) {
				debugger;
					$('#iddia_list').html(data.data);
					loadCoupon();
					/*if(data.iddia_count > 0){
						$( ".coupon_btn" ).removeClass('not_new_coupon');
						$( ".coupon_btn > span" ).remove();
						$( ".coupon_btn" ).append( '<span>' + data.iddia_count + '</span>' );
						debugger;
					}
					else {
						$( ".coupon_btn" ).removeClass('not_new_coupon').addClass('not_new_coupon');
						$( ".coupon_btn > span" ).remove();
					} */

					$( ".curr_bal" ).html( data.curr_bal + '<i></i>' );

				},
			});
            return false;
	}
	
	
	
</script>

<!-- END is_not_expired -->


			

<!-- BEGIN oxshar_xeberler -->

<!-- Wrapper Begin -->
<section id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<!-- SectionTitle Begin -->
				<div class="section_title">
					<h3>Oxşar xəbərlər</h3>
				</div>
				<!-- SectionTitle Begin -->
				<!-- News Begin -->
				<div class="news mt50">
					<div class="row">
						
					<!-- BEGIN items -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="news_box">
								<a href="{oxshar_xeberler.items.URL}" title="{oxshar_xeberler.items.NAME}">
									<div class="head">
										<figure>
											<img src="{oxshar_xeberler.items.IMG_380X280}" alt="{oxshar_xeberler.items.NAME}">
										</figure>
										<!--<span class="btn_favorite"></span>-->

										<!-- BEGIN approved -->
										<span class="time green">Tutdu</span>
										<!-- END approved -->

										<!-- BEGIN notapproved -->
										<span class="time red">Tutmadı</span>
										<!-- END notapproved -->

										<!-- BEGIN inprogress -->
										<span class="time orange">Gözləyir</span>
										<!-- END inprogress -->

										<!-- BEGIN duration_yes -->
										<span class="time">{oxshar_xeberler.items.DURATION}</span>
										<!-- END duration_yes -->
										<div class="title">
											<h2>{oxshar_xeberler.items.NAME}</h2>
										</div>
									</div>
									<!-- BEGIN is_expired -->
									<div class="bottom">
										<span class="val">Gəl mərcə</span>
										<div class="count">
											<span class="txt">{oxshar_xeberler.items.is_expired.YES} Hə</span>
											<span class="icon"></span>
											<span class="txt">{oxshar_xeberler.items.is_expired.NO} Yox</span>
										</div>
									</div>
									<!-- END is_expired -->

									<!-- BEGIN is_not_expired -->
									<div class="bottom">
										<span class="val">Gəl mərcə</span>
										<div class="count">
											<span class="icon"></span>
											<span class="txt">{oxshar_xeberler.items.is_not_expired.TOTAL} nəfər iddia edir</span>
										</div>
									</div>
									<!-- END is_not_expired -->
								</a>
							</div>
						</div>
						<!-- END items -->
						
					</div>
				</div>
				<!-- News End -->
			</div>
		</div>
	</div>
</section>
<!-- Wrapper End -->
<!-- END oxshar_xeberler -->

