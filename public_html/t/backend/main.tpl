
            <!-- BEGIN::CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN::CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN::PAGE HEADER-->
                    <!-- BEGIN::PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <span><a href="index.php?{MODULE_QS}=main">{DASHBOARD}</a></span>
                            </li>
                        </ul>
                    </div>
                    <!-- END::PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title">
						{TITLE}
                    </h3>
                    <!-- END PAGE TITLE-->
                    <!-- END::PAGE HEADER-->
                    <div class="portlet light bordered">
						<!-- BEGIN::PAGE TITLE-->
						<div class="portlet-title">
                            <div class="caption">
								<span class="caption-subject font-green-sharp sbold">{TITLE}</span>
								<small class="font-green-sharp">{TITLE_INFO}</small>
                            </div>
                        </div>
						<!-- END::PAGE TITLE-->
						<div class="tiles">
						<!-- BEGIN dashboard_stats -->
						
                            <!-- BEGIN tile -->
							<div class="tile bg-{dashboard_stats.COLOR_NAME} " onclick="document.location='{dashboard_stats.URL}'">
								<!-- BEGIN corner -->
								<div class="corner"> </div>
								<!-- END corner -->
								<div class="tile-body">
									<i class="{dashboard_stats.CLASS_FA}"></i>
								</div>
								<div class="tile-object">
									<div class="name"> {dashboard_stats.NAME} </div>
									<div class="number"> {dashboard_stats.NUMBER} </div>
								</div>
							</div>
							<!-- END tile -->
							
                            <!-- BEGIN tile2x -->
							<div class="tile double bg-{dashboard_stats.COLOR_NAME} " onclick="document.location='{dashboard_stats.URL}'">
								<div class="tile-body">
									<i class="{dashboard_stats.CLASS_FA}"></i>
								</div>
								<div class="tile-object">
									<div class="name"> {dashboard_stats.NAME} </div>
									<div class="number"> {dashboard_stats.NUMBER} </div>
								</div>
							</div>
							<!-- END tile2x -->
							
							<!-- BEGIN gallery -->
							<div class="tile double image " onclick="document.location='{dashboard_stats.URL}'">
								<div class="tile-body">
									<img src="{ROOT}img/gallery.jpg" alt="">
								</div>
								<div class="tile-object">
									<div class="name"> {dashboard_stats.NAME} </div>
									<!-- BEGIN number -->
									<div class="number"> {dashboard_stats.NUMBER} </div>
									<!-- END number -->
								</div>
							</div>
							<!-- END gallery -->
							
							<!-- BEGIN stat -->
							<div class="tile double bg-{dashboard_stats.COLOR_NAME} dashboard-stat2" onclick="document.location='{dashboard_stats.URL}'">
						        <div class="display">
                                    <div class="number">
                                        <h3 >
											<!-- BEGIN number -->
                                            <span data-counter="counterup" data-value="{dashboard_stats.NUMBER}">0</span>
											<!-- END number -->
											<!-- BEGIN txt -->
											<span>{dashboard_stats.NAME}</span>
											<!-- END txt -->
											<span>&nbsp;</span>
                                        </h3>
										<!-- BEGIN number -->
                                        <small><a href="{dashboard_stats.URL}">{dashboard_stats.NAME}</a></small>
										<!-- END number -->
										<small>&nbsp;</small>
                                    </div>
									<a href="{dashboard_stats.URL}">
                                    <div class="icon">
                                        <i class="{dashboard_stats.CLASS_FA}"></i>
                                    </div></a>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> <a href="{dashboard_stats.URL}">{dashboard_stats.NAME}</a> </div>
                                    </div>
                                </div>
                            </div>
							<!-- END stat -->
							
							<!-- BEGIN stat1x -->
							<div class="tile bg-{dashboard_stats.COLOR_NAME} dashboard-stat2" onclick="document.location='{dashboard_stats.URL}'">
						        <div class="display">
                                    <div class="number">
                                        <h3 >
											<!-- BEGIN number -->
                                            <span data-counter="counterup" data-value="{dashboard_stats.NUMBER}">0</span>
											<!-- END number -->
											<!-- BEGIN txt -->
											<span>{dashboard_stats.NAME}</span>
											<!-- END txt -->
											<span>&nbsp;</span>
                                        </h3>
										<!-- BEGIN number -->
                                        <small><a href="{dashboard_stats.URL}">{dashboard_stats.NAME}</a></small>
										<!-- END number -->
										<small>&nbsp;</small>
                                    </div>
									<a href="{dashboard_stats.URL}">
                                    <div class="icon">
                                        <i class="{dashboard_stats.CLASS_FA}"></i>
                                    </div></a>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> <a href="{dashboard_stats.URL}">{dashboard_stats.NAME}</a> </div>
                                    </div>
                                </div>
                            </div>
							<!-- END stat1x -->
                        <!-- END dashboard_stats -->
						</div>
					</div>
					<!-- BEGIN news_stats -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">		
                            <!-- BEGIN::INTERACTIVE CHART PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-bar-chart font-dark"></i>
                                        <span class="caption-subject font-dark sbold uppercase">{NEWS_STATISTICS}</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent btn-outline btn-circle btn-sm active">
												<a href="{NEWS_URL}" >{NEWS_TEXT}</a>
											</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chart_2" class="chart"> </div>
                                </div>
                            </div>
                            <!-- END::INTERACTIVE CHART PORTLET-->
						</div>
					</div>
					<!-- END news_stats -->
					<!-- BEGIN last_log -->
					<div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-blue"></i>
                                        <span class="caption-subject font-blue bold uppercase">{last_log.TITLE}</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                                        <ul class="feeds">
											<!-- BEGIN items -->
                                            <li>
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-info">
                                                                <i class="fa fa-check"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> 
																{last_log.items.MODE}
																<span class="label label-sm label-warning "> {last_log.items.USER} </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> {last_log.items.DATE} </div>
                                                </div>
                                            </li>
											<!-- END items -->
										</ul>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					<!-- END last_log -->
				</div>
                <!-- END::CONTENT BODY -->
            </div>
            <!-- END::CONTENT -->
        </div>
        <!-- END::CONTAINER -->
        
		<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		
		<!-- BEGIN news_charts -->
		<script src="{ROOT}assets/pages/scripts/charts-flotcharts.js" type="text/javascript"></script>
		<script type="text/javascript">
            //Interactive Chart
            function chart2() {
                if ($('#chart_2').size() != 1) {
                    return;
                }

                function randValue() {
                    return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
                }
                var newscount = [
					<!-- BEGIN newscount -->
					[{news_charts.newscount.DAY}, {news_charts.newscount.COUNT}],
					<!-- END newscount -->
                ];

                var plot = $.plot($("#chart_2"), [{
                    data: newscount,
                    label: " {NEWS_STATISTICS_COUNT}",
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0

                }], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: true,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#eee",
                        borderColor: "#eee",
                        borderWidth: 1
                    },
                    colors: ["#d12610", "#37b7f3", "#52e136"],
                    xaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#eee",
                    }
                });


                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x + 15,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        opacity: 0.80
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                $("#chart_2").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(0),
                                y = item.datapoint[1].toFixed(0);

                            showTooltip(item.pageX, item.pageY, item.series.label + ": " + y + " of " + x);
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }
		</script>
		<!-- END news_charts -->