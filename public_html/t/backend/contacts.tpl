
	<script>
		function click_cancel() {
			window.location.href = '?{MODULE_QS}=contacts';
		}
		function doAction(act, id, mess) {
			document.getElementById("action_id").value = id;
			document.getElementById("action").value = act;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("action_submit").click();
				}
			}
			else {
				document.getElementById("action_submit").click();
			}
		}

		function redirect(url) {
			window.location.href = url;
		}
	</script>

	
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=contacts">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN feedback -->
				<!-- BEGIN list -->

							<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
								<thead>
									<tr>
										<th style="width:0px !important;display:none;"></th>
										<th>Phone</th>
										<th>Name</th>
										<th>Rate</th>
										<th>Create date</th>
									</tr>
								</thead>
								<tbody>
				<!-- BEGIN items -->
									<tr>
										<td style="width:0px !important;display:none;"></td>
										<td>{feedback.list.items.PHONE}</td>
										<td>{feedback.list.items.NAME}</td>
										<td>{feedback.list.items.RATE}</td>
										<td>{feedback.list.items.CREATEDATE}</td>
									</tr>
				
				<!-- END items -->
								</tbody>
							</table>
				<!-- END list -->
				<!-- END feedback -->
				
			
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	