	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
		});
		
		function copyToClipboard(elem) {
			  // create hidden text element, if it doesn't already exist
			var targetId = "_hiddenCopyText_";
			var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
			var origSelectionStart, origSelectionEnd;
			if (isInput) {
				// can just use the original source element for the selection and copy
				target = elem;
				//alert(target);
				//origSelectionStart = elem.selectionStart;
				origSelectionStart = 0;
				//origSelectionEnd = elem.selectionEnd;
				origSelectionEnd = elem.value.length;
			} else {
				// must use a temporary form element for the selection and copy
				target = document.getElementById(targetId);
				if (!target) {
					var target = document.createElement("textarea");
					target.style.position = "absolute";
					target.style.left = "-9999px";
					target.style.top = "0";
					target.id = targetId;
					document.body.appendChild(target);
				}
				target.textContent = elem.textContent;
			}
			// select the content
			var currentFocus = document.activeElement;
			target.focus();
			target.setSelectionRange(0, target.value.length);
			
			// copy the selection
			var succeed;
			try {
				  succeed = document.execCommand("copy");
			} catch(e) {
				succeed = false;
			}
			//alert(succeed);
			
			return succeed;
		}
		
		function doBlockDelete(id) {
			if (confirm('{DELETE_CONFIRM}')) {
				document.getElementById("cat_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('cat_subm').click();
			}
		}
		function doCatAction(act, id, mess) {
			document.getElementById("cat_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("cat_subm").click();
				}
			}
			else {
				document.getElementById("cat_subm").click();
			}
		}

		function doBlocksAction(act, id, mess) {
			document.getElementById("blocks_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("blocks_subm").click();
				}
			}
			else {
				document.getElementById("blocks_subm").click();
			}
		}

		function redirect(url) {
			window.location.href = url;
		}

		function openID(id) {
			$("#" + id).dialog({
				modal: true,
				autoOpen: false,
				buttons: {
					Cancel: function () {
						$(this).dialog("close");
					}
				}
			});
			$("#" + id).dialog("open");
		}

		function clickCatAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=blocks&cateditid=' + act + ':' + fields;
		}

		<!-- BEGIN blocks -->
		<!-- BEGIN list -->
		function clickBlockAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=blocks&catid={blocks.list.CAT_ID}&blocksid=' + act + ':' + fields;
		}
		<!-- END list -->
		<!-- END blocks -->

		function click_cancel(act, id) {
			if (act == 'blocks') {
				window.location.href = '?{MODULE_QS}=blocks&catid=' + id;
			} else {
				window.location.href = '?{MODULE_QS}=blocks';
			}
		}
	</script>
	
	
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:600px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=blocks">{TITLE}</a></span>
						</li>
						<!-- BEGIN nav -->
						<li>
							<i class="fa fa-angle-right"></i>
							<span><a href="index.php?{MODULE_QS}=blocks{nav.URL}">{nav.TITLE}</a></span>
						</li>
						<!-- END nav -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN cat -->
				<div class="row">
					<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-eye"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
							</div>
						</div>
					<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
						<input type="hidden" id="cat_action" name="cat_action" value="">
						<input type="hidden" value="0" id="select_id" name="select_id">
						<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
						<!-- BEGIN list -->
						<div class="portlet-body">
							<!-- BEGIN perm_add -->
							<div class="clearfix">
								<div class="btn-group">
									<a href="{cat.list.ADD_URL}" class="btn green">
										{cat.list.ADD} &nbsp; <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<!-- END perm_add -->
							<br/>
							<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
								<thead>
									<tr>
										<th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                              data-set="#sample_1 .checkboxes"/></th>
										<th>{cat.list.NAME}</th>
										<th style="width : 80px !important;text-align : center;">{cat.list.ID}</th>
										<!-- BEGIN perm_edit -->
										<th style="width : 100px !important;text-align : center;">{cat.list.EDIT}</th>
										<!-- END perm_edit -->
										<!-- BEGIN perm_del -->
										<th style="width : 100px !important;text-align : center;">{cat.list.DELETE}</th>
										<!-- END perm_del -->
										<th style="width : 100px !important;"></th>
									</tr>
								</thead>
								<tbody>
								<!-- BEGIN items -->
								<tr data-position="{cat.list.items.POSITION_ID}" id="{cat.list.items.ID}">
									<td id="chk_{cat.list.items.ID}" style="width:8px;"><input type="checkbox" class="checkboxes"
																  value="{cat.list.items.ID}"/></td>
									<td><a href="{cat.list.URL}&catid={cat.list.items.ID}">{cat.list.items.NAME}</a></td>
									<td class="option text-center">
										<textarea class="input_copy" id="copy_{cat.list.items.ID}">&lt;!-- BEGIN blocks_{cat.list.items.ID} --&gt;
&lt;!-- END blocks_{cat.list.items.ID} --&gt;</textarea>
										<a onclick="copyToClipboard(document.getElementById('copy_{cat.list.items.ID}'));"> <img src="{ROOT}img/copy.png" alt="{cat.list.COPY_ID}" title="{cat.list.COPY_ID}"/> </a>
									</td>
									<!-- BEGIN perm_edit -->
									<td class="option"><a href="{cat.list.items.EDIT_URL}" class="btn yellow">
											{cat.list.EDIT}
											 &nbsp; <i class="fa fa-pencil"></i>
											</a></td>
									<!-- END perm_edit -->
									<!-- BEGIN perm_del -->
									<td class="option"><a href="JavaScript:doBlockDelete({cat.list.items.ID})"
														  title="{cat.list.DELETE}" class="btn red ask">
											{cat.list.DELETE}
											 &nbsp; <i class="fa fa-trash"></i>
											</a></td>
									<!-- END perm_del -->
									<td class="option">
										<!-- BEGIN active -->
										<a href="{cat.list.items.ACTIVE_URL}" class="btn btn-success">
										{cat.list.ACTIVE}
										&nbsp; <i class="fa fa-unlock"></i></a>
										<!-- END active -->
										<!-- BEGIN inactive -->
										<a href="{cat.list.items.INACTIVE_URL}"class="btn btn-danger">
										{cat.list.INACTIVE}
										&nbsp; <i class="fa fa-lock"></i></a>
										<!-- END inactive -->
									</td>
								</tr>
								<!-- END items -->
								</tbody>
							</table>
							<!-- BEGIN hidden -->
							<input type="hidden" name="sort_order" id="sort_order" value="{cat.list.hidden.VALUE}"/>
							<!-- END hidden -->
							<br/>
							<!-- BEGIN perm_edit -->
							<table>
								<tfoot>
								<tr>
									<td class="ac"><span class="label label-success"
														 onclick="clickCatAc('activate')">{cat.list.ACTIVATE}</span></td>
									<td style="width:10px;"></td>
									<td class="ac"><span class="label label-danger"
														 onclick="clickCatAc('inactivate')">{cat.list.INACTIVATE}</span></td>
									<td></td>
								</tr>
								</tfoot>
							</table>
							<!-- END perm_edit -->
						</div>
						<!-- END list -->
						
						<!-- BEGIN catedit -->
						<div class="portlet-body" id="panel_editbar">
							<div class="form-group form-md-checkboxes">
								<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
								<div class="col-md-6">
									<div class="md-checkbox-list">
										<div class="md-checkbox">
											<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {cat.catedit.ACTIVE_CHK} />
											<label for="txt_active">
												<span></span>
												<span class="check"></span>
												<span class="box"></span>{cat.catedit.ACTIVE}
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="tabbable-line boxless tabbable-reversed">
								<ul class="nav nav-tabs">
									<!-- BEGIN tab -->
									<li{cat.catedit.tab.CLASS}><a href="#content_{cat.catedit.tab.LANG}" data-toggle="tab">{cat.catedit.tab.LANG}</a></li>
									<!-- END tab -->
								</ul>
							</div>
							<div class="tab-content">
							<!-- BEGIN tab -->
							<div class="tab-pane {cat.catedit.tab.FADE_CLASS}" id="content_{cat.catedit.tab.LANG}">
								<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-language"></i>{cat.catedit.tab.LANG} 
									</div>
								</div>
								<div class="portlet-body form">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_name_{cat.catedit.tab.LANG}">{cat.catedit.tab.NAME}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_name_{cat.catedit.tab.LANG}"
											   id="txt_name_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.NAME_VALUE}"
											   placeholder="{cat.catedit.tab.NAME}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_more_txt_{cat.catedit.tab.LANG}">{cat.catedit.tab.MORE_TXT}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_more_txt_{cat.catedit.tab.LANG}"
											   id="txt_more_txt_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.MORE_TXT_VALUE}"
											   placeholder="{cat.catedit.tab.MORE_TXT}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
                                <div class="form-group form-md-line-input">
									<label class="col-md-3 control-label cursor-pointer" for="txt_imgid_{cat.catedit.tab.LANG}" onclick="openPhoto($('#txt_imgid_{cat.catedit.tab.LANG}').val(), 'txt_imgid_{cat.catedit.tab.LANG}')">{cat.catedit.tab.IMGID}</label>
									<div class="col-md-8">
										<input type="number" class="form-control" name="txt_imgid_{cat.catedit.tab.LANG}"
											   id="txt_imgid_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.IMGID_VALUE}"
											   placeholder="{cat.catedit.tab.IMGID}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="txt_info_{cat.catedit.tab.LANG}">{cat.catedit.tab.INFO}</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control ckeditor" name="txt_info_{cat.catedit.tab.LANG}"
                                                  id="txt_info_{cat.catedit.tab.LANG}" rows="3">{cat.catedit.tab.INFO_VALUE}</textarea>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="txt_embed_code_{cat.catedit.tab.LANG}">{cat.catedit.tab.EMBED_CODE}</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="txt_embed_code_{cat.catedit.tab.LANG}"
                                                  id="txt_embed_code_{cat.catedit.tab.LANG}" rows="3">{cat.catedit.tab.EMBED_CODE_VALUE}</textarea>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label" for="txt_url_{cat.catedit.tab.LANG}">{cat.catedit.tab.URL}</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_url_{cat.catedit.tab.LANG}"
											   id="txt_url_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.URL_VALUE}"
											   placeholder="{cat.catedit.tab.URL}">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="button" class="btn blue" id="btn_save" name="btn_save"
													onClick="doCatAction('save', {cat.catedit.ID},  '');"><i class="fa fa-ok"></i> {cat.catedit.SAVE}
											</button>
											<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
													onclick="click_cancel();">{cat.catedit.CANCEL}</button>
										</div>
									</div>
								</div>
								
								</div>
								</div>
							</div>
							<!-- END tab -->
							</div>
						</div>
						<!-- END catedit -->
					</form>
					</div>
					</div>
				</div>
				<!-- END cat -->
				
				
				
				
				<!-- BEGIN blocks -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::NEWS CAT-->
						<div class="portlet light bordered">
							<form action="" method="POST" class="form-horizontal" name="form_blocks" id="form_blocks">
								<input type="hidden" id="blocks_action" name="blocks_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="blocks_subm" name="blocks_subm" style="display:none;">
							
							<!-- BEGIN list -->
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-eye"></i>
									<span class="caption-subject bold uppercase">{TITLE} ({blocks.list.CAT})</span>
								</div>
							</div>
							<div class="portlet-body">
								<!-- BEGIN perm_add -->
								<div class="clearfix">
									<div class="btn-group">
										<a href="{blocks.list.ADD_URL}" class="btn green">
											{blocks.list.ADD} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
								<!-- END perm_add -->
								<br/>
								<table class="table table-striped table-bordered table-hover" width="100%" id="sample_1">
									<thead>
										<tr>
											<th style="width:0px !important;display:none;"></th>
											<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
											<th>{blocks.list.NAME}</th>
											<!-- BEGIN perm_edit -->
											<th style="width : 100px !important;text-align : center;">{blocks.list.EDIT}</th>
											<!-- END perm_edit -->
											<!-- BEGIN perm_del -->
											<th style="width : 100px !important;text-align : center;">{blocks.list.DELETE}</th>
											<!-- END perm_del -->
											<th style="width : 100px !important;"></th>
										</tr>
									</thead>
									<tbody>
										<!-- BEGIN items -->
										<tr data-position="{blocks.list.items.POSITION_ID}" id="{blocks.list.items.ID}">
											<td style="display:none;">{blocks.list.items.POSITION_ID}</td>
											<td style="width:12px !important;text-align:center;" id="chk_{blocks.list.items.ID}"><input type="checkbox" class="checkboxes" value="{blocks.list.items.ID}"/></td>
											<td>{blocks.list.items.NAME}</td>
											<!-- BEGIN perm_edit -->
											<td class="option">
												<a href="{blocks.list.items.EDIT_URL}" title="{blocks.list.EDIT}" class="btn yellow">
													{blocks.list.EDIT}
													 &nbsp; <i class="fa fa-pencil"></i>
												</a>
											</td>
											<!-- END perm_edit -->
											<!-- BEGIN perm_del -->
											<td class="option">
												<a href="JavaScript:doBlocksAction('delete', {blocks.list.items.ID}, '{blocks.list.DELETE_CONFIRM}')" title="{blocks.list.DELETE}" class="btn red ask"> 
													{blocks.list.DELETE}
													 &nbsp; <i class="fa fa-trash"></i>
												</a>
											</td>
											<!-- END perm_del -->
											<td class="option">
												<!-- BEGIN active -->
												<a href="{blocks.list.items.ACTIVE_URL}" class="btn btn-success">{blocks.list.ACTIVE}
													 &nbsp; <i class="fa fa-unlock"></i></a>
												<!-- END active -->
												<!-- BEGIN inactive -->
												<a href="{blocks.list.items.INACTIVE_URL}" class="btn btn-danger">{blocks.list.INACTIVE}
													 &nbsp; <i class="fa fa-lock"></i></a>
												<!-- END inactive -->
											</td>
										</tr>
										<!-- END items -->
									</tbody>
								</table>
								<!-- BEGIN hidden -->
								<input type="hidden" name="sort_order" id="sort_order" value="{blocks.list.hidden.VALUE}"/>
								<!-- END hidden -->
								<br/>
								<!-- BEGIN perm_edit -->
								<table>
									<tfoot>
									<tr>
										<td class="ac"><span class="label label-success"
															 onclick="clickBlockAc('activate')">{blocks.list.ACTIVATE}</span></td>
										<td style="width:10px;"></td>
										<td class="ac"><span class="label label-danger"
															 onclick="clickBlockAc('inactivate')">{blocks.list.INACTIVATE}</span></td>
										<td></td>
									</tr>
									</tfoot>
								</table>
								<!-- END perm_edit -->
							</div>
							<!-- END list -->
							
							<!-- BEGIN edit -->
							<div class="portlet-body" id="panel_editbar">
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {blocks.edit.ACTIVE_CHK} />
												<label for="txt_active">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{blocks.edit.ACTIVE}
												</label>
											</div>
										</div>
									</div>
								</div>							
								<div class="tabbable-line boxless tabbable-reversed">
									<ul class="nav nav-tabs">
										<!-- BEGIN tab -->
										<li{blocks.edit.tab.CLASS}><a href="#content_{blocks.edit.tab.LANG}" data-toggle="tab">{blocks.edit.tab.LANG}</a></li>
										<!-- END tab -->
									</ul>
								</div>
								
								<div class="tab-content">
									<!-- BEGIN tab -->
									<div class="tab-pane {blocks.edit.tab.FADE_CLASS}" id="content_{blocks.edit.tab.LANG}">
										<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-language"></i>{blocks.edit.tab.LANG} 
											</div>
										</div>
										<div class="portlet-body form">
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_name_{blocks.edit.tab.LANG}">{blocks.edit.tab.NAME}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_name_{blocks.edit.tab.LANG}"
													   id="txt_name_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.NAME_VALUE}"
													   placeholder="{blocks.edit.tab.NAME}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_link_name_{blocks.edit.tab.LANG}">{blocks.edit.tab.LINK_NAME}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_link_name_{blocks.edit.tab.LANG}"
													   id="txt_link_name_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.LINK_NAME_VALUE}"
													   placeholder="{blocks.edit.tab.LINK_NAME}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label cursor-pointer" for="txt_img_{blocks.edit.tab.LANG}" onclick="openPhoto($('#txt_img_{blocks.edit.tab.LANG}').val(), 'txt_img_{blocks.edit.tab.LANG}')">{blocks.edit.tab.IMG}</label>
											<div class="col-md-8">
												<input type="number" class="form-control" name="txt_img_{blocks.edit.tab.LANG}"
													   id="txt_img_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.IMG_VALUE}"
													   placeholder="{blocks.edit.tab.IMG}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label cursor-pointer" for="txt_img02_{blocks.edit.tab.LANG}" onclick="openPhoto($('#txt_img02_{blocks.edit.tab.LANG}').val(), 'txt_img02_{blocks.edit.tab.LANG}')">{blocks.edit.tab.IMG02}</label>
											<div class="col-md-8">
												<input type="number" class="form-control" name="txt_img02_{blocks.edit.tab.LANG}"
													   id="txt_img02_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.IMG02_VALUE}"
													   placeholder="{blocks.edit.tab.IMG02}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_img_url_{blocks.edit.tab.LANG}">{blocks.edit.tab.IMG_URL}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_img_url_{blocks.edit.tab.LANG}"
													   id="txt_img_url_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.IMG_URL_VALUE}"
													   placeholder="{blocks.edit.tab.IMG_URL}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_text_{blocks.edit.tab.LANG}">{blocks.edit.tab.TEXT}</label>
											<div class="col-md-8">
												<textarea class="form-control ckeditor" name="txt_text_{blocks.edit.tab.LANG}"
														  id="txt_text_{blocks.edit.tab.LANG}" rows="3">{blocks.edit.tab.TEXT_VALUE}</textarea>
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_url_{blocks.edit.tab.LANG}">{blocks.edit.tab.URL}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_url_{blocks.edit.tab.LANG}"
													   id="txt_url_{blocks.edit.tab.LANG}" value="{blocks.edit.tab.URL_VALUE}"
													   placeholder="{blocks.edit.tab.URL}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn blue" id="btn_save" name="btn_save"
															onClick="doBlocksAction('save', {blocks.edit.ID},  '');"><i class="fa fa-ok"></i> {blocks.edit.SAVE}
													</button>
													<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
															onclick="click_cancel('blocks', {blocks.edit.CATID});">{blocks.edit.CANCEL}</button>
												</div>
											</div>
										</div>
										</div>
										</div>
									</div>
									<!-- END tab -->
								</div>
							
							</div>
							<!-- END edit -->
							
							</form>
						</div>
						<!-- END::NEWS CAT-->
					</div>
				</div>
				<!-- END blocks -->
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	
	<!-- BEGIN blocks -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	 
	<!-- BEGIN edit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END edit -->
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#sample_1").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'blocks_drag.php?catid={blocks.CATID}', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
				
		});
	</script>
	<!-- END blocks -->
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    
	
	<!-- BEGIN cat -->
	<!-- BEGIN catedit -->
	<!-- BEGIN tab -->
	<script>
		CKEDITOR.replace( 'txt_info_{cat.catedit.tab.LANG}', {
			filebrowserImageBrowseUrl: 'ck.photo.php',
		} );
	</script>
	<!-- END tab -->
	<!-- END catedit -->
	<!-- END cat -->
	
	<!-- BEGIN blocks -->
	<!-- BEGIN edit -->
	<!-- BEGIN tab -->
	<script>
		CKEDITOR.replace( 'txt_text_{blocks.edit.tab.LANG}', {
			filebrowserImageBrowseUrl: 'ck.photo.php',
		} );
	</script>
	<!-- END tab -->
	<!-- END edit -->
	<!-- END blocks -->
	