var UITree = function () {
	
     var mCMSajaxTree = function() {
		
        $("#mcms_tree").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return './tree_json.php';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
			"state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
		
		
        $('#mcms_tree').on("move_node.jstree", function (e, data) {
		   //data.node, data.parent, data.old_parent is what you need
		   //console.log(data);
		   //alert(data.parent);
		   var var_hitmode = 'over';
		   if (data.parent == data.old_parent) {
			   var_hitmode = (data.old_position > data.position) ? 'before' : 'after';
		   }
		   
		   $.ajax({
				url: "tree_move.php",
				type: "POST",
				data: {move_id: data.parent, old_parent : data.old_parent, curr_id: data.node.id, hitmode : var_hitmode, old_position : data.old_position, position : data.position },
				dataType: "html"
			});
		});
		
		$('#mcms_tree').on("select_node.jstree", function (e, data) {
			//console.log(data);
			//alert(data.node.id);
			var pid = parseInt(data.node.parent);
			pid = (isNaN(pid)) ? 0 : pid;
			
			if (pid == 0) {
				document.getElementById('panel_toolbar').style.display = 'none';
				document.getElementById('panel_toolbar_zero').style.display = '';
				document.getElementById('panel-title').innerHTML = '';
				document.getElementById('panel-title-zero').innerHTML = data.node.text;
			} else {
				document.getElementById('panel_toolbar_zero').style.display = 'none';
				document.getElementById('panel_toolbar').style.display = '';
				document.getElementById('panel-title').innerHTML = data.node.text;
				document.getElementById('panel-title-zero').innerHTML = '';
			}
			//document.getElementById('panel_editbar').style.display = 'none';
			document.getElementById('select_id').value = data.node.id;
		});
    
    }
	
    return {
        //main function to initiate the module
        init: function () {
            mCMSajaxTree();
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
       UITree.init();
    });
}