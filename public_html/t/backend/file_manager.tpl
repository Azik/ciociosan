
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>

	<script>
	function copyToClipboard(elem) {
		  // create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			//alert(target);
			//origSelectionStart = elem.selectionStart;
			origSelectionStart = 0;
			//origSelectionEnd = elem.selectionEnd;
			origSelectionEnd = elem.value.length;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);
		
		// copy the selection
		var succeed;
		try {
			  succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}
		//alert(succeed);
		
		return succeed;
	}
	</script>
	
	<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:900px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=file_manager">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				<!-- BEGIN perm_add -->
				<div class="row">
					<div class="col-md-12">
						<form action="file_upload.php" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 100%;">
							<h3 class="sbold">Drop files here or click to upload</h3>
							<p> This is just a demo dropzone. Selected files are not actually uploaded. </p>
						</form>
					</div>
				</div>
				<!-- END perm_add -->
				
				<!-- BEGIN files -->
				<div class="row">
                    <script type="text/javascript">
                        function doDelete(id) {
                            if (confirm('{files.CONFIRM}')) {
                                document.getElementById('del_id').value = id;
                                document.getElementById('form_filelist').submit();
                            }
                        }

                        function doDelete1(id) {
                            document.getElementById('del_id').value = id;
                            document.getElementById('form_filelist').submit();
                        }

                        function imageOpener(id) {
                            if (id != '')
                                window.open('file_editor.php?v=' + id + '', '', 'width=1000,height=800,top=100,left=100,location=0');
                        }

                    </script>
					<div class="col-md-12">
					<br/><br/>
                    <form method="post" name="form_filelist" id="form_filelist">
                        <input type="hidden" name="del_id" id="del_id" value="0"/>
						<table class="table table-striped table-bordered table-hover" width="100%" id="file_list">
						<thead>
							<tr>
								<th style="width : 80px !important;" class="text-center">{files.THUMB}</th>
								<th style="width : 80px !important;" class="text-center">{files.ID}</th>
								<th>{files.FILENAME}</th>
								<th style="width : 100px !important;" class="text-center">{files.COPY_URL}</th>
								<th style="width : 100px !important;" class="text-center">{files.ACTION}</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
						
						
					</form>
					</div>
				</div>
				<!-- END files -->
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	<script src="{ROOT}assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
	
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/form-dropzone.js" type="text/javascript"></script>
	<script src="{ROOT}assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	<!-- BEGIN files -->
	<script>
	$(document).ready(function () {
		
		var filesTable= $("#file_list").dataTable({
			"bJQueryUI": true,
			"bDestroy": false,
			"columnDefs": [
				{"className": "dt-left", "targets": [2]},
				{"className": "dt-center", "targets": "_all"},
				{"width": "80px", "targets": [0, 1, 3] }
			],
			"bProcessing": true,
			"bSortable": false,
			"processing": true,
			"serverSide": true,
			"ajax":  'ajax_file_list.php?rand_num=' + Math.floor((Math.random() * 10000000))
		});
	});
	</script>
	<!-- END files -->