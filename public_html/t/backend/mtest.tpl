
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<link href="{ROOT}assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- END::PAGE LEVEL PLUGINS -->
	
	<link href="{ROOT}assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/facolor/jscolor.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		
		function clickAc(act) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=MTest&cateditid=' + act + ':' + fields;
		}
		function clickAcMTest(act, catid) {
			//alert(act);
			var set = $(".checkboxes");
			var fields = '';
			$(set).each(function () {
				var checked = $(this).is(":checked");
				if (checked) {
					var v = $(this).val();
					fields += (fields != '') ? ',' : '';
					fields += v;
					//alert("aaa:"+v);
				}
			});
			if (fields != '')
				window.location.href = '?{MODULE_QS}=MTest&catid=' + catid + '&questionid=' + act + ':' + fields;
		}
		function click_cancel(act, id, testid = 0) {
			if (act == 'MTest') {
				window.location.href = '?{MODULE_QS}=MTest&mod=MTest&testid=' + id;
			}
			else if (act == 'tests') {
				window.location.href = '?{MODULE_QS}=MTest&mod=tests&catid=' + id;
			}
			else if (act == 'answers') {
				window.location.href = '?{MODULE_QS}=MTest&mod=answers&catid=' + id  + '&testid='+testid;
			} else {
				window.location.href = '?{MODULE_QS}=MTest';
			}
		}

		function doCatDelete(id) {
			if (confirm('{CAT_CONFIRM}')) {
				document.getElementById("cat_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('cat_subm').click();
			}
		}
		
		function doCatAction(act, id, mess) {
			document.getElementById("cat_action").value = act;
			document.getElementById('select_id').value = id;
				debugger;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("cat_subm").click();
				}
			}
			else {
				document.getElementById("cat_subm").click();
			}
		}

		function doMTestDelete(id) {
			if (confirm('{MTest_CONFIRM}')) {
				document.getElementById("MTest_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('MTest_subm').click();
			}
		}


		
		function dotestsDelete(id) {
			if (confirm('{MTest_CONFIRM}')) {
				document.getElementById("tests_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('tests_subm').click();
			}
		}

		function doanswersDelete(id) {
			if (confirm('{MTest_CONFIRM}')) {
				document.getElementById("answers_action").value = 'delete';
				document.getElementById('select_id').value = id;
				document.getElementById('answers_subm').click();
			}
		}
		
		
		
		function dotestsAction(act, id, mess) {
			document.getElementById("tests_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("tests_subm").click();
				}
			}
			else {
				document.getElementById("tests_subm").click();
			}
		}
		
		
		function doMTestAction(act, id, mess) {
			document.getElementById("MTest_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("MTest_subm").click();
				}
			}
			else {
				document.getElementById("MTest_subm").click();
			}
		}
		
		function doanswersAction(act, id, mess) {
			document.getElementById("answers_action").value = act;
			document.getElementById('select_id').value = id;

			if (act == 'delete') {
				if (confirm(mess)) {
					document.getElementById("answers_subm").click();
				}
			}
			else {
				document.getElementById("answers_subm").click();
			}
		}
		


		function doOptionsAction(options_action) {
			document.getElementById("options_action").value = options_action;
			document.getElementById("options_subm").click();
		}
		
		function redirect(url) {
			window.location.href = url;
		}
	</script>

		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:450px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<!-- BEGIN cat -->
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest">{TITLE}</a></span>
						</li>
						<!-- END cat -->
						
						<!-- BEGIN tests -->
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest&mod=tests&catid={tests.CAT_ID}">{tests.CAT_NAME}</a></span>
							
						</li>
						<!-- END tests -->



						<!-- BEGIN MTest -->
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest&mod=tests&catid={MTest.CAT1_ID}">{MTest.CAT1_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest&mod=MTest&testid={MTest.CAT_ID}">{MTest.CAT_NAME}</a></span>
							
						</li>
						<!-- END MTest -->

						<!-- BEGIN answers -->
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest">{TITLE}</a></span>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest&catid={answers.CATES_ID}">{answers.CATES_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="index.php?{MODULE_QS}=MTest&mod=MTest&testid={answers.MTest_ID}">{answers.MTest_NAME}</a></span>
						</li>
						<li>
							<i class="fa fa-angle-right"></i>
						</li>
						<!--<li>
							<span><a href="index.php?{MODULE_QS}=MTest&mod=answers&questionid={answers.CAT_ID}">aaa{answers.CAT_NAME}</a></span>
						</li> -->
						<!-- END answers -->
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- END::PAGE HEADER-->
				
				<!-- BEGIN cat -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::MTest CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{TITLE}</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_cat" id="form_cat">
								<input type="hidden" id="cat_action" name="cat_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="cat_subm" name="cat_subm" style="display:none;">
							
							<!-- BEGIN list -->
							<div class="portlet-body">
								<!-- BEGIN perm_add -->
								<div class="clearfix">
								
									<!--<div class="btn-group">
										<a href="{cat.list.LAST_MTest_URL}" class="btn yellow">
											{cat.list.LAST_MTest} &nbsp; <i class="fa fa-list"></i>
										</a>
									</div> -->
									<div class="btn-group">
										<a href="{cat.list.ADD_URL}" class="btn green">
											{cat.list.ADD} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div>
									<!--<div class="btn-group">
										<a href="{cat.list.ADD_MTest_URL}" class="btn green">
											{cat.list.ADD_MTest} &nbsp; <i class="fa fa-plus"></i>
										</a>
									</div> -->
									
								</div>
								<!-- END perm_add -->
								<br/>
								<table class="table table-striped table-bordered table-hover" width="100%" id="MTest_cat_list">
									<thead>
											<th style="width:0px !important;display:none;"></th>
											<th style="width:12px !important;text-align:center;"><input type="checkbox" class="group-checkable" data-set="#MTest_cat_list .checkboxes"/></th>
											<th>{cat.list.NAME}</th>
											<th>Point</th>
											<th>{cat.list.HEADER}</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<!-- BEGIN items -->
										<tr data-position="{cat.list.items.POSITION}" id="{cat.list.items.ID}">
											<td style="display:none;">{cat.list.items.POSITION}</td>
											<td style="width:12px !important;text-align:center;" id="chk_{cat.list.items.ID}"><input type="checkbox" class="checkboxes" value="{cat.list.items.ID}"/></td>
											<td><a href="{cat.list.URL}&catid={cat.list.items.ID}">{cat.list.items.NAME}</a></td>
											<td>{cat.list.items.POINT}</td>
											<td>{cat.list.items.HEADER}</td>
											<td style="width : 320px !important;">
											    <!-- BEGIN perm_edit -->
												<a href="{cat.list.items.EDIT_URL}" title="{cat.list.EDIT}" class="btn yellow">
													{cat.list.EDIT}
													 &nbsp; <i class="fa fa-pencil"></i>
												</a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doCatDelete({cat.list.items.ID})" title="{cat.list.DELETE}" class="btn red ask"> 
													{cat.list.DELETE}
													 &nbsp; <i class="fa fa-trash"></i>
												</a>
												<!-- END perm_del -->
												
												<!-- BEGIN active -->
												<a href="{cat.list.items.ACTIVE_URL}" class="btn btn-success">{cat.list.ACTIVE}
													 &nbsp; <i class="fa fa-unlock"></i></a>
												<!-- END active -->
												<!-- BEGIN inactive -->
												<a href="{cat.list.items.INACTIVE_URL}" class="btn btn-danger">{cat.list.INACTIVE}
													 &nbsp; <i class="fa fa-lock"></i></a>
												<!-- END inactive -->
											</td>
										</tr>
										<!-- END items -->
									</tbody>
								</table>
								<!-- BEGIN hidden -->
								<input type="hidden" name="sort_order" id="sort_order" value="{cat.list.hidden.VALUE}"/>
								<!-- END hidden -->
								
								<br/>
								<!-- BEGIN perm_edit -->
								<table>
									<tfoot>
									<tr>
										<td class="ac"><span class="label label-success"
															 onclick="clickAc('activate')">{cat.list.ACTIVATE}</span></td>
										<td style="width:10px;"></td>
										<td class="ac"><span class="label label-danger"
															 onclick="clickAc('inactivate')">{cat.list.INACTIVATE}</span></td>
										<td></td>
									</tr>
									</tfoot>
								</table>
								<!-- END perm_edit -->
							</div>
							<!-- END list -->
							
							<!-- BEGIN catedit -->
							<div class="portlet-body" id="panel_editbar">
								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check"  {cat.catedit.ACTIVE_CHK} />
												<label for="txt_active">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{cat.catedit.ACTIVE}
												</label>
											</div>
										</div>
									</div>
								</div>
								
		
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label cursor-pointer" for="txt_color">Color</label>
									<div class="col-md-8">
										<input type="text" data-jscolor="{cat.catedit.COLOR_VALUE}" class="form-control txt_color" name="txt_color"
											   id="txt_color" value="{cat.catedit.COLOR_VALUE}"
											   placeholder="Color"/>
									   <div class="palette" id="colorPalette"></div>
										<div class="form-control-focus"> </div>
									</div>
								</div>
				

								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label cursor-pointer" for="txt_point">Point</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="txt_point"
											   id="txt_point" value="{cat.catedit.POINT_VALUE}"
											   placeholder="Point">
										<div class="form-control-focus"> </div>
									</div>
								</div>
								
								

								<div class="tabbable-line boxless tabbable-reversed">
									<ul class="nav nav-tabs">
										<!-- BEGIN tab -->
										<li{cat.catedit.tab.CLASS}><a href="#content_{cat.catedit.tab.LANG}" data-toggle="tab">{cat.catedit.tab.LANG}</a></li>
										<!-- END tab -->
									</ul>
								</div>
								
								<div class="tab-content">
									<!-- BEGIN tab -->
									<div class="tab-pane {cat.catedit.tab.FADE_CLASS}" id="content_{cat.catedit.tab.LANG}">
										<div class="portlet box green">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-language"></i>{cat.catedit.tab.LANG} 
											</div>
										</div>
										<div class="portlet-body form">
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_name_{cat.catedit.tab.LANG}">{cat.catedit.tab.NAME}</label>
											<div class="col-md-8">
												<input type="text" class="form-control" name="txt_name_{cat.catedit.tab.LANG}"
													   id="txt_name_{cat.catedit.tab.LANG}" value="{cat.catedit.tab.NAME_VALUE}"
													   placeholder="{cat.catedit.tab.NAME}">
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-group form-md-line-input">
											<label class="col-md-3 control-label" for="txt_header_{cat.catedit.tab.LANG}">{cat.catedit.tab.HEADER}</label>
											<div class="col-md-8">
												<textarea class="form-control ckeditor" name="txt_header_{cat.catedit.tab.LANG}"
														  id="txt_header_{cat.catedit.tab.LANG}" rows="3">{cat.catedit.tab.HEADER_VALUE}</textarea>
												<div class="form-control-focus"> </div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn blue" id="btn_save" name="btn_save"
															onClick="doCatAction('save', {cat.catedit.ID},  '');"><i class="fa fa-ok"></i> {cat.catedit.SAVE}
													</button>
													<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
															onclick="click_cancel('cat', 0);">{cat.catedit.CANCEL}</button>
												</div>
											</div>
										</div>
										</div>
										</div>
									</div>
									<!-- END tab -->
								</div>
							
							</div>
							<!-- END catedit -->
							
							</form>
						</div>
						<!-- END::MTest CAT-->
					</div>
				</div>
				<!-- END cat -->
				



				
				<!-- BEGIN tests -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::tests CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{tests.CAT_NAME} / Tests</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_tests" id="form_tests" enctype="multipart/form-data">
								<input type="hidden" id="tests_action" name="tests_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="tests_subm" name="tests_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<!-- BEGIN perm_add -->
									<div class="clearfix">
										<div class="btn-group">
											<a href="{tests.list.ADD_URL}" class="btn green">
												{tests.list.ADD} &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!-- END perm_add -->
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="MTest_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#MTest_list .checkboxes"/></th>
												<th>{tests.list.NAME}</th>
												<th style="width:120px !important;">{tests.list.DATE}</th>
												<th style="width:120px !important;">Active</th>
												<th style="width:100px !important;">Index</th>
												<th style="width:500px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{tests.list.items.ID}">
												<td id="chk_{tests.list.items.ID}"><input type="checkbox" class="checkboxes" value="{tests.list.items.ID}"/></td>
												<td><a href="{tests.list.items.EDIT_URL}" title="{tests.list.EDIT}">{tests.list.items.NAME}</a></td>
												<td>{tests.list.items.DATE}</td>
												<td>{tests.list.items.ACTIVE}</td>
												<td>{tests.list.items.INDEX}</td>
												<td>
												<a href="{tests.list.items.ANSWERS_URL}" title="answers" class="btn yellow"> Answers&nbsp;</a>
												<!-- BEGIN perm_edit -->
												<a href="{tests.list.items.EDIT_URL}" title="{tests.list.EDIT}" class="btn yellow"> {tests.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:dotestsDelete({tests.list.items.ID})" title="{tests.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {tests.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{tests.list.items.ACTIVE_URL}" class="btn btn-success">{tests.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{tests.list.items.INACTIVE_URL}" class="btn btn-danger">{tests.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{tests.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcMTest('activate', {tests.list.CATID})">{tests.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcMTest('inactivate', {tests.list.CATID})">{tests.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_category">{tests.edit.CATEGORY}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_category" id="txt_category">
												<!-- BEGIN category -->
												<option value="{tests.edit.category.VALUE}" {tests.edit.category.SELECTED}>{tests.edit.category.TEXT}</option>
												<!-- END category -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_music">Music</label>
										<div class="col-md-8">
										<input name="txt_music" class="form-control" id="txt_music" type="file" class="default" accept=".mp3"/>
										<div class="form-control-focus"> </div>
										
											<audio controls>
											  <source src="{tests.edit.MUSIC_VALUE}" type="audio/mpeg">
											Your browser does not support the audio element.
											</audio>

										</div>
									</div>
									

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {tests.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{tests.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
								
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{tests.edit.tab.CLASS}><a href="#content_{tests.edit.tab.LANG}" data-toggle="tab">{tests.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {tests.edit.tab.FADE_CLASS}" id="content_{tests.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{tests.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{tests.edit.tab.LANG}">{tests.edit.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{tests.edit.tab.LANG}"
														   id="txt_name_{tests.edit.tab.LANG}" value="{tests.edit.tab.NAME_VALUE}"
														   placeholder="{tests.edit.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>

											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_header_{tests.edit.tab.LANG}">Text</label>
												<div class="col-md-8">
													<textarea class="form-control ckeditor" name="txt_header_{tests.edit.tab.LANG}"
															  id="txt_header_{tests.edit.tab.LANG}" rows="3">{tests.edit.tab.HEADER_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
									
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="dotestsAction('save', {tests.edit.ID},  '');"><i class="fa fa-ok"></i> {tests.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('tests', {tests.edit.CATID});">{tests.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
							</form>
						</div>
						<!-- END::tests CAT-->
					</div>
				</div>
				<!-- END tests -->


				
				<!-- BEGIN MTest -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::MTest CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">{MTest.CAT_NAME} / MTest</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_MTest" id="form_MTest">
								<input type="hidden" id="MTest_action" name="MTest_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="MTest_subm" name="MTest_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<!-- BEGIN perm_add -->
									<div class="clearfix">
										<div class="btn-group">
											<a href="{MTest.list.ADD_URL}" class="btn green">
												ADD QUESTION &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<!-- END perm_add -->
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="MTest_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#MTest_list .checkboxes"/></th>
												<th>{MTest.list.NAME}</th>
												<th style="width:150px !important;">{MTest.list.DATE}</th>
												<th style="width:280px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{MTest.list.items.ID}">
												<td id="chk_{MTest.list.items.ID}"><input type="checkbox" class="checkboxes" value="{MTest.list.items.ID}"/></td>
												<td><a href="{MTest.list.items.EDIT_URL}" title="{MTest.list.EDIT}">{MTest.list.items.NAME}</a></td>
												<td>{MTest.list.items.DATE}</td>
												<td>{MTest.list.items.ACTIVE}</td>
												<td>{MTest.list.items.INDEX}</td>
												<td>{MTest.list.items.TOTAL_VIEWS}</td>
												<td>{MTest.list.items.MONTH_VIEWS}</td>
												<td>{MTest.list.items.READ_DATE}</td>
												<td>{MTest.list.items.COMMENT}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{MTest.list.items.EDIT_URL}" title="{MTest.list.EDIT}" class="btn yellow"> {MTest.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doMTestDelete({MTest.list.items.ID})" title="{MTest.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {MTest.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{MTest.list.items.ACTIVE_URL}" class="btn btn-success">{MTest.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{MTest.list.items.INACTIVE_URL}" class="btn btn-danger">{MTest.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{MTest.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcMTest('activate', {MTest.list.CATID})">{MTest.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcMTest('inactivate', {MTest.list.CATID})">{MTest.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label" for="txt_category">{MTest.edit.CATEGORY}</label>
										<div class="col-md-6">
											<select class="form-control" name="txt_category" id="txt_category">
												<!-- BEGIN category -->
												<option value="{MTest.edit.category.VALUE}" {MTest.edit.category.SELECTED}>{MTest.edit.category.TEXT}</option>
												<!-- END category -->
											</select>
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_header" onclick="openPhoto($('#txt_header').val(), 'txt_header')">Image</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_header"
												   id="txt_header" value="{MTest.edit.HEADER_VALUE}"
												   placeholder="Image">
											<div class="form-control-focus"> </div>
										</div>
									</div>
									
									
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_answers_limit">Answers limit</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_answers_limit"
												   id="txt_answers_limit" value="{MTest.edit.ANSWERS_LIMIT_VALUE}"
												   placeholder="Answers limit">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {MTest.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{MTest.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>

								
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{MTest.edit.tab.CLASS}><a href="#content_{MTest.edit.tab.LANG}" data-toggle="tab">{MTest.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {MTest.edit.tab.FADE_CLASS}" id="content_{MTest.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{MTest.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{MTest.edit.tab.LANG}">{MTest.edit.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{MTest.edit.tab.LANG}"
														   id="txt_name_{MTest.edit.tab.LANG}" value="{MTest.edit.tab.NAME_VALUE}"
														   placeholder="{MTest.edit.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
										
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="doMTestAction('save', {MTest.edit.ID},  '');"><i class="fa fa-ok"></i> {MTest.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('MTest', {MTest.edit.CATID});">{MTest.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
							</form>
						</div>
						<!-- END::MTest CAT-->
					</div>
				</div>
				<!-- END MTest -->
				
				
				



				
				<!-- BEGIN MTestcat_options -->
				<div class="row">
					<div class="col-md-12">
						<!-- B: TABLE PORTLET-->
						<button type="button" id="MTestcat_button" class="btn green" style="margin-bottom:15px !important;">{MTestcat_options.TITLE} &nbsp; <i class="fa fa-plus"></i></button>
						<script>
							$("#MTestcat_button").click(function () {
								$("#MTestcat_options").toggle("fade", 1000);
							});
						</script>
						
						<div class="portlet light bordered" id="MTestcat_options" style="display: none;">
							<div class="portlet-title" style="margin-bottom:-10px;">
								<h4>{MTestcat_options.TITLE}</h4>
							</div>
							<form action="" method="POST" name="options_form" id="options_form">
								<input type="hidden" id="options_action" name="action" value="">
								<input type="submit" id="options_subm" name="options_subm" style="display:none;">

								<div class="portlet-body form" id="panel_editbar">
								<div class="form-body">
									<div class="form-group">
										<label>Answers limit</label>
										<input type="number" class="form-control input-medium" name="txt_answers_limit" id="txt_answers_limit"
												   value="{MTestcat_options.ANSWERS_LIMIT_VALUE}"
												   placeholder="Answers limit" >
									</div>
								
									
									<div class="form-actions">
										<button type="button" class="btn blue" id="btn_save" name="btn_save"
												onClick="doOptionsAction('save_MTestcat_options');"><i class="fa fa-ok"></i> {MTestcat_options.SAVE}</button>
										<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
												onclick="click_cancel('cat', 0);">{MTestcat_options.CANCEL}</button>
									</div>
								</div>
								</div>

							</form>

						</div>
						<!-- E: TABLE PORTLET-->

					</div>
				</div>
				<!-- END MTestcat_options -->
				







	
				<!-- BEGIN answers -->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN::MTest CAT-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="fa fa-calendar"></i>
									<span class="caption-subject bold uppercase">Add / Update answer</span>
								</div>
							</div>
							<form action="" method="POST" class="form-horizontal" name="form_answers" id="form_answers">
								<input type="hidden" id="answers_action" name="answers_action" value="">
								<input type="hidden" value="0" id="select_id" name="select_id">
								<input type="submit" id="answers_subm" name="answers_subm" style="display:none;">
							
								<!-- BEGIN list -->
								<div class="portlet-body">
									<div class="clearfix">
										<div class="btn-group">
											<a href="{answers.list.ADD_URL}" class="btn green">
												Answer add &nbsp; <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									<br/>
									<table class="table table-striped table-bordered table-hover" width="100%" id="answers_list">
										<thead>
											<tr>
												<th style="width:12px !important;"><input type="checkbox" class="group-checkable" data-set="#answers_list .checkboxes"/></th>
												<th>{answers.list.NAME}</th>
												<th style="width:150px !important;">Düzgün cavab</th>
												<th style="width:150px !important;">{answers.list.DATE}</th>
												<th style="width:180px !important;">Active</th>
												<th style="width:340px !important;"></th>
											</tr>
										</thead>
										<tbody>
											<!-- BEGIN items -->
											<tr id="{answers.list.items.ID}">
												<td id="chk_{answers.list.items.ID}"><input type="checkbox" class="checkboxes" value="{answers.list.items.ID}"/></td>
												<td>{answers.list.items.NAME}</td>
												<td>{answers.list.items.CORRECT}</td>
												<td>{answers.list.items.DATE}</td>
												<td>{answers.list.items.ACTIVE}</td>
												<td>
												<!-- BEGIN perm_edit -->
												<a href="{answers.list.items.EDIT_URL}" title="{answers.list.EDIT}" class="btn yellow"> {answers.list.EDIT}&nbsp; <i class="fa fa-pencil"></i></a>
												<!-- END perm_edit -->
												<!-- BEGIN perm_del -->
												<a href="JavaScript:doanswersDelete({answers.list.items.ID})" title="{answers.list.DELETE}" class="btn red ask" title="{cat.list.DELETE}"> {answers.list.DELETE}&nbsp; <i class="fa fa-trash"></i></a>
												<!-- END perm_del -->
													<!-- BEGIN active -->
													<a href="{answers.list.items.ACTIVE_URL}" class="btn btn-success">{answers.list.ACTIVE}&nbsp; <i class="fa fa-unlock"></i></a>
													<!-- END active -->
													<!-- BEGIN inactive -->
													<a href="{answers.list.items.INACTIVE_URL}" class="btn btn-danger">{answers.list.INACTIVE}&nbsp; <i class="fa fa-lock"></i></a>
													<!-- END inactive -->
												</td>
											</tr>
											<!-- END items -->
										</tbody>
									</table>
									<!-- BEGIN hidden -->
									<input type="hidden" name="sort_order" id="sort_order" value="{answers.list.hidden.VALUE}"/>
									<!-- END hidden -->
									
									<br/>
									<!-- BEGIN perm_edit -->
									<table>
										<tfoot>
										<tr>
											<td class="ac"><span class="btn btn-success"
																 onclick="clickAcMTest('activate', {answers.list.CATID})">{answers.list.ACTIVATE}</span></td>
											<td style="width:10px;"></td>
											<td class="ac"><span class="btn btn-danger"
																 onclick="clickAcMTest('inactivate', {answers.list.CATID})">{answers.list.INACTIVATE}</span></td>
											<td></td>
										</tr>
										</tfoot>
									</table>
									<!-- END perm_edit -->
								</div>
								<!-- END list -->
								
								
								
								
								<!-- BEGIN edit -->
								<div class="portlet-body" id="panel_editbar">

									
									<div class="form-group form-md-line-input">
										<label class="col-md-3 control-label cursor-pointer" for="txt_position">Position</label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="txt_position"
												   id="txt_position" value="{answers.edit.POSITION_VALUE}"
												   placeholder="Position">
											<div class="form-control-focus"> </div>
										</div>
									</div>

									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_active">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_active" id="txt_active" value="1" class="md-check" {answers.edit.ACTIVE_CHK}/>
													<label for="txt_active">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>{answers.edit.ACTIVE}
													</label>
												</div>
											</div>
										</div>
									</div>
									
									
									<div class="form-group form-md-checkboxes">
										<label class="col-md-3 control-label" for="txt_correct">&nbsp;</label>
										<div class="col-md-6">
											<div class="md-checkbox-list">
												<div class="md-checkbox">
													<input type="checkbox" name="txt_correct" id="txt_correct" value="1" class="md-check" {answers.edit.CORRECT_CHK}/>
													<label for="txt_correct">
														<span></span>
														<span class="check"></span>
														<span class="box"></span>Düzgün cavab
													</label>
												</div>
											</div>
										</div>
									</div>
									
							
									<div class="tabbable-line boxless tabbable-reversed">
										<ul class="nav nav-tabs">
											<!-- BEGIN tab -->
											<li{answers.edit.tab.CLASS}><a href="#content_{answers.edit.tab.LANG}" data-toggle="tab">{answers.edit.tab.LANG}</a></li>
											<!-- END tab -->
										</ul>
									</div>
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {answers.edit.tab.FADE_CLASS}" id="content_{answers.edit.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{answers.edit.tab.LANG}
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{answers.edit.tab.LANG}">Answer</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{answers.edit.tab.LANG}"
														   id="txt_name_{answers.edit.tab.LANG}" value="{answers.edit.tab.NAME_VALUE}"
														   placeholder="Answer">
													<div class="form-control-focus"> </div>
												</div>
											</div>			
										
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onclick="doanswersAction('save', {answers.edit.ID},  '');"><i class="fa fa-ok"></i> {answers.edit.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel('answers', {answers.edit.ID}, {answers.edit.CATID});">{answers.edit.CANCEL}</button>
													</div>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
									
									
								
								</div>
								<!-- END edit -->
								
								
							</form>
						</div>
						<!-- END::MTest CAT-->
					</div>
				</div>
				<!-- END answers -->
				


				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	
	
	
	<!-- BEGIN MTest -->
	<!-- BEGIN::PAGE LEVEL PLUGINS -->
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL PLUGINS -->
	<!-- END MTest -->
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				var set = $(".checkboxes");
				$(set).each(function () {
					$(this).prop('checked', ch);
					var v = $(this).val();
					if (ch) {
						$("#chk_" + v).find('span').addClass('checked');
					} else {
						$("#chk_" + v).find('span').removeClass('checked');
					}
				});
			});
			
			$('#datetimepicker').datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				autoclose: true
			});
		});
	</script>
	<!-- BEGIN cat -->
	<script src="{ROOT}assets/global/scripts/dt/jquery.js" type="text/javascript"></script>
	<!-- BEGIN catedit -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<!-- END catedit -->
	<!-- 
	<script src="{ROOT}assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	 -->
	 
	<script src="{ROOT}assets/global/scripts/dt/jquery-ui.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowReordering.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/scripts/dt/jquery.dataTables.rowGrouping.js" type="text/javascript"></script>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function () {
			var sortInput = jQuery('#sort_order');
			
			var oTable= $("#MTest_cat_list").dataTable({
				"bJQueryUI": true,
				"bDestroy": false,
				"bProcessing": false,
				"bSortable": false,
			});
		
			oTable.rowReordering({
				sURL : 'MTest_drag.php', 
				fnAlert: function(message) {
					//alert("order"); 
				}
			});
							//.clear().destroy()
			$('.group-checkable').click(function(){
				//alert($('.group-checkable').is(":checked"));
				var ch = $('.group-checkable').is(":checked");
				$('.checkboxes').prop('checked', ch);
			});
		});
	</script>
	<!-- END cat -->
	<!-- BEGIN MTest -->
	<!-- BEGIN list -->
	<script>
	$(document).ready(function () {
		var MTestTable= $("#MTest_list").dataTable({
			"bJQueryUI": true,
			"bDestroy": false,
			"bProcessing": true,
			"bSortable": false,
			"processing": true,
			"serverSide": true,
			"ajax":  'ajax_MTest_list.php?testid={MTest.list.CATID}&rand_num=' + Math.floor((Math.random() * 10000000))
		});
	});
	</script>
	<!-- END list -->
	<!-- END MTest -->
	
	<script src="{ROOT}assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	<!-- BEGIN::PAGE LEVEL SCRIPTS -->
	<script src="{ROOT}assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
	<!-- END::PAGE LEVEL SCRIPTS -->
	
	<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
	
	<!-- BEGIN MTest -->
	<!-- BEGIN edit -->
	<!-- BEGIN tab -->
	<script>
		CKEDITOR.replace( 'txt_text_{MTest.edit.tab.LANG}', {
			filebrowserImageBrowseUrl: 'ck.photo.php',
		} );
	</script>
	<!-- END tab -->
	<!-- END edit -->
	<!-- END MTest -->
	