<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN::HEAD -->
<head>
	<meta content="noindex, nofollow" name="robots"/>
	<meta charset="utf-8" />
	<title>{TITLE}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta content="MicroPHP" name="author"/>
	<!-- BEGIN::GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/font-awesome/css/fontawesome-all.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<!-- END::GLOBAL MANDATORY STYLES -->
	<!-- BEGIN::THEME GLOBAL STYLES -->
	<link href="{ROOT}assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
	<link href="{ROOT}assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
	<!-- END::THEME GLOBAL STYLES -->
	<!-- BEGIN::THEME LAYOUT STYLES -->
	<link href="{ROOT}assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="{ROOT}assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<!-- END::THEME LAYOUT STYLES -->
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END::HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
	<!-- BEGIN::HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN::HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN::LOGO -->
			<div class="page-logo">
				<a href="index.php">
					<img src="{ROOT}assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
				<div class="menu-toggler sidebar-toggler"> </div>
			</div>
			<!-- END::LOGO -->
			<!-- BEGIN::RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
			<!-- END::RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN::TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- B:USER RIGHTS -->
					<li class="dropdown_info">
						<p>{USER_RIGHT}</p>
					</li>
					<!-- E:USER RIGHTS -->
					<!-- B:HOMEPAGE -->
					<li class="dropdown home" title="{VISIT_SITE}">
						<a class="dropdown-toggle" href="../" onclick="window.open(this.href,'_blank');return false;"
						   title="{VISIT_SITE}" title="{VISIT_SITE}">
							<i class="icon-home"></i>
						</a>
						<ul></ul>
					</li>
					<!-- E:HOMEPAGE -->
					<!-- BEGIN::USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="{ROOT}assets/layouts/layout/img/avatar.png" />
							<span class="username username-hide-on-mobile"> {USER_NAME} </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="index.php?{MODULE_QS}=options&mode=siteoptions"><i
									class="fa fa-cog"></i> {SITEOPTIONS}</a>
							</li>
							<li>
								<a href="index.php?{MODULE_QS}=options&mode=profile">
									<i class="icon-user"></i> {PROFILE} </a>
							</li>
							<li>
								<a href="index.php?{MODULE_QS}=options&mode=password">
									<i class="fa fa-lock"></i> {PASSWORD} </a>
							</li>
							<li class="divider"> </li>
							<li>
								<a href="index.php?{MODULE_QS}=action&act=logout">
									<i class="fa fa-power-off"></i> {LOGOUT} </a>
							</li>
						</ul>
					</li>
					<!-- END::USER LOGIN DROPDOWN -->
					<!-- BEGIN::QUICK SIDEBAR TOGGLER -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-quick-sidebar-toggler">
						<a href="index.php?{MODULE_QS}=action&act=logout" class="dropdown-toggle">
							<i class="icon-logout"></i>
						</a>
					</li>
					<!-- END::QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END::TOP NAVIGATION MENU -->
		</div>
		<!-- END::HEADER INNER -->
	</div>
	<!-- END::HEADER -->
	<!-- BEGIN::HEADER & CONTENT DIVIDER -->
	<div class="clearfix"> </div>
	<!-- END::HEADER & CONTENT DIVIDER -->
	<!-- BEGIN::CONTAINER -->
	<div class="page-container">
		<!-- BEGIN::SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- BEGIN::SIDEBAR -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN::SIDEBAR MENU -->
				<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
					<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
					<li class="sidebar-toggler-wrapper hide">
						<!-- BEGIN::SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler"> </div>
						<!-- END::SIDEBAR TOGGLER BUTTON -->
					</li>
					<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
					<li class="sidebar-search-wrapper">
						<!-- BEGIN::RESPONSIVE QUICK SEARCH FORM -->
						<form class="sidebar-search" action="">
							<div class="input-group">
								<span class="input-group-btn">
									<a href="javascript:;" class="btn submit">
										<i></i>
									</a>
								</span>
							</div>
						</form>
						<!-- END::RESPONSIVE QUICK SEARCH FORM -->
					</li>
					<!-- BEGIN menu -->
					<!-- BEGIN heading -->
					<li class="heading">
						<h3 class="uppercase">{menu.heading.NAME}</h3>
					</li>
					<!-- END heading -->
					<!-- BEGIN items -->
					<li class="nav-item {menu.items.ACTIVE_CLASS_NM}">
						<a href="{menu.items.URL}" class="nav-link nav-toggle">
							<i class="{menu.items.ICON}"></i>
							<span class="title">{menu.items.TEXT}</span>
							{menu.items.SELECTED_CLASS}
						</a>
						<!-- BEGIN sub -->
						<ul class="sub-menu">
							<!-- BEGIN link -->
							<li class="nav-item">
								<a href="{menu.items.sub.link.URL}" class="nav-link ">
									<span class="title">{menu.items.sub.link.TEXT}</span>
								</a>
							</li>
							<!-- END link -->
						</ul>
						<!-- END sub -->
					</li>
					<!-- END items -->
					<!-- END menu -->
				</ul>
				<br/><br/><br/><br/>
				<!-- END::SIDEBAR MENU -->
			</div>
			<!-- END::SIDEBAR -->
		</div>
		<!-- END::SIDEBAR -->
		<!-- BEGIN::CONTENT -->
		<div class="page-content-wrapper" style="min-width:850px !important;">
			<!-- BEGIN::CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN::PAGE HEADER-->
				<!-- BEGIN::PAGE TITLE-->
				<h3 class="page-title"> {TITLE} </h3>
				<!-- END::PAGE TITLE-->
				<!-- BEGIN::PAGE BAR -->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<a href="index.php">{HOME}</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<span><a href="help.php">{TITLE}</a></span>
						</li>
					</ul>
				</div>
				<!-- END::PAGE BAR -->
				<!-- END::PAGE HEADER-->
				
				<div class="row">
					<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-question"></i>
								<span class="caption-subject bold uppercase">{TITLE}</span>
							</div>
						</div>
						
						
				<div class="row">
                    <div class="col-md-4">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                            <!-- BEGIN panel -->
                            <li{panel.CLASS}><a href="#tab_{panel.TAB_ID}" data-toggle="tab"><i
                                            class="fa fa-{panel.ICON}"
                                            style="width:40px;text-align:center"></i> {panel.NAME}</a> {panel.AFTER}
                            </li>
                            <!-- END panel -->
                        </ul>
                    </div>
                    <div class="col-md-8">
					
						<div class="portlet-body tab-content">
							<!-- BEGIN panel -->
							<div class="tab-pane{panel.ACTIVE_CLASS}" id="tab_{panel.TAB_ID}">
                                <div class="accordion in collapse" id="accordion{panel.TAB_ID}" style="height: auto;">
								<!-- BEGIN tab -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion{panel.TAB_ID}" href="#collapse_{panel.TAB_ID}_{panel.tab.ID}"> {panel.tab.NAME} </a>
										</h4>
									</div>
									<div id="collapse_{panel.TAB_ID}_{panel.tab.ID}" class="panel-collapse collapse{panel.tab.IN}">
										<div class="panel-body">
											{panel.tab.TEXT}
										</div>
									</div>
								</div>
								<!-- END tab -->
							</div>
							</div>
							<!-- END panel -->
							
						</div>
						
                    </div>
                    <!--E:span9-->
                </div>
				
						
					</div>
					</div>
				</div>
				
			</div>
			<!-- END::CONTENT BODY -->
		</div>
		<!-- END::CONTENT -->
	</div>
	<!-- END::CONTAINER -->
	<!-- BEGIN::FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner"> 
			&copy; {YEAR}
			mCMS | Powered by <a href="http://microphp.com" onclick="window.open(this.href,'_blank');return false;">MicroPHP</a>
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END::FOOTER -->
	<!--[if lt IE 9]>
	<script src="{ROOT}assets/global/plugins/respond.min.js"></script>
	<script src="{ROOT}assets/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
	<!-- BEGIN::CORE PLUGINS -->
	<script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END::CORE PLUGINS -->
	<!-- BEGIN::THEME GLOBAL SCRIPTS -->
	<script src="{ROOT}assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END::THEME GLOBAL SCRIPTS -->
	<!-- BEGIN::THEME LAYOUT SCRIPTS -->
	<script src="{ROOT}assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
	<script src="{ROOT}assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
	<!-- END::THEME LAYOUT SCRIPTS -->
</body>

</html>