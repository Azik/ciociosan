
            <!-- BEGIN::CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN::CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN::PAGE HEADER-->
                    <!-- BEGIN::PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.php">{HOME}</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span><a href="index.php?{MODULE_QS}=products_category">{TITLE}</a></span>
                            </li>
                        </ul>
					</div>
                    <!-- END::PAGE BAR -->
                    <!-- BEGIN::PAGE TITLE-->
                    <h3 class="page-title"> {TITLE} </h3>
                    <!-- END::PAGE TITLE-->
                    <!-- END::PAGE HEADER-->
                    <!-- BEGIN::PAGE CONTENT-->
					<div class="row">
                        <div class="col-md-12">
						
						  <form action="" method="post" name="form_edit" id="form_edit" class="form-horizontal">
							<input type="submit" id="subm" name="subm" style="display:none;">
							<input type="hidden" value="" id="action" name="action">
							<input type="hidden" value="0" id="select_id" name="select_id">
							
							<!-- BEGIN toolbar -->
							<table style="display: none;" id="panel_toolbar_zero" width="100%" border="0" cellpadding="0"
								   cellspacing="0">
								<tr>
									<td style="width:300px !important;margin-top:0px;">&nbsp;</td>
									<td>
										<h3 class="panel-title" id="panel-title-zero"></h3>
										<!-- BEGIN perm_add -->
										<button type="button" class="btn blue" onclick="mCMSadd();"><i class="m-fa fa-swapright"></i> {toolbar.ADD}</button>
										<!-- END perm_add -->
									</td>
								</tr>
							</table>

							<table style="display: none;" id="panel_toolbar" width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td style="width:300px !important;margin-top:0px;">&nbsp;</td>
									<td>
										<h3 class="panel-title" id="panel-title"></h3>
										<!-- BEGIN perm_add -->
										<button type="button" class="btn blue" onclick="mCMSadd();"><i class="m-fa fa-swapright"></i> {toolbar.ADD}</button>
										<!-- END perm_add -->
										<!-- BEGIN perm_edit -->
										<button type="button" class="btn green" onclick="mCMSedit();"><i class="fa fa-edit"></i> {toolbar.EDIT}</button>
										<!-- END perm_edit -->
										<!-- BEGIN perm_del -->
										<button type="button" class="btn red" onclick="doAct('delete', '{toolbar.DELETE_CONFIRM}');"><i class="fa fa-remove"></i> {toolbar.DELETE}</button>
										<!-- END perm_del -->
									</td>
								</tr>
							</table>
							<br/>
							<!-- END toolbar -->
							
							<!-- BEGIN mcms_tree -->
                            <div class="portlet">
                                <div class="portlet-body">
                                    <div id="mcms_tree" class="tree-demo" {LEFT_STYLE}> </div>
                                </div>
                            </div>
							<!-- END mcms_tree -->
						  
							<!-- BEGIN editbar -->
							<div style="display: {editbar.SHOW};" id="panel_editbar">

								<div class="form-group form-md-checkboxes">
									<label class="col-md-3 control-label" for="txt_visible">&nbsp;</label>
									<div class="col-md-6">
										<div class="md-checkbox-list">
											<div class="md-checkbox">
												<input type="checkbox" name="txt_visible" value="1" id="txt_visible" class="md-check"  {editbar.VISIBLE_VALUE}>
												<label for="txt_visible">
													<span></span>
													<span class="check"></span>
													<span class="box"></span>{editbar.VISIBLE}
												</label>
											</div>
										</div>
									</div>
								</div>
							
							
								

								<div class="tabbable-line boxless tabbable-reversed">
									<ul class="nav nav-tabs">
										<!-- BEGIN tab -->
										<li{editbar.tab.CLASS}><a href="#content_{editbar.tab.LANG}" data-toggle="tab">{editbar.tab.LANG}</a></li>
										<!-- END tab -->
									</ul>
									
									<div class="tab-content">
										<!-- BEGIN tab -->
										<div class="tab-pane {editbar.tab.FADE_CLASS}" id="content_{editbar.tab.LANG}">
											<div class="portlet box green">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-language"></i>{editbar.tab.LANG} 
												</div>
											</div>
											<div class="portlet-body form">
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_name_{editbar.tab.LANG}">{editbar.tab.NAME}</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="txt_name_{editbar.tab.LANG}"
														   id="txt_name_{editbar.tab.LANG}" value="{editbar.tab.NAME_VALUE}"
														   placeholder="{editbar.tab.NAME}">
													<div class="form-control-focus"> </div>
												</div>
											</div>
											
											<div class="form-group form-md-line-input" id="tr_content_{editbar.tab.LANG}_1">
												<label class="col-md-3 control-label" for="txt_comment_{editbar.tab.LANG}">{editbar.tab.COMMENT}</label>
												<div class="col-md-8">
													<textarea class="form-control" name="txt_comment_{editbar.tab.LANG}"
															  id="txt_comment_{editbar.tab.LANG}" rows="3">{editbar.tab.COMMENT_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											<div class="form-group form-md-line-input" id="tr_content_{editbar.tab.LANG}_2">
												<label class="col-md-3 control-label" for="txt_text_{editbar.tab.LANG}">{editbar.tab.TEXT}</label>
												<div class="col-md-8">
													<textarea class="form-control ckeditor" name="txt_text_{editbar.tab.LANG}"
															  id="txt_text_{editbar.tab.LANG}" rows="3">{editbar.tab.TEXT_VALUE}</textarea>
													<div class="form-control-focus"> </div>
												</div>
											</div>
											
											
											<div class="form-group form-md-line-input">
												<label class="col-md-3 control-label" for="txt_slug_{editbar.tab.LANG}">{editbar.tab.SLUG}</label>
												<div class="col-md-9">
													<div class="input-group">
														<input type="text" class="form-control" name="txt_slug_{editbar.tab.LANG}" id="txt_slug_{editbar.tab.LANG}" value="{editbar.tab.SLUG_VALUE}" placeholder="{editbar.tab.SLUG}">
														<span class="input-group-addon">
															<i class="fa fa-i-cursor"></i>
														</span>
														<div class="form-control-focus"> </div>
													</div>
												</div>
											</div>
											
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="button" class="btn blue" id="btn_save" name="btn_save"
																onClick="doAct('save', '');"><i class="fa fa-ok"></i> {editbar.SAVE}
														</button>
														<button type="reset" class="btn" id="btn_cancel" name="btn_cancel"
																onclick="click_cancel();">{editbar.CANCEL}</button>
													</div>
												</div>
											</div>

											</div>
											</div>
										</div>
										<!-- END tab -->
									</div>
								</div>
							</div>
							<script>defineShowHide({editbar.TYPEINDEX});</script>
							<!-- END editbar -->
							
						  </form>
                        </div>
                    </div>
					<!-- END::PAGE CONTENT-->
                </div>
                <!-- END::CONTENT BODY -->
            </div>
            <!-- END::CONTENT -->
        </div>
        <!-- END::CONTAINER -->
		
		<script src="{ROOT}assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        
		<!-- BEGIN editbar -->
        <!-- BEGIN tab -->
		<script>
			CKEDITOR.replace( 'txt_text_{editbar.tab.LANG}', {
				filebrowserImageBrowseUrl: 'ck.photo.php',
			} );
		</script>
        <!-- END tab -->
        <!-- END editbar -->