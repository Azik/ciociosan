<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta content="noindex, nofollow" name="robots"/>
    <meta charset="utf-8"/>
    <title>{TITLE}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Dashboard" name="description"/>
    <meta content="MicroPHP" name="author"/>
    <link href="{ROOT}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{ROOT}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />    
	<!-- BEGIN::THEME LAYOUT STYLES -->
	<link href="{ROOT}assets/layouts/layout/css/layout.css" rel="stylesheet" type="text/css" />
	<link href="{ROOT}assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
	<link href="{ROOT}assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
	<!-- END::THEME LAYOUT STYLES -->
    <link rel="stylesheet" type="text/css" href="{ROOT}assets/image-crop/css/imgareaselect-default.css"/>
    <link href="{ROOT}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" />

    <script src="{ROOT}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{ROOT}assets/image-crop/scripts/jquery.imgareaselect.pack.js"></script>

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-md">


<!-- BEGIN::HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN::HEADER INNER -->
	<div class="page-header-inner ">
		<!-- BEGIN::LOGO -->
		<div class="page-logo">
			<a href="index.php"><img src="{ROOT}assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /></a>
		</div>
		<!-- END::LOGO -->
	</div>
	<!-- END::HEADER INNER -->
</div>
<!-- END::HEADER -->
<!-- BEGIN::HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END::HEADER & CONTENT DIVIDER -->


<!-- BEGIN::CONTAINER -->
<div class="page-container">
	<!-- BEGIN::CONTENT -->
		<!-- BEGIN::CONTENT BODY -->
		<div class="page-content" style="padding:40px;">
			<!-- BEGIN::PAGE TITLE-->
			<h3 class="page-title"> {CROP_TITLE}</h3>
			<!-- END::PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					
					
				<div class="img_content">
					<br/><br/>

					<div id="img_content">
						
						<!-- BEGIN image_crop -->
						<script type="text/javascript">
							$(document).ready(function () {
								<!-- BEGIN tab -->
								$('#photo_{image_crop.tab.SIZE}').imgAreaSelect({
									x1: {image_crop.tab.X1},
									y1: {image_crop.tab.Y1},
									x2: {image_crop.tab.X2},
									y2: {image_crop.tab.Y2},
									aspectRatio: '{image_crop.tab.W}:{image_crop.tab.H}',
									minWidth:{image_crop.tab.W},
									handles: true,
									onSelectEnd: function (img, selection) {
										$('input[name="img{image_crop.tab.SIZE}_x1"]').val(selection.x1);
										$('input[name="img{image_crop.tab.SIZE}_y1"]').val(selection.y1);
										$('input[name="img{image_crop.tab.SIZE}_x2"]').val(selection.x2);
										$('input[name="img{image_crop.tab.SIZE}_y2"]').val(selection.y2);
									}
								});
								<!-- END tab -->
							});
						</script>


						<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
							<!-- BEGIN tab -->
							<tr>
								<td valign="top" height="18">
									<ul id="myTab" class="nav nav-tabs">
										<li{image_crop.tab.CLASS1}><a data-toggle="tab">{image_crop.tab.SIZE}</a></li>
									</ul>
								</td>
							</tr>
							<tr title="{image_crop.tab.SIZE}">
								<td valign="top" title="{image_crop.tab.SIZE}">
									<img src="{image_crop.IMG}" alt="{image_crop.tab.SIZE}" title="{image_crop.tab.SIZE}"
										 id="photo_{image_crop.tab.SIZE}">
								</td>
							</tr>
							<!-- END tab -->

						</table>

						<br/>

						<form action="{image_crop.ACTION}" method="post">
							<!-- BEGIN tab -->
							<input type="hidden" name="img{image_crop.tab.SIZE}_x1" value="{image_crop.tab.X1}"/>
							<input type="hidden" name="img{image_crop.tab.SIZE}_y1" value="{image_crop.tab.Y1}"/>
							<input type="hidden" name="img{image_crop.tab.SIZE}_x2" value="{image_crop.tab.X2}"/>
							<input type="hidden" name="img{image_crop.tab.SIZE}_y2" value="{image_crop.tab.Y2}"/>
							<!-- END tab -->
							<div class="form-actions">
								<button type="submit" class="btn blue" id="btn_submit" name="btn_submit"><i
											class="fa fa-save"></i> {IMAGES_CROP_SUBMIT}</button>
							</div>
						</form>

						<!-- END image_crop -->

						<!-- BEGIN close -->
						<div style="height:20px"></div>
						<div class="form-actions">
							<button type="button" class="btn blue" id="btn_close" name="btn_close" onclick="self.close()"><i
										class="fa fa-power-off"></i> {IMAGES_CLOSE}</button>
						</div>
						<!-- END close -->
					</div>
				</div>
					
				</div>
			</div>
		</div>
		<!-- END::CONTENT BODY -->
	
	<!-- END::CONTENT -->
</div>
<!-- END::CONTAINER -->


<div style="height:50px"></div>
<!-- BEGIN::FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner"> 
		&copy; {YEAR}
		mCMS | Powered by <a href="http://microphp.com" onclick="window.open(this.href,'_blank');return false;">MicroPHP</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END::FOOTER -->

<div style="height:50px"></div>

</body>
</html>