
    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->

      <div class="page-header">
        <h1>{NAME}</h1>
      </div>
      <div class="well">
        {TEXT}
		{MESSAGE}
		
		<form action="{CONTACTS_URL}" method="post">
		  <input type="hidden" name="teiken" value="{TOKEN}">
		  <div class="form-group">
			<label for="contact_name">{_CONTACT_NAME_}:</label>
			<input id="contact_name" name="contact_name" type="text" class="form-control" placeholder="{_CONTACT_NAME_}">
		  </div>
		  <div class="form-group">
			<label for="contact_email">{_CONTACT_EMAIL_}:</label>
			<input id="contact_email" name="contact_email" type="email" class="form-control" placeholder="{_CONTACT_EMAIL_}">
		  </div>
		  <div class="form-group">
			<label for="contact_subject">{_CONTACT_SUBJECT_}:</label>
			<input id="contact_subject" name="contact_subject" type="text" class="form-control" placeholder="{_CONTACT_SUBJECT_}">
		  </div>
		  <div class="form-group">
			<label for="contact_text">{_CONTACT_TEXT_}:</label>
			<textarea class="form-control" id="contact_text" name="contact_text" ></textarea>
		  </div>
		  <button type="submit" class="btn btn-default">{_CONTACT_SEND_}</button>
		</form>
		
		<!-- BEGIN sub -->
		<div><a href="{sub.URL}" {sub.BLANK}>{sub.NAME}</a></div>
		<!-- END sub -->
      </div>

    </div> <!-- /container -->

