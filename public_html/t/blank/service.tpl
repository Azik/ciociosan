

    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->


      <div class="page-header">
        <h1>{NAME}</h1>
      </div>
	  <!-- BEGIN yes_sub -->
      <div class="row">
        <!-- BEGIN sub -->
		<div class="col-sm-6">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">{yes_sub.sub.NAME}</h3>
            </div>
            <div class="panel-body">
				<img class="example-image" src="{yes_sub.sub.IMG_FILE}" alt=""/>
             {yes_sub.sub.TEXT}
            </div>
          </div>
        </div><!-- /.col-sm-6 -->
		<!-- END sub -->
	  </div>
	  <!-- END yes_sub -->

    </div> <!-- /container -->


