

    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->
		
		<!-- BEGIN news_cat -->
      <div class="page-header">
        <h1>{news_cat.CAT_NAME}</h1>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="list-group">
            <!-- BEGIN items -->
			<a href="{news_cat.items.URL}" class="list-group-item">
              <h4 class="list-group-item-heading">{news_cat.items.NAME}</h4>
			  <strong>{news_cat.items.DATE}</strong>
              <p class="list-group-item-text">{news_cat.items.COMMENT}</p>
            </a>
            <!-- END items -->
          </div>
        </div><!-- /.col-sm-12 -->
      </div>
	  
	  
<!-- BEGIN pages -->
      <div class="row">
        <div class="col-sm-12">

<!-- BEGIN pg -->
<a {news_cat.pages.pg.HREF} class="btn btn-default">{news_cat.pages.pg.NUM}</a>
<!-- END pg -->
        </div><!-- /.col-sm-12 -->
      </div>

<!-- END pages -->

	  
	  <!-- END news_cat -->


    </div> <!-- /container -->


