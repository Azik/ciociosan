<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="{CHARSET}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{TITLE_DESCRIPTION}">
    <meta name="keyword" content="{TITLE_KEYWORD}">
    <meta name="author" content="{AUTHOR}">

    <title>{TITLE_HEADER}{TITLE_SEPERATOR}{TITLE}{TITLE_INSIDE2}</title>

    <!-- Bootstrap core CSS -->
    <link href="{SITE_FOLDER}bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="{SITE_FOLDER}bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{SITE_FOLDER}bootstrap/doc/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{SITE_FOLDER}theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="{SITE_FOLDER}bootstrap/doc/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <!-- BEGIN blocks_35 -->
          <a class="navbar-brand" href="{INDEX_URL}">{blocks_35.NAME}</a>
		  <!-- END blocks_35 -->
        </div>
		
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<!-- BEGIN top_menu -->
            <li 
			<!-- BEGIN curr_class -->
			class="active"
			<!-- END curr_class -->
			 <!-- BEGIN sub -->
			 class="dropdown"
			 <!-- END sub -->
			 >
				<a href="{top_menu.URL}" {top_menu.BLANK}
				<!-- BEGIN sub -->
				class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
				<!-- END sub -->
				>{top_menu.NAME}
				<!-- BEGIN sub -->
				<span class="caret"></span>
				<!-- END sub -->
				</a>
				<!-- BEGIN sub -->
				<ul class="dropdown-menu">
					<!-- BEGIN menu -->
					<li><a href="{top_menu.sub.menu.URL}" {top_menu.sub.menu.BLANK}>{top_menu.sub.menu.NAME}</a></li>
					<!-- END menu -->
				</ul>
				<!-- END sub -->
			</li>
			<!-- END top_menu -->
          </ul>
		  
		  <!-- BEGIN langs -->
			<!-- BEGIN list -->
			<a href="{langs.list.URL}">{langs.list.NAME}</a>
			<!-- END list -->
		  <!-- END langs -->
        </div><!--/.nav-collapse -->
      </div>
    </nav>