
		
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
			  <!-- BEGIN middle_menu -->
              <li><a href="{middle_menu.URL}" {middle_menu.BLANK}>{middle_menu.NAME}</a></li>
			  <!-- END middle_menu -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>

	  
    <div class="container theme-showcase" role="main">
	  <!-- BEGIN slider -->	
      <div class="page-header">
        <h1>Carousel</h1>
      </div>
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <!-- BEGIN items -->
		  <li data-target="#carousel-example-generic" data-slide-to="{slider.items.NDX0}" {slider.items.CLASS_ACTIVE}></li>
          <!-- END items -->
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- BEGIN items -->
		  <div class="item{slider.items.CLASS_VAL_ACTIVE}">
            <a {slider.items.HREF} target="_blank"><img src="{slider.items.IMG_FILE}" alt="{slider.items.TEXT_NOTAG}"></a>
          </div>
		  <!-- END items -->
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
	  <!-- END slider -->	

	  <!-- BEGIN blocks_25 -->
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>{blocks_25.NAME}</h1>
        <p>{blocks_25.INFO_NOTAG}</p>
      </div>
	  <!-- END blocks_25 -->
	  
	  <!-- BEGIN blocks_27 -->
      <div class="page-header">
        <h1>{blocks_27.NAME}</h1>
      </div>
      <div class="well">
        <p>{blocks_27.INFO_NOTAG}</p>
      </div>
	  <!-- END blocks_27 -->

	  
<h2>{_MODULE_SUBSCRIBE_}</h2>
<form action="{SUBSCRIBE_MODULE_FORM_URL}" method="post">
  <div class="form-group">
    <label for="subscribe_name">{_MODULE_SUBSCRIBE_NAME_}:</label>
	<input id="subscribe_name" name="subscribe_name" type="text" class="form-control" placeholder="{_MODULE_SUBSCRIBE_NAME_}">
  </div>
  <div class="form-group">
    <label for="subscribe_email">{_MODULE_SUBSCRIBE_EMAIL_}:</label>
	<input id="subscribe_email" name="subscribe_email" type="email" class="form-control" placeholder="{_MODULE_SUBSCRIBE_EMAIL_}">
  </div>
  <div class="form-group">
    <label for="subscribe_phone">{_MODULE_SUBSCRIBE_PHONE_}:</label>
	<input id="subscribe_phone" name="subscribe_phone" type="text" class="form-control" placeholder="{_MODULE_SUBSCRIBE_PHONE_}">
  </div>
  <button type="submit" class="btn btn-default">{_MODULE_SUBSCRIBE_SEND_}</button>
</form>
    </div> <!-- /container -->


