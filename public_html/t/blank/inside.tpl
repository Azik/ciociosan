
    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->

      <div class="page-header">
        <h1>{NAME}</h1>
      </div>
      <div class="well">
        {TEXT}
		
		<!-- BEGIN sub -->
		<div><a href="{sub.URL}" {sub.BLANK}>{sub.NAME}</a></div>
		<!-- END sub -->
      </div>

    </div> <!-- /container -->

