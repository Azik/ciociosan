	<div id="page-content">
		<br/><br/>
		<div class="container">
			<div class="row">
				<!-- BEGIN blocks_14 -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{blocks_14.NAME}</h3>
					</div>
					<div class="panel-body">
						<p>{blocks_14.INFO_NOTAG}</p>
					</div>
				</div>
				</div>
				<!-- END blocks_14 -->
				<!-- BEGIN blocks_21 -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{blocks_21.NAME}</h3>
					</div>
					<div class="panel-body">
						<ul>
							<!-- BEGIN items -->
							<li><a {blocks_21.items.HREF}><i class="{blocks_21.items.LINK_NAME}"></i> {blocks_21.items.NAME}</a></li>
							<!-- END items -->
						</ul>
					</div>
				</div>
				</div>
				<!-- END blocks_21 -->
				<!-- BEGIN foot_menu -->
				<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{foot_menu.NAME}</h3>
					</div>
					<!-- BEGIN sub -->
					<div class="panel-body">
						<ul>
							<!-- BEGIN menu -->
							<li><a href="{foot_menu.sub.menu.URL}" {foot_menu.sub.menu.BLANK}><i class="{foot_menu.sub.menu.COMMENT}"></i> {foot_menu.sub.menu.NAME}</a></li>
							<!-- END menu -->
						</ul>
					</div>
					<!-- END sub -->
				</div>
				</div>
				<!-- END foot_menu -->
			</div>
		</div>
		<div class="container">
			<div class="row">
			<div class="col-sm-6">
				<p>{SITE_COPYRIGHT}</p>
			</div>
			<div class="col-sm-6 text-right">
			 <!-- BEGIN bottom_menu -->
			 <br/><a href="{bottom_menu.URL}">{bottom_menu.NAME}</a>
			 <!-- END bottom_menu -->
			</div>
			</div>
		</div>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{SITE_FOLDER}bootstrap/doc/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{SITE_FOLDER}bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{SITE_FOLDER}bootstrap/doc/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
