
    <div class="container theme-showcase" role="main">
		<!-- BEGIN where -->
		<ol class="breadcrumb">
		  <!-- BEGIN for -->
		  <li><a href="{where.for.URL}">{where.for.NAME}</a></li>
		  <!-- END for -->
		  <!-- BEGIN end -->
		  <li class="active">{where.end.NAME}</li>
		  <!-- END end -->
		</ol>
		<!-- END where -->

      <div class="page-header">
        <h1>{NAME}</h1>
      </div>
      <div class="well">
        {TEXT}
		
		<!-- BEGIN gallery -->
		<div class="row">
			<article class="box-shadow">
				<div class="art-header">	
					<h2>{gallery.NAME}</h2>
				</div>
				<div class="art-content">
					{gallery.DESC}
					<!-- BEGIN list -->
					<div class="col-md-3">
						<div class="post">
							<div class="item-container">
								<div class="item-caption">
									<div class="item-caption-inner">
										<div class="item-caption-inner1">
											<a class="example-image-link" href="{gallery.list.IMG_X612}" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
											</a>
										</div>
									</div>
								</div>
								<a href="{gallery.list.IMG_X612}" ><img src="{gallery.list.IMG_150X150}" /></a>
							</div>
							{gallery.list.CHECK_150X150}
							{gallery.list.CHECK_X612}
						</div>
					</div>
					<!-- END list -->
				</div>
			</article>
		</div>
		<!-- END gallery -->
		
		<!-- BEGIN sub -->
		<div><a href="{sub.URL}" {sub.BLANK}>{sub.NAME}</a></div>
		<!-- END sub -->
      </div>

    </div> <!-- /container -->

