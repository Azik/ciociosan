<?php

$language['slider']['admin']['title'] = 'Slider Manager';
$language['slider']['admin']['slide_title'] = 'Categories';
$language['slider']['admin']['slide_add'] = 'Add new slider';
$language['slider']['admin']['slide_name'] = 'Name';
$language['slider']['admin']['slide_more_txt'] = 'More text';
$language['slider']['admin']['slide_edit'] = 'Edit';
$language['slider']['admin']['slide_delete'] = 'Delete';
$language['slider']['admin']['slide_confirm'] = 'Are you sure?';
$language['slider']['admin']['slide_active'] = 'Active';
$language['slider']['admin']['slide_text'] = 'Text';
$language['slider']['admin']['slide_embed_code'] = 'Embed code';
$language['slider']['admin']['slide_url'] = 'URL';
$language['slider']['admin']['slide_save'] = 'Save';
$language['slider']['admin']['slide_cancel'] = 'Cancel';
$language['slider']['admin']['slider_active'] = 'Active';
$language['slider']['admin']['slider_inactive'] = 'Blocked';
$language['slider']['admin']['slider_activate'] = 'Activate selected';
$language['slider']['admin']['slider_inactivate'] = 'Block selected';

$language['slider']['admin']['up_down'] = 'Up / Down ';
$language['slider']['admin']['slide_up'] = 'UP';
$language['slider']['admin']['slide_up_name'] = 'UP [name]';
$language['slider']['admin']['slide_down'] = 'DOWN';
$language['slider']['admin']['slide_down_name'] = 'DOWN [name]';

$language['slider']['admin']['slide_img'] = 'Slide Img';
