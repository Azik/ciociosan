<?php

$language['options_site']['admin']['title'] = 'Site Options';
$language['options_site']['admin']['siteoptions'] = 'Site Options';
$language['options_site']['admin']['profile'] = 'Your Profile';
$language['options_site']['admin']['password'] = 'Change Your Password';
$language['options_site']['admin']['save'] = 'Save';
$language['options_site']['admin']['cancel'] = 'Cancel';
$language['options_site']['admin']['charset'] = 'Encoding';
$language['options_site']['admin']['slider'] = 'Show slider';
$language['options_site']['admin']['parallax'] = 'Parallax(One page) site';

$language['options_site']['admin']['meta'] = 'META Tag';
$language['options_site']['admin']['site_name'] = 'Site name';
$language['options_site']['admin']['site_title'] = 'Site title';
$language['options_site']['admin']['meta_keywords'] = 'Keywords';
$language['options_site']['admin']['meta_description'] = 'Description';
$language['options_site']['admin']['meta_email'] = 'E-mail';
$language['options_site']['admin']['meta_author'] = 'Author';
$language['options_site']['admin']['site_copyright'] = 'Site Copyright';

$language['options_site']['admin']['lang'] = 'Languages';
$language['options_site']['admin']['site_lang'] = 'Site Languages';
$language['options_site']['admin']['admin_lang'] = 'Admin Panel Language';
$language['options_site']['admin']['admin_lang_main'] = 'Language';
$language['options_site']['admin']['lang_main'] = 'Main lang';
$language['options_site']['admin']['lang_show'] = 'Show';
$language['options_site']['admin']['lang_lang'] = 'Lang';

$language['options_site']['admin']['old_pass'] = 'Old Password';
$language['options_site']['admin']['new_pass'] = 'New Password';
$language['options_site']['admin']['confirm_new_pass'] = 'Confirm New Password';
$language['options_site']['admin']['passwords_not_match'] = 'Passwords does not match! Please retype password';
$language['options_site']['admin']['pass_success'] = 'Your password has been changed';
$language['options_site']['admin']['pass_incorrect'] = 'Your old password is not correct. Please try again';

$language['options_site']['admin']['email'] = 'E-mail';
$language['options_site']['admin']['name'] = 'Name';
