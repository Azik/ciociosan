<?php

$language['login']['admin']['title'] = 'Login to your account';
$language['login']['admin']['username'] = 'Username';
$language['login']['admin']['password'] = 'Password';
$language['login']['admin']['enter'] = 'Sign In';
$language['login']['admin']['reset'] = 'Reset';
$language['login']['admin']['incorrect'] = 'Invalid username or password!';
$language['login']['admin']['username_request'] = 'Please enter username and password.';

$language['login']['view']['field_email'] = 'E-mail';
$language['login']['view']['field_password'] = 'Password';
$language['login']['view']['logged_ok'] = 'You have successfully logged in';
$language['login']['view']['logged_error'] = 'Error occured while logging in to your account. Please try again!';
