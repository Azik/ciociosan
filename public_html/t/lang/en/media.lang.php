<?php

$language['media']['admin']['title'] = 'media Manager';
$language['media']['admin']['slide_title'] = 'Categories';
$language['media']['admin']['slide_add'] = 'Add new media';
$language['media']['admin']['slide_name'] = 'Name';
$language['media']['admin']['slide_more_txt'] = 'More text';
$language['media']['admin']['slide_edit'] = 'Edit';
$language['media']['admin']['slide_delete'] = 'Delete';
$language['media']['admin']['slide_confirm'] = 'Are you sure?';
$language['media']['admin']['slide_active'] = 'Active';
$language['media']['admin']['slide_text'] = 'Text';
$language['media']['admin']['slide_embed_code'] = 'Embed code';
$language['media']['admin']['slide_url'] = 'URL';
$language['media']['admin']['slide_save'] = 'Save';
$language['media']['admin']['slide_cancel'] = 'Cancel';
$language['media']['admin']['media_active'] = 'Active';
$language['media']['admin']['media_inactive'] = 'Blocked';
$language['media']['admin']['media_activate'] = 'Activate selected';
$language['media']['admin']['media_inactivate'] = 'Block selected';

$language['media']['admin']['up_down'] = 'Up / Down ';
$language['media']['admin']['slide_up'] = 'UP';
$language['media']['admin']['slide_up_name'] = 'UP [name]';
$language['media']['admin']['slide_down'] = 'DOWN';
$language['media']['admin']['slide_down_name'] = 'DOWN [name]';

$language['media']['admin']['slide_img'] = 'Slide Img';
$language['media']['admin']['start_date'] = 'Start date';
$language['media']['admin']['end_date'] = 'End date';
$language['media']['admin']['date_between'] = 'Даты';
