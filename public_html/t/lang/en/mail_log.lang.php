<?php

$language['mail_log']['admin']['title'] = 'Sent mails';

$language['mail_log']['admin']['date'] = 'Date';
$language['mail_log']['admin']['mail_to'] = 'Mail to';
$language['mail_log']['admin']['subject'] = 'Subject';
$language['mail_log']['admin']['mail'] = 'Mail';

$language['mail_log']['admin']['save'] = 'Save';
$language['mail_log']['admin']['cancel'] = 'Cancel';
$language['mail_log']['admin']['cat_title'] = 'mail_log';
$language['mail_log']['admin']['cat_add'] = 'Add category';
$language['mail_log']['admin']['cat_active'] = 'Active&nbsp;&nbsp;&nbsp;';
$language['mail_log']['admin']['cat_index'] = 'Index';
$language['mail_log']['admin']['cat_inactive'] = 'Blocked';
$language['mail_log']['admin']['cat_activate'] = 'Activate selected';
$language['mail_log']['admin']['cat_inactivate'] = 'Block selected';
$language['mail_log']['admin']['cat_name'] = 'Name';
$language['mail_log']['admin']['cat_imgid'] = 'Img id';
$language['mail_log']['admin']['cat_header'] = 'Description';
$language['mail_log']['admin']['cat_slug'] = 'Slug';
$language['mail_log']['admin']['cat_position'] = 'Position';
$language['mail_log']['admin']['cat_edit'] = 'Edit';
$language['mail_log']['admin']['cat_delete'] = 'Delete';
$language['mail_log']['admin']['cat_confirm'] = 'Are you sure?';
$language['mail_log']['admin']['cat_save'] = 'Save';
$language['mail_log']['admin']['cat_cancel'] = 'Cancel';


$language['mail_log']['admin']['email'] = 'E-mail';
$language['mail_log']['admin']['email_subject'] = 'Subject';
$language['mail_log']['admin']['log_time'] = 'Time';
$language['mail_log']['admin']['add'] = 'Add New User';
$language['mail_log']['admin']['edit'] = 'Edit';
$language['mail_log']['admin']['delete'] = 'Delete';

?>