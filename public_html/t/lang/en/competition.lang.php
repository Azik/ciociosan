<?php

$language['competition']['admin']['title'] = 'competition Manager';
$language['competition']['admin']['slide_title'] = 'Categories';
$language['competition']['admin']['slide_add'] = 'Add new competition';
$language['competition']['admin']['slide_name'] = 'Name';
$language['competition']['admin']['slide_more_txt'] = 'More text';
$language['competition']['admin']['slide_edit'] = 'Edit';
$language['competition']['admin']['slide_delete'] = 'Delete';
$language['competition']['admin']['slide_confirm'] = 'Are you sure?';
$language['competition']['admin']['slide_active'] = 'Active';
$language['competition']['admin']['slide_text'] = 'Text';
$language['competition']['admin']['slide_embed_code'] = 'Embed code';
$language['competition']['admin']['slide_url'] = 'URL';
$language['competition']['admin']['slide_save'] = 'Save';
$language['competition']['admin']['slide_cancel'] = 'Cancel';
$language['competition']['admin']['competition_active'] = 'Active';
$language['competition']['admin']['competition_inactive'] = 'Blocked';
$language['competition']['admin']['competition_activate'] = 'Activate selected';
$language['competition']['admin']['competition_inactivate'] = 'Block selected';

$language['competition']['admin']['up_down'] = 'Up / Down ';
$language['competition']['admin']['slide_up'] = 'UP';
$language['competition']['admin']['slide_up_name'] = 'UP [name]';
$language['competition']['admin']['slide_down'] = 'DOWN';
$language['competition']['admin']['slide_down_name'] = 'DOWN [name]';

$language['competition']['admin']['slide_img'] = 'Slide Img';
$language['competition']['admin']['start_date'] = 'Start date';
$language['competition']['admin']['end_date'] = 'End date';
$language['competition']['admin']['date_between'] = 'Даты';
