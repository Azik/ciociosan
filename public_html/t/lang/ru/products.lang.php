<?php

$language['products']['admin']['title'] = 'Məhsulların idarəetmə sistemi';
$language['products']['admin']['products_title'] = 'Categories';
$language['products']['admin']['products_add'] = 'Yeni məhsul';
$language['products']['admin']['products_name'] = 'Ad';
$language['products']['admin']['products_more_txt'] = 'Qısa təsvir';
$language['products']['admin']['products_edit'] = 'Düzəliş';
$language['products']['admin']['products_delete'] = 'Sil';
$language['products']['admin']['products_confirm'] = 'Silməyə əminsiniz?';
$language['products']['admin']['products_active'] = 'Aktiv';
$language['products']['admin']['products_text'] = 'Mətn';
$language['products']['admin']['products_url'] = 'Sayt';
$language['products']['admin']['products_save'] = 'Yadda saxla';
$language['products']['admin']['products_cancel'] = 'Ləğv et';
$language['products']['admin']['products_active'] = 'Aktiv';
$language['products']['admin']['products_inactive'] = 'Deaktiv';
$language['products']['admin']['products_activate'] = 'Seçilənlər aktiv et';
$language['products']['admin']['products_inactivate'] = 'Seçilənlər deaktiv et';

$language['products']['admin']['up_down'] = 'Up / Down ';
$language['products']['admin']['products_up'] = 'UP';
$language['products']['admin']['products_up_name'] = 'UP [name]';
$language['products']['admin']['products_down'] = 'DOWN';
$language['products']['admin']['products_down_name'] = 'DOWN [name]';

$language['products']['admin']['products_img'] = 'Şəkil';
