<?php

$language['member_iddia_gonder']['admin']['title'] = 'Mağazaların idarəetmə sistemi';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_title'] = 'Categories';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_add'] = 'Yeni mağaza';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_name'] = 'Ad';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_more_txt'] = 'Qısa təsvir';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_edit'] = 'Düzəliş';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_delete'] = 'Sil';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_confirm'] = 'Silməyə əminsiniz?';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_active'] = 'Aktiv';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_text'] = 'Mətn';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_url'] = 'Sayt';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_save'] = 'Yadda saxla';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_cancel'] = 'Ləğv et';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_active'] = 'Aktiv';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_inactive'] = 'Deaktiv';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_activate'] = 'Seçilənlər aktiv et';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_inactivate'] = 'Seçilənlər deaktiv et';

$language['member_iddia_gonder']['admin']['up_down'] = 'Up / Down ';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_up'] = 'UP';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_up_name'] = 'UP [name]';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_down'] = 'DOWN';
$language['member_iddia_gonder']['admin']['member_iddia_gonder_down_name'] = 'DOWN [name]';

$language['member_iddia_gonder']['admin']['member_iddia_gonder_img'] = 'Şəkil';
