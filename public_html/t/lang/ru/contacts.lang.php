<?php

$language['contacts']['admin']['title'] = 'Contacts';

$language['contacts']['admin']['mail_to'] = 'Mail to';
$language['contacts']['admin']['mail_from'] = 'Mail from';
$language['contacts']['admin']['mail_user'] = 'Mail user';
$language['contacts']['admin']['mail_pass'] = 'Mail pass';
$language['contacts']['admin']['mail_pop3'] = 'Mail pop3';
$language['contacts']['admin']['mail_smtp'] = 'Mail smtp';
$language['contacts']['admin']['mail_host'] = 'Mail host';
$language['contacts']['admin']['save'] = 'Save';
$language['contacts']['admin']['cancel'] = 'Cancel';

$language['contacts']['admin']['siteoptions'] = 'Site Options';
$language['contacts']['admin']['profile'] = 'Your Profile';
$language['contacts']['admin']['password'] = 'Change Your Password';
$language['contacts']['admin']['charset'] = 'Encoding';

$language['contacts']['admin']['meta'] = 'META Tag';
$language['contacts']['admin']['site_name'] = 'Site name';
$language['contacts']['admin']['site_title'] = 'Site title';
$language['contacts']['admin']['meta_keywords'] = 'Keywords';
$language['contacts']['admin']['meta_description'] = 'Description';
$language['contacts']['admin']['meta_email'] = 'E-mail';
$language['contacts']['admin']['meta_author'] = 'Author';

$language['contacts']['admin']['lang'] = 'Languages';
$language['contacts']['admin']['site_lang'] = 'Site Languages';
$language['contacts']['admin']['admin_lang'] = 'Admin Panel Language';
$language['contacts']['admin']['admin_lang_main'] = 'Language';
$language['contacts']['admin']['lang_main'] = 'Main lang';
$language['contacts']['admin']['lang_show'] = 'Show';
$language['contacts']['admin']['lang_lang'] = 'Lang';

$language['contacts']['admin']['old_pass'] = 'Old Password';
$language['contacts']['admin']['new_pass'] = 'New Password';
$language['contacts']['admin']['confirm_new_pass'] = 'Confirm New Password';
$language['contacts']['admin']['passwords_not_match'] = 'Passwords does not match! Please retype password';
$language['contacts']['admin']['pass_success'] = 'Your password has been changed';
$language['contacts']['admin']['pass_incorrect'] = 'Your old password is not correct. Please try again';

$language['contacts']['admin']['email'] = 'E-mail';
$language['contacts']['admin']['name'] = 'Name';
