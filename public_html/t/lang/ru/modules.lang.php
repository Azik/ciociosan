<?php

$language['modules']['admin']['title'] = 'Module Manager';
$language['modules']['admin']['add'] = 'Add New Module';
$language['modules']['admin']['name'] = 'Name';
$language['modules']['admin']['module_title'] = 'Title';
$language['modules']['admin']['module_seo_title'] = 'SEO Title';
$language['modules']['admin']['description'] = 'Description';
$language['modules']['admin']['icon'] = 'Icon';
$language['modules']['admin']['edit'] = 'Edit';
$language['modules']['admin']['modules_active'] = 'Active';
$language['modules']['admin']['modules_admin'] = 'Have admin';
$language['modules']['admin']['modules_view'] = 'Have view';
$language['modules']['admin']['modules_change'] = 'Change';
$language['modules']['admin']['modules_remove'] = 'Remove';
$language['modules']['admin']['modules_select_file'] = 'Select file';
$language['modules']['admin']['active'] = 'Status';
$language['modules']['admin']['active_0'] = 'Inactive';
$language['modules']['admin']['active_1'] = 'Active';
$language['modules']['admin']['operations'] = 'Operations';
$language['modules']['admin']['edit_view'] = 'Edit view module';
$language['modules']['admin']['edit_view_template'] = 'Edit view template';
$language['modules']['admin']['edit_admin'] = 'Edit admin module';
$language['modules']['admin']['edit_admin_template'] = 'Edit admin template';
$language['modules']['admin']['edit_language'] = 'Edit language file';
$language['modules']['admin']['edit_cnfg'] = 'Edit module configuration';
$language['modules']['admin']['edit_seo'] = 'Edit SEO configuration';
$language['modules']['admin']['delete'] = 'Delete';
$language['modules']['admin']['delete_confirm'] = 'Are you sure?';
$language['modules']['admin']['save'] = 'Save';
$language['modules']['admin']['cancel'] = 'Cancel';
$language['modules']['admin']['lang_file'] = 'Language File';
$language['modules']['admin']['module_file'] = 'Module File';
$language['modules']['admin']['view_template_file'] = 'View Template';
$language['modules']['admin']['file'] = 'Module Zip File';
$language['modules']['admin']['install'] = 'Install';
$language['modules']['admin']['module_exist'] = 'Sorry, this module is already installed.';
$language['modules']['admin']['invalid_file'] = 'Sorry, this module file is not valid file.';
$language['modules']['admin']['invalid_file_type'] = 'Sorry, this file is not valid module (Please upload .zip file).';
$language['modules']['admin']['module_activate'] = 'Activate selected';
$language['modules']['admin']['module_inactivate'] = 'Block selected';
$language['modules']['admin']['seo_table'] = 'SEO Table';
$language['modules']['admin']['table_id'] = 'Table ID';
$language['modules']['admin']['title_header'] = 'Title header';
$language['modules']['admin']['title_description'] = 'Title description';
$language['modules']['admin']['title_keywords'] = 'Title keywords';
