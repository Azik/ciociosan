<?php
/******************* ajax.register.php *******************
 *
 *
 ******************** ajax.register.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxRegister extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        
		if ($this->member->IsLogin()) {
            return;
        }
		
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'register':
                    $this->Register();
				break;
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }

	
	/**
     * Register 	
     */
    public function Register()
    {
		$red_url = '';
		$this->messages = array();
		$this->isSuccess = true;
		$reg_name = trim($this->utils->UserPost('reg_name'));
		$reg_lastname = trim($this->utils->UserPost('reg_lastname'));
		$reg_email = trim($this->utils->UserPost('reg_email'));
		$reg_pass = trim($this->utils->UserPost('reg_pass'));
		$reg_pass2 = trim($this->utils->UserPost('reg_pass2'));
		
		$captcha = $this->utils->UserPost('g-recaptcha-response');

	//	$phone = $this->utils->getSession('phone');
		if(strlen($reg_name) < 3){
			$this->isSuccess = false;
			$this->messages['reg_name']['code']='ERR06';
			$this->messages['reg_name']['message']='Adı minimum 3 simvol ola bilər';
		}	

		if(strlen($reg_lastname) < 3){
			$this->isSuccess = false;
			$this->messages['reg_lastname']['code']='ERR05';
			$this->messages['reg_lastname']['message']='Soyad minimum 3 simvol ola bilər';
		}	
		/*
		if(strlen($captcha) <= 0){
			$this->isSuccess = false;
			$this->messages['reg_captcha']['code']='ERR07';
			$this->messages['reg_captcha']['message']='Təhlükəsizlik kodu yalnışdır';
		}
		$secretKey = "6LdIC8UZAAAAAGL2waNkPMb7zS4U6UbvewVZ2B9c";
        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response,true);
		
		
		if(strlen($responseKeys["success"]) == 0) {
			$this->isSuccess = false;
			$this->messages['reg_captcha']['code']='ERR08';
			$this->messages['reg_captcha']['message']='Təhlükəsizlik kodu yalnışdır';
        }*/
		
		if(strlen($reg_pass) < 5){
			$this->isSuccess = false;
			$this->messages['reg_pass']['code']='ERR01';
			$this->messages['reg_pass']['message']='Şifrə minimum 5 simvol olmalıdır';
		}	

		if($reg_pass != $reg_pass2){
			$this->isSuccess = false;
			$this->messages['reg_pass2']['code']='ERR04';
			$this->messages['reg_pass2']['message']='Təkrar şifrə düzgün daxil edilməyib';
		}

		if(!$this->utils->isValidEmail($reg_email)){
			$this->isSuccess = false;
			$this->messages['reg_email']['code']='ERR02';
			$this->messages['reg_email']['message']='E-poçt düzgün daxil edilməyib';
		}
		
		$user_row = $this->member->ExistUserbyMail($reg_email);
		if(count($user_row) > 0) {
			$this->isSuccess = false;
			$this->messages['reg_email']['code']='ERR03';
			$this->messages['reg_email']['message']=$reg_email. ' e-poçt adresi ilə qeydiyyat mövcuddur';
		}
		if($this->isSuccess) {
			/* B: OK */
			
			//$activate_info = $this->utils->generateString(60);
			$add_date = time();
			$activate_info = 'notactive_' . $add_date;
			$data['oauth_provider'] = 'site';
			$data['activate_info'] = $activate_info;
			$data['active'] = 0;
			$data['m_name'] = $reg_name .' '. $reg_lastname;
			$data['m_user'] = $reg_email;
			$data['m_mail'] = $reg_email;
			$data['m_pass'] = trim(SHA1($reg_pass));

			$member_id = $this->member->CreateMember($data);	

			$activatedata = sha1("reg" . $member_id . 'u' . $activate_info);
			$activate_link = $this->fromConfig('siteUrl') . "/" . str_replace('[activatedata]', $activatedata, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_register_activate'][$this->curr_lang]));

			
			$this->messages['success']['code']='SUCCESS01';
			$this->messages['success']['message']= 'Qeydiyyat uğurla tamamlandı. E-poçtunuza gələn təsdiq linkinə daxil olaraq hesabınızı aktivləşdirin.';
			
			$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
			$this->isSuccess = true;
		

			$email = 'abbasov.azik@gmail.com';
			$subscribe_subject = 'Test Subject';
			$Pother = 'activate link: '.$activate_link;

			$this->sendMailQueue($email, $subscribe_subject, $Pother);
		
		
		}
		
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->messages;
		die(json_encode($this->response));
	
    }
	
}

$index = new ajaxRegister();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.register.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.register.php ******************/;