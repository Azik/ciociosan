<?php
/******************* file_img.php *******************
 *
 * Image file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** file_img.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class fileImage extends \mcms5xx\classes\ViewPage
{
	/** 
	* Image width 
	*/
    public $w = 0;
	
	/** 
	* Image height 
	*/
    public $h = 0;

    public function __construct()
    {
        parent::__construct();
        $this->w = $this->fromConfig('imgResize');
    }

    public function onLoad()
    {
        //@header('Content-Type: image/jpeg');
        //@header('Content-Type: image/png');
        $thumb_img = '';
        $imgid = $this->utils->UserGetInt('i');
        $images_query = 'SELECT * FROM ' . $this->db->prefix . "files WHERE (`category`='image') &&  (`fileid`=" . $imgid . ')';
        //echo($images_query);
        $images_result = $this->db->query($images_query);
        if ($images_row = $this->db->fetch($images_result)) {
            $imgid = $images_row['fileid'];
            $thumb_limit = $this->fromConfig('thumb_limit');
            $imgSizes = $this->fromConfig('imgSizes');
            $width = $this->utils->UserGetInt('w');
            $height = $this->utils->UserGetInt('h');

            $filename = $images_row['filename'];
            $dir = $this->fromConfig('upload_folder') . 'image/';
            $dir = $this->io->dateFolder($dir, $images_row['add_time']) . '/';
            $img_file = $dir . $filename;
            if (strlen($img_file) > 10) {
                //echo "A";
                list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);
                $folder = $this->io->dateFolder('img/file_img/', $images_row['add_time']);

                if (($width > 0) && ($height == 0)) {
                    $thumb_img = $folder . '/' . $imgid . '_' . $width . 'xx.jpg';
                    $calcP = array($original_width, $width);
                    $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                    $height = round(($original_height / $calc_p) * 100, 4);
                    $src_w = round(($width / 100) * $calc_p, 4);
                    $src_h = round(($height / 100) * $calc_p, 4);
                }
                if (($width == 0) && ($height > 0)) {
                    $thumb_img = $folder . '/' . $imgid . '_xx' . $height . '.jpg';
                    $calcP = array($original_height, $height);
                    $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                    $width = round(($original_width / $calc_p) * 100, 4);
                    $src_w = round(($width / 100) * $calc_p, 4);
                    $src_h = round(($height / 100) * $calc_p, 4);
                }
                if (($width > 0) && ($height > 0)) {
                    $thumb_img = $folder . '/' . $imgid . '_' . $width . 'x' . $height . '.jpg';
                    //$calcP = array($original_width, $this->w);
                    $calcP = ($original_width > $original_height) ? array($original_height, $height) : array($original_width, $width);
                    $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                    $src_w = round(($width / 100) * $calc_p, 4);
                    $src_h = round(($height / 100) * $calc_p, 4);
                }
                //echo $sz.': '.$x1.'x'.$y1.' - '.$x2.'x'.$y2.'<br/>';
                if ((!@is_file($thumb_img)) || (@filesize($thumb_img) < $thumb_limit)) {
                    $crop = false;
                    $sz = $width . 'x' . $height;
                    //echo $sz;

                    /* $pos = $this->utils->UserGetInt('pos');
                    $pos = ( ($pos <= 0) || ($pos > count($imgSizes) ) ) ? 0 : ($pos-1);
                    $sz = $imgSizes[$pos]; */
                    list($imgW, $imgH) = explode('x', $sz);
                    $x1 = 0;
                    $y1 = 0;
                    $x2 = $imgW;
                    $y2 = $imgH;
                    $sql = 'SELECT *
						FROM `' . $this->db->prefix . 'files_image_sizes`
						WHERE 
							(`imgid` = ' . $imgid . ")
						 && (`size`= '" . $sz . "')
						";
                    $result = $this->db->query($sql);
                    if ($row = $this->db->fetch($result)) {
                        $crop = true;
                        $x1 = $row['x1'];
                        $y1 = $row['y1'];
                        $x2 = $row['x2'];
                        $y2 = $row['y2'];
                    }

                    if ($crop) {
                        // If Crop
                        if ($this->w >= $original_width) {
                            $this->w = $original_width;
                            $this->h = $original_height;
                            $new_w = $imgW;
                            $new_h = $imgH;
                        } else {
                            $calcP = array($original_width, $this->w);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $this->w = round(($original_width / $calc_p) * 100, 4);
                            $this->h = round(($original_height / $calc_p) * 100, 4);
                            $x1 = ($x1 == 0) ? $x1 : round(($x1 / 100) * $calc_p, 4);
                            $y1 = ($y1 == 0) ? $y1 : round(($y1 / 100) * $calc_p, 4);
                            $x2 = ($x2 == 0) ? $x2 : round(($x2 / 100) * $calc_p, 4);
                            $y2 = ($y2 == 0) ? $y2 : round(($y2 / 100) * $calc_p, 4);
                            $new_w = round(($imgW / 100) * $calc_p, 4);
                            $new_h = round(($imgH / 100) * $calc_p, 4);
                        }
                        $imgW = ($this->w > $imgW) ? $imgW : $this->w;
                        $imgH = ($this->h > $imgH) ? $imgH : $this->h;
                        $x2 = ($this->w > $x2) ? $x2 : $this->w;
                        $y2 = ($this->h > $y2) ? $y2 : $this->h;
                    } else {
                        $calcP = array($original_width, $this->w);
                        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                        $this->w = round(($original_width / $calc_p) * 100, 4);
                        $this->h = round(($original_height / $calc_p) * 100, 4);
                        $new_w = $src_w;
                        $new_h = $src_h;
                    }
                    /* echo 'IMG:'.$thumb_img.'<br/>';
                    echo 'img_file:'.$img_file.'<br/>';
                    echo 'img:'.$imgW.'='.$imgH.'<br/>';
                    echo 'Resized:'.$this->w.'='.$this->h.'<br/>';
                    echo 'New WxH:'.$new_w.'='.$new_h.'<br/>';
                    echo 'Original:'.$original_width.'='.$original_height.'<br/>';
                    echo $calc_p.'<br/>'; //Resizedi Bol 100-e, vur calc_p-e`
                    echo $sz.': '.$x1.'x'.$y1.' - '.$x2.'x'.$y2.'<br/>'; */

                    $create = 'jpg';
                    $extension = $images_row['extension'];
                    switch ($extension) {
                        case 'jpg':
                        case 'jpeg': {
                            $image_p = imagecreatetruecolor($imgW, $imgH);
                            $image = imagecreatefromjpeg($img_file);
                            break;
                        }
                        case 'gif': {
                            $image_p = imagecreatetruecolor($imgW, $imgH);
                            $image = imagecreatefromgif($img_file);
                            break;
                        }
                        case 'png': {
                            //$image_p = imagecreate($imgW, $imgH);
                            $image_p = imagecreatetruecolor($imgW, $imgH);
                            $red = imagecolorallocate($image_p, 255, 0, 0);
                            $background_color = imagecolorallocate($image_p, 255, 255, 255);

                            imagecolortransparent($image_p, $red);

                            $create = 'png';
                            $image = imagecreatefrompng($img_file);
                            break;
                        }
                        default: {
                            $image_p = imagecreatetruecolor($imgW, $imgH);
                            $image = imagecreatefromjpeg($img_file);
                            break;
                        }
                    }

                    imagecopyresampled($image_p, $image, 0, 0, $x1, $y1, $imgW, $imgH, $new_w, $new_h);

                    //@header('Content-Type: image/jpeg');
                    //@imagejpeg($image_p, '', 100);
                    /* @header('Content-Type: image/png');
                     * imagepng($image_p); */

                    switch ($create) {
                        case 'png': {
                            $thumb_img = str_replace('.jpg', '.png', $thumb_img);
                            imagepng($image_p, $thumb_img);
                            @chmod($thumb_img, 0767);
                            @imagedestroy($image_p);
                            break;
                        }
                        default: {
                            @header('Content-Type: image/jpeg');
                            @imagejpeg($image_p, $thumb_img, 100);
                            @chmod($thumb_img, 0767);
                            @imagedestroy($image_p);
                            break;
                        }
                    }
                }

                @header('location: ' . $thumb_img);
            }
        }
    }
}

$image = new \mcms5xx\fileImage();
$image->onLoad();

/******************* file_img.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** file_img.php ******************/;
