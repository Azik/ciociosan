<?php
/******************* coupon.class.php *******************
 *
 * User class
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** coupon.class.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\classes;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/**
 * Coupon class
 */
class Coupon
{
    /**
     * Utilites class
     */
    public $utils;
	
	/**
     * Member class
     */
    public $member;

    /**
     * Database class
     */
    public $db;

    /**
     * parrent
     */
    public $parent;

    /**
     * Default module
     */
    public $def_module;

    /**
     * User type string
     */
    public $usertype_str = '';

    /**
     * Permission string (length = 5)
     * full: 0,1;
     * del: 0,1;
     * edit: 0,1;
     * add: 0,1;
     * view: 0,1;
     */
    public $perm_string = '00000';

    /**
     * Class constructor.
     *
     * @param $parent
     * @param $utils - Utilites class
     * @param $db   - Database class
     */
    public function __construct($parent, $utils, $member, $db)
    {
        $this->parent = $parent;
        $this->utils = $utils;
        $this->member = $member;
        $this->db = $db;
        $this->OnLoad();
    }

    /**
     * Configure default options for class.
     */
    private function OnLoad()
    {
        $this->def_module = $this->utils->getFromConfig('admin_def_module');
    }



    /**
     * get All Coupons
     */
	public function getCoupons($status = 'inprogress', $PageCount = 50, $iddiaLimit=50){
		
		
		$data = array();
		$coupons = array();
        $page = $this->utils->UserGetInt('page');

		$PageCount = $PageCount;
		if($page > 0)
			$PageCount = $page;

		$limitTo = $iddiaLimit; 
		$limitFrom = ($iddiaLimit * $PageCount) - $limitTo;
		
		// && (N.expiredate>=" . time() . ")
		
		$sql = 'SELECT C.* FROM
			' . $this->db->prefix . "coupons C
			WHERE 
				(C.status='".$status."') && (C.userid='".$this->member->GetUserId()."')
			ORDER BY C.createdate ASC
			";
			
			
		$total_items = $this->db->num_rows($sql);	
		//$sql .= $this->db->get_limit($limitFrom, $limitTo);;
		
		/* B: Page limits */
			
		//	echo '<pre>';
		//	echo $sql."<br/>"; 
		$rows = $this->db->fetch_all($sql);
		$news = array();
		foreach($rows as $row) {
			$coupons[] = $row;
		}
		
	
		$data['list'] = $coupons;
		$data['total_items'] = $total_items;
		return $data;
	}

	
    /**
     * change bal
     *
     */
    public function ChangeBal($member_id, $cp_bal, $mod) {
		$uInfo = $this->GetUser($member_id);
		
		$new_bal = $uInfo['m_bal'];
		if($mod == 'up')
			$new_bal = (double)$uInfo['m_bal'] + (double)$cp_bal;

		if($mod == 'down')
			$new_bal = (double)$uInfo['m_bal'] - (double)$cp_bal;
		
		$dataUpdate = array();
		$dataUpdate['m_bal'] = $new_bal;
		$this->db->update($this->db->prefix.'members' , $dataUpdate, " member_id=".$member_id."");
		return true;
	}
	
    /**
     * check it the user exist
     *
     * @param $email
     * @param $pass
     */
    public function ExistUser($email, $pass) {
	
		$row = array();
		$chk_query = "SELECT * FROM `" . $this->db->prefix . "members` WHERE (upper(`m_mail`) = '" . strtoupper($email) . "') AND (`m_pass` = '" . SHA1($pass) . "') AND (`active` = 1) ";
		//echo $chk_query; exit;
		$chk_result = $this->db->query($chk_query);
		if ($chk_row = $this->db->fetch($chk_result)) {
			
			$row = $chk_row;
		}
		return $row;
	}	
	
    /**
     * check it the user exist
     *
     * @param $email
     */
    public function ExistUserbyMail($email) {
	
		$row = array();
		$chk_query = "SELECT * FROM `" . $this->db->prefix . "members` WHERE (upper(`m_mail`) = '" . strtoupper($email) . "') ";
		//echo $chk_query; exit;
		$chk_result = $this->db->query($chk_query);
		if ($chk_row = $this->db->fetch($chk_result)) {
			
			$row = $chk_row;
		}
		return $row;
	}	
	
	
    /**
     * Check if logged
     */
    public function IsLogin()
    {
        $login = false;
        if ($this->utils->GetSession('username') != '') {
            $login = true;
        }

        return $login;
    }

    /**
     * get user type by index id
     *
     * @param $index
     */
    public function GetUserTypeOptions($index = 3)
    {
        if ($this->usertype_str == '') {
            $this->usertype_str = '1:'.$this->fromLangIndex('usertype_superadmin').';2:'.$this->fromLangIndex('usertype_admin').';3:'.$this->fromLangIndex('usertype_manager');
        }

        $options = '';

        $types = explode(';', $this->usertype_str);
        for ($i = 0; $i < count($types); ++$i) {
            $parts = explode(':', $types[$i]);
            $id = $parts[0];
            if (!$this->IsInType(1) && ($id == 1 || $id == 2)) {
                continue;
            }
            if ($id == $index) {
                $options .= '<option value="'.$id.'" selected>'.$parts[1].'</option>';
            } else {
                $options .= '<option value="'.$id.'">'.$parts[1].'</option>';
            }
        }

        return $options;
    }

    /**
     * Define language value from index.lang.php file.
     *
     * @param string $key
     * @param string $sub1
     * @param string $sub2
     */
    public function fromLangIndex($key, $sub1 = '', $sub2 = '')
    {
        return $this->utils->GetFromLangCommon('index', 'admin', $key, $sub1, $sub2);
    }

    /**
     * is in Type
     *
     * @param $index
     */
    public function IsInType($index)
    {
        $type = $this->GetUserType();

        return $type == $index;
    }

    /**
     * get use type from session
     */
    public function GetUserType()
    {
        return $this->utils->GetSession('usertype');
    }


    /**
     * get current user type
     */
    public function GetCurrentUserTypeText()
    {
        $index = $this->GetUserType();

        if ($index != 0) {
            return $this->GetUserTypeText($index);
        } else {
            return $this->fromLangIndex('guest');
        }
    }

    /**
     * get user type
     *
     * @param $index
     */
    public function GetUserTypeText($index)
    {
        if ($this->usertype_str == '') {
            $this->usertype_str = '1:'.$this->fromLangIndex('usertype_superadmin').';2:'.$this->fromLangIndex('usertype_admin').';3:'.$this->fromLangIndex('usertype_manager');
        }

        $usertype = '';
        $types = explode(';', $this->usertype_str);
        for ($i = 0; $i < count($types); ++$i) {
            $parts = explode(':', $types[$i]);
            if ($parts[0] == $index) {
                $usertype = $parts[1];
                break;
            }
        }

        return $usertype;
    }

    /**
     * get user info
     *
     * @param integer $member_id
     */
    public function GetUser($member_id)
    {
        $query = 'SELECT * FROM `'.$this->db->prefix."members` WHERE `member_id`='".intval($member_id)."'";
		
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            return $row;
        }

        return null;
    }

    /**
     * get user name
     *
     * @param integer $userid
     */
    public function GetUserName($userid = 0)
    {
        if ($userid === 0) {
            return $this->utils->GetSession('username');
        } elseif ($userid == '') {
            return '';
        } else {
            $query = 'SELECT * FROM `'.$this->db->prefix."users` WHERE `userid` = '".$userid."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                return $row['username'];
            }

            return '';
        }
    }

   
    /**
     * get user ID
     *
     * @param $username
     */
    public function GetUserIdByUserName($username)
    {
        $userid = '';

        // check it the user exist
        $query = 'Select * from `'.$this->db->prefix."users` where `username`='".$username."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $userid = $row['userid'];
        }

        return $userid;
    }

    /**
     * current user hase permission for current module or not
     *
     * @param $module
     */
    public function hasPermission($module)
    {
        $is_right = false;
        $result = $this->db->query('SELECT * FROM `'.$this->db->prefix."modules` WHERE (`have_admin`=1) && (`name`='".$module."') ");
        //$mdl_nums = $this->db->num_rows("SELECT * FROM " . $this->db->prefix . "modules WHERE (`name`='" . $module . "') ");
        $mdl_nums = $this->db->num_rows('SELECT * FROM `'.$this->db->prefix."modules` WHERE (`have_admin`=1) && (`name`='".$module."') ");
        if ($row = $this->db->fetch($result)) {
            $moduleid = $row['moduleid'];
            $type = $row['type'];
            $active = $row['active'];
            if ($active == 1) {
                switch ($type) {
                    case 1: {
                        if ($this->IsInType(1)) {
                            $is_right = true;
                            $this->perm_string = '11111';
                        }
                        break;
                    }
                    case 2: {
                        if ($this->IsInTypes('1,2')) {
                            $is_right = true;
                            $this->perm_string = '11111';
                        }
                        break;
                    }
                    default: {
                        $userid = $this->GetUserId();
                        $perms = $this->GetUserPermissions($userid);
                        //if ($this->IsInTypes("1,2") || array_key_exists($moduleid, $perms))
                        if ($this->IsInTypes('1,2') || (array_key_exists($moduleid, $perms) && ($perms[$moduleid] > 0))) {
                            $is_right = true;
                            $this->perm_string = @$perms[$moduleid];
                            $this->perm_string = ($this->IsInTypes('1,2')) ? '11111' : @$perms[$moduleid];
                            $this->perm_string = (strlen(@$this->perm_string) == 5) ? @$this->perm_string : '00001';
                        }
                        break;
                    }
                }
            }
        } elseif (($mdl_nums > 0) || ($module == $this->def_module) || ($module == 'login') || ($module == 'change_status') || ($module == 'action') || ($module == 'help')) {
            $is_right = true;
        }

        return $is_right;
    }

    /**
     * is in type
     *
     * @param $indexes
     */
    public function IsInTypes($indexes)
    {
        $isin = false;
        $arr = explode(',', $indexes);
        $type = $this->GetUserType();
        for ($i = 0; $i < count($arr); ++$i) {
            if ($type == $arr[$i]) {
                $isin = true;
                break;
            }
        }

        return $isin;
    }

    /**
     * get current user ID from session
     */
    public function GetUserId()
    {
        return $this->utils->filterInt($this->utils->GetSession('UID'));
    }

    /**
     * get user permissions
     *
     * @param $userid
     */
    public function GetUserPermissions($userid)
    {
        $arr = array();
        $arr[] = 0;

        if ($userid == '') {
            return $arr;
        }
        $usertype = 3;
        $perm_str = '';
        $username = '';
        $query = 'SELECT * FROM '.$this->db->prefix."users WHERE userid='".$userid."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $perm_str = $row['permissions'];
            $username = $row['username'];
            $usertype = $row['usertype'];
        }
        if ($perm_str != '') {
            $perm_arr = explode(';', $perm_str);
            $arr = array();
            foreach ($perm_arr as $perm_txt) {
                $perm_txtArr = explode('::', $perm_txt);
                @$perm_txtArr['1'] = ($usertype == 3) ? @$perm_txtArr['1'] : '11111';
                @$perm_txtArr['1'] = (strlen(@$perm_txtArr['1']) == 5) ? @$perm_txtArr['1'] : '00001';
                $arr[$perm_txtArr['0']] = @$perm_txtArr['1'];
            }
        }
        $arr['username'] = $username;

        return $arr;
    }

    /**
     * Log User operations
     *
     * @param integer $user_id
     * @param string $module_name
     * @param integer $moduleid
     * @param string $operation_mode
     */
    public function logOperation($user_id = 0, $module_name = '', $moduleid = 0, $operation_mode = '')
    {
        $ip = $this->utils->GetIP();
        $comp = $this->utils->GetComp();
        $page = $this->utils->txt_request_filter_user($_SERVER['QUERY_STRING']);
        $query = 'INSERT INTO `'.$this->db->prefix.'logs`(`logsid`, `user_id`, `ip`, `comp`, `date_int`, `module_name`, `moduleid`, `operation_mode`, `page`)
					VALUES(NULL, ' .$user_id.", '".$ip."', '".$comp."', ".time().", '".$module_name."', ".$moduleid.", '".$operation_mode."', '".$page."')";
        $this->db->query($query);
    }
}

/******************* coupon.class.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** coupon.class.php ******************/;
