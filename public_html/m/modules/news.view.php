<?php
/******************* news.view.php *******************
 *
 * News view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once 'm/classes/paging.class.php';

class News extends \mcms5xx\classes\ViewPage
{
    public $news_template = 'news';
    protected $catslug = 0;
    protected $catid = 0;
    protected $url = 0;
    protected $page = 0;
    protected $sort = 0;
    protected $urlQuery = array();

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->catslug = $this->utils->Get('slug');
		$this->url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[slug]', $this->catslug, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang]));
		$newsSortList = $this->fromConfig('newsSortList');

		$this->page = $this->utils->UserGetInt('page');
		$this->sort = $this->utils->Get('sort');
		if(in_array($this->sort, $newsSortList))				
			$this->urlQuery['sort'] = $this->sort;
		
		if($this->page > 0)
			$this->urlQuery['page'] = $this->page;
		

        $this->buildPage();
        $this->builNewsNavigation();
		$this->buildNewsSortList();
        $this->buildNewsItems();
    }

    private function buildPage()
    {
        $this->buildMenu();
        
    }
	
	private function buildNewsSortList()
	{
		$newsSortList = $this->fromConfig('newsSortList');

		$this->template->assign_block_vars('sort', array());
		$url = "";
		
		foreach($newsSortList as $key => $value) {
			
			$this->urlQuery['sort'] = $key;
			$this->template->assign_block_vars('sort.list', array(
					'KEY' => $key,
					'VALUE' => $value,
					'URL' => $this->getURL(),
			));
		}
	

	}
	
	private function getURL() {
		$url = http_build_query( $this->urlQuery );
		return $this->url."?".$url;
	}
	
	
    private function builNewsNavigation()
    {
        /* B: Navigation bar */
        $this->get_nav(0);
        if (strlen($this->catslug) > 0) {
            $cat_query = 'SELECT C.*, CL.name as cat_name FROM
			' . $this->db->prefix . 'newscategories C
			INNER JOIN ' . $this->db->prefix . "newscategorylocalizations CL on (CL.catid = C.catid)
			WHERE (C.active='1') && (CL.lang='" . $this->lang . "') && (CL.slug = '" . $this->catslug . "')
			";
            $cat_result = $this->db->query($cat_query);
            if ($cat_row = $this->db->fetch($cat_result)) {
                $cat_name = $cat_row['cat_name'];
                $cat_id = $cat_row['catid'];
				$this->catid = $cat_id;
                $this->template->assign_block_vars('where.for', array(
                    'NAME' => $this->fromLangIndex('news_archive'),
                    'SPACE' => $this->fromLangIndex('where_space'),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
                ));
                $this->template->assign_block_vars('where.end', array(
                    'NAME' => $cat_name,
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[catid]', $cat_id, $this->permalinks[$this->perma_type]['news_cat'][$this->curr_lang])),
                ));
            } else {
                $this->template->assign_block_vars('where.end', array(
                    'NAME' => $this->fromLangIndex('news_archive'),
                    'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
                ));
            }
        } else {
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $this->fromLangIndex('news_archive'),
                'URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_news'][$this->curr_lang]),
            ));
        }
        /* E: Navigation bar */
        $this->template->assign_vars(array(
            'NAME' => $this->fromLangIndex('news_archive'),
        ));
    }

    private function buildNewsItems()
    {
        $news_index_limit = $this->fromConfig('news_index_limit');
        $news = $this->AllNews($this->catid, 1, 'news');
    }
	
	
	
	private function AllNews($catid, $PageCount, $template){
		$news_limit = $this->fromConfig('news_index_limit');
		$data = $this->news($catid, $PageCount, $news_limit);
		$list = $data['list'];
		$ndx = 0;
		foreach($list as $item) {
			++$ndx;
			
			$expired_status = ($item['is_expired']) ? '.is_expired' : '.is_not_expired';

			if ($ndx == 1) 
                $this->template->assign_block_vars($template, array());

			$this->template->assign_block_vars($template.'.items', array(
				'ID' => $item['id'],
				'NAME' => $item['name'],
				'IMG_380X280' => $item['img_380x280'],
				'DURATION' => $item['duration'],
				'URL' => $item['url'],
			));
			

			$this->template->assign_block_vars($template.'.items'.$expired_status, array(
				'TOTAL' => $item['iddia']['count'],
				'YES' => $item['iddia']['yes'],
				'NO' => $item['iddia']['no']
			));				
		
		}


		if(count($list) > 0) {
        $page_limit = $this->fromConfig('news_index_limit');
		$all_pages = ceil($data['total_items'] / $page_limit);
		$page = $this->page;
        $page = (($page <= 0) || ($page > $all_pages)) ? 1 : $page;
        $page_val = ((($page + 1) <= 0) || (($page + 1) > $all_pages)) ? 1 : ($page + 1);
		
		if ($all_pages > 1) {
			$this->template->assign_block_vars($template.'.pages', array());
			for ($p = 1; $p <= $all_pages; ++$p) {
				
				$this->urlQuery['page'] = $p;
				$pg_url = $this->getURL();
				$pg_href = ($p == $page) ? '' : ' href="' . $pg_url . '"';
				$curr_class = ($p == $page) ? 'active' : '';
				$pgArr = array(
					'NUM' => $p,
					'URL' => $pg_url,
					'HREF' => $pg_href,
					'CURR_CLASS' => $curr_class,
				);
				$this->template->assign_block_vars($template.'.pages.pg', $pgArr);
				
				if($curr_class != ''){
					$this->template->assign_block_vars($template.'.pages.pg.is_curr', array());
				}
				else {
					$this->template->assign_block_vars($template.'.pages.pg.is_not_curr', array());
				}
			}
			$arr = $this->getPagingArray($data['total_items']);
			
			if($arr['PREV_PAGE']){
				$this->urlQuery['page'] = $arr['PREV_PAGE'];
				$url = $this->getURL();
				$this->template->assign_block_vars($template.'.pages.is_yes_prev_page', array(
					'PREV_PAGE' => $url,
				));
			}

			if($arr['NEXT_PAGE']){
				$this->urlQuery['page'] = $arr['NEXT_PAGE'];
				$url = $this->getURL();
				$this->template->assign_block_vars($template.'.pages.is_yes_next_page', array(
					'NEXT_PAGE' => $url,
				));
			}
//			echo '<pre>';
//			print_r($arr);
//			exit;
			}
		}
		


		return $data;
	}
	
	
    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('news_index_limit');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;
        return $arr;
    }
}

$news = new News();
$news->template->pparse($news->news_template);

/******************* news.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news.view.php ******************/;
