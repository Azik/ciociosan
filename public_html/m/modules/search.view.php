<?php
/******************* search.view.php *******************
 *
 * Search view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** search.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Search extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
        $this->template->assign_block_vars('where.end', array(
            'NAME' => $this->fromLangIndex('search'),
            'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['search'][$this->curr_lang]),
        ));
        $steiken = $this->utils->UserPost('steiken');
        $searchtoken = $this->utils->dataFullFilter($_SESSION['stoken']);

        //echo '<br/><br/><br/><br/><br/>'.$search_text.": ".$steiken."==".$searchtoken."<br/>";
        $search_text = $this->utils->UserSearchPost('search_text');
        if ((strlen($search_text) > 1) && (strlen($steiken) > 5) && ($steiken == $searchtoken)) {
            //if (strlen($search_text)>1) {
            $query = 'SELECT S.*
					FROM `' .$this->db->prefix."search` S
					WHERE 
						(S.`lang`='" .$this->lang."')
					 && (
						(S.`name` like '" .$search_text."%')
					 || (S.`name` like '% " .$search_text."%')
					 || (S.`text` like '" .$search_text."%')
					 || (S.`text` like '% " .$search_text."%')
					 || (S.`text` like '%" .$search_text." %')
						)
					 ";
            //echo($query);
            if ($this->db->num_rows($query) <= 0) {
                $this->template->assign_block_vars('error', array(
                    'MESSAGE' => $this->fromLang('no_result'),
                ));
            } else {
                $this->template->assign_block_vars('search_result', array(
                    'SEARCH_KEY' => $this->fromLang('search_key'),
                    'KEY' => $search_text,
                ));
                $result = $this->db->query($query);
                while ($row = $this->db->fetch($result)) {
                    $url = '';
                    switch ($row['pg']) {
                        case 'inside_lang': {
                            $url = $this->curr_folder.str_replace('[slug]', $this->utils->url_filter($row['slug']), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $row['id'], str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['inside'][$this->curr_lang]))));
                            break;
                        }
                        case 'news_lang': {
                            $url = $this->curr_folder.str_replace('[year]', date('Y', $row['date']), str_replace('[slug]', $this->utils->url_filter($row['slug']), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $row['id'], str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
                            break;
                        }
                        case 'device_info_lang': {
                            $url = $this->curr_folder.str_replace('[deviceid]', $row['id'], str_replace('[catid]', $row['catid'], str_replace('[year]', date('Y', $row['date']), str_replace('[slug]', $this->utils->url_filter($row['slug']), str_replace('[name]', $this->utils->url_filter($row['name']), str_replace('[id]', $row['id'], str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['device_info'][$this->curr_lang])))))));
                            break;
                        }

                    }

                    $this->template->assign_block_vars('search', array(
                        'NAME' => $row['name'],
                        'TEXT' => $this->utils->q_txt($row['text']),
                        'URL' => $url,
                    ));
                    $this->template->assign_block_vars('search_result.items', array(
                        'NAME' => $row['name'],
                        'TEXT' => $this->utils->q_txt($row['text']),
                        'URL' => $url,
                    ));
                }
            }
        } else {
            $this->template->assign_block_vars('error', array(
                'MESSAGE' => $this->fromLang('text_result'),
            ));
        }
    }
}

$search = new Search();
$search->template->pparse('inside');

/******************* search.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** search.view.php ******************/;
