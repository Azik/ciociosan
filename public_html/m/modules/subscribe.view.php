<?php
/******************* subscribe.view.php *******************
 *
 * Subscription view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** subscribe.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Subscribe extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->sendForm();
        }
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
        $query = 'SELECT M.mid, M.parentid, M.htaccessname, ML.*
				FROM `' .$this->db->prefix.'menu` M
				LEFT OUTER JOIN `' .$this->db->prefix."menulocalizations` ML ON M.mid = ML.mid
				WHERE ML.lang='" .$this->lang."' AND M.htaccessname='subscribe'";

        $header = $comment = '';
        $name = $this->fromLangIndex('module_subscribe');
        $text = '';

        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $name = $row['name'];
            $text = $this->site->f_tags($row['text']);
            $text = $this->site->replaceImageTemplate($text, $this->curr_folder);
            $text = str_replace('[alt]', $name, $text);
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $name,
                'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe'][$this->curr_lang]),
            ));
        } else {
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $this->fromLangIndex('module_subscribe'),
                'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe'][$this->curr_lang]),
            ));
        }

        $this->template->assign_var('NAME', $name);
        $this->template->assign_var('HEADER_UP', strtoupper($header));
        $this->template->assign_var('HEADER', $header);
        $this->template->assign_var('TEXT', $text);

        $tp = $this->utils->UserGet('tp');
		switch ($tp) {
			case 'ok':{
				$this->template->assign_var('MESSAGE', $this->fromLangIndex('module_subscribe_sended'));
				$this->template->assign_block_vars('ok', array(
					'MESSAGE' => $this->fromLangIndex('module_subscribe_sended'),
				));
				break;
			}
			case 'error':{
				$this->template->assign_var('MESSAGE', '<span style="color:#f00">'.$this->fromLangIndex('module_subscribe_send_error').'</span>');
				$this->template->assign_block_vars('error', array(
					'MESSAGE' => $this->fromLangIndex('module_subscribe_send_error'),
				));
				break;
			}
		}
    }

    private function sendForm()
    {
        $subscribe_name = $this->utils->UserPost('subscribe_name');
        $subscribe_email = $this->utils->UserPost('subscribe_email');
        $subscribe_phone = $this->utils->UserPost('subscribe_phone');
        if ((strlen($subscribe_name) > 1) && (strlen($subscribe_email) > 5)) {
            $sid = $this->db->find_id($this->db->prefix.'subscribe');
            $add_date = time();
            $vadd_query = 'INSERT INTO '.$this->db->prefix.'subscribe(`sid`, `add_date`, `name`, `email`, `phone`, `lang`, `active` ) 
						VALUES(' .$sid.', '.$add_date.", '".$subscribe_name."', '".$subscribe_email."', '".$subscribe_phone."', '".$this->lang."', 1)";
            if ($this->db->query($vadd_query)) {
                $unsubscribe_link = $this->db->site_host.'/'.str_replace('[slug]', sha1($sid.$add_date.$subscribe_email), str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['unsubscribe'][$this->curr_lang]));
                $Pother = str_replace('[unsubscribe_link]', $unsubscribe_link, str_replace('[full_name_value]', $subscribe_name, str_replace('[email_value]', $subscribe_email, str_replace('[phone_value]', $subscribe_phone, $this->fromLangIndex('subscribe_body')))));

                if (strlen($subscribe_email) > 5) {
                    $this->sendMailQueue($subscribe_email, $this->fromLangIndex('subscribe_subject'), $Pother);
                }

                $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe_ok'][$this->curr_lang]);
                @header('location: '.$red_url);
                $this->utils->Redirect($red_url);
                exit();
            } else {
                /* B: error */
                $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe_error'][$this->curr_lang]);
                //echo "BBB".$red_url;
                @header('location: '.$red_url);
                $this->utils->Redirect($red_url);
                exit();
                /* E: error */
            }
        } else {
            /* B: error */
            $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_subscribe_error'][$this->curr_lang]);
            @header('location: '.$red_url);
            $this->utils->Redirect($red_url);
            exit();
            /* E: error */
        }
    }
}

$subscribe = new Subscribe();
$subscribe->template->pparse('subscribe');

/******************* subscribe.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** subscribe.view.php ******************/;
