<?php
/******************* unsubscribe.cls.php *******************
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** unsubscribe.cls.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

/*
 *  Use $this->template->assign_vars() and so, to show on all pages (header, footer and so.)
 */

/******************* unsubscribe.cls.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** unsubscribe.cls.php ******************/;
