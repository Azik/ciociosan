<?php
/******************* register.view.php *******************
 *
 * register view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** register.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class registerView extends \mcms5xx\classes\ViewPage
{
    public $page_template = 'register';
	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        if ($this->member->IsLogin()) {
			$this->utils->Redirect($this->fromConfig('siteUrl'));
			exit;
        }
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
		$this->processActivate();
		/*
        $this->template->assign_block_vars('where.for', array(
            'NAME' => "Profilim",
            'SPACE' => $this->fromLangIndex('where_space'),
            'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_register'][$this->curr_lang]),
        ));
		*/

		
		$this->template->assign_block_vars('where.end', array(
            'NAME' => "Qeydiyyat təsdiqi",
		));
//        $this->template->assign_var('BAL', $userInfo['m_bal']);

        
    }
	
	private function processActivate() {
		
                $id = $this->utils->UserGet('id');
                $query = "SELECT * FROM `" . $this->db->prefix . "members` WHERE SHA1(CONCAT('reg', `member_id`, 'u', `activate_info`)) = '" . $id . "'";
                $result = $this->db->query($query);
                if ($row = $this->db->fetch($result)) {
					
                    if ($row['active'] == 0) {
						$dataUpdate  = array();
						$dataUpdate['activate_info'] = '';
						$dataUpdate['active'] = 1;
						$this->db->update($this->db->prefix.'members' , $dataUpdate, " member_id=".$row['member_id']."");

						$this->utils->SetSession($this->session_logged, 9);
						$this->utils->SetSession($this->session_userid, $row['member_id']);
						$this->utils->SetSession($this->session_usertype, 'site');
						$this->utils->Redirect($this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['home'][$this->curr_lang]));
						exit;
                        $this->template->assign_block_vars('ok', array(
                            'TITLE' => $this->fromLang('reg_activate'),
                            'INFO' => $this->fromLang('reg_ok_activation'),
                        ));
                    } else {
                        $this->template->assign_block_vars('error', array(
                            'TITLE' => $this->fromLang('reg_activate'),
                            'INFO' => $this->fromLang('reg_status_active'),
                        ));
						$this->utils->Redirect($this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['home'][$this->curr_lang]));
						exit;
                    }
                } else {
                    $this->template->assign_block_vars('error', array(
                        'TITLE' => $this->fromLang('reg_activate'),
                        'INFO' => $this->fromLang('reg_error_activation'),
                    ));
                }
		
	}
	

}

$register = new registerView();
$register->template->pparse($register->page_template);

/******************* register.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** register.view.php ******************/;
