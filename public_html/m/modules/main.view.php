<?php
/******************* main.view.php *******************
 *
 * index, main page view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** main.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once 'm/classes/paging.class.php';

class mainView extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
		/*
echo '<pre>';
print_r($_GET);
exit;		*/
        $this->buildPage();
    }

    private function buildPage()
    {
		
		if(!isset($_GET['catid']))
			$_GET['catid'] = 298;
		//298
		//echo '<pre>';
		//print_r($_GET);
		//exit; 
//		if(isset($_GET['idProductCatsArr']))
//		print_r($_GET['idProductCatsArr']); 
		$this->buildMainPrCats('product_cats');
		$this->buildProductsSubMenu('product_sub_cats');

        $this->buildMain();

        $this->buildTitle();
        
        $this->buildMenu();
    }

    private function buildMain()
    {
		$this->template->assign_vars(array(
            'MAIN_INDEX' => $this->fromLangIndex('main_index'),
            'TEST_URL' => $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_competition'][$this->curr_lang]),
        ));
				
        /*if ($this->getKey('slider', '1') == 1) {
            $this->slider();
        }*/
		
		$news = $this->buildCatProducts();
		
		
    }


}

$main = new mainView();
$main->template->pparse('main');

/******************* main.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** main.view.php ******************/;
