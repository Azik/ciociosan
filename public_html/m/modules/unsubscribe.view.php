<?php
/******************* unsubscribe.view.php *******************
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** unsubscribe.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if class included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class unSubscribe extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->cnfg = parent::callClass('unsubscribe_cls');
        $this->onLoad();
    }

    public function onLoad()
    {
        $this->buildPage();

        $tp = $this->utils->UserGet('tp');
        switch ($tp) {
            case 'ok': {
                $this->template->assign_var('MESSAGE', $this->fromLangIndex('unsubscribe_ok'));
                $this->template->assign_block_vars('ok', array(
                    'MESSAGE' => $this->fromLangIndex('unsubscribe_ok'),
                ));
                break;
            }
            case 'error': {
                $this->template->assign_var('MESSAGE', '<span style="color:#f00">'.$this->fromLangIndex('unsubscribe_error').'</span>');
                $this->template->assign_block_vars('error', array(
                    'MESSAGE' => $this->fromLangIndex('unsubscribe_error'),
                ));
                break;
            }
            default:{
                $slug = $this->utils->UserGet('slug');
                $cat_zero_query = 'SELECT * FROM `'.$this->db->prefix."subscribe` WHERE (SHA1(concat(`sid`, `add_date`, `email`)) = '".$slug."') ";
                $cat_zero_result = $this->db->query($cat_zero_query);
                if ($cat_zero_row = $this->db->fetch($cat_zero_result)) {
                    $del_sql = 'DELETE FROM `'.$this->db->prefix.'subscribe` WHERE `sid` = '.$cat_zero_row['sid'];
                    if ($this->db->query($del_sql)) {
                        $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->cnfg->permalinks[$this->perma_type]['unsubscribe_ok'][$this->curr_lang]);
                        //echo "AAA".$red_url;
                        @header('location: '.$red_url);
                        $this->utils->Redirect($red_url);
                        exit();
                    } else {
                        $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->cnfg->permalinks[$this->perma_type]['unsubscribe_error'][$this->curr_lang]);
                        //echo "AAA".$red_url;exit();
                        @header('location: '.$red_url);
                        $this->utils->Redirect($red_url);
                        exit();
                    }
                } else {
                    $red_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->cnfg->permalinks[$this->perma_type]['unsubscribe_error'][$this->curr_lang]);
                    //echo "AAA".$red_url;
                    @header('location: '.$red_url);
                    $this->utils->Redirect($red_url);
                    exit();
                }
                break;
            }
        }
    }

    public function buildPage()
    {
        $this->template->assign_var('NAME',  $this->fromLangIndex('unsubscribe_title'));
        $this->template->assign_var('NAME_UP',  $this->nameup($this->fromLangIndex('unsubscribe_title')));
    }
}

$unsubscribe = new unSubscribe();
$unsubscribe->template->set_filenames(array('unubscribe' => 'unubscribe.tpl'));
$unsubscribe->template->pparse('unubscribe');

/******************* unsubscribe.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** unsubscribe.view.php ******************/;
