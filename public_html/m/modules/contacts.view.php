<?php
/******************* contacts.view.php *******************
 *
 * Contacts view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** contacts.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class contactsView extends \mcms5xx\classes\ViewPage
{
    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
        //if ( (@$_SERVER['REQUEST_METHOD']=='POST') && (strpos(@$_SERVER['HTTP_REFERER'], 'contacts')!==false) ) $this->sendForm();
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->sendForm();
        } 
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
        $this->generateToken();
        $query = 'SELECT 
					M.mid, 
					M.parentid, 
					M.htaccessname,
					ML.*
				FROM `' .$this->db->prefix.'menu` M
				LEFT OUTER JOIN `' .$this->db->prefix."menulocalizations` ML ON M.mid = ML.mid
				WHERE 
				    ML.lang='" .$this->lang."' 
				AND M.htaccessname='contacts' ";
				
        $header = $comment = '';
        $name = $this->fromLangIndex('error');
        $text = $this->fromLangIndex('error_info');

        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $name = $row['name'];
            $comment = $row['comment'];
			$header = $row['header'];
            $text = $this->site->f_tags($row['text']);
            $text = $this->site->replaceImageTemplate($text, $this->curr_folder);
            $text = str_replace('[alt]', $name, $text);
            $this->template->assign_block_vars('where.end', array(
                'NAME' => $name,
                'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_contacts'][$this->curr_lang]),
            ));
        }

        $this->template->assign_var('NAME', $name);
        $this->template->assign_var('NAME_UP', $this->nameup($name));
        $this->template->assign_var('HEADER_UP', $this->nameup($header));
        $this->template->assign_var('HEADER', $header);
        $this->template->assign_var('TEXT', $text);
        $this->template->assign_var('COMMENT', $comment);

        $tp = $this->utils->UserGet('tp');
        switch ($tp) {
			case 'ok':{
				$this->template->assign_var('MESSAGE', $this->fromLangIndex('contact_sended'));
				$this->template->assign_block_vars('ok', array(
					'MESSAGE' => $this->fromLangIndex('contact_sended'),
				));
				break;
			}
			case 'error':{
				$this->template->assign_var('MESSAGE', '<span style="color:#f00">'.$this->fromLangIndex('contact_send_error').'</span>');
				$this->template->assign_block_vars('error', array(
					'MESSAGE' => $this->fromLangIndex('contact_send_error'),
				));
				break;
			}
			default:{
				$this->template->assign_block_vars('contact_info', array());
				break;
			}
		}
    }

    private function sendForm()
    {
        $contact_rate = $this->utils->UserPost('contact_rate');
        $contact_phone = $this->utils->UserPost('contact_phone');
        $contact_name = $this->utils->UserPost('contact_name');
		
        if ((strlen($contact_rate) > 0) && (strlen($contact_name) > 1) && (strlen($contact_phone) > 1) ) {

			$dataInsert = array();
			$dataInsert['phone'] = $contact_phone;
			$dataInsert['name'] = $contact_name;
			$dataInsert['rate'] = $contact_rate;
			$dataInsert['create_date'] = time();
			$insert_id = $this->db->insert($this->db->prefix.'feedback' , $dataInsert);

            $contacts_ok_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['contacts_ok'][$this->curr_lang]);
            @header('location: '.$contacts_ok_url);
            $this->utils->Redirect($contacts_ok_url);
            exit();
        } else {
            /* B: error */
            $contacts_error_url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['contacts_error'][$this->curr_lang]);
            @header('location: '.$contacts_error_url);
            $this->utils->Redirect($contacts_error_url);
            exit();
            /* E: error */
        }
    }
}

$contacts = new contactsView();
$contacts->template->pparse('contacts');

/******************* contacts.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** contacts.view.php ******************/;
