<?php
/******************* member_dashboard.view.php *******************
 *
 * member_dashboard view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** member_dashboard.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class member_dashboardView extends \mcms5xx\classes\ViewPage
{
    public $page_template = 'member_dashboard';

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        if (!$this->member->IsLogin()) {
			$this->utils->Redirect('index.php');
			exit;
        }
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
		$this->getMenus();
		/*
        $this->template->assign_block_vars('where.for', array(
            'NAME' => "Profilim",
            'SPACE' => $this->fromLangIndex('where_space'),
            'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_dashboard'][$this->curr_lang]),
        ));
		*/

		
		$this->template->assign_block_vars('where.end', array(
            'NAME' => "Profilim",
		));

		$userInfo = $this->member->GetUser($this->utils->GetSession('member_id'));

        $this->template->assign_var('USERNAME', $userInfo['m_name']);
        $this->template->assign_var('BAL', $userInfo['m_bal']);
/*        if ($header > 0) {
            $this->template->assign_block_vars('have_image', array());
        }
*/

        
    }
	
	private function getMenus(){
		$subMenus = $this->member->getSubMenus();
		$this->template->assign_block_vars('menus', array());
		foreach($subMenus as $menu){
			$active = ($this->module == $menu['module']) ? 'active' : '';
			$url = $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_'.$menu['module']][$this->curr_lang]);
			$this->template->assign_block_vars('menus.items', array(
				'ICON' => $menu['icon'],
				'TITLE' => $menu['title'],
				'MODULE' => $menu['module'],
				'ACTIVE' => $active,
				'URL' => $url,
			));
		}
	}
}

$member_dashboard = new member_dashboardView();
$member_dashboard->template->pparse($member_dashboard->page_template);

/******************* member_dashboard.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** member_dashboard.view.php ******************/;
