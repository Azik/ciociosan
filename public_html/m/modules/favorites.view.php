<?php
/******************* favorites.view.php *******************
 *
 * favorites view module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** favorites.view.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class favoritesView extends \mcms5xx\classes\ViewPage
{
    public $page_template = 'favorites';

    public function __construct()
    {
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildPage();
    }

    private function buildPage()
    {
        $this->buildMenu();
        $this->get_nav(0);
	
		/*
        $this->template->assign_block_vars('where.for', array(
            'NAME' => "Profilim",
            'SPACE' => $this->fromLangIndex('where_space'),
            'URL' => $this->curr_folder.str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_favorites'][$this->curr_lang]),
        ));
		*/

		
		$this->template->assign_block_vars('where.end', array(
            'NAME' => "Favourites",
		));

		$userInfo = $this->member->GetUser($this->utils->GetSession('member_id'));

		$favorites = $this->utils->getCookie('favorites');
		$favorites = json_decode($favorites, true);
		$this->template->assign_block_vars('products', array());

		$news_idx = 0;
		foreach($favorites as $favorite){
			$productInfo = $this->getProductById($favorite['pid']);
			++$news_idx;
			$this->buildProductInfo('products.items', $productInfo, $news_idx, count($productInfo));
			
		}
		//print_r($favorites);
		

/*        if ($header > 0) {
            $this->template->assign_block_vars('have_image', array());
        }
*/

        
    }
	
}

$favorites = new favoritesView();
$favorites->template->pparse($favorites->page_template);

/******************* favorites.view.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** favorites.view.php ******************/;
