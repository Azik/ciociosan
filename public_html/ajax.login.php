<?php
/******************* ajax.login.php *******************
 *
 *
 ******************** ajax.login.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxLogin extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        
		if ($this->member->IsLogin()) {
            return;
        }
		
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'login':
                    $this->Login();
				break;
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }

	
	/**
     * Login 	
     */
    private function Login()
    {
		$red_url = '';
		$this->errors = array();
		$this->isSuccess = true;
		$lg_email = $this->utils->UserPost('lg_email');
		$lg_pass = $this->utils->UserPost('lg_pass');

	//	$phone = $this->utils->getSession('phone');
		if(strlen($lg_pass) < 5){
			$this->isSuccess = false;
			$this->errors['lg_pass']['code']='ERR01';
			$this->errors['lg_pass']['message']='Şifrə minimum 5 simvol olmalıdır';
		}

		if(!$this->utils->isValidEmail($lg_email)){
			$this->isSuccess = false;
			$this->errors['lg_email']['code']='ERR02';
			$this->errors['lg_email']['message']='E-poçt düzgün daxil edilməyib';
		}
		
		$user_row = $this->member->ExistUser($lg_email, $lg_pass);
		if(count($user_row) == 0) {
			$this->isSuccess = false;
			$this->errors['lg_email']['code']='ERR03';
			$this->errors['lg_email']['message']='Belə istifadəçi yoxdur';
		}
		
		if($this->isSuccess) {
			/* B: OK */
			
			$this->utils->SetSession($this->session_logged, 9);
			$this->utils->SetSession($this->session_userid, $user_row['member_id']);
			$this->utils->SetSession($this->session_usertype, 'site');
			$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
			$this->isSuccess = true;
			$this->errors	= array(); 
			
		}
		
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
}

$index = new ajaxLogin();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.login.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.login.php ******************/;