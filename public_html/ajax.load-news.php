<?php
/******************* ajax.load-news.php *******************
 *
 *
 ******************** ajax.load-news.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';
require_once 'm/classes/paging.class.php';

/**
 * Login members
 */
class ajaxLoadNews extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'load':
					$this->buildLoadNews();
				break;
              
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }
	
	/**
     * load news 	
     */
    private function buildLoadNews()
    {
		$news_limit = $this->fromConfig('news_index_limit');
		$step = $this->utils->UserPostInt('step');
		$cats = array();
		$this->isSuccess = false;
		$data = $this->AllNews(0, $step);
		
		$this->response['items'] = $data;
		
		$arr = $this->getPagingArray($this->getAllNewsCount(0));

		if($step < $arr['TOTAL_PAGES']) 
			$this->response['finish'] = false;
		else 
			$this->response['finish'] = true;		
		
		die(json_encode($this->response));
	
    }
	

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('news_index_limit');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;
        return $arr;
    }
	
	private function AllNews($catid, $PageCount){
	
		$data = $this->news($catid, $PageCount, $this->fromConfig('news_index_limit'));
		$list = $data['list'];
		$ndx = 0;
		foreach($list as $item) {
			++$ndx;
			
			$expired_status = ($item['is_expired']) ? '.is_expired' : '.is_not_expired';

			$this->template->assign_block_vars('items', array(
				'ID' => $item['id'],
				'NAME' => $item['name'],
				'IMG_380X280' => $item['img_380x280'],
				'DURATION' => $item['duration'],
				'URL' => $item['url'],
			));
			

			$this->template->assign_block_vars('items'.$expired_status, array(
				'TOTAL' => $item['iddia']['count'],
				'YES' => $item['iddia']['yes'],
				'NO' => $item['iddia']['no']
			));				
		
			if($item['duration'])
				$this->template->assign_block_vars('items.duration_yes', array());				
			else 	
				$this->template->assign_block_vars('items.'.$item['status'], array());				

		}
		
		$data = $this->generateResult('section_load_news');
	
		return $data;
	}	
	
	private function generateResult($tpl)
    {
        $this->template->set_filenames(array($tpl => ''.$tpl.'.tpl'));
        $return_string = $this->template->pparse($tpl, true);
        return $return_string;
    }
}

$index = new ajaxLoadNews();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.load-news.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.load-news.php ******************/;