<?php
/******************* ajax.categories.php *******************
 *
 *
 ******************** ajax.categories.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxcategories extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $response = array();
    public $isSuccess = true;

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'categories':
					$this->buildCategories();
				break;
              
            }


		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }
	
	/**
     * categories 	
     */
    private function buildCategories()
    {
		$red_url = '';
		$cats = array();
		$this->isSuccess = false;
		$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['index'][$this->curr_lang]);
	
		
		/* All OK */
             $sql = 'SELECT 
            S.*, SL.name
				FROM '.$this->db->prefix.'testscategories S
				INNER JOIN ' .$this->db->prefix."testscategorylocalizations SL ON SL.catid = S.catid
				WHERE SL.lang='" .$this->lang."' && S.active = 1
				ORDER BY S.point DESC
			";
			$result = $this->db->query($sql);
			$ndx = 0;
			$slide_nums = $this->db->num_rows($sql);
    
			if($slide_nums > 0)
				$this->isSuccess = true;

			while ($row = $this->db->fetch($result)) {
					
				$id = $row['catid'];
				$color = $row['color'];
				$point = $row['point'];
				$name = $row['name'];

				$cats[$ndx]['id'] = $id;
				$cats[$ndx]['color'] = $color;
				$cats[$ndx]['point'] = $point;
				$cats[$ndx]['name'] = $name;
				
				++$ndx;	
			}
		
		
		$this->response['categories'] = $cats;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
	
	
}

$index = new ajaxcategories();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.categories.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.categories.php ******************/;