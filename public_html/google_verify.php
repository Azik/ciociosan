<?php
/******************* news_rss.php *******************
 *
 * News RSS file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news_rss.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class GoogleVerify extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    /**
     * Build page
     */
    private function buildPage()
    {
       
	   
	   //This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
		if(isset($_GET["code"]))
		{
		 //It will Attempt to exchange a code for an valid authentication token.
		 $token = $this->google_client->fetchAccessTokenWithAuthCode($this->utils->Get('code'));

		 //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
		 echo '<pre>';
		 print_r($token['error']);exit;
		 if(!isset($token['error']))
		 {
		  //Set the access token used for requests
		  $this->google_client->setAccessToken($token['access_token']);

		  //Store "access_token" value in $_SESSION variable for future use.
		  $_SESSION['access_token'] = $token['access_token'];

		  //Create Object of Google Service OAuth 2 class
		  $google_service = new \Google_Service_Oauth2($this->google_client);

		  //Get user profile data from google
		  $data = $google_service->userinfo->get();
		  $id = $data['id'];
		  $email = $data['email'];
		  $name = $data['name'];

				$userData = $this->member->ExistUserbyMail($email);
				if(count($userData) == 0) {
					$data = array();
					$data['oauth_provider'] = 'google';
					$data['google_id'] = $id;
					$data['m_name'] = $name;
					$data['m_user'] = $email;
					$data['m_mail'] = $email;
					$member_id = $this->member->CreateMember($data);	
					$is_logged = $this->member->Login($member_id, 'google');			
					$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_dashboard'][$this->curr_lang]);
					$this->utils->Redirect($redirect_url);
				}
				else {
					$data = array();
					$data['oauth_provider'] = 'google';
					$data['google_id'] = $id;
					//$data['m_name'] = $name;
					$data['m_mail'] = $email;
					$this->member->ModifyMember($userData['member_id'], $data);			
					$is_logged = $this->member->Login($userData['member_id'], 'google');			
					$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_dashboard'][$this->curr_lang]);
					$this->utils->Redirect($redirect_url);
				}
		  
		 
		  //Below you can find Get profile data and store into $_SESSION variable
		 /* if(!empty($data['given_name']))
		  {
		   $_SESSION['user_first_name'] = $data['given_name'];
		  }

		  if(!empty($data['family_name']))
		  {
		   $_SESSION['user_last_name'] = $data['family_name'];
		  }

		  if(!empty($data['email']))
		  {
		   $_SESSION['user_email_address'] = $data['email'];
		  }

		  if(!empty($data['gender']))
		  {
		   $_SESSION['user_gender'] = $data['gender'];
		  }

		  if(!empty($data['picture']))
		  {
		   $_SESSION['user_image'] = $data['picture'];
		  }*/
		 }
		 else {
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
			 exit;
			 
		 }
		}
		else {
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
			exit;
		}

		exit;

    }



}

/**
 * Assign page class
 */
$google_auth = new GoogleVerify();
include $google_auth->lg_folder . '/index.lang.php';
$google_auth->onLoad();

/******************* google_auth.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** google_auth.php ******************/;
