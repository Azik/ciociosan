<?php
/******************* ajax.question.php *******************
 *
 *
 ******************** ajax.question.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * search members
 */
class ajaxquestion extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $data = '';
    public $response = array();
    public $isSuccess = true;
    public $queryWhere = '';
    public $productsCount = 0;
    public $filterItems = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->buildQuestion();
        }
    }


    /**
     * build Question process
     */
    private function buildQuestion()
    {
        $this->isSuccess = true;
        //$subCatsArr = $this->utils->UserPostIntArr('sub_cats');
        $segment = $this->utils->UserPostInt('segment');
       
       

        if ($this->isSuccess) {
            /* All OK */
           $this->response['id'] = $segment;
           $this->response['title'] = "Who is the singer?";
           $this->response['score'] = 2;
           $this->response['item_1'] = "aaa";
           $this->response['item_2'] = "bbb";
           $this->response['item_3'] = "ccc";
           $this->response['item_4'] = "ddd";
           $this->response['created_at'] = '2020-03-02 02:04:11';
           $this->response['updated_at'] = '2020-03-02 02:04:11';
           $this->response['status'] = 1;
           $this->response['song']['id'] = 138;
           $this->response['song']['status'] = 1;
           $this->response['song']['created_at'] = '2020-03-02 02:04:11';
           $this->response['song']['updated_at'] = '2020-03-02 02:04:11';
           $this->response['song']['song'] = "[{\"download_link\":\"songs\\/March2020\\/ky8s3YkM3EKJyQEkVNq9.mp3\",\"original_name\":\"calvin-harris-my-way_10sec.mp3\"}]";
           $this->response['song']['category_id'] = 1;
		   
           $this->response['score_data']['id'] = 2;
           $this->response['score_data']['name'] = "Black";
           $this->response['score_data']['point'] = 10;
           $this->response['score_data']['color'] = "#0c7dca";
           $this->response['score_data']['sort_order'] = 2;
           $this->response['score_data']['extra'] = null;
        }
		die(json_encode($this->response));
        
    }

	
	

}

$index = new ajaxquestion();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.question.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.question.php ******************/;
