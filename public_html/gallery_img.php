<?php
/******************* gallery_img.php *******************
 *
 * Gallery image file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** gallery_img.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class galleryImage extends \mcms5xx\classes\ViewPage
{
    /**
     * Image width
     */
    public $w = 0;

    /**
     * Image height
     */
    public $h = 0;

    public function __construct()
    {
        parent::__construct();

        /**
         * Set Image width
         */
        $this->w = $this->utils->UserGetInt('w');

        /**
         * Set Image height
         */
        $this->h = $this->utils->UserGetInt('h');

    }

    public function onLoad()
    {
        /**
         * Set header type to jpeg
         */
        header('Content-Type: image/jpeg');

        /**
         * Size limit for thumbnail image
         */
        $thumb_limit = $this->fromConfig('thumb_limit');

        $id = $this->utils->UserGetInt('i');
        $query = 'SELECT * FROM ' . $this->db->prefix . 'gallery_images WHERE (`imgid`=' . $id . ')';
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            /**
             * Create folder
             */
            $folderw = ($this->w == 0) ? "" : $this->w;
            $folderh = ($this->h == 0) ? "" : $this->h;
            $folder = $this->io->dateFolder('img_gallery/img_' . $folderw . 'x' . $folderh . '/', $row['img_date']);

            $thumb_img = $folder . '/' . $id . '.jpg';
            if ((!@is_file($thumb_img)) || (@filesize($thumb_img) < $thumb_limit)) {
                $filename = $row['img_name'];
                $dir = $this->fromConfig('gallery_folder') . '/';
                $gallery_sub = $this->fromConfig('gallery_sub');
                $dir .= str_replace('[id]', $row['gid'], $gallery_sub) . '/';
                $img_file = $dir . $filename;
                if (strlen($img_file) > 10) {
                    list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);

                    if ($this->w == 0) {
                        if ($this->h >= $original_height) {
                            $this->w = $new_w = $original_width;
                            $new_h = $original_height;
                        } else {
                            $calcP = array($original_height, $this->h);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $this->w = $new_w = round(($original_width / $calc_p) * 100, 4);
                            $this->h = $new_h = round(($original_height / $calc_p) * 100, 4);
                        }
                        $image_p = imagecreatetruecolor($this->w, $this->h);
                    } elseif ($this->h == 0) {
                        if ($this->w >= $original_width) {
                            $this->h = $new_h = $original_height;
                            $new_w = $original_width;
                        } else {
                            $calcP = array($original_width, $this->w);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $this->w = $new_w = round(($original_width / $calc_p) * 100, 4);
                            $this->h = $new_h = round(($original_height / $calc_p) * 100, 4);
                        }
                        $image_p = imagecreatetruecolor($this->w, $this->h);
                    } elseif ($this->w == $this->h) {
                        $calcP = ($original_width > $original_height) ? array($original_height, $this->h) : array($original_width, $this->w);
                        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                        $new_w = round(($original_width / $calc_p) * 100, 4);
                        $new_h = round(($original_height / $calc_p) * 100, 4);
                        $image_p = imagecreatetruecolor($this->w, $this->h);
                        imagesavealpha($image_p, true);
                    } else {
                        $calcP = array($original_height, $this->h);
                        $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                        $new_w = round(($original_width / $calc_p) * 100, 4);
                        $new_h = round(($original_height / $calc_p) * 100, 4);
                        if ($new_w < $this->w) {
                            $calcP = array($original_width, $this->w);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $new_w = round(($original_width / $calc_p) * 100, 4);
                            $new_h = round(($original_height / $calc_p) * 100, 4);
                        }
                        $image_p = imagecreatetruecolor($this->w, $this->h);
                        imagesavealpha($image_p, true);
                    }


                    $extension = $this->site->getFileExt($filename);
                    switch ($extension) {
                        case 'jpg':
                        case 'jpeg': {
                            $image = imagecreatefromjpeg($img_file);
                            break;
                        }
                        case 'gif': {
                            $image = imagecreatefromgif($img_file);
                            break;
                        }
                        case 'png': {
                            $image = imagecreatefrompng($img_file);
                            break;
                        }
                        default: {
                            $image = imagecreatefromjpeg($img_file);
                            break;
                        }
                    }
                    imagecopyresampled($image_p, $image, 0, -round(($new_h - $this->h) / 5, 2), 0, 0, $new_w, $new_h, $original_width, $original_height);

                    if ($this->getKey('watermark', 0) == 1) {
                        $textcolor = imagecolorallocate($image_p, 0, 0, 0);
                        imagestring($image_p, 5, 0, 0, $this->getKey('watermark_name', ''), $textcolor);
                    }

                    @imagejpeg($image_p, $thumb_img, 100);
                    @chmod($thumb_img, 0767);
                    @imagedestroy($image_p);
                }
            }
            @header('location: ' . $thumb_img);
        } else {
            @header('location: index.php');
        }
    }
}

$image = new \mcms5xx\galleryImage();
$image->onLoad();

/******************* gallery_img.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** gallery_img.php ******************/;
