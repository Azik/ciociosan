<?php
/******************* cron_subscribe.php *******************
 *
 * File for cron job, send subscribers mail
 *
 * For send mails add to cron.
 * ( * * * * * lynx -dump http://{SITE_ADDRESS}/cron_subscribe.php >/dev/null 2>&1 )
 * {SITE_ADDRESS} - address of site: example.com
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** cron_subscribe.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Send mail to Subscribers
 */
class cronSubscribe extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    /**
     * Build page
     */
    private function buildPage()
    {
        $subscribe_mail_sends = $this->fromConfig('subscribe_mail_sends');
        $query = 'SELECT
			ST.*, 
			(SELECT COUNT(SBM.bid) FROM ' . $this->db->prefix . 'subscribe_broadcast_mails SBM WHERE (SBM.bid = ST.bid) AND (SBM.sended=1)) AS sended_mails,
			(SELECT COUNT(SBMO.bid) FROM ' . $this->db->prefix . 'subscribe_broadcast_mails SBMO WHERE (SBMO.bid = ST.bid) AND (SBMO.sended=0)) AS waiting_mails
		FROM ' . $this->db->prefix . 'subscribe_broadcast ST
		WHERE 
			(ST.startdate <= ' . time() . ')
		AND (ST.active = 1)
		AND (ST.finished = 0)
		';
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $bid = $row['bid'];
            $tid = $row['tid'];
            if ($row['sended_mails'] >= $row['waiting_mails']) {
                // B: finished update
                //echo "finished update";
                $up_query = 'UPDATE `' . $this->db->prefix . 'subscribe_broadcast`
				SET 
					`finished` = 9
				WHERE 
					`bid` = ' . $bid . '';
                $this->db->query($up_query);
                // E: finished update
            }
            //echo $row['sended_mails'].' == '.$row['waiting_mails'];

            $contacts_mail_from = base64_decode($this->getKey('contacts_mail_from', 'aW5mb0BtaWNyb3BocC5jb20='));
            $contacts_mail_user = base64_decode($this->getKey('contacts_mail_user', 'aW5mb0BtaWNyb3BocC5jb20='));
            $contacts_mail_pass = base64_decode($this->getKey('contacts_mail_pass', 'cGFzc3dvcmQ='));
            $contacts_mail_host = base64_decode($this->getKey('contacts_mail_host', 'bWljcm9waHAuY29t'));
            $contacts_mail_pop3 = base64_decode($this->getKey('contacts_mail_pop3', 'bWljcm9waHAuY29t'));
            $contacts_mail_smtp = base64_decode($this->getKey('contacts_mail_smtp', 'bWljcm9waHAuY29t'));

            $headers = "MIME-Version: 1.0\r \n";
            $headers .= "Content-type: text/html; charset=UTF-8\r \n";
            $headers .= 'From: "' . $contacts_mail_from . '" <' . $contacts_mail_from . ">\r \n";
            $headers .= 'Reply-To: "' . $contacts_mail_from . '" <' . $contacts_mail_from . ">\r \n";
            $headers .= "X-Priority: 3\r \n";
            $headers .= "X-MSMail-Priority: High\r \n";
            $headers .= 'X-Mailer: Just My Server';

            include_once 'm/classes/phpmailer/PHPMailerAutoload.php';
            //Create a new PHPMailer instance
            $mail = new PHPMailer();
            //Tell PHPMailer to use SMTP
            $mail->IsSMTP();

            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 2;

            //Ask for HTML-friendly debug output
            $mail->Debugoutput = 'html';

            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;

            //Set the hostname of the mail server
            $mail->Host = $contacts_mail_smtp;

            //Set the SMTP port number - likely to be 25, 465 or 587
            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            switch ($contacts_mail_smtp) {
                case "smtp.gmail.com": {
                    //Set the encryption system to use - ssl (deprecated) or tls
                    $mail->SMTPSecure = 'tls';
                    $mail->Port = 587;
                    break;
                }
                default: {
                    //Set the encryption system to use - ssl (deprecated) or tls
                    $mail->SMTPSecure = 'ssl';
                    $mail->Port = 465;
                    break;
                }
            }

            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = $contacts_mail_user;
            //Password to use for SMTP authentication
            $mail->Password = $contacts_mail_pass;

            //Set who the message is to be sent from
            $mail->SetFrom($contacts_mail_from, $contacts_mail_from);

            $s_sql = 'SELECT
				S.*,
				STL.`text`,
				STEMT.`name` AS `subject`,
				STEMT.`template`
			FROM ' . $this->db->prefix . 'subscribe S
			INNER JOIN ' . $this->db->prefix . 'subscribe_broadcast_mails SBM ON (SBM.sid = S.sid) AND (SBM.bid = ' . $bid . ') AND (SBM.sended = 0)
			INNER JOIN ' . $this->db->prefix . 'subscribe_broadcastlocalizations STL ON (STL.bid = ' . $bid . ') AND (STL.lang = S.lang)
			INNER JOIN ' . $this->db->prefix . 'subscribe_templateslocalizations STEMT ON (STEMT.tid = ' . $tid . ') AND (STEMT.lang = S.lang)
			WHERE 
				(S.`active` = 1)
			' . $this->db->get_limit(0, $subscribe_mail_sends);
            //echo("<pre>".$s_sql."</pre>");
            $s_result = $this->db->query($s_sql);
            while ($s_row = $this->db->fetch($s_result)) {
                $sid = $s_row['sid'];
                $contacts_mail_to = $s_row['email'];
                $subscribe_subject = $s_row['subject'];
                $template = $s_row['template'];
                $template = str_replace('[TEXT]', $s_row['text'], $template);
                $sendtime = time();

                $ups_query = 'UPDATE `' . $this->db->prefix . 'subscribe_broadcast_mails`
				SET 
					`sendtime` = ' . $sendtime . ',
					`sended` = 1
				WHERE 
					(`bid` = ' . $bid . ')
				AND (`sid` = ' . $sid . ')
				';
                $this->db->query($ups_query);

                //Set the subject line
                $mail->Subject = $subscribe_subject;

                //Read an HTML message body from an external file, convert referenced images to embedded,
                //convert HTML into a basic plain-text alternative body
                $mail->MsgHTML($template);

                $email = $contacts_mail_to;

                //Set who the message is to be sent to
                $mail->AddAddress($email, $email);

                //send the message, check for errors
                if (!$mail->Send()) {
                    @mail($email, $subscribe_subject, $template, $headers);
                }
                $mail->ClearAddresses();

                /* echo "Subject:".$mail->Subject."<br/>";
                echo "Template:".$template."<br/>"; */

                echo $sendtime;
            }
        }
    }
}

$index = new cronSubscribe();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* cron_subscribe.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** cron_subscribe.php ******************/;
