<?php
/******************* ajax.coupon.php *******************
 *
 *
 ******************** ajax.coupon.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxCoupon extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $messages = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

	public $session_logged = "member_logged";
	public $session_email = "member_email";
	public $session_userid = "member_id";
	public $session_usertype = "member_usertype";
	
    public $prf_logged = -1;
    public $prf_logged_id = -1;

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();

        $this->prf_logged = $this->utils->filterInt($this->utils->GetSession($this->session_logged));
        $this->prf_logged_id = $this->utils->filterInt($this->utils->GetSession($this->session_userid));
        
		/*
		if ($this->member->IsLogin()) {
            return;
        }
		*/
		
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
                case 'loadAllIddia':
                    $this->loadAllIddia();
				break;
                case 'loadCoupon':
                    $this->loadCoupon();
				break;
                case 'addCoef':
                    $this->Add();
				break;
                case 'addIddia':
                    $this->addIddia();
				break;
                case 'removeIddia':
                    $this->removeIddia();
				break;
                case 'cancelCoupon':
                    $this->cancelCoupon();
				break;
            }

		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }

	
	/**
     * addIddia 	
     */
    private function addIddia()
    {
		$red_url = '';
		$this->errors = array();
		$this->isSuccess = true;

		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);

		$cp_bal = (double)$this->utils->UserPost('cp_bal');
		$minimum_bal_limit = $this->fromConfig('minimum_bal_limit');

		if($cp_bal < $minimum_bal_limit){
			$this->isSuccess = false;
			$this->errors['err_cp_bal']['code']='ERR01';
			$this->errors['err_cp_bal']['message']='İddia balınız minumum '.$minimum_bal_limit.' bal ola bilər';
		}
		
		$uInfo = $this->member->GetUser($this->prf_logged_id);
		if((double)$uInfo['m_bal'] < (double)$cp_bal){
			$this->isSuccess = false;
			$this->errors['err_cp_bal']['code']='ERR02';
			$this->errors['err_cp_bal']['message']='İddia balınız balansınızdan çox ola bilməz';			
		}

		if(is_array($coupon) && is_Array($coupon['alliddia']) && count($coupon['alliddia']) <= 1){
			$this->isSuccess = false;
			$this->errors['err_idd_min_count']['code']='ERR03';
			$this->errors['err_idd_min_count']['message']='Kuponda minimum 2 iddia ola bilər';
		}

		if(is_array($coupon) && is_Array($coupon['alliddia']) && count($coupon['alliddia']) > 10){
			$this->isSuccess = false;
			$this->errors['err_idd_max_count']['code']='ERR04';
			$this->errors['err_idd_max_count']['message']='Kuponda maximum 10 iddia ola bilər';
		}
		
		$userid = $this->prf_logged_id;
		$total_coefficient = 0;
		$winning_point = 0;
		
		$AllIddia = array();
		$couponAll0k = true;
		if($this->isSuccess) {
		foreach($coupon['alliddia'] as $key=>$iddia) {
			$newsid = $iddia['newsid'];
			$answer_type = $iddia['answer']['type'];
			$answer_coefficient = $iddia['answer']['coefficient'];
			
			$newsInfo = $this->getNewsById($newsid);
			if(count($newsInfo) == 0){
				$couponAll0k = false;
				break;
			}
			else { //is OK
				$total_coefficient += (double)$answer_coefficient;
				$AllIddia[$key]['newsid'] = $newsid;
				$AllIddia[$key]['answer'] = $answer_type;
				$AllIddia[$key]['coefficient'] = $answer_coefficient;
			}
			
		}
		
		$winning_point = $total_coefficient * $cp_bal;
		if($couponAll0k) {
			$dataInsertCoupon = array();
			$dataInsertCoupon['userid'] = $this->prf_logged_id;
			$dataInsertCoupon['winning_point'] = $winning_point;
			$dataInsertCoupon['iddia_bal'] = $cp_bal;
			$dataInsertCoupon['createdate'] = time();
			$dataInsertCoupon['status'] = 'inprogress';
			$dataInsertCoupon['active'] = 1;
			$couponid = $this->db->insert($this->db->prefix.'coupons' , $dataInsertCoupon);
		
			foreach($AllIddia as $iddia) {
				$dataInsertIddia = array();
				$dataInsertIddia['couponid'] = $couponid;
				$dataInsertIddia['newsid'] = $iddia['newsid'];
				$dataInsertIddia['answer'] = $iddia['answer'];
				$dataInsertIddia['coefficient'] = $iddia['coefficient'];
				$dataInsertIddia['createdate'] = time();
				$dataInsertIddia['status'] = 'inprogress';
				$couponid = $this->db->insert($this->db->prefix.'iddia' , $dataInsertIddia);
				
			}
			
		
			$red_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_coupons'][$this->curr_lang]);
			$this->coupon->ChangeBal($this->prf_logged_id, $cp_bal, 'down');
			$this->isSuccess = true;
			$this->errors['success']['code']='SUCCESS01';
			$this->errors['success']['message']='#'.$couponid . ' nömrəli kupon uğurla yaradıldı. Sizi bütün kuponlarnızı görməyə yönləndiririk.';
			$this->utils->setCookie("coupon", null);
		}
		else
			$this->isSuccess = false;
	
		}
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }

	/**
     * cancelCoupon 	
     */
    private function cancelCoupon()
    {
		$this->messages = array();
		$this->isSuccess = true;

		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
		
		
		//unset($coupon[$key]); 
		
		$this->utils->setCookie('coupon', null );
		$couponAll0k = true;
		$this->isSuccess = true;
		
		$this->messages['success']['code']='SUCCESS01';
		$this->messages['success']['message']='Kupon uğurla ləğv olundu';

		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->messages;
		die(json_encode($this->response));
	}
	
	/**
     * removeIddia 	
     */
    private function removeIddia()
    {
		$this->messages = array();
		$this->isSuccess = true;
		$news_id = (double)$this->utils->UserPostInt('id');


		if($news_id <= 0){
			$this->isSuccess = false;
			$this->messages['err_iid']['code']='ERR01';
			$this->messages['err_iid']['message']='Yalnış daxil olma';
		}
		
		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
		
		if($this->isSuccess) {
			/* remove */
			$iddia = array();
			foreach($coupon['alliddia'] as $c) {
				if($c['newsid'] != $news_id)
					$iddia[] = $c;
			}
			$coupon['alliddia'] = $iddia;
			//unset($coupon[$key]); 
			
			$this->utils->setCookie('coupon', base64_encode( json_encode( $coupon ) ));
			$couponAll0k = true;
			$this->isSuccess = true;
			$this->messages['success']['code']='SUCCESS01';
			$this->messages['success']['message']='İddia ləğv olundu';
		}
		
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->messages;
		$this->response['data'] = $this->showIddiaList();
		die(json_encode($this->response));
	}
	
	
	/**
     * loadAllIddia 	
     */
    private function loadAllIddia()
    {		
		$data = $this->showIddiaList();
		die(json_encode($data));
		exit;
	}
	
	
	/**
     * loadCoupon 	
     */
    private function loadCoupon()
    {		
		$data = $this->showCoupon();
		die($data);
		exit;
	}

	private function showIddiaList(){
		$news_id = (double)$this->utils->UserPostInt('id');
		$newsInfo = $this->getNewsById($news_id);
		
		$curr_bal = 0;
		
		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
				$this->template->assign_block_vars('coupon', array(
					'NEWS_ID' => $newsInfo['newsid'],
					'YES_COEFFICIENT' => $newsInfo['yes_coefficient'],
					'NO_COEFFICIENT' => $newsInfo['no_coefficient'],
				));
        //$this->template->assign_block_vars('iddia', array());
		$ndx = 0;
		if(is_array($coupon)) {
			
			foreach($coupon['alliddia'] as $c) {
				++$ndx;
				$newsInfo = $this->getNewsById($c['newsid']);
				$news_more_url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[slug]', $newsInfo['slug'], $this->permalinks[$this->perma_type]['module_news_more'][$this->curr_lang]));

				$this->template->assign_block_vars('coupon.iddia', array(
					'NDX' => $ndx,
					'NEWS_TITLE' => $c['news_title'],
					'NEWS_ID' => $c['newsid'],
					'COEFFICIENT' => $c['answer']['coefficient'],
					'TYPE' => $c['answer']['type'],
					'NEWS_MORE_URL' => $news_more_url
				));
			}
			
			if($ndx > 0) {
				$this->template->assign_block_vars('coupon.have_iddia', array());
			}
		}

		
		$data['data'] = $this->generateResult('section_iddia_list');
		$data['curr_bal'] = $this->member->GetBal($this->prf_logged_id);
		return $data;
	}
	
	private function showCoupon(){
		
		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
				$this->template->assign_block_vars('coupon', array());
		$ndx = 0;
		if(is_array($coupon)) {
			
			foreach($coupon['alliddia'] as $c) {
				++$ndx;
				$newsInfo = $this->getNewsById($c['newsid']);
				$news_more_url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[slug]', $newsInfo['slug'], $this->permalinks[$this->perma_type]['module_news_more'][$this->curr_lang]));
			}
			
			if($ndx > 0) {
				$this->template->assign_block_vars('coupon.have_new_coupon', array(
						'COUPON_URL' => $news_more_url
				));
			}
			else {
				$coupon_url = "#";
				if ($this->prf_logged == 9) {
					$coupon_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_coupons'][$this->curr_lang]);
				}
				$this->template->assign_block_vars('coupon.have_not_new_coupon', array(
						'COUPON_URL' => $coupon_url
				));				
			}
		}
		else {
			$coupon_url = "#";
			$class = "not_new_coupon";
			if ($this->prf_logged == 9) {
				$coupon_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_coupons'][$this->curr_lang]);
				$class = "";
			}
			$this->template->assign_block_vars('coupon.have_not_new_coupon', array(
					'COUPON_URL' => $coupon_url,
					'CLASS' => $class,
			));				
		}

		
		$data = $this->generateResult('section_header_coupon');
		return $data;
	}
	
	
	private function generateResult($tpl)
    {
        $this->template->set_filenames(array($tpl => ''.$tpl.'.tpl'));
        $return_string = $this->template->pparse($tpl, true);
        return $return_string;
    }	
	
	/**
     * Add 	
     */
    private function Add()
    {
		$red_url = '';
		$this->errors = array();
		$this->isSuccess = true;
		$type = $this->utils->UserPost('type');
		$newsid = $this->utils->UserPost('id');

		if($type != "yes" && $type != "no"){
			$this->isSuccess = false;
			$this->errors['err_type']['code']='ERR01';
			$this->errors['err_type']['message']='Düzgün seçim edilməyib';
		}

		$newsInfo = $this->getNewsById($newsid);
		if(count($newsInfo) == 0){
			$this->isSuccess = false;
			$this->errors['err_news']['code']='ERR02';
			$this->errors['err_news']['message']='Xəbər düzgün seçilməyib';
		}
		
		if($this->isSuccess) {
			/* B: OK */
			
			$iddia = array();
			$iddia['newsid'] = $newsid;
			$iddia['news_title'] = $newsInfo['name'];
			$iddia['answer']['type'] = $type;
			$iddia['answer']['coefficient'] = $newsInfo[$type."_coefficient"];
			
			//Coupon session
			$coupon = $this->utils->getCookie('coupon');
			$coupon = base64_decode( $coupon );
			$coupon = json_decode($coupon, true);
		
			if($coupon == '')
				$coupon = array();

			if(!isset($coupon['alliddia']))
				$coupon['alliddia'] = array();
			
	
			$idd_exist = $this->findNewsInCoupon($coupon, $newsid);
			if($idd_exist == -1){
				$coupon['alliddia'][] = $iddia;
			}
			else {
				$coupon['alliddia'][$idd_exist] = $iddia;
			}
			
			$this->utils->setCookie('coupon', base64_encode( json_encode( $coupon ) ));
			//End Coupon

			$this->isSuccess = true;
			$this->errors	= array(); 
			
			
		}
		
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		$this->response['data'] = $this->showIddiaList();
		die(json_encode($this->response));
	
    }
	
	private function findNewsInCoupon($coupon, $newsid) {
		$idd_exist = -1;
		foreach($coupon['alliddia'] as $key=>$idd) {
			if($idd['newsid'] == $newsid){
				$idd_exist = $key;
			}
		}
		return $idd_exist;
	}
	
}

$index = new ajaxCoupon();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.coupon.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.coupon.php ******************/;