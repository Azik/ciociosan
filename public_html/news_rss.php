<?php
/******************* news_rss.php *******************
 *
 * News RSS file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news_rss.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class rssPage extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    /**
     * Build page
     */
    private function buildPage()
    {
        header('Content-type: text/xml');

        $this->buildRSSNews();

    }

    private function buildRSSNews()
    {
        echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        echo '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0" xmlns:media="http://search.yahoo.com/mrss/">' . "\n";
        echo "\t" . '<channel>' . "\n";
        echo "\t\t" . '<title><![CDATA[' . $this->fromLangIndex('title_home') . ']]></title>' . "\n";
        echo "\t\t" . '<description><![CDATA[' . $this->fromLangIndex('title_home') . ']]></description>' . "\n";
        echo "\t\t" . '<link>' . $this->db_cfg->site_host . '</link>' . "\n";

        $news_limit = $this->getKey('news_last_limit', 20);
        $news_query = "SELECT
          N.*,
          NL.name,
          NL.text, 
          NL.header,
          NL.comment,
          NL.slug,
          NCL.name as cat_name,
          NCL.header AS cat_header,
          NR.read_date,
          NR.total_views,
          NR.month_views
        FROM " . $this->db->prefix . "news N
		INNER JOIN " . $this->db->prefix . "newslocalizations NL ON NL.newsid = N.newsid
		INNER JOIN " . $this->db->prefix . "newscategories NC ON NC.catid = N.catid
		INNER JOIN " . $this->db->prefix . "newscategorylocalizations NCL ON (NCL.catid = NC.catid) && (NCL.lang='" . $this->lang . "')
		LEFT JOIN " . $this->db->prefix . "news_read NR ON NR.newsid = N.newsid
		WHERE
		    (N.`active` = '1')
		 && (NL.`lang`  = '" . $this->lang . "')
		 && (NL.`name` != '')
		 && (N.`newsdate` <= " . time() . ") 
		ORDER BY N.`newsdate` DESC, N.`newsid` DESC 
		" . $this->db->get_limit(0, $news_limit);

        $in_news_idx = 0;
        $news_result = $this->db->query($news_query);
        while ($news_row = $this->db->fetch_assoc($news_result)) {
            $in_news_idx++;

            $newsid = $news_row['newsid'];

            $news_link = $this->db_cfg->site_host . "/" . str_replace('[slug]', $this->utils->url_filter($news_row['slug']), str_replace('[year]', date('Y', $news_row['newsdate']), str_replace('[name]', $this->utils->url_filter($news_row['name']), str_replace('[id]', $newsid, str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['news'][$this->curr_lang])))));
            $imgid = (int)$news_row['header'];
            $img400x300_file = "";
            if ($imgid > 0) {
                $images_query = 'SELECT * FROM ' . $this->db->prefix . "files WHERE (`category`='image') &&  (`fileid`=" . $imgid . ')';
                $images_result = $this->db->query($images_query);
                if ($images_row = $this->db->fetch($images_result)) {
                    $folder = $this->io->dateFolder('img/file_img/', $images_row['add_time']);
                    $img400x300_file = $folder . '/' . $imgid . '_400x300.jpg';
                    if ((@is_file($img400x300_file)) && (@filesize($img400x300_file) > 20)) {
                        $img400x300_file = $this->db_cfg->site_host . "/" . $img400x300_file;
                    } else {
                        $img400x300_file = $folder . '/' . $imgid . '_400x300.png';
                        if ((@is_file($img400x300_file)) && (@filesize($img400x300_file) > 20)) {
                            $img400x300_file = $this->db_cfg->site_host . "/" . $img400x300_file;
                        } else {
                            $img400x300_file = $this->db_cfg->site_host . "/file_img.php?i=" . $imgid . "&w=400&h=300";
                        }
                    }
                }
            }

            $img400x300_media = (strlen($img400x300_file) > 0) ? '<media:thumbnail width="400" height="300" url="' . $img400x300_file . '"/>' : '';

            echo "\t\t\t" . '<item>
				<title><![CDATA[' . addslashes($news_row['name']) . ']]></title>
				<description><![CDATA[' . addslashes($news_row['name']) . ']]></description>
				<link>' . $news_link . '</link>
				<guid isPermaLink="true">' . $news_link . '</guid>
				<pubDate>' . date('r', $news_row['newsdate']) . '</pubDate>
				' . $img400x300_media . '
			</item>
';
        }

        echo "\t" . '</channel>' . "\n";
        echo '</rss>';
    }


}

/**
 * Assign page class
 */
$news_rss = new rssPage();
include $news_rss->lg_folder . '/index.lang.php';
$news_rss->onLoad();

/******************* news_rss.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news_rss.php ******************/;
