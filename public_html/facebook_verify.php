<?php
/******************* news_rss.php *******************
 *
 * News RSS file
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** news_rss.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx;

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

class FacebookVerify extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    /**
     * Build page
     */
    private function buildPage()
    {
       
		$fb = $this->getFacebook();
		$helper = $fb->getRedirectLoginHelper();
		try {
			$accessToken = $helper->getAccessToken();
			if($accessToken == null){
				$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
				$this->utils->Redirect($redirect_url);
				exit;
			}
			$response = $fb->get('/me?fields=id,name,email,location,gender,birthday,hometown', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
//		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			
		  // When validation fails or other local issues
//		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
		  exit;
		}
		
		if (! isset($accessToken)) {
		  if ($helper->getError()) {
			  /*
			header('HTTP/1.0 401 Unauthorized');
			echo "Error: " . $helper->getError() . "\n";
			echo "Error Code: " . $helper->getErrorCode() . "\n";
			echo "Error Reason: " . $helper->getErrorReason() . "\n";
			echo "Error Description: " . $helper->getErrorDescription() . "\n";
			*/
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);

		  } else {
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
			  /*
			header('HTTP/1.0 400 Bad Request');
			echo 'Bad request';
			*/
		  }
		  exit;
		}

		// Logged in
/*		echo '<h3>Access Token</h3>';
		var_dump($accessToken->getValue());
*/
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
/*		echo '<h3>Metadata</h3>';
		echo '<pre>';
		print_r($tokenMetadata);
		var_dump($tokenMetadata);
*/
		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId($this->fromConfig('FB_APP_ID'));
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e) {
//			echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['error404'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);

			exit;
		  }

/*		  echo '<h3>Long-lived</h3>';
		  var_dump($accessToken->getValue());*/
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;
		// Get user details from facebook.
		$me = $response->getGraphUser();
		
		$userData = $this->member->ExistUserbyMail($me['email']);
		if(count($userData) == 0) {
			$data = array();
			$data['oauth_provider'] = 'facebook';
			$data['facebook_id'] = $me['id'];
			$data['m_name'] = $me['name'];
			$data['m_user'] = $me['email'];
			$data['m_mail'] = $me['email'];
			$member_id = $this->member->CreateMember($data);	
			$is_logged = $this->member->Login($member_id, 'facebook');			
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_dashboard'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
		}
		else {
			$data = array();
			$data['oauth_provider'] = 'facebook';
			$data['facebook_id'] = $me['id'];
			//$data['m_name'] = $me['name'];
			$data['m_mail'] = $me['email'];
			$this->member->ModifyMember($userData['member_id'], $data);			
			$is_logged = $this->member->Login($userData['member_id'], 'facebook');			
			$redirect_url = $this->curr_folder . str_replace('[lang]', $this->lang, $this->permalinks[$this->perma_type]['module_member_dashboard'][$this->curr_lang]);
			$this->utils->Redirect($redirect_url);
		}
		//$me['email']
		//$me['name']
		//$me['id']
		exit;

    }



}

/**
 * Assign page class
 */
$news_rss = new FacebookVerify();
include $news_rss->lg_folder . '/index.lang.php';
$news_rss->onLoad();

/******************* news_rss.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** news_rss.php ******************/;
