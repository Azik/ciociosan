<?php
/******************* ajax.categories.php *******************
 *
 *
 ******************** ajax.categories.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * search members
 */
class ajaxcategories extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $data = '';
    public $response = array();
    public $isSuccess = true;
    public $queryWhere = '';
    public $productsCount = 0;
    public $filterItems = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->buildCategories();
        }
    }


    /**
     * build categories process
     */
    private function buildCategories()
    {
        $this->isSuccess = true;
        //$subCatsArr = $this->utils->UserPostIntArr('sub_cats');
       

        if ($this->isSuccess) {
            /* All OK */
             $sql = 'SELECT 
            S.*, SL.name
				FROM '.$this->db->prefix.'testscategories S
				INNER JOIN ' .$this->db->prefix."testscategorylocalizations SL ON SL.catid = S.catid
				WHERE SL.lang='" .$this->lang."' && S.active = 1
				ORDER BY S.point DESC
			";
			$result = $this->db->query($sql);
			$ndx = 0;
			$slide_nums = $this->db->num_rows($sql);
    
            $ndx = 0;

			   while ($row = $this->db->fetch($result)) {
					
					$id = $row['catid'];
					$color = $row['color'];
					$point = $row['point'];
					$name = $row['name'];
				   
					if ($ndx == 0) {
						$this->template->assign_block_vars('categories', array());
					}
					
					++$ndx;	
					$this->template->assign_block_vars('categories.items', array(
						'ID' => $id,
						'NDX' => $ndx,
						'NAME' => $name,
						'NAME_UP' => $this->nameup($name),
						'COLOR' => $color,
						'POINT' => $point,
						'TITLE' => $name,
					));
					
				}
           // $this->generateResult();
        }

        echo $this->data;

    }

	
	


    private function generateResult()
    {
        $this->template->set_filenames(array('categories_section' => 'categories_section.tpl'));
        $return_string = $this->template->pparse('categories_section', true);
        echo $return_string;
    }


}

$index = new ajaxcategories();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.categories.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.categories.php ******************/;
