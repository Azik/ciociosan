<?php
/******************* ajax.favorite.php *******************
 *
 *
 ******************** ajax.favorite.php ******************/

/**
 * Include view page class
 */
require_once 'm/classes/viewpage.class.php';

/**
 * Login members
 */
class ajaxCoupon extends \mcms5xx\classes\ViewPage
{
    public $langs;
    public $permalinks = '';
    public $perma_type = '';
    public $inside_lang = '';
    public $index_lang = '';
    public $errors = array();
    public $messages = array();
    public $response = array();
    public $isSuccess = true;
    public $redUrl = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
		
        if (@$_SERVER['REQUEST_METHOD'] == 'POST') {
            $action = $this->utils->Post('action');
            switch ($action) {
               
                case 'add':
                    $this->Add();
				break;
            }

		} 
    }

    /**
     * Build page
     */
    private function buildPage()
    {
		
	
    }

	
	
	/**
     * removeIddia 	
     */
    private function removeIddia()
    {
		$this->messages = array();
		$this->isSuccess = true;
		$news_id = (double)$this->utils->UserPostInt('id');


		if($news_id <= 0){
			$this->isSuccess = false;
			$this->messages['err_iid']['code']='ERR01';
			$this->messages['err_iid']['message']='Yalnış daxil olma';
		}
		
		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
		
		if($this->isSuccess) {
			/* remove */
			$iddia = array();
			foreach($coupon['alliddia'] as $c) {
				if($c['newsid'] != $news_id)
					$iddia[] = $c;
			}
			$coupon['alliddia'] = $iddia;
			//unset($coupon[$key]); 
			
			$this->utils->setCookie('coupon', base64_encode( json_encode( $coupon ) ));
			$couponAll0k = true;
			$this->isSuccess = true;
			$this->messages['success']['code']='SUCCESS01';
			$this->messages['success']['message']='İddia ləğv olundu';
		}
		
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->messages;
		$this->response['data'] = $this->showIddiaList();
		die(json_encode($this->response));
	}
	
	

	private function showIddiaList(){
		$news_id = (double)$this->utils->UserPostInt('id');
		$newsInfo = $this->getNewsById($news_id);
		
		$curr_bal = 0;
		
		$coupon = $this->utils->getCookie('coupon');
		$coupon = base64_decode( $coupon );
		$coupon = json_decode($coupon, true);
				$this->template->assign_block_vars('coupon', array(
					'NEWS_ID' => $newsInfo['newsid'],
					'YES_COEFFICIENT' => $newsInfo['yes_coefficient'],
					'NO_COEFFICIENT' => $newsInfo['no_coefficient'],
				));
        //$this->template->assign_block_vars('iddia', array());
		$ndx = 0;
		if(is_array($coupon)) {
			
			foreach($coupon['alliddia'] as $c) {
				++$ndx;
				$newsInfo = $this->getNewsById($c['newsid']);
				$news_more_url = $this->curr_folder . str_replace('[lang]', $this->lang, str_replace('[slug]', $newsInfo['slug'], $this->permalinks[$this->perma_type]['module_news_more'][$this->curr_lang]));

				$this->template->assign_block_vars('coupon.iddia', array(
					'NDX' => $ndx,
					'NEWS_TITLE' => $c['news_title'],
					'NEWS_ID' => $c['newsid'],
					'COEFFICIENT' => $c['answer']['coefficient'],
					'TYPE' => $c['answer']['type'],
					'NEWS_MORE_URL' => $news_more_url
				));
			}
			
			if($ndx > 0) {
				$this->template->assign_block_vars('coupon.have_iddia', array());
			}
		}

		
		$data['data'] = $this->generateResult('section_iddia_list');
		$data['curr_bal'] = $this->member->GetBal($this->prf_logged_id);
		return $data;
	}
	

	/**
     * Add 	
     */
    private function Add()
    {
		$red_url = '';
		$this->errors = array();
		$this->isSuccess = true;
		
		$pid = $this->utils->UserPost('pid');

	
		$productInfo = $this->getProductById($pid);
		if(count($productInfo) == 0){
			$this->isSuccess = false;
			$this->errors['err_news']['code']='ERR02';
			$this->errors['err_news']['message']='Xəbər düzgün seçilməyib';
		}
		$mod = 'off';
		if($this->isSuccess) {
			/* B: OK */
			
			$new_favorites = array();
			$new_favorites['pid'] = $pid;
			
		
			//favorites session
			$favorites = $this->utils->getCookie('favorites');
			$favorites = json_decode($favorites, true);
		
			if($favorites == '')
				$favorites = array();

	
			$idd_exist = $this->findFav($favorites, $pid);
			if($idd_exist == -1){
				$mod = 'on';
				$favorites[] = $new_favorites;
			}
			else {
				$mod = 'off';
				array_splice($favorites, $idd_exist,1);
//				$favorites[$idd_exist] = $new_favorites;
			}
			
			$this->utils->setCookie('favorites',  json_encode( $favorites ) );
			//End favorites

			$this->isSuccess = true;
			$this->errors	= array(); 
			
			
		}
		
		$this->response['mod'] = $mod;
		$this->response['redURL'] = $red_url;
		$this->response['isSuccess'] = $this->isSuccess;
		$this->response['messages'] = $this->errors;
		die(json_encode($this->response));
	
    }
	
	private function findFav($favorites, $pid) {
		$idd_exist = -1;
		foreach($favorites as $key=>$idd) {
			if($idd['pid'] == $pid){
				$idd_exist = $key;
			}
		}
		return $idd_exist;
	}
	
}

$index = new ajaxCoupon();

include $index->lg_folder . '/index.lang.php';
$index->onLoad();

/******************* ajax.favorite.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** ajax.favorite.php ******************/;