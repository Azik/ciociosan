<?php
/******************* tree_json_products_category.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** tree_json_products_category.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
$_GET['module'] = 'section';

class Index extends \mcms5xx\classes\AdminPage
{
    public $menuData = array();
    public $jsonArr = array();
    public $jsonPids = array();
    public $exp_id = 0;
    public $expandedArr = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        header('Content-type: text/json');
        header('Content-type: application/json');
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        /* $p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "GET:".$p);
        @fclose($handle);

        $p = '';
        foreach ($_POST as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle); */

        $parent_id = $this->utils->UserGet('parent');

        if ($parent_id == '#') {
            $this->jsonArr['0'] = array();
            $this->jsonArr['0']['id'] = 0;
            $this->jsonArr['0']['icon'] = 'fa fa-folder open icon-lg icon-state-warning';
            $this->jsonArr['0']['text'] = $this->fromLangIndex('root_menu');
            $this->jsonArr['0']['state'] = array('opened' => true, 'disabled' => true);
            $this->jsonArr['0']['children'] = true;
            echo json_encode($this->jsonArr);
            exit;
        } else {
            $parent_id = $this->utils->UserGetInt('parent');
        }

        $link_id = $this->utils->UserGetInt('link_id');

        $sql = "SELECT 
				M.*, 
				MSUB.mid as sub_mid, 
				IF ((ML.name!=''), ML.name, MLIN.name) as name
			FROM  " .$this->db->prefix.'products_category M
			LEFT JOIN ' .$this->db->prefix.'products_category MSUB ON ( MSUB.parentid = M.mid )
			LEFT JOIN ' .$this->db->prefix."products_categorylocalizations ML ON ( ML.mid = M.mid ) AND (ML.lang = '".$this->default_lang."')
			LEFT JOIN " .$this->db->prefix."products_categorylocalizations MLIN ON ( MLIN.mid = M.mid ) AND (MLIN.name!='')
			WHERE 
				(M.parentid = ".$parent_id.')
			GROUP BY M.mid
			ORDER BY 
				M.parentid,
				M.position
		';

        /* $handle = @fopen("file.txt", "a+");
        @fwrite($handle, $sql);
        @fclose($handle); */
        //echo "<pre>".$sql."</pre>";
        $result = $this->db->query($sql);
        $ndx = 0;
        while ($row = $this->db->fetch($result)) {
            $pid = $row['parentid'];
            $sub_mid = (int) $row['sub_mid'];
            $id = $row['mid'];
            $this->jsonArr[$ndx] = array();
            $this->jsonArr[$ndx]['id'] = $id;
            $this->jsonArr[$ndx]['icon'] = (($sub_mid > 0) || ($pid == 0)) ? 'fa fa-folder open icon-lg icon-state-warning' : 'fa fa-file icon-lg icon-state-info';
            $this->jsonArr[$ndx]['text'] = ($link_id==$id) ? '<strong class="curr_selected_link">'.$this->utils->txt_filter($row['name']).'</strong>' : $this->utils->txt_filter($row['name']);
            $this->jsonArr[$ndx]['children'] = ($sub_mid > 0) ? true : false;
            ++$ndx;
        }
		


        /* $handle = @fopen("file.txt", "a+");
        @fwrite($handle, json_encode($this->jsonArr));
        @fclose($handle); */
        echo json_encode($this->jsonArr);
    }

    public function drawTree($parentId)
    {
        if (isset($this->jsonPids[$parentId])) {
            foreach ($this->jsonPids[$parentId] as $itemId) {
                $arr[] = $this->menuData[$itemId];
                if (isset($this->jsonPids[$itemId])) {
                    end($arr)->folder = true;
                    end($arr)->children = $this->drawTree($itemId);
                }
            }

            return $arr;
        }
    }

    public function getTree()
    {
        $this->getExpanded($this->exp_id);
        $sql = "SELECT 
				M.*, 
				IF ((ML.name!=''), ML.name, MLIN.name) as name
			FROM  " .$this->db->prefix.'products_category M
			LEFT JOIN ' .$this->db->prefix."products_categorylocalizations ML ON ( ML.mid = M.mid ) AND (ML.lang = '".$this->default_lang."')
			LEFT JOIN " .$this->db->prefix."products_categorylocalizations MLIN ON ( MLIN.mid = M.mid ) AND (MLIN.name!='')
			GROUP BY M.mid
			ORDER BY 
				M.parentid,
				M.position
		";
        //echo "<pre>".$sql."</pre>";
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $pid = $row['parentid'];
            $id = $row['mid'];
            $this->jsonPids[$pid][] = $id;
            $this->menuData[$id] = new stdClass();
            $this->menuData[$id]->pid = $pid;
            $this->menuData[$id]->key = $id;
            $this->menuData[$id]->expanded = (in_array($id, $this->expandedArr)) ? true : false;
            $this->menuData[$id]->folder = ($pid == 0) ? true : false;
            $this->menuData[$id]->title = $this->utils->txt_filter($row['name']);
        }
        //print_r($this->jsonPids);
    }

    public function getExpanded($id)
    {
        $query = 'SELECT M.*
			FROM `' .$this->db->prefix."products_category` M
		WHERE 
			M.mid='" .$id."'
		";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $parentid = $row['parentid'];
            $this->expandedArr[] = $parentid;
            if ($parentid != 0) {
                $this->getExpanded($parentid);
            }
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* tree_json_products_category.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** tree_json_products_category.php ******************/;
