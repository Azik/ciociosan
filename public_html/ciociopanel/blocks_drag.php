<?php
/******************* blocks_drag.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** blocks_drag.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'blocks';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        /*$p = '';
        foreach ($_POST as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle);*/
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->upPosition();
        }
    }

    private function upPosition()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $catid = $this->utils->UserGetInt('catid');

                $new_position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');

                if ($old_position < $new_position) {
                    $ndx = 0;
                    $sql = 'SELECT blocksid, position
                    FROM ' .$this->db->prefix.'blocks
                    WHERE position <= '.$new_position.' AND (`catid`='.$catid.')
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);

                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'blocks
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                             (`blocksid`=' .$row['blocksid'].') AND (`catid`='.$catid.')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                } else {
                    $ndx = $new_position + 1;
                    $sql = 'SELECT blocksid, position
                    FROM ' .$this->db->prefix.'blocks
                    WHERE position >= '.$new_position.' AND (`catid`='.$catid.')
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);
                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'blocks
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                              (`blocksid`=' .$row['blocksid'].') AND (`catid`='.$catid.')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                }
                $curr_id = $this->utils->UserPostInt('curr_id');
                $up_query = 'UPDATE '.$this->db->prefix.'blocks
							SET
								`position` = ' .$new_position.'
							WHERE
								(`blocksid` = ' .$curr_id.') AND (`catid`='.$catid.')
						';

                /*$handle = @fopen("file.txt", "a+");
                @fwrite($handle, $up_query);
                @fclose($handle);*/

                $this->db->query($up_query);

                $this->orderBlocks($catid);
            }
        }
        echo time();
    }

    private function upPosution1()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $catid = $this->utils->UserGetInt('catid');

                $position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');
                $curr_id = $this->utils->UserPostInt('curr_id');
                $hitmode = $this->utils->UserPost('hitmode');

                switch ($hitmode) {

                    case 'before': {
                        /* B: Move before section */

                        $up_query = 'UPDATE '.$this->db->prefix.'blocks
							SET
								`position` = `position` - 2
							WHERE
								(`position` > ' .$position.')
							AND (`catid`=' .$catid.')
						';
                        $this->db->query($up_query);
                        /* $handle = @fopen("file.txt", "a+");
                        @fwrite($handle, "\n\n"."POST:".$up_query);
                        @fclose($handle); */

                        $this->db->query('UPDATE '.$this->db->prefix."blocks
							SET
								`position` = '" .($position - 1)."'
							WHERE
								 (`blocksid` = " .$curr_id.')
							 AND (`catid`=' .$catid.')
							');

                        break;
                        /* E: Move before section */
                    }

                    case 'after': {
                        /* B: Move after section */
                        $up_query = 'UPDATE '.$this->db->prefix.'blocks
							SET
								`position` = `position` - 2
							WHERE
								 (`position` <= ' .($position + 1).')
							 AND (`catid`=' .$catid.')
						';
                        $this->db->query($up_query);
                        /* $handle = @fopen("file.txt", "a+");
                        @fwrite($handle, "\n\n"."POST:".$up_query);
                        @fclose($handle); */

                        $this->db->query('UPDATE '.$this->db->prefix."blocks
							SET
								`position` = '" .$position."'
							WHERE
								 (`blocksid` = " .$curr_id.')
							 AND (`catid`=' .$catid.')
							');

                        break;
                        /* E: Move after section */
                    }
                }

                $this->orderBlocks($catid);
                echo time();

                //print_r($sortArr);
                /* $p = '';
                foreach ($_POST as $key=>$index) {
                    $p .= $key ."=>". $index ."\n";
                }
                $handle = @fopen("file.txt", "a+");
                @fwrite($handle, $p);
                @fclose($handle); */
                //echo $p
            }
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* blocks_drag.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** blocks_drag.php ******************/;
