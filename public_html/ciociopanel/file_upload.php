<?php
/******************* file_upload.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** file_upload.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->buildPage();
        }
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        if (!empty($_FILES)) {
            $filename = $this->utils->filename_filter($_FILES['file']['name']);

            $originalname = strtolower($filename);
            if (trim($filename) != '') {
                $ext = $this->utils->GetFileExtension($filename);
                if (in_array($ext, $this->getFileTypesArray())) {
                    $cat = $this->getFileCat($ext);
                    $file_dir = '../'.$this->fromConfig('upload_folder').$cat.'/';
                    $add_time = time();
                    $file_dir = $this->io->dateFolder($file_dir, $add_time).'/';
                    $filename = $this->utils->GetFileName($file_dir, $filename);
                    $titles[$i] = $this->utils->txt_request_filter($titles[$i]);
                    $img_file = $file_dir.$filename;
                    if (move_uploaded_file($_FILES['file']['tmp_name'], $img_file) && chmod($img_file, 0777)) {
                        if ($cat == 'image') {
                            $image_thumb_folder = $this->fromConfig('image_thumb_folder');
                            $image_thumb_width = $this->fromConfig('image_thumb_width');
                            $thumb_dir = '../'.$this->fromConfig('upload_folder').$cat.'/'.$image_thumb_folder.'/';
                            $thumb_dir = $this->io->dateFolder($thumb_dir, $add_time).'/';
                            $thumb_img = $thumb_dir.$filename;
                            list($original_width, $original_height, $src_t, $src_a) = getimagesize($img_file);
                            $calcP = array($original_width, $image_thumb_width);
                            $calc_p = $this->utils->calc_perc($calcP['1'], $calcP['0']);
                            $image_thumb_height = round(($original_height / $calc_p) * 100, 4);
                            $src_w = round(($image_thumb_width / 100) * $calc_p, 4);
                            $src_h = round(($image_thumb_height / 100) * $calc_p, 4);
                            $image_p = imagecreatetruecolor($image_thumb_width, $image_thumb_height);

                            switch ($ext) {
                                case 'jpg':
                                case 'jpeg': {
                                    $image = imagecreatefromjpeg($img_file);
                                    break;
                                }
                                case 'gif': {
                                    $image = imagecreatefromgif($img_file);
                                    break;
                                }
                                case 'png': {
                                    $image = imagecreatefrompng($img_file);
                                    break;
                                }
                                default: {
                                $image = imagecreatefromjpeg($img_file);
                                break;
                                }
                            }
                            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $image_thumb_width, $image_thumb_height, $src_w, $src_h);
                            switch ($ext) {
                                case 'jpg':
                                case 'jpeg': {
                                    @imagejpeg($image_p, $thumb_img, 100);
                                    break;
                                }
                                case 'gif': {
                                    @imagegif($image_p, $thumb_img);
                                    break;
                                }
                                case 'png': {
                                    @imagepng($image_p, $thumb_img, 9);
                                    break;
                                }
                                default: {
                                @imagejpeg($image_p, $thumb_img, 100);
                                break;
                                }
                            }
                            @chmod($thumb_img, 0767);
                            @imagedestroy($image_p);
                        }
                        $query = 'INSERT INTO '.$this->db->prefix."files (`originalname`, `filename`, `extension`, `category`, `title`, `add_time`) VALUES('".$originalname."','".$filename."','".$ext."','".$cat."','".strtolower($titles[$i])."', ".$add_time.' )';
                        $this->db->query($query);

                        $file_id = $this->db->insert_id();
                        $this->user->logOperation($this->user->GetUserId(), 'file_manager', $file_id, 'upload_file');

                        echo time();
                    }
                }
            }
        } /* else {
             $result  = array();
             $storeFolder = "../u/image/2014/06/26";

             $files = scandir($storeFolder);                 //1
             if ( false!==$files ) {
                  foreach ( $files as $file ) {
                        if ( '.'!=$file && '..'!=$file) {       //2
                             $obj['name'] = $file;
                             $obj['size'] = filesize($storeFolder.'/'.$file);
                             $result[] = $obj;
                        }
                  }
             }

             header('Content-type: text/json');              //3
             header('Content-type: application/json');
             echo json_encode($result);
        } */
    }

    public function getFileTypesArray()
    {
        $arr = array();
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            for ($i = 0; $i < count($value); ++$i) {
                $arr[] = $value[$i];
            }
        }

        return $arr;
    }

    public function getFileCat($ext)
    {
        $cat = '';
        $file_types = $this->fromConfig('file_types');

        foreach ($file_types as $key => $value) {
            for ($i = 0; $i < count($value); ++$i) {
                if ($ext == $value[$i]) {
                    return $key;
                }
            }
        }

        return $cat;
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* file_upload.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** file_upload.php ******************/;
