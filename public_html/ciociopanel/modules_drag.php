<?php
/******************* modules_drag.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** modules_drag.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'modules';

class modulesDrag extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        $this->buildPage();
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        /*$p = '';
        foreach ($_GET as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "GET:".$p);
        @fclose($handle);*/

        /*$p = '';
        foreach ($_POST as $key=>$index) {
            $p .= $key ."=>". $index ."\n";
        }
        $handle = @fopen("file.txt", "a+");
        @fwrite($handle, "\n\n"."POST:".$p);
        @fclose($handle);*/

        $this->upPosition();
    }

    private function upPosition()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $do_submit = $this->utils->UserPostInt('do_submit');
            if ($do_submit == 1) {
                $new_position = $this->utils->UserPostInt('position');
                $old_position = $this->utils->UserPostInt('old_position');

                if ($old_position < $new_position) {
                    $ndx = 0;
                    $sql = 'SELECT moduleid, position
                    FROM ' .$this->db->prefix.'modules
                    WHERE position <= '.$new_position.'
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);
                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'modules
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                             (`moduleid`=' .$row['moduleid'].')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                } else {
                    $ndx = $new_position + 1;
                    $sql = 'SELECT moduleid, position
                    FROM ' .$this->db->prefix.'modules
                    WHERE position >= '.$new_position.'
                    ORDER BY position ASC';
                    $result = $this->db->query($sql);
                    /*$handle = @fopen("file.txt", "a+");
                    @fwrite($handle, $sql);
                    @fclose($handle);*/

                    while ($row = $this->db->fetch($result)) {
                        $up_query = 'UPDATE '.$this->db->prefix.'modules
                        SET
                            `position` = ' .$ndx.'
                        WHERE
                             (`moduleid`=' .$row['moduleid'].')
                        ';

                        /*$handle = @fopen("file.txt", "a+");
                        @fwrite($handle, $up_query);
                        @fclose($handle);*/

                        $this->db->query($up_query);
                        ++$ndx;
                    }
                }
                $curr_id = $this->utils->UserPostInt('curr_id');
                $up_query = 'UPDATE '.$this->db->prefix.'modules
							SET
								`position` = ' .$new_position.'
							WHERE
								(`moduleid` = ' .$curr_id.')
						';

                /*$handle = @fopen("file.txt", "a+");
                @fwrite($handle, $up_query);
                @fclose($handle);*/

                $this->db->query($up_query);

                $this->orderModules();
            }
        }
        echo time();
    }

}

$index = new modulesDrag();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();

/******************* modules_drag.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** modules_drag.php ******************/;
