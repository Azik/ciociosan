<?php
/******************* file_editor.php *******************
 *
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** file_editor.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin;

define('iFolded', '../');
require_once iFolded.'m/classes/adminpage.class.php';
@$_GET['module'] = 'file_manager';

class Index extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        parent::__construct();
    }

    public function onLoad()
    {
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->sendPage();
            } else {
                $this->buildPage();
            }
        }
    }

    public function sendPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->template->assign_vars(array(
            'TITLE' => $this->fromLangIndex('images_editor'),
            'CROP_TITLE' => $this->fromLangIndex('images_crop'),
            'IMAGES_CROP_SUBMIT' => $this->fromLangIndex('images_crop_submit'),
            'IMAGES_CLOSE' => $this->fromLangIndex('images_close'),
        ));
        $this->template->assign_block_vars('close', array());
        $id = $this->utils->UserGetInt('v');
        if ($id > 0) {
            $filename = $this->site->getFile($id, 'image');
            if (strlen($filename) > 5) {
                /* echo $filename; */
                $this->db->query('DELETE FROM `'.$this->db->prefix.'files_image_sizes` WHERE `imgid`='.$id);

                $imgSizes = $this->fromConfig('imgSizes');
                $ndx = 0;
                foreach ($imgSizes as $sz) {
                    ++$ndx;
                    list($portfolioW, $portfolioH) = explode('x', $sz);
                    $x1 = $this->utils->UserPostInt('img'.$sz.'_x1');
                    $y1 = $this->utils->UserPostInt('img'.$sz.'_y1');
                    $x2 = $this->utils->UserPostInt('img'.$sz.'_x2');
                    $y2 = $this->utils->UserPostInt('img'.$sz.'_y2');
                    /* echo $sz.': '.$x1.'x'.$y1.' - '.$x2.'x'.$y2.'<br/>'; */
                    $query = 'INSERT INTO `'.$this->db->prefix.'files_image_sizes`(`imgid`, `size`, `x1`, `y1`, `x2`, `y2`)
						VALUES(' .$id.", '".$sz."', ".$x1.', '.$y1.', '.$x2.', '.$y2.')';
                    $this->db->query($query);
                }
            }
        }
    }

    public function buildPage()
    {
        $user_type = $this->user->GetCurrentUserTypeText();

        if (!$this->user->IsLogin()) {
            return;
        }

        $this->template->assign_vars(array(
            'TITLE' => $this->fromLangIndex('images_editor'),
            'CROP_TITLE' => $this->fromLangIndex('images_crop'),
            'IMAGES_CROP_SUBMIT' => $this->fromLangIndex('images_crop_submit'),
            'IMAGES_CLOSE' => $this->fromLangIndex('images_close'),
        ));
        $this->template->assign_block_vars('close', array());
        $id = $this->utils->UserGetInt('v');
        if ($id > 0) {
            /* echo "ID".$id; */
            $filename = $this->site->getFile($id, 'image');
            if (strlen($filename) > 5) {
                /* echo $filename; */
                $imgSizes = $this->fromConfig('imgSizes');

                //$img_file = '../'.$filename;
                $img_file = './file_for_edit.php?i='.$id;

                $this->template->assign_block_vars('image_crop', array(
                    'IMG' => $img_file,
                    'ID' => $id,
                    'ACTION' => 'file_editor.php?v='.$id,
                ));
                $ndx = 0;
                foreach ($imgSizes as $sz) {
                    ++$ndx;
                    $class = ($ndx == 1) ? ' class="active"' : '';
                    $fade_class = ($ndx == 1) ? ' in active' : '';
                    list($portfolioW, $portfolioH) = explode('x', $sz);
                    $x1 = 0;
                    $y1 = 0;
                    $x2 = $portfolioW;
                    $y2 = $portfolioH;
                    $sql = 'SELECT *
						FROM `' .$this->db->prefix.'files_image_sizes`
						WHERE 
							(`imgid` = ' .$id.")
						 && (`size`= '" .$sz."')
						";
                    /* echo($sql); */
                    $result = $this->db->query($sql);
                    if ($row = $this->db->fetch($result)) {
                        $x1 = $row['x1'];
                        $y1 = $row['y1'];
                        $x2 = $row['x2'];
                        $y2 = $row['y2'];
                    }

                    $this->template->assign_block_vars('image_crop.tab', array(
                        'X1' => $x1,
                        'Y1' => $y1,
                        'X2' => $x2,
                        'Y2' => $y2,
                        'SIZE' => $sz,
                        'W' => $portfolioW,
                        'H' => $portfolioH,
                        'CLASS' => $class,
                        'FADE_CLASS' => $fade_class,
                    ));
                }
            }
        }
    }
}

$index = new Index();
include $index->lg_folder.'/index.lang.php';

$index->onLoad();
$index->template->pparse('images_editor');

/******************* file_editor.php *******************
*
* Copyright : (C) 2004 - 2019. All Rights Reserved
*
******************** file_editor.php ******************/;
