<?php
/******************* modules.admin.php *******************
 *
 * Modules admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** modules.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';
require_once '../m/classes/pclzip.class.php';

class Modules extends \mcms5xx\classes\AdminPage
{
    protected $message = '';

    public function __construct()
    {
        $this->curr_module = 'modules';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        if (@$this->user->perm_string['0'] == 1) {
            // Full perm
            $this->doAction();
            $this->buildPage();
        }
    }

    private function doAction()
    {
        $action = $this->utils->Post('action');
        $action_id = $this->utils->Post('action_id');

        switch ($action) {
            case 'delete': {
                $this->deleteModule($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'update': {
                $this->updateModule($action_id);
                break;
            }
            case 'add': {
                $this->addModule();
                break;
            }
            case 'language_update': {
                $this->updateLanguage($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'view_module_update': {
                $this->updateViewModule($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'admin_module_update': {
                $this->updateAdminModule($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'view_template_update': {
                $this->updateViewTemplate($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'admin_template_update': {
                $this->updateAdminTemplate($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'config_update': {
                $this->updateConfig($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
            case 'seo_update': {
                $this->updateSEO($action_id);
                $this->utils->Redirect('?'.$this->module_qs.'=modules');
                break;
            }
        }
    }

    private function buildPage()
    {
        $this->buildMain();

        $mode = $this->utils->Get('mode');
        $id = $this->utils->UserGetInt('id');
        switch ($mode) {
            case 'add': {
                $this->buildInsertMode();
                break;
            }
            case 'activate':
            case 'inactive': {
                $this->buildActivate($id, $mode);
                break;
            }
            case 'edit_language':{
                $this->buildEditLanguage($id);
                break;
            }
            case 'edit_view':{
                $this->buildEditView($id);
                break;
            }
            case 'edit_admin':{
                $this->buildEditAdmin($id);
                break;
            }
            case 'edit_view_template':{
                $this->buildEditViewTemplate($id);
                break;
            }
            case 'edit_admin_template':{
                $this->buildEditAdminTemplate($id);
                break;
            }
            case 'edit_cnfg':{
                $this->buildEditConfig($id);
                break;
            }
            case 'edit_seo':{
                $this->buildEditSEO($id);
                break;
            }
            default: {
                if ($id > 0) {
                    $this->buildEditMode($id);
                } else {
                    $this->buildList();
                }
                break;
            }
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_block_vars('modules', array(
            'URL' => '?'.$this->module_qs.'=modules',
        ));
    }

    private function buildList()
    {
        $this->template->assign_block_vars('modules.list', array(
            'ADD' => $this->fromLang('add'),
            'NAME' => $this->fromLang('name'),
            'TITLE' => $this->fromLang('module_title'),
            'DESCRIPTION' => $this->fromLang('description'),
            'ACTIVE' => $this->fromLang('active'),
            'OPERATIONS' => $this->fromLang('operations'),
            'ACTIVATE' => $this->fromLang('module_activate'),
            'INACTIVATE' => $this->fromLang('module_inactivate'),
            'EDIT' => $this->fromLang('edit'),
            'EDIT_VIEW' => $this->fromLang('edit_view'),
            'EDIT_ADMIN' => $this->fromLang('edit_admin'),
            'EDIT_VIEW_TEMPLATE' => $this->fromLang('edit_view_template'),
            'EDIT_ADMIN_TEMPLATE' => $this->fromLang('edit_admin_template'),
            'EDIT_LANGUAGE' => $this->fromLang('edit_language'),
            'EDIT_CNFG' => $this->fromLang('edit_cnfg'),
            'EDIT_SEO' => $this->fromLang('edit_seo'),
            'DELETE' => $this->fromLang('delete'),
            'DELETE_CONFIRM' => $this->fromLang('delete_confirm'),
        ));
        $ndx = 0;
        $hidden_val = '';

        $result = $this->db->query('SELECT M.*, ML.`title`, M.`description`
			FROM ' .$this->db->prefix.'modules M
			INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
			WHERE ML.`lang` = '" .$this->lang."'
			ORDER BY M.`position` ASC");
        while ($row = $this->db->fetch($result)) {
            $id = $row['moduleid'];
            $position = $row['position'];
            $name = $row['name'];
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $active = $row['active'];
            $deleteable = $row['deleteable'];
            $title = $row['title'];
            $description = $row['description'];

            $title = $this->utils->GetPartOfString($title, 50);
            $description = $this->utils->GetPartOfString($description, 50);

            $active_css = ($active == 0) ? 'class="inactive"' : '';
            if ($deleteable == 1) {
                $active_link = ($active == 1) ? '<a href="?'.$this->module_qs.'=modules&id='.$id.'&mode=inactive" class="btn btn-success">'.$this->fromLang('active_1').'&nbsp; <i class="fa fa-unlock"></i></a></a>' : '<a href="?'.$this->module_qs.'=modules&id='.$id.'&mode=activate" class="btn btn-danger">'.$this->fromLang('active_0').' &nbsp; <i class="fa fa-lock"></i></a></a>';
            } else {
                $active_link = '';
            }
            $this->template->assign_block_vars('modules.list.items', array(
                'ID' => $id,
                'POSITION' => $position,
                'NAME' => $name,
                'TITLE' => $title,
                'DESCRIPTION' => $description,
                'ACTIVE' => $active_link,
                'ACTIVE_CSS' => $active_css,
                'EDIT_VIEW' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_view',
                'EDIT_VIEW_TEMPLATE' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_view_template',
                'EDIT_ADMIN' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_admin',
                'EDIT_ADMIN_TEMPLATE' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_admin_template',
                'EDIT_LANGUAGE' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_language',
                'EDIT_CNFG' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_cnfg',
                'EDIT_SEO' => '?'.$this->module_qs.'=modules&id='.$id.'&mode=edit_seo',
            ));

            if ($deleteable == 1) {
                $this->template->assign_block_vars('modules.list.items.delete', array());
            } else {
                $this->template->assign_block_vars('modules.list.items.no_delete', array());
            }

            $have_view = (int) $row['have_view'];
            if ($have_view == 1) {
                $this->template->assign_block_vars('modules.list.items.view_link', array());
                $this->template->assign_block_vars('modules.list.items.view_link_template', array());
            }

            $have_admin = (int) $row['have_admin'];
            if ($have_admin == 1) {
                $this->template->assign_block_vars('modules.list.items.admin_link', array());
                $this->template->assign_block_vars('modules.list.items.admin_link_template', array());
            }

            $have_cnfg = (int) $row['have_cnfg'];
            if ($have_cnfg == 1) {
                $this->template->assign_block_vars('modules.list.items.cnfg_link', array());
            }
            $this->template->assign_block_vars('modules.list.items.language_link', array());
            $this->template->assign_block_vars('modules.list.items.seo_link', array());
        }
        $this->template->assign_block_vars('modules.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildActivate($id, $mode)
    {
        $active = ($mode == 'activate') ? 1 : 0;
        $query = 'UPDATE `'.$this->db->prefix."modules` SET `active`='".$active."' WHERE (`moduleid`=".$id.') && (`deleteable`=1) ';
        $this->db->query($query);
        $this->utils->Redirect('?'.$this->module_qs.'=modules');
    }

    private function buildEditMode($id)
    {
        $query = 'SELECT * FROM '.$this->db->prefix."modules WHERE moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $active_chk = ($row['active'] == 1) ? 'checked' : '';
            $admin_chk = ($row['have_admin'] == 1) ? 'checked' : '';
            $view_chk = ($row['have_view'] == 1) ? 'checked' : '';
            $description = $row['description'];
            $icon = $row['icon'];

            $this->template->assign_block_vars('modules.edit', array(
                'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'ACTIVE' => $this->fromLang('modules_active'),
                'ACTIVE_CHK' => $active_chk,
                'ADMIN' => $this->fromLang('modules_admin'),
                'ADMIN_CHK' => $admin_chk,
                'VIEW' => $this->fromLang('modules_view'),
                'VIEW_CHK' => $view_chk,
                'DESCRIPTION' => $this->fromLang('description'),
                'DESCRIPTION_VALUE' => $description,
                'ICON' => $this->fromLang('icon'),
                'ICON_VALUE' => $icon,
                'ID' => $id,
            ));

            $faArr = $this->fromConfig('faArr');
            foreach ($faArr as $sIcon => $sVal) {
                $selected = ($icon == 'fa '.$sIcon) ? 'selected' : '';
                $this->template->assign_block_vars('modules.edit.icons', array(
                    'ICON' => $sIcon,
                    'VALUE' => 'fa '.$sIcon,
                    'ICON_CODE' => '&#x'.$sVal,
                    'SELECTED' => $selected,
                ));
            }

            $ndx = 0;
            foreach ($this->langs as $key => $value) {
                ++$ndx;
                $lang = $value;
                $title = $seo_title = '';
                $class = ($ndx == 1) ? ' class="active"' : '';
                $fade_class = ($ndx == 1) ? ' in active' : '';

                $query = 'SELECT * FROM '.$this->db->prefix."modulelocalizations WHERE moduleid='".$id."' AND lang='".$lang."'";
                $result = $this->db->query($query);
                if ($row = $this->db->fetch($result)) {
                    $title = $row['title'];
                    $seo_title = $row['seo_title'];
                }

                $this->template->assign_block_vars('modules.edit.tab', array(
                    'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                    'LANG' => $lang,
                    'CLASS' => $class,
                    'FADE_CLASS' => $fade_class,
                    'TITLE' => $this->fromLang('module_title'),
                    'TITLE_VALUE' => $title,
                    'SEO_TITLE' => $this->fromLang('module_seo_title'),
                    'SEO_TITLE_VALUE' => $seo_title,
                ));
            }
        }
    }

    private function buildEditLanguage($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));
            $this->template->assign_block_vars('modules.edit_language', array(
                'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'LANG_FILE' => $this->fromLang('lang_file'),
                'ID' => $id,
            ));

            $lang_folder = $this->fromConfig('lang_folder');
            $ndx = 0;
            foreach ($this->langs as $key => $value) {
                if ($this->utils->io->file_exists('../'.$lang_folder.$value.'/'.$module_name.'.lang.php')) {
                    ++$ndx;
                    $lang = $value;
                    $lang_file_value = $this->utils->io->GetFileContent('../'.$lang_folder.$value.'/'.$module_name.'.lang.php');
                    $class = ($ndx == 1) ? ' class="active"' : '';
                    $fade_class = ($ndx == 1) ? ' in active' : '';

                    $this->template->assign_block_vars('modules.edit_language.tab', array(
                        'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                        'LANG' => $lang,
                        'CLASS' => $class,
                        'FADE_CLASS' => $fade_class,
                        'LANG_FILE_VALUE' => $lang_file_value,
                    ));
                }
            }
        }
    }

    private function updateLanguage($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];

            $lang_folder = $this->fromConfig('lang_folder');
            $ndx = 0;
            foreach ($this->langs as $key => $value) {
                if ($this->utils->io->file_exists('../'.$lang_folder.$value.'/'.$module_name.'.lang.php')) {
                    ++$ndx;
                    $lang = $value;
                    $lang_file = '../'.$lang_folder.$value.'/'.$module_name.'.lang.php';
                        //$lang_file_value = $this->utils->io->GetFileContent("../" . $lang_folder . $value . "/" . $module_name.".lang.php");
                    //$lang_file_value = $this->utils->Post('txt_lang_file_' . $lang);
                    $name_field = 'txt_lang_file_'.$lang;
                    //$lang_file_value = array_key_exists($name_field, $_POST) ? $_POST[$name_field] : "";
                    $lang_file_value = $this->utils->simplePost('txt_lang_file_'.$lang);

                    //echo $lang_file."\n";
                    //echo $lang_file_value."\n\n";
                    $this->utils->io->SaveToFile($lang_file, $lang_file_value);
                }
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_language');
    }

    private function buildEditView($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));
            if ($this->utils->io->file_exists('../m/modules/'.$module_name.'.view.php')) {
                $module_file_value = $this->utils->io->GetFileContent('../m/modules/'.$module_name.'.view.php');
                $this->template->assign_block_vars('modules.edit_view', array(
                    'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                    'SAVE' => $this->fromLang('save'),
                    'CANCEL' => $this->fromLang('cancel'),
                    'URL' => '?'.$this->module_qs.'=modules',
                    'MODULE_FILE' => $this->fromLang('module_file'),
                    'MODULE_FILE_VALUE' => $module_file_value,
                    'ID' => $id,
                ));
            }
        }
    }

    private function updateViewModule($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            if ($this->utils->io->file_exists('../m/modules/'.$module_name.'.view.php')) {
                $module_file = '../m/modules/'.$module_name.'.view.php';
                $module_file_value = $this->utils->simplePost('txt_module_file');
                //echo $module_file."\n";
                //echo $module_file_value."\n\n";exit();
                $this->utils->io->SaveToFile($module_file, $module_file_value);
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_view_moodule');
    }

    private function buildEditAdmin($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));
            if ($this->utils->io->file_exists('./modules/'.$module_name.'.admin.php')) {
                $module_file_value = $this->utils->io->GetFileContent('./modules/'.$module_name.'.admin.php');
                $this->template->assign_block_vars('modules.edit_admin', array(
                    'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
                    'SAVE' => $this->fromLang('save'),
                    'CANCEL' => $this->fromLang('cancel'),
                    'URL' => '?'.$this->module_qs.'=modules',
                    'MODULE_FILE' => $this->fromLang('module_file'),
                    'MODULE_FILE_VALUE' => $module_file_value,
                    'ID' => $id,
                ));
            }
        }
    }

    private function updateAdminModule($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            if ($this->utils->io->file_exists('./modules/'.$module_name.'.admin.php')) {
                $module_file = './modules/'.$module_name.'.admin.php';
                $module_file_value = $this->utils->simplePost('txt_module_file');
                //echo $module_file."\n";
                //echo $module_file_value."\n\n";exit();
                $this->utils->io->SaveToFile($module_file, $module_file_value);
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_admin_moodule');
    }

    private function buildEditViewTemplate($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));
            $view_template_files = $row['view_template_files'];
            $template_folder = $this->fromConfig('template_folder');
            $view_template = $this->fromConfig('view_template');
            $view_templateFiles = explode(',', $view_template_files);
            $ndx = 0;
            $this->template->assign_block_vars('modules.edit_view_template', array(
                'TAB_HEADER_WIDTH' => (count($view_templateFiles) * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'MODULE_FILE' => $this->fromLang('view_template_file'),
                'ID' => $id,
            ));
            for ($i = 0; $i < count($view_templateFiles); ++$i) {
                //echo "<br/>AA";
                $view_template_file = trim($view_templateFiles[$i]);
                if (strlen($view_template_file) > 0) {
                    if (strpos($view_template_file, './') === false) {
                        if (substr($view_template_file, -1) != '/') {
                            $file_name = '../'.$template_folder.$view_template.'/'.$view_template_file;
                            //echo "<br/><br/><br/><br/><br/><br/>".$file_name;
                            if ($this->utils->io->file_exists($file_name)) {
                                ++$ndx;
                                $class = ($ndx == 1) ? ' class="active"' : '';
                                $fade_class = ($ndx == 1) ? ' in active' : '';
                                $module_file_value = $this->utils->io->GetFileContent($file_name);

                                $this->template->assign_block_vars('modules.edit_view_template.tab', array(
                                    'TAB_HEADER_WIDTH' => (count($view_templateFiles) * 75),
                                    'LANG' => $view_template_file,
                                    'NDX' => $ndx,
                                    'CLASS' => $class,
                                    'FADE_CLASS' => $fade_class,
                                    'FILE_VALUE' => $module_file_value,
                                ));
                            }
                        }
                    }
                }
            }
        }
    }

    private function updateViewTemplate($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $view_template_files = $row['view_template_files'];
            $template_folder = $this->fromConfig('template_folder');
            $view_template = $this->fromConfig('view_template');
            $view_templateFiles = explode(',', $view_template_files);
            $ndx = 0;

            for ($i = 0; $i < count($view_templateFiles); ++$i) {
                $view_template_file = trim($view_templateFiles[$i]);
                if (strlen($view_template_file) > 0) {
                    if (strpos($view_template_file, './') === false) {
                        if (substr($view_template_file, -1) != '/') {
                            $file_name = '../'.$template_folder.$view_template.'/'.$view_template_file;
                            if ($this->utils->io->file_exists($file_name)) {
                                ++$ndx;

                                $module_file_value = $this->utils->simplePost('txt_file_'.$ndx);
                                //echo $file_name."\n";
                                //echo $module_file_value."\n\n";exit();
                                $this->utils->io->SaveToFile($file_name, $module_file_value);
                            }
                        }
                    }
                }
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_view_template');
    }

    private function buildEditAdminTemplate($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));
            $admin_template_files = $row['admin_template_files'];
            $template_folder = $this->fromConfig('template_folder');
            $admin_template = $this->fromConfig('admin_template');
            $admin_templateFiles = explode(',', $admin_template_files);
            $ndx = 0;
            $this->template->assign_block_vars('modules.edit_admin_template', array(
                'TAB_HEADER_WIDTH' => (count($admin_templateFiles) * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'MODULE_FILE' => $this->fromLang('view_template_file'),
                'ID' => $id,
            ));
            for ($i = 0; $i < count($admin_templateFiles); ++$i) {
                $admin_template_file = trim($admin_templateFiles[$i]);
                if (strlen($admin_template_file) > 0) {
                    if (strpos($admin_template_file, './') === false) {
                        if (substr($admin_template_file, -1) != '/') {
                            $file_name = '../'.$template_folder.$admin_template.'/'.$admin_template_file;
                            if ($this->utils->io->file_exists($file_name)) {
                                ++$ndx;
                                $class = ($ndx == 1) ? ' class="active"' : '';
                                $fade_class = ($ndx == 1) ? ' in active' : '';
                                $module_file_value = $this->utils->io->GetFileContent($file_name);

                                $this->template->assign_block_vars('modules.edit_admin_template.tab', array(
                                    'TAB_HEADER_WIDTH' => (count($admin_templateFiles) * 75),
                                    'LANG' => $admin_template_file,
                                    'NDX' => $ndx,
                                    'CLASS' => $class,
                                    'FADE_CLASS' => $fade_class,
                                    'FILE_VALUE' => htmlspecialchars($module_file_value),
                                ));
                            }
                        }
                    }
                }
            }
        }
    }

    private function updateAdminTemplate($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $admin_template_files = $row['admin_template_files'];
            $template_folder = $this->fromConfig('template_folder');
            $admin_template = $this->fromConfig('admin_template');
            $admin_templateFiles = explode(',', $admin_template_files);
            $ndx = 0;

            for ($i = 0; $i < count($admin_templateFiles); ++$i) {
                $admin_template_file = trim($admin_templateFiles[$i]);
                if (strlen($admin_template_file) > 0) {
                    if (strpos($admin_template_file, './') === false) {
                        if (substr($admin_template_file, -1) != '/') {
                            $file_name = '../'.$template_folder.$admin_template.'/'.$admin_template_file;
                            if ($this->utils->io->file_exists($file_name)) {
                                ++$ndx;

                                $module_file_value = $this->utils->simplePost('txt_file_'.$ndx);
                                //echo $file_name."\n";
                                //echo $module_file_value."\n\n";exit();
                                $this->utils->io->SaveToFile($file_name, $module_file_value);
                            }
                        }
                    }
                }
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_admin_template');
    }

    private function buildEditConfig($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));

            $this->template->assign_block_vars('modules.edit_config', array(
                'TAB_HEADER_WIDTH' => (2 * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'MODULE_FILE' => $this->fromLang('view_template_file'),
                'ID' => $id,
            ));
            $ndx = 0;
            $cnfg_file_01 = '../m/modules/'.$module_name.'.cnfg.php';
            if ($this->utils->io->file_exists($cnfg_file_01)) {
                ++$ndx;
                $class = ($ndx == 1) ? ' class="active"' : '';
                $fade_class = ($ndx == 1) ? ' in active' : '';
                $module_file_value = $this->utils->io->GetFileContent($cnfg_file_01);

                $this->template->assign_block_vars('modules.edit_config.tab', array(
                    'TAB_HEADER_WIDTH' => (2 * 75),
                    'LANG' => $module_name.'.cnfg.php',
                    'NDX' => $ndx,
                    'CLASS' => $class,
                    'FADE_CLASS' => $fade_class,
                    'FILE_VALUE' => htmlspecialchars($module_file_value),
                ));
            }

            $cnfg_file_02 = '../m/modules/'.$module_name.'.cls.php';
            if ($this->utils->io->file_exists($cnfg_file_02)) {
                ++$ndx;
                $class = ($ndx == 1) ? ' class="active"' : '';
                $fade_class = ($ndx == 1) ? ' in active' : '';
                $module_file_value = $this->utils->io->GetFileContent($cnfg_file_02);

                $this->template->assign_block_vars('modules.edit_config.tab', array(
                    'TAB_HEADER_WIDTH' => (2 * 75),
                    'LANG' => $module_name.'.cnfg.php',
                    'NDX' => $ndx,
                    'CLASS' => $class,
                    'FADE_CLASS' => $fade_class,
                    'FILE_VALUE' => htmlspecialchars($module_file_value),
                ));
            }
        }
    }

    private function updateConfig($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $ndx = 0;
            $cnfg_file_01 = '../m/modules/'.$module_name.'.cnfg.php';
            if ($this->utils->io->file_exists($cnfg_file_01)) {
                ++$ndx;
                $module_file_value = $this->utils->simplePost('txt_file_'.$ndx);
                //echo $cnfg_file_01."\n";
                //echo $module_file_value."\n\n";exit();
                $this->utils->io->SaveToFile($cnfg_file_01, $module_file_value);
            }

            $cnfg_file_02 = '../m/modules/'.$module_name.'.cls.php';
            if ($this->utils->io->file_exists($cnfg_file_02)) {
                ++$ndx;
                $module_file_value = $this->utils->simplePost('txt_file_'.$ndx);
                //echo $cnfg_file_01."\n";
                //echo $module_file_value."\n\n";exit();
                $this->utils->io->SaveToFile($cnfg_file_02, $module_file_value);
            }
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_config');
    }

    private function buildEditSEO($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $module_name = $row['name'];
            $this->template->assign_block_vars('modules.sub_name', array(
                'TITLE' => $row['title'],
            ));

            $this->template->assign_block_vars('modules.edit_seo', array(
                'TAB_HEADER_WIDTH' => (2 * 75),
                'SAVE' => $this->fromLang('save'),
                'CANCEL' => $this->fromLang('cancel'),
                'URL' => '?'.$this->module_qs.'=modules',
                'SEO_TABLE' => $this->fromLang('seo_table'),
                'TABLE_ID' => $this->fromLang('table_id'),
                'TITLE_HEADER' => $this->fromLang('title_header'),
                'TITLE_DESCRIPTION' => $this->fromLang('title_description'),
                'TITLE_KEYWORDS' => $this->fromLang('title_keywords'),
                'ID' => $id,
                'PREFIX' => $this->db->prefix,
            ));
            $tableColumnsArr = array();
            $tablesArr = explode(',', $row['tables']);
            for ($s = 0; $s < count($tablesArr); ++$s) {
                $tableColumn = $tablesArr[$s];
                if (strlen($tableColumn) > 0) {
                    $this->template->assign_block_vars('modules.edit_seo.tables', array(
                        'NAME' => $tableColumn,
                    ));
                    $tableColumnsArr[$this->db->prefix.$tableColumn] = array();
                    $column_result = $this->db->query('SHOW COLUMNS FROM '.$this->db->prefix.$tableColumn);
                    while ($column_row = $this->db->fetch_assoc($column_result)) {
                        //print_r($column_row);
                        //print_r($column_row['Field']);
                        $tableColumnsArr[$tableColumn][] = $column_row['Field'];
                        $this->template->assign_block_vars('modules.edit_seo.tables.columns', array(
                            'VALUE' => $column_row['Field'],
                        ));
                        if ($row['seo_table'] == $tableColumn) {
                            $id_selected = ($row['table_id'] == $column_row['Field']) ? ' selected' : ' ';
                            $this->template->assign_block_vars('modules.edit_seo.select_table_id', array(
                                'TEXT' => $column_row['Field'],
                                'VALUE' => $column_row['Field'],
                                'SELECTED' => $id_selected,
                            ));

                            $header_selected = ($row['title_header'] == $column_row['Field']) ? ' selected' : ' ';
                            $this->template->assign_block_vars('modules.edit_seo.select_title_header', array(
                                'TEXT' => $column_row['Field'],
                                'VALUE' => $column_row['Field'],
                                'SELECTED' => $header_selected,
                            ));

                            $description_selected = ($row['title_description'] == $column_row['Field']) ? ' selected' : ' ';
                            $this->template->assign_block_vars('modules.edit_seo.select_title_description', array(
                                'TEXT' => $column_row['Field'],
                                'VALUE' => $column_row['Field'],
                                'SELECTED' => $description_selected,
                            ));

                            $keywords_selected = ($row['title_keywords'] == $column_row['Field']) ? ' selected' : ' ';
                            $this->template->assign_block_vars('modules.edit_seo.select_title_keywords', array(
                                'TEXT' => $column_row['Field'],
                                'VALUE' => $column_row['Field'],
                                'SELECTED' => $keywords_selected,
                            ));
                        }
                    }
                    $selected = ($row['seo_table'] == $tableColumn) ? ' selected' : ' ';
                    $this->template->assign_block_vars('modules.edit_seo.select_seo_table', array(
                        'TEXT' => $this->db->prefix.$tableColumn,
                        'VALUE' => $tableColumn,
                        'SELECTED' => $selected,
                    ));
                }
            }
            //echo "<pre>\n\n\n\n\n\n".print_r($tableColumnsArr, true)."</pre>";
        }
    }

    private function updateSEO($id)
    {
        $query = 'SELECT M.*, ML.`title`
        FROM ' .$this->db->prefix.'modules M
        INNER JOIN ' .$this->db->prefix."modulelocalizations ML ON ML.`moduleid` = M.`moduleid`
        WHERE (ML.`lang` = '" .$this->lang."') AND M.moduleid='".$id."'";
        $result = $this->db->query($query);
        if ($row = $this->db->fetch($result)) {
            $txt_seo_table = $this->utils->UserPost('txt_seo_table');
            $txt_table_id = $this->utils->UserPost('txt_table_id');
            $txt_title_header = $this->utils->UserPost('txt_title_header');
            $txt_title_description = $this->utils->UserPost('txt_title_description');
            $txt_title_keywords = $this->utils->UserPost('txt_title_keywords');
            $query = 'UPDATE `'.$this->db->prefix."modules`
			SET
				`seo_table` = '" .$txt_seo_table."',
				`table_id` = '" .$txt_table_id."',
				`title_header` = '" .$txt_title_header."',
				`title_description` = '" .$txt_title_description."',
				`title_keywords` = '" .$txt_title_keywords."'
			WHERE
				(`moduleid`=" .$id.')';
            $this->db->query($query);
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_seo');
    }

    private function buildInsertMode()
    {
        $this->template->assign_block_vars('modules.add', array(
            'FILE' => $this->fromLang('file'),
            'INSTALL' => $this->fromLang('install'),
            'CANCEL' => $this->fromLang('cancel'),
            'ACTIVE' => $this->fromLang('modules_active'),
            'ACTIVE_CHK' => 'checked',
            'CHANGE' => $this->fromLang('modules_change'),
            'REMOVE' => $this->fromLang('modules_remove'),
            'SELECT_FILE' => $this->fromLang('modules_select_file'),
        ));

        if ($this->message != '') {
            $this->template->assign_block_vars('modules.add.alert', array(
                'MESSAGE' => $this->message, ));
        }
    }

    private function deleteModule($id)
    {
        $lang_folder = $this->fromConfig('lang_folder');

        $result = $this->db->query('SELECT
          *
          FROM ' .$this->db->prefix.'modules
          WHERE
             (`moduleid` = ' .$id.')
          && (`deleteable`=1)
        ');
        if ($row = $this->db->fetch($result)) {
            $moduleid = $row['moduleid'];
            $module_name = $row['name'];
            $module_name = trim($module_name);
            $tables = $row['tables'];

            //delete module tables from database
            $tableArr = explode(',', $tables);
            for ($i = 0; $i < count($tableArr); ++$i) {
                $sql = 'DROP TABLE '.$this->db->prefix.$tableArr[$i];
                //echo "<pre>" . $sql . "</pre>";
                $this->db->query($sql);
            }

            // Delete files
            $foldersArr = array();
            $filesArr = array();

            $have_view = $this->utils->filterInt($row['have_view']);
            if ($have_view == 1) {
                $filesArr[] = '../m/modules/'.$module_name.'.view.php';
            }

            $have_admin = $this->utils->filterInt($row['have_admin']);
            if ($have_admin == 1) {
                $filesArr[] = './modules/'.$module_name.'.admin.php';
            }

            $admin_filesArr = explode(',', $row['admin_files']);
            for ($i = 0; $i < count($admin_filesArr); ++$i) {
                if (strlen(trim($admin_filesArr[$i])) > 0) {
                    $filesArr[] = './'.trim($admin_filesArr[$i]);
                }
            }

            $view_filesArr = explode(',', $row['view_files']);
            for ($i = 0; $i < count($view_filesArr); ++$i) {
                if (strlen(trim($view_filesArr[$i])) > 0) {
                    $filesArr[] = '../'.trim($view_filesArr[$i]);
                }
            }

            $have_cnfg = $this->utils->filterInt($row['have_cnfg']);
            if ($have_cnfg == 1) {
                $filesArr[] = '../m/modules/'.$module_name.'.cnfg.php';
                $filesArr[] = '../m/modules/'.$module_name.'.cls.php';
            }

            $folders = $this->utils->io->GetSubFolders('../'.$lang_folder);
            for ($i = 0; $i < count($folders); ++$i) {
                $files = $this->utils->io->GetFiles('../'.$lang_folder.$folders[$i]);
                for ($j = 0; $j < count($files); ++$j) {
                    if ($files[$j] == $module_name.'.lang.php') {
                        $filesArr[] = '../'.$lang_folder.$folders[$i].'/'.$files[$j];
                    }
                }
            }

            $template_folder = $this->fromConfig('template_folder');
            $view_template = $this->fromConfig('view_template');
            $view_templateFiles = explode(',', $row['view_template_files']);
            for ($i = 0; $i < count($view_templateFiles); ++$i) {
                $view_template_file = trim($view_templateFiles[$i]);
                if (strlen($view_template_file) > 0) {
                    if (strpos($view_template_file, './') === false) {
                        if (substr($view_template_file, -1) == '/') {
                            // Is folder
                            $foldersArr[] = '../'.$template_folder.$view_template.'/'.$view_template_file;
                        } else {
                            // Is file
                            $filesArr[] = '../'.$template_folder.$view_template.'/'.$view_template_file;
                        }
                    }
                }
            }

            $admin_template = $this->fromConfig('admin_template');
            $admin_templateFiles = explode(',', $row['admin_template_files']);
            for ($i = 0; $i < count($admin_templateFiles); ++$i) {
                $admin_template_file = trim($admin_templateFiles[$i]);
                if (strlen($admin_template_file) > 0) {
                    if (strpos($admin_template_file, './') === false) {
                        if (substr($admin_template_file, -1) == '/') {
                            // Is folder
                            $foldersArr[] = '../'.$template_folder.$admin_template.'/'.$admin_template_file;
                        } else {
                            // Is file
                            $filesArr[] = '../'.$template_folder.$admin_template.'/'.$admin_template_file;
                        }
                    }
                }
            }

            /*echo "<pre>";
            print_r($filesArr);
            print_r($foldersArr);
            echo "</pre>";*/

            for ($i = 0; $i < count($filesArr); ++$i) {
                $this->io->deleteFile($filesArr[$i]);
            }

            for ($i = 0; $i < count($foldersArr); ++$i) {
                $this->io->DeleteDirTree($foldersArr[$i], true);
            }

            //delete module from database
            $del_sql = 'DELETE FROM '.$this->db->prefix.'modules WHERE moduleid = '.$moduleid;
            //echo "<pre>" . $del_sql . "</pre>";
            $this->db->query($del_sql);

            $del_sql = 'DELETE FROM '.$this->db->prefix.'modulelocalizations WHERE moduleid = '.$moduleid;
            //echo "<pre>" . $del_sql . "</pre>";
            $this->db->query($del_sql);
        }
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'delete_module');
    }

    private function addLocalization($id, $titles, $seo_titles)
    {
        $query = 'DELETE FROM '.$this->db->prefix."modulelocalizations WHERE moduleid='".$id."'";
        $this->db->query($query);

        foreach ($titles as $key => $value) {
            $lang = $key;
            $title = $value;
            $seo_title = $seo_titles[$key];

            $query = 'INSERT INTO '.$this->db->prefix."modulelocalizations(`moduleid`, `lang`, `title`, `seo_title`)
            VALUES('" .$id."', '".$lang."', '".$title."', '".$seo_title."')";
            $this->db->query($query);
        }
    }

    private function updateLocalization($id)
    {
        $query = 'DELETE FROM '.$this->db->prefix."modulelocalizations WHERE moduleid='".$id."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $title = $this->utils->Post('txt_title_'.$lang);
            $seo_title = $this->utils->Post('txt_seo_title_'.$lang);

            $query = 'INSERT INTO '.$this->db->prefix."modulelocalizations(moduleid, lang, `title`, `seo_title`) VALUES('".$id."','".$lang."', '".$title."', '".$seo_title."' )";
            $this->db->query($query);
        }
    }

    private function updateModule($id)
    {
        $active = $this->utils->UserPostInt('txt_active');
        $have_admin = $this->utils->UserPostInt('txt_admin');
        $have_view = $this->utils->UserPostInt('txt_view');
        $icon = $this->utils->UserPost('txt_icon');
        $description = $this->utils->UserPost('txt_description');
        $active = ($active == 1) ? 1 : 0;
        $have_admin = ($have_admin == 1) ? 1 : 0;
        $have_view = ($have_view == 1) ? 1 : 0;
        $query = 'UPDATE `'.$this->db->prefix.'modules`
			SET 
				`active` = ' .$active.", 
				`description` = '" .$description."', 
				`have_admin` = " .$have_admin.', 
				`have_view` = ' .$have_view.", 
				`icon` = '" .$icon."'
			WHERE 
				(`moduleid`=" .$id.')';
        $this->db->query($query);

        $this->updateLocalization($id);
        $this->user->logOperation($this->user->GetUserId(), 'modules', $id, 'update_moodule');
        $this->utils->Redirect('?'.$this->module_qs.'=modules');
    }

    /**
     * Add module process.
     */
    private function addModule()
    {
        $inserted_id = 0;
        $filename = $this->utils->txt_request_filter_user($this->utils->File('txt_file', 'name'));
        /*
         * Check file
         */
        if (strlen($filename) <= 5) {
            $this->message = $this->fromLang('invalid_file');

            return;
        }

        /**
         * Check file type by extension.
         */
        $ext = $this->utils->GetFileExtension($filename);
        if ($ext != 'zip') {
            $this->message = $this->fromLang('invalid_file_type');

            return;
        }

        $temp_folder = $this->fromConfig('temp_folder');
        $template_folder = $this->fromConfig('template_folder');
        $lang_folder = $this->fromConfig('lang_folder');

        //delete temp files
        $this->utils->io->DeleteDirTree('../'.$temp_folder, false);

        $zip = new \mcms5xx\classes\PclZip($this->utils->File('txt_file', 'tmp_name'));
        $list = $zip->listContent();

        /*print("<pre>");
        print_r($list);
        print("</pre>");*/
        //exit();
        //print(count($list));

        $ret = true;
        for ($i = 0; $i < count($list); ++$i) {
            if ($list[$i]['stored_filename'] == 'install.xml') {
                $zip->extract('../'.$temp_folder);
                chmod('../'.$temp_folder, 0777);
                $ret = false;
            }
        }

        /*
         * if in zip file not found install.xml, then delete all data and return
         */
        if ($ret) {
            //delete temp files
            $this->utils->io->DeleteDirTree('../'.$temp_folder, false);
            $this->message = $this->fromLang('invalid_file');

            return;
        }
        //exit();

        $file = '../'.$temp_folder.'install.xml';
        $xml = simplexml_load_file($file);
        /*
         * if install.xml is not correct then delete all data and return
         */
        if (!$xml) {
            //delete temp files
            $this->utils->io->DeleteDirTree('../'.$temp_folder, false);
            $this->message = $this->fromLang('invalid_file');

            return;
        }
        $json = json_encode($xml);
        $data = json_decode($json, true);

        /*print("<pre>");
        print_r($data);
        print("</pre>");*/

        $module_name = $this->utils->txt_request_filter_user($data['name']);

        $result = $this->db->query('SELECT * FROM '.$this->db->prefix."modules WHERE `name` = '".$module_name."'");
        if ($this->db->fetch($result)) {
            //delete temp files
            $this->utils->io->DeleteDirTree('../'.$temp_folder, false);
            $this->message = $this->fromLang('module_exist');

            return;
        }
        $module_description = $this->utils->txt_request_filter_user($data['description']);
        $have_view = $this->utils->filterInt($data['have_view']);
        $have_admin = $this->utils->filterInt($data['have_admin']);
        $have_cnfg = $this->utils->filterInt($data['have_cnfg']);
        $admin_files = $this->utils->txt_request_filter_user($data['admin_files']);
        $view_files = $this->utils->txt_request_filter_user($data['view_files']);
        $admin_template_files = $this->utils->txt_request_filter_user($data['admin_template_files']);
        $view_template_files = $this->utils->txt_request_filter_user($data['view_template_files']);
        $tables = $this->utils->txt_request_filter_user($data['tables']);
        $seo_table = $this->utils->txt_request_filter_user($data['seo_table']);
        $table_id = $this->utils->txt_request_filter_user($data['table_id']);
        $title_header = $this->utils->txt_request_filter_user($data['title_header']);
        $title_description = $this->utils->txt_request_filter_user($data['title_description']);
        $title_keywords = $this->utils->txt_request_filter_user($data['title_keywords']);
        $icon = $this->utils->txt_request_filter_user($data['icon']);
        $view_type = $this->utils->txt_request_filter_user($data['view_type']);
        $active = $this->utils->UserPostInt('txt_active');

        // Copy files and folders
        $foldersArr = array();
        $filesArr = array();

        if ($have_view == 1) {
            if ($this->utils->io->file_exists('../'.$temp_folder.'m/modules/'.$module_name.'.view.php')) {
                $filesArr[] = array(
                    'source' => '../'.$temp_folder.'m/modules/'.$module_name.'.view.php',
                    'dest' => '../m/modules/'.$module_name.'.view.php',
                );
                $have_view = 1;
            } else {
                $have_view = 0;
            }
        }

        if ($have_cnfg == 1) {
            if (
                ($this->utils->io->file_exists('../'.$temp_folder.'m/modules/'.$module_name.'.cnfg.php'))
             && ($this->utils->io->file_exists('../'.$temp_folder.'m/modules/'.$module_name.'.cls.php'))
            ) {
                $filesArr[] = array(
                    'source' => '../'.$temp_folder.'m/modules/'.$module_name.'.cnfg.php',
                    'dest' => '../m/modules/'.$module_name.'.cnfg.php',
                );
                $filesArr[] = array(
                    'source' => '../'.$temp_folder.'m/modules/'.$module_name.'.cls.php',
                    'dest' => '../m/modules/'.$module_name.'.cls.php',
                );
                $have_cnfg = 1;
            } else {
                $have_cnfg = 0;
            }
        }

        if ($have_admin == 1) {
            if ($this->utils->io->file_exists('../'.$temp_folder.'admin/modules/'.$module_name.'.admin.php')) {
                $filesArr[] = array(
                    'source' => '../'.$temp_folder.'admin/modules/'.$module_name.'.admin.php',
                    'dest' => './modules/'.$module_name.'.admin.php',
                );
                $have_admin = 1;
            } else {
                $have_admin = 0;
            }
        }

        $folders = $this->utils->io->GetSubFolders('../'.$temp_folder.'lang/');
        for ($i = 0; $i < count($folders); ++$i) {
            $files = $this->utils->io->GetFiles('../'.$temp_folder.'lang/'.$folders[$i]);
            for ($j = 0; $j < count($files); ++$j) {
                if (($files[$j] == $module_name.'.lang.php') && ($this->utils->io->file_exists('../'.$temp_folder.'lang/'.$folders[$i].'/'.$files[$j]))) {
                    $filesArr[] = array(
                        'source' => '../'.$temp_folder.'lang/'.$folders[$i].'/'.$files[$j],
                        'dest' => '../'.$lang_folder.$folders[$i].'/'.$files[$j],
                    );
                }
            }
        }

        $admin_filesArr = explode(',', $admin_files);
        for ($i = 0; $i < count($admin_filesArr); ++$i) {
            if (strlen(trim($admin_filesArr[$i])) > 0) {
                if ($this->utils->io->file_exists('../'.$temp_folder.'admin/'.trim($admin_filesArr[$i]))) {
                    $filesArr[] = array(
                        'source' => '../'.$temp_folder.'admin/'.trim($admin_filesArr[$i]),
                        'dest' => './'.trim($admin_filesArr[$i]),
                    );
                }
            }
        }

        $view_filesArr = explode(',', $view_files);
        for ($i = 0; $i < count($view_filesArr); ++$i) {
            if (strlen(trim($view_filesArr[$i])) > 0) {
                if ($this->utils->io->file_exists('../'.$temp_folder.'m/'.trim($view_filesArr[$i]))) {
                    $filesArr[] = array(
                        'source' => '../'.$temp_folder.'m/'.trim($view_filesArr[$i]),
                        'dest' => '../'.trim($view_filesArr[$i]),
                    );
                }
            }
        }

        $template_folder = $this->fromConfig('template_folder');
        $view_template = $this->fromConfig('view_template');
        $view_templateFiles = explode(',', $view_template_files);
        for ($i = 0; $i < count($view_templateFiles); ++$i) {
            $view_template_file = trim($view_templateFiles[$i]);
            if (strlen($view_template_file) > 0) {
                if (strpos($view_template_file, './') === false) {
                    if (substr($view_template_file, -1) == '/') {
                        // Is folder
                        $foldersArr[] = array(
                            'source' => '../'.$temp_folder.'templates/view/'.$view_template_file,
                            'dest' => '../'.$template_folder.$view_template.'/'.$view_template_file,
                        );
                    } else {
                        // Is file
                        $filesArr[] = array(
                            'source' => '../'.$temp_folder.'templates/view/'.$view_template_file,
                            'dest' => '../'.$template_folder.$view_template.'/'.$view_template_file,
                        );
                    }
                }
            }
        }

        $admin_template = $this->fromConfig('admin_template');
        $admin_templateFiles = explode(',', $admin_template_files);
        for ($i = 0; $i < count($admin_templateFiles); ++$i) {
            $admin_template_file = trim($admin_templateFiles[$i]);
            if (strlen($admin_template_file) > 0) {
                if (strpos($admin_template_file, './') === false) {
                    if (substr($admin_template_file, -1) == '/') {
                        // Is folder
                        $foldersArr[] = array(
                            'source' => '../'.$temp_folder.'templates/admin/'.$admin_template_file,
                            'dest' => '../'.$template_folder.$admin_template.'/'.$admin_template_file,
                        );
                    } else {
                        // Is file
                        $filesArr[] = array(
                            'source' => '../'.$temp_folder.'templates/admin/'.$admin_template_file,
                            'dest' => '../'.$template_folder.$admin_template.'/'.$admin_template_file,
                        );
                    }
                }
            }
        }

        for ($i = 0; $i < count($filesArr); ++$i) {
            $this->utils->io->copyFile($filesArr[$i]['source'], $filesArr[$i]['dest'], false);
        }

        for ($i = 0; $i < count($foldersArr); ++$i) {
            $this->utils->io->CopyFolder($foldersArr[$i]['source'], $foldersArr[$i]['dest'], false);
        }

        /*echo "<pre>";
        print_r($filesArr);
        print_r($foldersArr);
        echo "</pre>";exit();*/

        //insert to db
        $result = $this->db->query('SELECT MAX(position)+1 AS position FROM '.$this->db->prefix.'slider ');
        $position = 1;
        if ($row = $this->db->fetch($result)) {
            $position = $row['position'];
            if ($position < 1) {
                $position = 1;
            }
        }
        $query = 'INSERT INTO '.$this->db->prefix."modules (`moduleid`, `name`, `description`, `deleteable`, `have_view`, `have_admin`, `have_cnfg`, `admin_files`, `view_files`, `admin_template_files`, `view_template_files`, `tables`, `seo_table`, `table_id`, `title_header`, `title_description`, `title_keywords`, `type`, `icon`, `view_type`, `position`, `active`)
		VALUES(NULL, '" .$module_name."', '".$module_description."', 1, ".$have_view.', '.$have_admin.', '.$have_cnfg.", '".$admin_files."', '".$view_files."', '".$admin_template_files."', '".$view_template_files."', '".$tables."', '".$seo_table."', '".$table_id."', '".$title_header."', '".$title_description."', '".$title_keywords."', 3, '".$icon."', '".$view_type."', ".$position.', '.$active.')';
        if ($this->db->query($query)) {
            $inserted_id = $this->db->insert_id();
            $this->addLocalization($inserted_id, $data['title'], $data['seo_title']);

            //run sql
            for ($i = 0; $i < count($data['sql']); ++$i) {
                $sql = $data['sql'][$i];
                $sql = str_replace('{PREFIX}', $this->db->prefix, $sql);
                $this->db->query($sql);
            }
            $this->utils->io->DeleteDirTree('../'.$temp_folder, false);
        }

        $this->user->logOperation($this->user->GetUserId(), 'modules', $inserted_id, 'add_module');
        $this->utils->Redirect('?'.$this->module_qs.'=modules');
    }
}

$modules = new Modules();
$modules->template->pparse('modules');

/******************* modules.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** modules.admin.php ******************/;
