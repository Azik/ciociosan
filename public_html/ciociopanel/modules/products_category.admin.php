<?php
/******************* section.admin.php *******************
 *
 * Sections admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** section.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class Section extends \mcms5xx\classes\AdminPage
{
    public $show_editbat = 'none';
    public $load_id = 0;
    public $exp_id = 0;

    public function __construct()
    {
        $this->curr_module = 'products_category';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $edit = false;
        $id = $this->utils->UserPostInt('select_id');
        $c_id = $this->utils->UserGetInt('c_id');
        $pid = $this->utils->Get('to');
        if ($c_id != 0) {
            $this->template->assign_var('LEFT_STYLE', 'style="display: none;visibility:hidden; width:100px !important;"');
            $this->load_id = $c_id;
            $this->show_editbat = '';
            if ($c_id != -1) {
                $edit = true;
            }
        }

        $action = $this->utils->Post('action');

        switch ($action) {
            case 'save': {
                $this->sectionSave($pid, $edit, $this->load_id);
                break;
            }
            case 'delete': {
                if (@$this->user->perm_string['1'] == 1) {
                    //Perm for Del
                    $exp_id = $id;
                    $d_result = $this->db->query('SELECT mid, parentid FROM '.$this->db->prefix.'products_category WHERE mid='.$id);
                    if ($d_row = $this->db->fetch($d_result)) {
                        $exp_id = $d_row['parentid'];
                        $this->sectionDelete($id);
                    }
                }
                $this->utils->Redirect('?'.$this->module_qs.'=products_category&exp='.$exp_id);
                break;
            }
        }
    }

    private function sectionSave($pid, $edit, $editid)
    {
        $this->exp_id = $pid;
        if ($edit) {
            if (@$this->user->perm_string['2'] == 1) {
                //Perm for Edit
                $this->sectionUpdate($editid);
            }
        } else {
            if (@$this->user->perm_string['3'] == 1) {
                //Perm for Add
                $this->sectionAdd($pid);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=products_category&exp='.$this->exp_id);
    }

    private function sectionUpdate($id)
    {
        $visible = $this->utils->UserPostInt('txt_visible');

        $query = 'UPDATE '.$this->db->prefix."products_category
					SET
					visible='" .$visible."'
					WHERE mid='" .$id."'
					";
        /* echo($query); */
        $this->db->query($query);
        $this->exp_id = $id;
        $this->user->logOperation($this->user->GetUserId(), 'section', $id, 'section_update');

        $this->addLocalization($id);
    }

    private function addLocalization($mid)
    {
        $query = 'DELETE FROM '.$this->db->prefix."products_categorylocalizations WHERE mid='".$mid."'";
        $this->db->query($query);
        /*$this->site->deleteFromSearch($mid, 'section');*/

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_'.$lang);
			if (strlen($name) > 0) {
				$comment = $text = $slug = '';
				/*if (($type == "content") || ($type == "nolink") || ($type == "module") || ($type == "module")) {
					$comment = $this->utils->Post('txt_comment_' . $lang);
					$text = $this->utils->Post('txt_text_' . $lang);
				}*/
				$comment = $this->utils->Post('txt_comment_'.$lang);
				$text = $this->utils->Post('txt_text_'.$lang);

				$slug = $this->utils->Post('txt_slug_'.$lang);
				$slug = (strlen($slug) > 0) ? $this->generateSlug($slug, $lang) : $this->generateSlug($name, $lang);
				$query = 'INSERT INTO '.$this->db->prefix."products_categorylocalizations(`mid`, `lang`, `name`, `comment`, `text`, `slug`)
				VALUES('" .$mid."', '".$lang."','".$name."','".$comment."', '".$text."', '".$slug."')";
				
				$this->db->query($query);
				/* echo ($query); */
			}
        }
    }

    private function sectionAdd($pid)
    {
        if (!is_numeric($pid) || $pid < 0) {
            $pid = 0;
        }

        $visible = $this->utils->UserPostInt('txt_visible');
        $txt_type = $this->utils->Post('txt_type');
        
        $result = $this->db->query('SELECT MAX(position)+1 AS position FROM '.$this->db->prefix."products_category WHERE parentid='".$pid."'");
        $position = 1;
        if ($row = $this->db->fetch($result)) {
            $position = $row['position'];
            if ($position < 1) {
                $position = 1;
            }
        }


        $query = 'INSERT INTO '.$this->db->prefix."products_category(`parentid`, visible, position) VALUES('".$pid."', '".$visible."','".$position."')";
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();
        $this->exp_id = $inserted_id;

        $this->user->logOperation($this->user->GetUserId(), 'section', $inserted_id, 'section_add');

        $this->addLocalization($inserted_id);
    }

    private function sectionDelete($id)
    {
        $result = $this->db->query('SELECT mid, parentid FROM '.$this->db->prefix.'products_category WHERE parentid='.$id);

        while ($row = $this->db->fetch($result)) {
            $this->sectionDelete($row['mid']);
        }

        $this->user->logOperation($this->user->GetUserId(), 'section', $id, 'section_delete');
        $this->db->query('DELETE FROM '.$this->db->prefix.'products_category WHERE mid='.$id);
        $this->db->query('DELETE FROM '.$this->db->prefix.'products_categorylocalizations WHERE mid='.$id);
    }

    private function buildPage()
    {
        $this->buildMain();
        if ($this->load_id == 0) {
            $this->template->assign_block_vars('mcms_tree', array());
            $this->buildToolBar();
        } else {
            if (@$this->user->perm_string['2'] == 1) {
                //Perm for Edit
                $this->buildEditBar();
            }
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('LANG_LIST', $this->getJSLangsArr());
        $exp = $this->utils->UserGetInt('exp');
        $this->template->assign_var('EXP_ID', $exp);
        $this->template->assign_var('ROOT_MENU_TEXT', $this->fromLang('root_menu_text'));
    }

    private function getJSLangsArr()
    {
        $arr_str = '';
        for ($i = 0; $i < count($this->langs); ++$i) {
            $arr_str .= "'".$this->langs[$i]."'";
            if ($i < count($this->langs) - 1) {
                $arr_str .= ', ';
            }
        }
        //return "var arrLangs = new Array(".$arr_str.")";
        return $arr_str;
    }

    private function buildToolBar()
    {
        $this->template->assign_block_vars('toolbar', array(
            'ADD' => $this->fromLang('toolbar_add'),
            'EDIT' => $this->fromLang('toolbar_edit'),
            'DELETE' => $this->fromLang('toolbar_delete'),
            'DELETE_CONFIRM' => $this->fromLang('toolbar_confirm'),
            'UP' => $this->fromLang('toolbar_up'),
            'DOWN' => $this->fromLang('toolbar_down'),
        ));
        if (@$this->user->perm_string['0'] == 1) {
            // Full perm
            $this->template->assign_block_vars('toolbar.perm_add', array());
            $this->template->assign_block_vars('toolbar.perm_edit', array());
            $this->template->assign_block_vars('toolbar.perm_del', array());
        } else {
            //Perm for Add
            if (@$this->user->perm_string['3'] == 1) {
                $this->template->assign_block_vars('toolbar.perm_add', array());
            }
            //Perm for Edit
            if (@$this->user->perm_string['2'] == 1) {
                $this->template->assign_block_vars('toolbar.perm_edit', array());
            }
            //Perm for Del
            if (@$this->user->perm_string['1'] == 1) {
                $this->template->assign_block_vars('toolbar.perm_del', array());
            }
        }
    }

    private function buildEditBar()
    {
        $visible = 'checked';
        $type = '0';
        $module = '';
        $sel_mod = '';

        if ($this->load_id != 0 && $this->load_id != -1) {
            $query = 'SELECT * FROM '.$this->db->prefix."products_category WHERE mid='".$this->load_id."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $visible = ($row['visible'] == 1) ? 'checked' : '';
            }
        }

        $this->template->assign_block_vars('editbar', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SHOW' => $this->show_editbat,
            'VISIBLE' => $this->fromLang('editbar_visible'),
            'VISIBLE_VALUE' => $visible,
            'MODULE' => $this->fromLang('editbar_module'),
            'SAVE' => $this->fromLang('editbar_save'),
            'CANCEL' => $this->fromLang('editbar_cancel'),
            'TYPEINDEX' => $type,
        ));
        

        if ($this->load_id > 0) {
            $this->template->assign_block_vars('editbar.copy_move', array(
                'COPY' => $this->fromLang('editbar_copy'),
                'MOVE' => $this->fromLang('editbar_move'),
            ));
            $this->template->assign_block_vars('editbar.copy_move.copy', array(
                'VALUE' => '-1',
                'TXT' => '===',
            ));
            $this->template->assign_block_vars('editbar.copy_move.move', array(
                'VALUE' => '-1',
                'TXT' => '===',
            ));
            if ($row['parentid'] != 0) {
                $this->template->assign_block_vars('editbar.copy_move.copy', array(
                    'VALUE' => 0,
                    'TXT' => $this->fromLang('root_menu_text'),
                ));
                $this->template->assign_block_vars('editbar.copy_move.move', array(
                    'VALUE' => 0,
                    'TXT' => $this->fromLang('root_menu_text'),
                ));
            }

            $cm_result = $this->db->query('SELECT M.*, ML.name FROM '.$this->db->prefix.'products_category M
				INNER JOIN ' .$this->db->prefix."products_categorylocalizations ML ON ML.mid = M.mid
				WHERE
					(M.parentid='0') &&
					(ML.lang='" .$this->default_lang."')
				ORDER BY M.parentid, M.position");
            while ($cm_row = $this->db->fetch($cm_result)) {
                $this->template->assign_block_vars('editbar.copy_move.copy', array(
                    'VALUE' => $cm_row['mid'],
                    'TXT' => $cm_row['name'],
                    'CSS' => 'style="background:#C0C0C0;"',
                ));
                $this->template->assign_block_vars('editbar.copy_move.move', array(
                    'VALUE' => $cm_row['mid'],
                    'TXT' => $cm_row['name'],
                    'CSS' => 'style="background:#C0C0C0;"',
                ));
                $this->build_cm($cm_row['mid'], $row['mid']);
            }
        }

        $this->buildModuleList($sel_mod);
        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $comment = $text = $slug = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';
            $query = 'SELECT * FROM '.$this->db->prefix."products_categorylocalizations WHERE mid='".$this->load_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $this->ekranFilter($row['name']);
                $comment = $row['comment'];
                $text = $row['text'];
                $slug = $row['slug'];
                $text = $this->rteSafe($text);
                //$text = addslashes(str_replace("\r",' ',str_replace("\n",' ',$text)));
            }

            $this->template->assign_block_vars('editbar.tab', array(
                'LANG' => $lang,
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'NAME' => $this->fromLang('editbar_name'),
                'NAME_VALUE' => $name,
                'COMMENT' => $this->fromLang('editbar_comment'),
                'COMMENT_VALUE' => $comment,
                'TEXT' => $this->fromLang('editbar_text'),
                'TEXT_VALUE' => htmlspecialchars($text),
                'SLUG' => $this->fromLang('editbar_slug'),
                'SLUG_VALUE' => $slug
            ));
        }
    }

    private function build_cm($id, $mid = 0, $css = 0, $space = '')
    {
        if ($id != $mid) {
            $cm_result = $this->db->query('SELECT M.*, ML.name FROM '.$this->db->prefix.'products_category M
				INNER JOIN ' .$this->db->prefix.'products_categorylocalizations ML ON ML.mid = M.mid
				WHERE
					(M.parentid=' .$id.") &&
					(ML.lang='" .$this->default_lang."')
				ORDER BY M.parentid, M.position
			");
            $css += 10;
            $space .= '&nbsp;';
            $css_value = 'style="margin-left:'.$css.'px;"';
            while ($cm_row = $this->db->fetch($cm_result)) {
                if ($cm_row['mid'] != $mid) {
                    $this->template->assign_block_vars('editbar.copy_move.copy', array(
                        'VALUE' => $cm_row['mid'],
                        'TXT' => $space.$cm_row['name'],
                        'CSS' => $css_value,
                    ));
                    $this->template->assign_block_vars('editbar.copy_move.move', array(
                        'VALUE' => $cm_row['mid'],
                        'TXT' => $space.$cm_row['name'],
                        'CSS' => $css_value,
                    ));
                    $this->build_cm($cm_row['mid'], $mid, $css, $space);
                }
            }
        }
    }

    private function buildModuleList($sel_mod)
    {
        $result = $this->db->query('SELECT * FROM '.$this->db->prefix.'modules WHERE (`have_view`=1) && (`active`=1)');
        while ($row = $this->db->fetch($result)) {
            $id = $row['moduleid'];
            $name = $row['name'];
            $description = $row['description'];
            $title = '';

            $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."modulelocalizations WHERE moduleid='".$id."' AND lang='".$this->lang."'");
            if ($row_loc = $this->db->fetch($result_loc)) {
                $title = $row_loc['title'];
            }

            $mod_name = $name;
            $mod_text = $title;

            $selected = '';
            if ($mod_name == $sel_mod) {
                $selected = 'selected';
            }
            $this->template->assign_block_vars('editbar.module', array(
                'SELECTED' => $selected,
                'NAME' => $mod_name,
                'TEXT' => $mod_text,
            ));
        }
    }

    private function rteSafe($strText)
    {
        //returns safe code for preloading in the RTE
        $tmpString = $strText;

        //convert all types of single quotes
        /*$tmpString = str_replace(chr(145), chr(39), $tmpString);
        $tmpString = str_replace(chr(146), chr(39), $tmpString);
        $tmpString = str_replace("'", "&#39;", $tmpString);*/

        //convert all types of double quotes
        /*$tmpString = str_replace(chr(147), chr(34), $tmpString);
        $tmpString = str_replace(chr(148), chr(34), $tmpString);*/
        //$tmpString = str_replace("\"", "\"", $tmpString);

        //replace carriage returns & line feeds
        /*$tmpString = str_replace(chr(10), " ", $tmpString);
        $tmpString = str_replace(chr(13), " ", $tmpString);*/

        return $tmpString;
    }
}

$section = new Section();
$section->template->pparse('products_category');

/******************* section.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** section.admin.php ******************/;
