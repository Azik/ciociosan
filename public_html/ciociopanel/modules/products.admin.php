<?php
/******************* products.admin.php *******************
 *
 * products admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** products.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class products extends \mcms5xx\classes\AdminPage
{
    protected $productsCount;

    public function __construct()
    {
        $this->curr_module = 'products';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $id = $this->utils->Post('select_id');

        $products_action = $this->utils->Post('products_action');

        switch ($products_action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteproducts($id);
                }
                break;
            }
            case 'save': {
                $this->saveproducts($id);
                break;
            }
        }
    }

    private function deleteproducts($id)
    {
		$this->db->delete($this->db->prefix . 'products', " sid='".$id."'");
		$this->db->delete($this->db->prefix . 'productslocalizations', " sid='".$id."'");
        $this->user->logOperation($this->user->GetUserId(), 'products', $id, 'delete');
    }

    private function addproductsLocalization($productsid)
    {
		$this->db->delete($this->db->prefix . 'productslocalizations', " sid='".$productsid."'");

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_'.$lang);
            $more_txt = $this->utils->Post('txt_more_txt_'.$lang);
            $text = $this->utils->Post('txt_text_'.$lang);
            $txt_slug = $this->utils->Post('txt_slug_'.$lang);
			$txt_slug = (strlen($txt_slug) > 0) ? $this->generateSlug($txt_slug, $lang, 'productslocalizations') : $this->generateSlug($name, $lang, 'productslocalizations');

			$dataInsert = array();
			$dataInsert['lang'] = $lang;
			$dataInsert['sid'] = $productsid;
			$dataInsert['name'] = $name;
			$dataInsert['slug'] = $txt_slug;
			$dataInsert['more_txt'] = $more_txt;
			$dataInsert['text'] = $text;
			$insert_id = $this->db->insert($this->db->prefix.'productslocalizations' , $dataInsert);

        }
    }

    private function saveproducts($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addproducts();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateproducts($id);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=products');
    }

    private function addproducts()
    {
        $active = $this->utils->UserPostInt('txt_active');
        $txt_img = $this->utils->UserPostInt('txt_img');
        $txt_category = $this->utils->UserPostInt('txt_category');
        $txt_amount = $this->utils->Post('txt_amount');
        $txt_discount = $this->utils->Post('txt_discount');
        
        $result = $this->db->query('SELECT MAX(position)+1 AS position FROM '.$this->db->prefix.'products ');
        $position = 1;
        if ($row = $this->db->fetch($result)) {
            $position = $row['position'];
            if ($position < 1) {
                $position = 1;
            }
        }

		$dataInsert = array();
		$dataInsert['category'] = $txt_category;
		$dataInsert['amount'] = $txt_amount;
		$dataInsert['discount'] = $txt_discount;
		$dataInsert['img'] = $txt_img;
		$dataInsert['position'] = $position;
		$dataInsert['active'] = $active;
		$inserted_id = $this->db->insert($this->db->prefix.'products' , $dataInsert);

        //$this->order_me();

        $this->addproductsLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'products', $inserted_id, 'add');
    }

    private function updateproducts($id)
    {
        $active = $this->utils->UserPostInt('txt_active');
        $txt_img = $this->utils->UserPostInt('txt_img');
        $txt_category = $this->utils->UserPostInt('txt_category');
        $txt_amount = $this->utils->Post('txt_amount');
        $txt_discount = $this->utils->Post('txt_discount');

		$dataUpdate = array();
		$dataUpdate['category'] = $txt_category;
		$dataUpdate['amount'] = $txt_amount;
		$dataUpdate['discount'] = $txt_discount;
		$dataUpdate['img'] = $txt_img;
		$dataUpdate['active'] = $active;
		$this->db->update($this->db->prefix.'products' , $dataUpdate, " sid=".$id."");

        $this->addproductsLocalization($id);

        $this->user->logOperation($this->user->GetUserId(), 'products', $id, 'update');
    }

    private function buildPage()
    {
        $this->buildMain();

        $productsid = $this->utils->Get('productsid');
        if (!is_numeric($productsid) || $productsid == 0) {
            $this->buildproducts();
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('DELETE_CONFIRM', $this->fromLang('products_confirm'));
    }

    private function buildproducts()
    {
        $this->template->assign_block_vars('products', array());

        $pos = $this->utils->Get('pos');

        if ($pos == 'up') {
            $id = $this->utils->Get('id');
            $table = $this->db->prefix.'products';
            $sel = $this->db->query('SELECT * FROM `'.$table.'` WHERE sid='.$id.' ');
            if ($row = $this->db->fetch($sel)) {
                $sub_query = 'SELECT * FROM `'.$table.'` WHERE (position<'.$row['position'].') ORDER BY position DESC';
                $sub_sel = $this->db->query($sub_query);
                if ($sub_row = @$this->db->fetch($sub_sel)) {
                    $this->db->query('UPDATE `'.$table."` SET `position`=`position`+1 WHERE sid='".$sub_row['sid']."' ");
                }
                $this->db->query('UPDATE `'.$table."` SET `position`=`position`-1 WHERE sid='".$row['sid']."' ");
            }
            $this->order_me();
            $this->utils->Redirect('?'.$this->module_qs.'=products');
            @header('location: index.php?module=products');
        } elseif ($pos == 'down') {
            $id = $this->utils->Get('id');
            $table = $this->db->prefix.'products';
            $sel = $this->db->query('SELECT * FROM `'.$table.'` WHERE sid='.$id.' ');
            if ($row = $this->db->fetch($sel)) {
                $sub_query = 'SELECT * FROM `'.$table.'` WHERE (`position`>'.$row['position'].') ORDER BY `position`';
                $sub_sel = $this->db->query($sub_query);
                if ($sub_row = $this->db->fetch($sub_sel)) {
                    $this->db->query('UPDATE `'.$table."` SET `position`=`position`-1 WHERE sid='".$sub_row['sid']."' ");
                }
                $this->db->query('UPDATE `'.$table."` SET `position`=`position`+1 WHERE sid='".$row['sid']."' ");
            }
            $this->order_me();
            $this->utils->Redirect('?'.$this->module_qs.'=products');
            @header('location: index.php?module=products');
        }

        $products_edit_id = $this->utils->Get('productseditid');
        if (!is_numeric($products_edit_id) || $products_edit_id == 0) {
            if (strlen($products_edit_id) > 5) {
                $actArr = explode(':', $products_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `sid` IN ( '.$actArr['1'].' )' : ' `gid`=0';
                $query = 'UPDATE '.$this->db->prefix.'products SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=products');
            } else {
                $this->buildproductsList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'products SET '.$activeSql." WHERE `sid`='".$products_edit_id."'";
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=products');
            } else {
                $this->buildproductsedit($products_edit_id);
            }
        }
    }

    private function order_me()
    {
        $table = $this->db->prefix.'products';
        $sel = $this->db->query('SELECT * FROM '.$table.' ORDER BY `position` ASC');
        $pos_id = 0;
        while ($row = $this->db->fetch($sel)) {
            ++$pos_id;
            $this->db->query('UPDATE  '.$table." SET `position`='".$pos_id."' WHERE sid='".$row['sid']."' ");
        }

    }

    private function buildproductsList()
    {
        $this->template->assign_block_vars('products.list', array(
            'products' => $this->fromLang('products_title'),
            'ADD' => $this->fromLang('products_add'),
            'ADD_URL' => '?'.$this->module_qs.'=products&productseditid=-1',
            'ACTIVE' => $this->fromLang('products_active'),
            'INACTIVE' => $this->fromLang('products_inactive'),
            'ACTIVATE' => $this->fromLang('products_activate'),
            'INACTIVATE' => $this->fromLang('products_inactivate'),
            'URL' => '?'.$this->module_qs.'=products',
            'NAME' => $this->fromLang('products_name'),
            'UP_DOWN' => $this->fromLang('up_down'),
            'EDIT' => $this->fromLang('products_edit'),
            'DELETE' => $this->fromLang('products_delete'),
            'DELETE_CONFIRM' => $this->fromLang('products_confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('products.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('products.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('products.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';
        $sql = 'SELECT S.*, SL.name
			FROM ' .$this->db->prefix.'products S
			INNER JOIN ' .$this->db->prefix."productslocalizations SL ON SL.sid=S.sid
			WHERE
				SL.lang='" .$this->default_lang."'
			ORDER BY S.position ASC";
        $result = $this->db->query($sql);
        $products_sayi = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['sid'];
            $position = $row['position'];
            $name = $this->utils->GetPartOfString($row['name'], 100);
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $status = ($row['active'] == 1) ? ' checked="checked"' : '';

            $this->template->assign_block_vars('products.list.items', array(
                'ID' => $id,
                'POSITION' => $position,
                'NAME' => $name,
                'STATUS' => $status,
                'EDIT_URL' => '?'.$this->module_qs.'=products&productseditid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=products&productseditid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=products&productseditid='.$id.'&active=2',
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('products.list.items.perm_edit', array());
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('products.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('products.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildproductsedit($products_edit_id)
    {
        $products_query = 'SELECT * FROM '.$this->db->prefix."products WHERE sid='".$products_edit_id."' ";
        $products_result = $this->db->query($products_query);
        

		$category = 0;
		$active_chk = '';
        $active_chk = '';
        $txt_img = 0;
		$txt_amount = 0;
		$txt_discount = 0;
        if ($products_row = $this->db->fetch($products_result)) {
			
			$category = $products_row['category'];
			$txt_amount = $products_row['amount'];
			$txt_discount = $products_row['discount'];
			$txt_img = $products_row['img'];
			$active_chk = (($products_row['active'] == 1) || ($products_edit_id == -1)) ? 'checked' : '';
		}
		
        $this->template->assign_block_vars('products.productsedit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'products_TYPE' => $this->fromLang('products_type'),
            'ACTIVE' => $this->fromLang('products_active'),
            'ACTIVE_CHK' => $active_chk,
            'products_IMG' => $this->fromLang('products_img'),
            'TXT_AMOUNT' => $txt_amount,
            'TXT_discount' => $txt_discount,
            'TXT_IMG' => $txt_img,
            'SAVE' => $this->fromLang('products_save'),
            'CANCEL' => $this->fromLang('products_cancel'),
            'URL' => '?'.$this->module_qs.'=products',
            'ID' => $products_edit_id,
        ));


		$this->buildDBMenu($category);
		
    
		
        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $more_txt = $text_value = $slug = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."productslocalizations WHERE sid='".$products_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $slug = $row['slug'];
                $more_txt = $row['more_txt'];
                $text_value = $row['text'];
            }

            $this->template->assign_block_vars('products.productsedit.tab', array(
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'LANG' => $lang,
                'NAME' => $this->fromLang('products_name'),
                'NAME_VALUE' => $name,
                'SLUG_VALUE' => $slug,
                'MORE_TXT' => $this->fromLang('products_more_txt'),
                'MORE_TXT_VALUE' => $more_txt,
                'TEXT' => $this->fromLang('products_text'),
                'TEXT_VALUE' => $text_value,
            ));
        }
    }



    /**
     * buildDBMenu Method
     * Show menu.
     *
     *
     * @param string $menu_value
     */
    public function buildDBMenu($category)
    {
		$menu_value = 'products.productsedit.category';
        $sql = 'SELECT
					M.mid
			FROM `' . $this->db->prefix . "products_category` M
			WHERE
				( M.`parentid` = 0 )
			AND ( M.`visible`  = 1 )
			ORDER BY M.`position`
		";
        $result = $this->db->query($sql);
        if ($row = $this->db->fetch_assoc($result)) {
            /** parent menu id */
            $parentid = $row['mid'];


            /** Menu index  */
            $indx = 0;

            $sub_query = 'SELECT
					M.*,
					ML.name,
					ML.comment,
					ML.text,
					ML.header,
					ML.slug,
					COUNT(MS.mid) AS sub_counts
				FROM `' . $this->db->prefix . 'products_category` M
				LEFT JOIN `' . $this->db->prefix . 'products_categorylocalizations` ML ON M.mid = ML.mid
				LEFT JOIN `' . $this->db->prefix . "products_category` MS ON (M.visible='1') AND (MS.parentid = M.mid)
				WHERE
					( M.`visible` = 1 )
				AND ( M.`parentid`= " . $parentid . " )
				AND ( ML.`lang`	 = '" . $this->lang . "' )
				AND ( ML.`name`	!='' )
				GROUP BY M.`mid`
				ORDER BY M.`position`
			";
            $sub_result = $this->db->query($sub_query);
            $num_rows = $this->db->num_result_rows($sub_result);
            while ($sub_row = $this->db->fetch_assoc($sub_result)) {
                ++$indx;
                $mid = $sub_row['mid'];

                $this->blockRow($menu_value, $sub_row, $indx, $num_rows, $category);

                /** If have sub menus */
                $sub_counts = $sub_row['sub_counts'];
                if ($sub_counts > 0) {
                    $this->buildSubMenu($menu_value, $mid, 1, $category);
                }
            }
        }
    }


    /** Section manager: inside, menu */
    /**
     * buildSubMenu Method
     * Show Sub menu.
     *
     *
     * @param string $menu_value
     * @param int $parentid
     * @param int $level
     * @param int $id
     */
    public function buildSubMenu($menu_value = 'top_menu', $parentid = 0, $level = 1, $id = 0)
    {
        $levelBlock = ($level == 1) ? '' : $level;
        $levelIn = ++$level;
        $si = 0;
        $sub_query = 'SELECT
				M.*,
				ML.name,
				ML.comment,
				ML.text,
				ML.header,
				ML.slug,
				COUNT(MS.mid) AS sub_counts
			FROM `' . $this->db->prefix . 'products_category` M
			LEFT JOIN `' . $this->db->prefix . 'products_categorylocalizations` ML ON ML.mid = M.mid
			LEFT JOIN `' . $this->db->prefix . "products_category` MS ON (M.visible='1') AND (MS.parentid = M.mid)
			WHERE
				( M.`visible` = 1 )
			AND ( M.`parentid`= " . $parentid . " )
			AND ( ML.`lang`	  = '" . $this->lang . "' )
			AND ( ML.`name`	  != '' )
			GROUP BY M.`mid`
			ORDER BY M.`position`
		";
        $sub_result = $this->db->query($sub_query);
        $num_rows = $this->db->num_result_rows($sub_result);
        while ($sub_row = $this->db->fetch($sub_result)) {
            ++$si;
            $mid = $sub_row['mid'];
            if ($si == 1) {
                $this->template->assign_block_vars($menu_value . '.sub' . $levelBlock, array());
            }

                $this->blockRow($menu_value . '.sub' . $levelBlock . '.menu' . $levelBlock, $sub_row, $si, $num_rows, $id);
                $this->blockRow($menu_value . '.sub_menu' . $levelBlock, $sub_row, $si, $num_rows, $id);

                /** If have sub menus */
                $sub_counts = $sub_row['sub_counts'];
                if ($sub_counts > 0) {
                    $this->buildSubMenu($menu_value . '.sub' . $levelBlock . '.menu' . $levelBlock, $mid, $levelIn, $id);
                }
            
        }
    }

    /**
     * Menu show.
     *
     * @param string $blockname
     * @param array $row (mid
     *                           parentid
     *                           name
     *                           comment
     *                           text
     *                           newwindow (0, 1)
     *                           menutype
     *                           link
     *                           lang_link)
     * @param int $curr_index
     * @param int $num_rows
     * @param int $id
     */
    public function blockRow($blockname = 'top_menu', $row = array('mid' => 0, 'parentid' => 0, 'name' => '', 'comment' => '', 'text' => '', 'newwindow' => 0, 'menutype' => 'content', 'link' => '', 'lang_link' => ''), $curr_index = 1, $num_rows = 0, $id = 0)
    {
        /** ID. From menu table */
        $mid = $row['mid'];
		$selected = ($mid == $id) ? "selected='selected'" : "";
        $this->template->assign_block_vars($blockname, array(
            'ID' => $mid,
            'SLUG' => $row['slug'],
            'NAME' => $row['name'],
            'NAME_UP' => $row['name'],
            'TITLE' => $row['name'],
            'SELECTED' => $selected,
            'COMMENT' => $row['comment'],
            'TEXT' => $row['text'],
        ));

    }


    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->productsCount);

        /*
        print("<pre>");
        print_r($arr);
        print("</pre>");
        */

        $prev_url = '';

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="'.$arr['prefix'].$arr['PREV_LINK_PAGE'].'"';
        }

        $next_url = '';

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="'.$arr['prefix'].$arr['NEXT_LINK_PAGE'].'"';
        }

        $this->template->assign_block_vars('products.list.paging', array(
            'PREV_URL' => $prev_url,
            'NEXT_URL' => $next_url, ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="'.$arr['prefix'].$num.'"';
            }

            $this->template->assign_block_vars('products.list.paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num, ));

            if ($sep) {
                $this->template->assign_block_vars('products.list.paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('products_admin_in_page');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function getproductsName($productsid)
    {
        $name = '';

        $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."productscategorylocalizations WHERE productsid='".$productsid."' AND lang='".$this->default_lang."'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }
}

$products = new products();
$products->template->pparse('products');

/******************* products.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** products.admin.php ******************/;
