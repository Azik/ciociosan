<?php
/******************* slider.admin.php *******************
 *
 * Slider admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** slider.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

require_once '../m/classes/paging.class.php';

class Slider extends \mcms5xx\classes\AdminPage
{
    protected $sliderCount;

    public function __construct()
    {
        $this->curr_module = 'slider';
        parent::__construct();
        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->doAction();
        $this->buildPage();
    }

    private function doAction()
    {
        $id = $this->utils->Post('select_id');

        $slide_action = $this->utils->Post('slide_action');

        switch ($slide_action) {
            case 'delete': {
                if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                    //Perm for Del
                    $this->deleteSlide($id);
                }
                break;
            }
            case 'save': {
                $this->saveSlide($id);
                break;
            }
        }
    }

    private function deleteSlide($id)
    {
        $this->db->query('DELETE FROM '.$this->db->prefix.'slider WHERE sid='.$id);
        $this->db->query('DELETE FROM '.$this->db->prefix.'sliderlocalizations WHERE sid='.$id);
        $this->user->logOperation($this->user->GetUserId(), 'slider', $id, 'delete');
    }

    private function addSlideLocalization($slideid)
    {
        $query = 'DELETE FROM '.$this->db->prefix."sliderlocalizations WHERE sid='".$slideid."'";
        $this->db->query($query);

        foreach ($this->langs as $key => $value) {
            $lang = $value;
            $name = $this->utils->Post('txt_name_'.$lang);
            $more_txt = $this->utils->Post('txt_more_txt_'.$lang);
            $text = $this->utils->Post('txt_text_'.$lang);
            $embed_code = $this->utils->Post('txt_embed_code_'.$lang);
            $url = $this->utils->Post('txt_url_'.$lang);

            $query = 'INSERT INTO '.$this->db->prefix."sliderlocalizations(`sid`, `lang`, `name`, `more_txt`, `text`, `embed_code`, `url`)
            VALUES('" .$slideid."', '".$lang."', '".$name."', '".$more_txt."', '".$text."', '".$embed_code."', '".$url."')";
            $this->db->query($query);
        }
    }

    private function saveSlide($id)
    {
        if ($id == -1) {
            if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Add
                $this->addSlide();
            }
        } else {
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->updateSlide($id);
            }
        }

        $this->utils->Redirect('?'.$this->module_qs.'=slider');
    }

    private function addSlide()
    {
        $active = $this->utils->UserPostInt('txt_active');
        $txt_img = $this->utils->UserPostInt('txt_img');

        $result = $this->db->query('SELECT MAX(position)+1 AS position FROM '.$this->db->prefix.'slider ');
        $position = 1;
        if ($row = $this->db->fetch($result)) {
            $position = $row['position'];
            if ($position < 1) {
                $position = 1;
            }
        }

        $query = 'INSERT INTO '.$this->db->prefix."slider(`sid`, `slide_img`, `position`, `active`)
			VALUES('', " .$txt_img.', '.$position.', '.$active.')';
        $this->db->query($query);
        $inserted_id = $this->db->insert_id();

        //$this->order_me();

        $this->addSlideLocalization($inserted_id);

        $this->user->logOperation($this->user->GetUserId(), 'slider', $inserted_id, 'add');
    }

    private function updateSlide($id)
    {
        $active = $this->utils->UserPostInt('txt_active');
        $txt_img = $this->utils->UserPostInt('txt_img');

        $query = 'UPDATE '.$this->db->prefix."slider SET `slide_img`='".$txt_img."', `active`='".$active."' WHERE `sid`='".$id."'";
        $this->db->query($query);

        $this->addSlideLocalization($id);

        $this->user->logOperation($this->user->GetUserId(), 'slider', $id, 'update');
    }

    private function buildPage()
    {
        $this->buildMain();

        $slideid = $this->utils->Get('slideid');
        if (!is_numeric($slideid) || $slideid == 0) {
            $this->buildSlide();
        }
    }

    private function buildMain()
    {
        $this->template->assign_var('TITLE', $this->fromLang('title'));
        $this->template->assign_var('DELETE_CONFIRM', $this->fromLang('slide_confirm'));
    }

    private function buildSlide()
    {
        $this->template->assign_block_vars('slide', array());

        $pos = $this->utils->Get('pos');

        if ($pos == 'up') {
            $id = $this->utils->Get('id');
            $table = $this->db->prefix.'slider';
            $sel = $this->db->query('SELECT * FROM `'.$table.'` WHERE sid='.$id.' ');
            if ($row = $this->db->fetch($sel)) {
                $sub_query = 'SELECT * FROM `'.$table.'` WHERE (position<'.$row['position'].') ORDER BY position DESC';
                $sub_sel = $this->db->query($sub_query);
                if ($sub_row = @$this->db->fetch($sub_sel)) {
                    $this->db->query('UPDATE `'.$table."` SET `position`=`position`+1 WHERE sid='".$sub_row['sid']."' ");
                }
                $this->db->query('UPDATE `'.$table."` SET `position`=`position`-1 WHERE sid='".$row['sid']."' ");
            }
            $this->order_me();
            $this->utils->Redirect('?'.$this->module_qs.'=slider');
            @header('location: index.php?module=slider');
        } elseif ($pos == 'down') {
            $id = $this->utils->Get('id');
            $table = $this->db->prefix.'slider';
            $sel = $this->db->query('SELECT * FROM `'.$table.'` WHERE sid='.$id.' ');
            if ($row = $this->db->fetch($sel)) {
                $sub_query = 'SELECT * FROM `'.$table.'` WHERE (`position`>'.$row['position'].') ORDER BY `position`';
                $sub_sel = $this->db->query($sub_query);
                if ($sub_row = $this->db->fetch($sub_sel)) {
                    $this->db->query('UPDATE `'.$table."` SET `position`=`position`-1 WHERE sid='".$sub_row['sid']."' ");
                }
                $this->db->query('UPDATE `'.$table."` SET `position`=`position`+1 WHERE sid='".$row['sid']."' ");
            }
            $this->order_me();
            $this->utils->Redirect('?'.$this->module_qs.'=slider');
            @header('location: index.php?module=slider');
        }

        $slide_edit_id = $this->utils->Get('slideeditid');
        if (!is_numeric($slide_edit_id) || $slide_edit_id == 0) {
            if (strlen($slide_edit_id) > 5) {
                $actArr = explode(':', $slide_edit_id);
                $activeSql = ($actArr['0'] == 'activate') ? ' `active`=1' : ' `active`=0';
                $ids = (strlen($actArr['1']) > 0) ? ' `sid` IN ( '.$actArr['1'].' )' : ' `gid`=0';
                $query = 'UPDATE '.$this->db->prefix.'slider SET '.$activeSql.' WHERE '.$ids.'';
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=slider');
            } else {
                $this->buildSlideList();
            }
        } else {
            $active = $this->utils->UserGetInt('active');
            if ($active > 0) {
                $activeSql = ($active == 1) ? ' `active`=0' : ' `active`=1';
                $query = 'UPDATE '.$this->db->prefix.'slider SET '.$activeSql." WHERE `sid`='".$slide_edit_id."'";
                $this->db->query($query);
                $this->utils->Redirect('?'.$this->module_qs.'=slider');
            } else {
                $this->buildSlideEdit($slide_edit_id);
            }
        }
    }

    private function order_me()
    {
        $table = $this->db->prefix.'slider';
        $sel = $this->db->query('SELECT * FROM '.$table.' ORDER BY `position` ASC');
        $pos_id = 0;
        while ($row = $this->db->fetch($sel)) {
            ++$pos_id;
            $this->db->query('UPDATE  '.$table." SET `position`='".$pos_id."' WHERE sid='".$row['sid']."' ");
        }

    }

    private function buildSlideList()
    {
        $this->template->assign_block_vars('slide.list', array(
            'SLIDE' => $this->fromLang('slide_title'),
            'ADD' => $this->fromLang('slide_add'),
            'ADD_URL' => '?'.$this->module_qs.'=slider&slideeditid=-1',
            'ACTIVE' => $this->fromLang('slider_active'),
            'INACTIVE' => $this->fromLang('slider_inactive'),
            'ACTIVATE' => $this->fromLang('slider_activate'),
            'INACTIVATE' => $this->fromLang('slider_inactivate'),
            'URL' => '?'.$this->module_qs.'=slider',
            'NAME' => $this->fromLang('slide_name'),
            'UP_DOWN' => $this->fromLang('up_down'),
            'EDIT' => $this->fromLang('slide_edit'),
            'DELETE' => $this->fromLang('slide_delete'),
            'DELETE_CONFIRM' => $this->fromLang('slide_confirm'),
        ));
        if ((@$this->user->perm_string['3'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Add
            $this->template->assign_block_vars('slide.list.perm_add', array());
        }
        if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Edit
            $this->template->assign_block_vars('slide.list.perm_edit', array());
        }
        if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
            //Perm for Del
            $this->template->assign_block_vars('slide.list.perm_del', array());
        }

        $ndx = 0;
        $hidden_val = '';
        $sql = 'SELECT S.*, SL.name
			FROM ' .$this->db->prefix.'slider S
			INNER JOIN ' .$this->db->prefix."sliderlocalizations SL ON SL.sid=S.sid
			WHERE
				SL.lang='" .$this->default_lang."'
			ORDER BY S.position ASC";
        $result = $this->db->query($sql);
        $slide_sayi = $this->db->num_rows($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['sid'];
            $position = $row['position'];
            $name = $this->utils->GetPartOfString($row['name'], 100);
            $hidden_val .= ($ndx == 0) ? '' : ',';
            $hidden_val .= $id;
            ++$ndx;
            $status = ($row['active'] == 1) ? ' checked="checked"' : '';

            $this->template->assign_block_vars('slide.list.items', array(
                'ID' => $id,
                'POSITION' => $position,
                'NAME' => $name,
                'STATUS' => $status,
                'EDIT_URL' => '?'.$this->module_qs.'=slider&slideeditid='.$id,
                'ACTIVE_URL' => '?'.$this->module_qs.'=slider&slideeditid='.$id.'&active=1',
                'INACTIVE_URL' => '?'.$this->module_qs.'=slider&slideeditid='.$id.'&active=2',
            ));
            if ((@$this->user->perm_string['2'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Edit
                $this->template->assign_block_vars('slide.list.items.perm_edit', array());
            }
            if ((@$this->user->perm_string['1'] == 1) || (@$this->user->perm_string['0'] == 1)) {
                //Perm for Del
                $this->template->assign_block_vars('slide.list.items.perm_del', array());
            }
        }
        $this->template->assign_block_vars('slide.list.hidden', array(
            'VALUE' => $hidden_val,
        ));
    }

    private function buildSlideEdit($slide_edit_id)
    {
        $slide_query = 'SELECT * FROM '.$this->db->prefix."slider WHERE sid='".$slide_edit_id."' ";
        $slide_result = $this->db->query($slide_query);
        $slide_row = $this->db->fetch($slide_result);

        $active_chk = (($slide_row['active'] == 1) || ($slide_edit_id == -1)) ? 'checked' : '';
        $txt_img = $slide_row['slide_img'];

        $this->template->assign_block_vars('slide.slideedit', array(
            'TAB_HEADER_WIDTH' => (count($this->langs) * 75),
            'SLIDE_TYPE' => $this->fromLang('slide_type'),
            'ACTIVE' => $this->fromLang('slide_active'),
            'ACTIVE_CHK' => $active_chk,
            'SLIDE_IMG' => $this->fromLang('slide_img'),
            'TXT_IMG' => $txt_img,
            'SAVE' => $this->fromLang('slide_save'),
            'CANCEL' => $this->fromLang('slide_cancel'),
            'URL' => '?'.$this->module_qs.'=slider',
            'ID' => $slide_edit_id,
        ));

        $ndx = 0;
        foreach ($this->langs as $key => $value) {
            ++$ndx;
            $lang = $value;
            $name = $more_txt = $text_value = $embed_code_value = $url = '';
            $class = ($ndx == 1) ? ' class="active"' : '';
            $fade_class = ($ndx == 1) ? ' in active' : '';

            $query = 'SELECT * FROM '.$this->db->prefix."sliderlocalizations WHERE sid='".$slide_edit_id."' AND lang='".$lang."'";
            $result = $this->db->query($query);
            if ($row = $this->db->fetch($result)) {
                $name = $row['name'];
                $more_txt = $row['more_txt'];
                $text_value = $row['text'];
                $embed_code_value = $row['embed_code'];
                $url = $row['url'];
            }

            $this->template->assign_block_vars('slide.slideedit.tab', array(
                'CLASS' => $class,
                'FADE_CLASS' => $fade_class,
                'LANG' => $lang,
                'NAME' => $this->fromLang('slide_name'),
                'NAME_VALUE' => $name,
                'MORE_TXT' => $this->fromLang('slide_more_txt'),
                'MORE_TXT_VALUE' => $more_txt,
                'TEXT' => $this->fromLang('slide_text'),
                'TEXT_VALUE' => $text_value,
                'EMBED_CODE' => $this->fromLang('slide_embed_code'),
                'EMBED_CODE_VALUE' => $embed_code_value,
                'URL' => $this->fromLang('slide_url'),
                'URL_VALUE' => $url,
            ));
        }
    }

    private function buildPaging()
    {
        $arr = $this->getPagingArray($this->sliderCount);

        /*
        print("<pre>");
        print_r($arr);
        print("</pre>");
        */

        $prev_url = '';

        if ($arr['PREV_LINK_PAGE']) {
            $prev_url = 'href="'.$arr['prefix'].$arr['PREV_LINK_PAGE'].'"';
        }

        $next_url = '';

        if ($arr['NEXT_LINK_PAGE']) {
            $next_url = 'href="'.$arr['prefix'].$arr['NEXT_LINK_PAGE'].'"';
        }

        $this->template->assign_block_vars('slider.list.paging', array(
            'PREV_URL' => $prev_url,
            'NEXT_URL' => $next_url, ));

        /*
        print("<pre>");
        print_r($arr["PAGE_NUMBERS"]);
        print("</pre>");
        */

		$count = (is_array($arr['PAGE_NUMBERS'])) ? count($arr['PAGE_NUMBERS']) : 0;
        for ($i = 0; $i < $count; ++$i) {
            $num = $arr['PAGE_NUMBERS'][$i];
            $num_url = '';
            $sep = false;
            if ($i < ($count - 1)) {
                $sep = true;
            }

            if ($arr['CURRENT_PAGE'] != $num) {
                $num_url = 'href="'.$arr['prefix'].$num.'"';
            }

            $this->template->assign_block_vars('slider.list.paging.numbers', array(
                'URL' => $num_url,
                'TEXT' => $num, ));

            if ($sep) {
                $this->template->assign_block_vars('slider.list.paging.numbers.sep', array());
            }
        }
    }

    private function getPagingArray($count)
    {
        $in_page = $this->fromConfig('slider_admin_in_page');

        $page_count = @ceil($count / $in_page);

        //paging begin
        $new_url = $_SERVER['REQUEST_URI'];
        $new_url = $this->utils->removeQueryString($new_url, 'page');
        $new_url .= '&';

        $paging = new \mcms5xx\classes\PagedResults();
        $paging->TotalResults = $count;
        $paging->ResultsPerPage = $in_page;
        $paging->LinksPerPage = 10;
        $paging->PageVarName = 'page';
        $paging->UrlPrefix = $new_url;
        //paging end

        $arr = $paging->InfoArray();
        $arr['prefix'] = $paging->Prefix;

        return $arr;
    }

    private function getSlideName($slideid)
    {
        $name = '';

        $result_loc = $this->db->query('SELECT * FROM '.$this->db->prefix."slidercategorylocalizations WHERE slideid='".$slideid."' AND lang='".$this->default_lang."'");
        if ($row_loc = $this->db->fetch($result_loc)) {
            $name = $row_loc['name'];
        }

        return $name;
    }
}

$slider = new Slider();
$slider->template->pparse('slider');

/******************* slider.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** slider.admin.php ******************/;
