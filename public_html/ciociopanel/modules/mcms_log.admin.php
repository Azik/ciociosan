<?php
/******************* mcms_log.admin.php *******************
 *
 * Events log admin module
 *
 * @author MicroPHP <info@microphp.com>
 * @web www.microphp.com
 * @copyright 2004-2019 MicroPHP
 *
 ******************** mcms_log.admin.php ******************/

/** 
 * Define Namespace 
 */
namespace mcms5xx\admin\modules;

/**
 * Checking if module included normally.
 */
if (!defined('mCMScheck')) {
    die('mCMS by MicroPHP');
}

class mcms_log extends \mcms5xx\classes\AdminPage
{
    public function __construct()
    {
        $this->curr_module = 'mcms_log';
        parent::__construct();

        $this->onLoad();
    }

    private function onLoad()
    {
        $this->buildMenu();
        $this->buildMain();
    }

    private function buildMain()
    {
        $this->template->assign_vars(array(
            'TITLE' => $this->fromLang('title'),
        ));
        $this->buildmCMSlog();
    }

    private function buildmCMSlog()
    {
        $this->template->assign_block_vars('events_log', array());
        $this->template->assign_block_vars('events_log.list', array(
            'USER' => $this->fromLang('user'),
            'LOG_TIME' => $this->fromLang('log_time'),
            'URL' => "?" . $this->module_qs . "=mail_log",
            'OPERATION' => $this->fromLang('operation'),
            'PAGE' => $this->fromLang('page'),
            'IP' => $this->fromLang('ip'),
            'USER_AGENT' => $this->fromLang('user_agent'),
        ));
		$sql = "SELECT
          MCMSLOG.*,
		  U.username AS username
        FROM " . $this->db->prefix . "logs MCMSLOG
		LEFT JOIN " . $this->db->prefix . "users U ON U.`userid` = MCMSLOG.user_id
        ";
        $sql .= " ORDER BY MCMSLOG.logsid DESC LIMIT 0, 200 ";
        $result = $this->db->query($sql);
        while ($row = $this->db->fetch($result)) {
            $id = $row['logsid'];
            $user = $row['username'];
            $operation = $row['operation_mode'];
            $page = $row['page'];
            $ip = $row['ip'];
            $user_agent = $row['comp'];

            $log_time = DATE("r", $row['date_int']);

            $this->template->assign_block_vars('events_log.list.items', array(
                'ID' => $id,
                'USER' => $user,
                'OPERATION' => $operation,
                'LOG_TIME' => $log_time,
                'PAGE' => $page,
                'IP' => $ip,
                'USER_AGENT' => $user_agent,
            ));
        }
    }

}

$mcms_log_obj = new mcms_log();
$mcms_log_obj->template->set_filenames(array('mcms_log' => "mcms_log.tpl"));
$mcms_log_obj->template->pparse('mcms_log');


/******************* mcms_log.admin.php *******************
 *
 * Copyright : (C) 2004 - 2019. All Rights Reserved
 *
 ******************** mcms_log.admin.php ******************/;
